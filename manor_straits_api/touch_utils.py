﻿import re, json
from datetime import datetime
from collections import defaultdict
import pandas as pd

try:
    from . import utils, standardization, db_tables
except:
    import utils, standardization, db_tables


s_TOUCH_TABLE = db_tables.s_TOUCH_TABLE
s_OWNER_MAPS_TABLE =  db_tables.s_OWNER_MAPS_TABLE
a_TOUCH_COLS = [
    'event_type', 'created', 'hubspot_owner',
    'item_id', 'workflow_id',
    'email_campaign', 'email_campaign_group', 'metadata',
    'hubspot_vid', 'email']


def _timestamp_to_date(i_hubspot_timestamp):
    return datetime.fromtimestamp(i_hubspot_timestamp / 1000.0)


def _date_to_timestamp(dt_date):
    return int(dt_date.timestamp() * 1000)


def update_existing_record(e_new, e_old):
    if not e_old: return e_new
    
    e_updated = {k: v for k, v in e_old.items() if not pd.isnull(v)}
    
    for k, v in e_new.items():
        if k not in e_updated:
            e_updated[k] = v
        else:
            if isinstance(v, (datetime, pd.Timestamp)):
                e_updated[k] = v
            elif isinstance(v, (int, float)):
                e_updated[k] += v
    return e_updated


def engagement_to_touch(e_engagement, e_vid_email={}, **kwargs):
    ''' turn an engagement object (like that returned by create_event) into a touch dict(s) '''
    a_touches = []
    
    e_engagement_keys = {
        "id": "item_id",
        "ownerId": "hubspot_owner",
        "createdAt": "created",
        "recipient": "email",
        "type": "event_type",
        "metadata": "metadata",}

    a_contact_ids = e_engagement.get("associations", {}).get("contactIds", [])
    e_eng = {
        e_engagement_keys[k]: v
        for k, v in e_engagement.get("engagement", {}).items()
        if k in e_engagement_keys}

    if "created" in e_eng: e_eng["created"] = _timestamp_to_date(e_eng["created"])
    if e_engagement.get("metadata"):
        e_eng["metadata"] = e_engagement["metadata"]

    s_event_type = e_eng.get("event_type", "")
    if s_event_type == "NOTE":
        re_start = None
        if isinstance(e_eng["metadata"], dict):
            re_start = re.search(r"^[-_ A-Za-z0-9]{,50}(?=:)", e_eng["metadata"].get("body", ""))
        elif isinstance(e_eng["metadata"], str):
            re_start = re.search(r"^[-_ A-Za-z0-9]{,50}(?=:)", e_eng["metadata"])
        if re_start:
            s_event_type = re_start.group()
    elif s_event_type == "CALL" and kwargs.get("call_disposition_types"):
        s_disposition = self._call_dispositions.get(e_eng.get("metadata", {}).get("disposition"), "")
        if s_disposition: s_event_type = f"CALL {s_disposition.upper()}"

    dt_created = e_eng.get("created", "")
    i_item_id = e_eng.get("item_id", 0)

    # should we store touches without any contact ids?
    for i_contact in set(a_contact_ids):
        if kwargs.get("b_debug"): print( i_contact)
        e_touch = e_eng.copy()
        e_touch["hubspot_vid"] = i_contact
        s_email = e_vid_email.get(i_contact, "")
        #if isinstance(s_email, dict): s_email = s_email.get("value", "")
        e_touch["email"] = e_vid_email.get(i_contact, "")

        i_hubspot_vid = e_touch.get("hubspot_vid", 0)
        assert i_contact == i_hubspot_vid
        #print("s_event_type", s_event_type, type(s_event_type))
        #print("dt_created", dt_created, type(dt_created))
        #print("i_item_id", i_item_id, type(s_item_id))
        #print("i_hubspot_vid", i_hubspot_vid, type(i_hubspot_vid))
        #print((s_event_type, dt_created, i_item_id, i_hubspot_vid))

        a_touches.append(e_touch)
        #e_unique_engagements[(s_event_type, dt_created, i_item_id, i_hubspot_vid)] = e_touch
    return a_touches


def touch_to_db_row(e_touch, **kwargs):
    if not e_touch: return None
    s_metadata = ""
    if e_touch.get("metadata"):
        if isinstance(e_touch["metadata"], str):
            s_metadata = e_touch["metadata"]
        elif isinstance(e_touch["metadata"], (float, int)):
            s_metadata = str(e_touch["metadata"])
        else:
            s_metadata = json.dumps(utils.json_dict_prep(e_touch["metadata"]))

    if e_touch.get("created") and isinstance(e_touch["created"], datetime):
        e_touch["created"] = e_touch["created"].isoformat()

    s_event_type = e_touch.get("event_type")
    if s_event_type == "NOTE":
        re_start = None
        if isinstance(e_touch["metadata"], dict):
            re_start = re.search(r"^[-_ A-Za-z0-9]{,50}(?=:)", e_touch["metadata"].get("body", ""))
        elif isinstance(e_touch["metadata"], str):
            re_start = re.search(r"^[-_ A-Za-z0-9]{,50}(?=:)", e_touch["metadata"])
        if re_start:
            s_event_type = re_start.group()
    
    t_row = (
        s_event_type,
        e_touch.get("created"),
        e_touch.get("hubspot_owner"),
        e_touch.get("item_id"),
        e_touch.get("workflow_id"),
        e_touch.get("email_campaign"),
        e_touch.get("email_campaign_group"),
        s_metadata,
        e_touch["hubspot_vid"],
        e_touch.get("email"),
    )
    return t_row



def _touch_row_to_json(t_row):
    ''' Convert a raw DB row of touch summaries to a json-serializable dict '''
    t_output = (
        v.isoformat() if isinstance(v, datetime)
        else v
        for v in t_row)
    return t_output


def get_touch_summaries(o_DB, hubspot_vid=None, owner_id=None, **kwargs):
    if hubspot_vid:
        s_where = f"WHERE hubspot_vid = {hubspot_vid}"
    elif owner_id:
        s_where = f"WHERE {s_OWNER_MAPS_TABLE}.owner_id = {owner_id}"
    else:
        s_where = ""
    s_sql = f"""
SELECT {s_OWNER_MAPS_TABLE}.owner_id, hubspot_vid, email, event_type, max(created), count(*)
FROM {s_TOUCH_TABLE} LEFT JOIN {s_OWNER_MAPS_TABLE}
ON {s_TOUCH_TABLE}.hubspot_vid = {s_OWNER_MAPS_TABLE}.other_id AND {s_OWNER_MAPS_TABLE}.other_table = "Hubspot"
{s_where}
GROUP BY hubspot_vid, event_type;"""
    a_rows = o_DB.execute(s_sql)
    
    df_summary = pd.DataFrame(a_rows, columns=["owner_id", "hubspot_vid", "email", "event_type", "last", "count"])
    
    e_hubspot_owner_ids = dict(zip(df_summary["hubspot_vid"], df_summary["owner_id"]))
    e_hubspot_emails = dict(zip(df_summary["hubspot_vid"], df_summary["email"]))
    
    # Convert datetime objects to isoformat
    e_hubspot_lasts_touches = df_summary.groupby("hubspot_vid").max()["last"].apply(lambda dt: dt.isoformat() if not pd.isnull(dt) else None).to_dict()
    df_summary["last"] = df_summary["last"].apply(lambda dt: dt.isoformat() if not pd.isnull(dt) else None)
    
    df_summary_by_hubspot = df_summary.pivot(index="hubspot_vid", columns="event_type", values=["last", "count"])
    df_summary_by_hubspot.columns = [
        re.sub("marketing_email", "marketing", '_'.join(reversed(col)).strip().replace(" ", "_").lower())
        for col in df_summary_by_hubspot.columns.values]
    df_summary_by_hubspot.reset_index(inplace=True)
    
    df_summary_by_hubspot.insert(1, "last_update", df_summary_by_hubspot["hubspot_vid"].apply(e_hubspot_lasts_touches.get))
    df_summary_by_hubspot.insert(1, "email", df_summary_by_hubspot["hubspot_vid"].apply(e_hubspot_emails.get))
    df_summary_by_hubspot.insert(0, "owner_id", df_summary_by_hubspot["hubspot_vid"].apply(e_hubspot_owner_ids.get))
    
    #e_summaries = utils.df_to_sparse(df_summary_by_hubspot)
    a_summaries = utils.df_to_sparse(df_summary_by_hubspot).values()
    
    for e_contact in a_summaries:
        if e_contact.get("owner_id"):
            e_contact["owner_id"] = int(e_contact["owner_id"])
    
    if kwargs.get("b_by_hubspot_vid"):
        e_summaries = {
            int(e_contact["hubspot_vid"]): e_contact
            for e_contact in a_summaries
            if e_contact.get("hubspot_vid")
        }
    else:
        e_summaries = {
            int(e_contact["owner_id"]): e_contact
            for e_contact in a_summaries
            if e_contact.get("owner_id")
        }
    return e_summaries


def get_touch_details(o_DB, c_match_types, hubspot_vid=None, owner_id=None, **kwargs):
    if hubspot_vid:
        s_where = f"WHERE hubspot_vid = {hubspot_vid}"
    elif owner_id:
        s_where = f"WHERE {s_OWNER_MAPS_TABLE}.owner_id = {owner_id}"
    else:
        s_where = ""
    
    o_DB.load()
    a_touch_cols = ["owner_id"]
    a_touch_cols.extend(o_DB.get_table_cols(s_TOUCH_TABLE))
    o_DB.close()
    
    s_sql = f"""
SELECT {s_OWNER_MAPS_TABLE}.owner_id, {s_TOUCH_TABLE}.*
FROM {s_TOUCH_TABLE} LEFT JOIN {s_OWNER_MAPS_TABLE}
ON {s_TOUCH_TABLE}.hubspot_vid = {s_OWNER_MAPS_TABLE}.other_id AND {s_OWNER_MAPS_TABLE}.other_table = "Hubspot"
{s_where}
;"""
    #print(s_sql)
    
    a_rows = o_DB.execute(s_sql)
    b_by_hubspot_vid = kwargs.get("b_by_hubspot_vid")
    b_keep_non_mapped = kwargs.get("b_keep_non_mapped")
    
    e_touches = defaultdict(list)
    for t_row in a_rows:
        t_row = _touch_row_to_json(t_row)
        e_row = {k: v for k, v in zip(a_touch_cols, t_row) if v and not pd.isnull(v)}
        s_owner_id, s_hubspot_vid = str(e_row.get("owner_id", "")), str(e_row.get("hubspot_vid", ""))
        
        if c_match_types and e_row.get("event_type", "").upper().strip() not in c_match_types: continue
        
        #if "metadata" in e_row: del e_row["metadata"]
        #print('e_row', e_row)
        if b_by_hubspot_vid:
            e_touches[s_hubspot_vid].append(e_row)
        else:
            if not b_keep_non_mapped and not s_owner_id: continue
            if b_keep_non_mapped:
                e_touches[",".join([s_owner_id, s_hubspot_vid])].append(e_row)
            else:
                e_touches[s_owner_id].append(e_row)
        
    return e_touches
