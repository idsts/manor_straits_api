﻿import sys, os, re, shutil, json
from zipfile import ZipFile
from datetime import datetime, timedelta
from time import time
from functools import reduce, wraps
from collections import defaultdict
import pandas as pd

__all__ = [
    
    
    "seconds_to_str", "timedelta_to_str", "timer", "get_time", "roundTime",
    "get_local_path",
    
    "download_file", "get_local_path", "create_folder",
    "clear_sub_folders", "get_new_write_filename", "generate_temp_filename",
    "get_file_list", "get_file_in_path",
    
    "zip_directory",
    
    "to_string",
    "flatten_key_value", "flatten_dict", "df_to_sparse",
    "json_dict_prep",
    "convert_command_line_arg", "parse_command_line_args"]

s_DATA_FOLDER = ''



def seconds_to_str(t):
    ''' convert seconds into a human-readable time string '''
    return "%d:%02d:%02d.%03d" % \
        reduce(lambda ll,b : divmod(ll[0],b) + ll[1:],
            [(t*1000,),1000,60,60])

def timedelta_to_str(time_delta):
    ''' convert a datetime.timedelta into a human-readable string '''
    hours, remainder = divmod(time_delta.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    return ', '.join([x[0] for x in [
        (f'{time_delta.days:,} days', time_delta.days),
        (f'{int(hours):,} hours', hours),
        (f'{int(minutes):,} minutes', minutes),
        (f'{int(seconds):,} seconds', seconds),
    ] if x[1]])


def timer(func):
    ''' timing function decorator '''
    @wraps(func)
    def wrap(*args, **kwargs):
        n_start = time()
        if "function_time" in kwargs:
            s_func = kwargs["function_time"]
            #del kwargs["function_time"]
            kwargs = {key: val for key, val in kwargs if key != "function_time"}
        else:
            s_func = func.__name__
        result = func(*args, **kwargs)
        print("{} took: {}".format(s_func, seconds_to_str(time() - n_start)))
        return result
    return wrap


def get_time(format="%Y-%m-%d %H:%M:%S"):
    return datetime.now().strftime(format)


def roundTime(dt=None, dateDelta=timedelta(minutes=1)):
    """Round a datetime object to a multiple of a timedelta
    dt : datetime.datetime object, default now.
    dateDelta : timedelta object, we round to a multiple of this, default 1 minute.
    Author: Thierry Husson 2012 - Use it as you want but don't blame me.
            Stijn Nevens 2014 - Changed to use only datetime objects as variables
    Examples:
        print(utils.roundTime(dt_created, timedelta(seconds=30)))
        print(utils.roundTime(dt_created, timedelta(minutes=15)))
        print(utils.roundTime(dt_created, timedelta(hours=1)))
    """
    roundTo = dateDelta.total_seconds()
    
    if isinstance(dt, pd.Timestamp): dt = dt.to_pydatetime()
    if dt == None : dt = datetime.now()
    seconds = (dt - dt.min).seconds
    # // is a floor division, not a comment on following line:
    rounding = (seconds + roundTo/2) // roundTo * roundTo
    return dt + timedelta(0, rounding-seconds, -dt.microsecond)


def download_file(url, filename=''):
    ''' download web url
        Args:
            url: {str} the http link to the file to be downloaded
            filename: {str} optionally specify the local file name to use.
                      if omitted, it will use the filename from the url and
                      the directory of the python script file
        Returns:
            {str} the local filename that the file was successfully downloaded
            or if there is already a local file matching the remote file
            return an empty str if download failed
    '''
    if not url or url[:4].lower() != 'http':
        print('not an http url')
        return ''
    local_filename = url.split('/')[-1]
    if filename:
        s_folder, s_file = os.path.split(filename)
        
        b_folder = True if s_folder and os.path.isdir(s_folder) else False
        if b_folder and s_file:
            local_filename = filename
        elif b_folder and not s_file:
            local_filename = os.path.join(s_folder, local_filename)
        elif s_file:
            local_filename = s_file
    if not local_filename:
        raise Exception("no local filename/path specified")
    
    if not os.path.isfile(local_filename):
        try:
            head = requests.head(url)
            if head.status_code != 200:
                print('Trouble accessing url: code {} from "{}"'.format(head.status_code, url))
                print('Headers:', head.headers)
                return ''
            e_head = head.headers
        except:
            print('no URL head from request from "{}"'.format(url))
            return ''
        if not e_head:
            print('no URL head from request from "{}"'.format(url))
            return ''
        if e_head.get('Content-Length', 0) == 0:
            print('no content length in URL head from request from "{}"'.format(url))
            return ''
        b_download = True
    else:
        b_download = False
        try:
            head = requests.head(url)
            if head.status_code != 200:
                print('Trouble accessing url: code {} from "{}"'.format(head.status_code, url))
                print('Headers:', head.headers)
                return ''
            e_head = head.headers
        except:
            print('no status code in URL head from request from "{}"'.format(url))
            return ''
        
        if not e_head:
            print('no URL head from request from "{}"'.format(url))
            return ''
        
        if isinstance(e_head, dict) and e_head:
            if e_head.get('Content-Length', 0) == 0:
                print('no content length in URL head from request from "{}"'.format(url))
                return ''
            
            remote_time = datetime.strptime (e_head.get("Last-Modified"), '%a, %d %b %Y %H:%M:%S %Z')
            local_time = os.path.getmtime(local_filename)
            if time.mktime(remote_time.timetuple()) > local_time:
                # remote is newer
                print('remote file is newer. redownload')
                b_download = True
        
            remote_hash = re.sub('^[\'"]|[\'"]$', "", e_head.get("ETag", ""))
            local_hash = hashlib.md5(open(local_filename,'rb').read()).hexdigest()
            
            if remote_hash and remote_hash != local_hash:
                # remote has different hash
                print('remote file has changed. redownload')
                b_download = True
        
    #print('file: {}, download: {}'.format(local_filename, b_download))
    if b_download:
        r = requests.get(url, stream=True)
        
        if not os.path.isdir(os.path.split(local_filename)[0]):
            raise Exception('Download folder doesn\'t exist: "{}"'.format(os.path.split(local_filename)[0]))
        
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024): 
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)
    else:
        print('local file is current')
    return local_filename


def get_local_path(s_path):
    ''' get the path if a folder isn't specified and if the file is in the local data directory
        Arguments:
            s_file: {str} a filename
        Returns:
            {str} the absolute or relative path
    '''
    if not s_path: return s_path
    s_folder, s_file = os.path.split(s_path.strip())
    if s_folder: return s_path
    
    global s_DATA_FOLDER
    if not s_DATA_FOLDER:
        s_DATA_FOLDER = os.path.join(os.path.dirname(__file__), 'data')
    s_new_path = os.path.join(s_DATA_FOLDER, s_file)
    if os.path.isfile(s_new_path):
        return s_new_path
    
    print('not local file', s_path, s_file)
    return s_path


def create_folder(s_path):
    ''' Create a folder if it doesn't already exist,
        even if multiple nested folders must be created
        Arguments:
            s_path: {str} of the folder path to be created (must end in separator if last element is folder)
        Returns:
            {bool} if folder is successfully made
    '''
    if os.path.exists(s_path):
        return True
    s_drive, s_folder_path = os.path.splitdrive(s_path)
    s_folder_path, s_file = os.path.split(s_folder_path)
    s_drive, s_folder_path, s_file
    
    if os.path.altsep:
        re_path_split = re.compile(r"[{}{}]".format(re.escape(os.path.sep), re.escape(os.path.altsep)))
    else:
        re_path_split = re.compile(r"[{}]".format(re.escape(os.path.sep)))
    
    a_folders = [
        x for x in re_path_split.split(s_folder_path)
        if x]
    
    for i_len in range(1, len(a_folders) + 1):
        s_check_path = os.path.join(s_drive, os.path.sep, *a_folders[:i_len])
        if not os.path.isdir(s_check_path):
            os.mkdir(s_check_path)
        #print(s_path, os.path.isdir(s_check_path))
    
    return os.path.isdir(os.path.join(s_drive, os.path.sep, s_folder_path))


def clear_sub_folders(s_sub_folder, s_folder, b_only_if_empty=True, **kwargs):
    ''' Clear each level under s_sub_folder
        Arguments:
            s_sub_folder: {str} relative subfolders to remove like "folder1/folder2"
            s_folder: {str} the rest of the path not to remove
            b_only_if_empty: {bool} only remove the subfolders if they are already empty
    '''
    c_include_exts = {str(x).lower() for x in kwargs.get("include_extensions", [])}
    c_exclude_exts = {str(x).lower() for x in kwargs.get("exclude_extensions", [])}
    
    #print('s_sub_folder', s_sub_folder)
    if not os.path.isdir(os.path.join(s_folder, s_sub_folder)):
        s_sub_folder = os.path.split(s_sub_folder)[0]  # strip of any actual filenames
    if not s_sub_folder or not s_folder or not os.path.isdir(os.path.join(s_folder, s_sub_folder)):
        print("Folder + subfolder path doesn't exist: '{}' '{}'".format(s_folder, s_sub_folder,))
        return
    #print(s_sub_folder)
    a_sub_folders = [x for x in re.split(r"[/\\]", s_sub_folder) if x]
    a_removed_files = []
    
    for i in reversed(range(1, len(a_sub_folders) + 1)):
        #print(a_sub_folders[:i])
        s_path = os.path.join(s_folder, *a_sub_folders[:i])
        #print("Start checking folder:", s_path)
        a_existing_files = os.listdir(s_path)
        if a_existing_files:
            for s_existing_file in a_existing_files:
                if os.path.isdir(os.path.join(s_path, s_existing_file)):
                    #print("\tRemove subfolder:",os.path.join(s_path, s_existing_file))
                    a_removed_files.extend(clear_sub_folders(
                        os.path.join(s_existing_file, ''),
                        s_path,
                        b_only_if_empty=b_only_if_empty,
                        **kwargs))
                    
                else:
                    _, s_ext = os.path.splitext(s_existing_file)
                    #print(_, "s_ext", s_ext)
                    s_file = os.path.join(s_path, s_existing_file)
                    
                    if c_include_exts and s_ext.lower() in c_include_exts:
                        os.remove(s_file)
                        a_removed_files.append(s_file)
                        #print("\tDeleting:", s_file)
                        continue
                    
                    if c_exclude_exts and s_ext.lower() not in c_exclude_exts:
                        os.remove(s_file)
                        a_removed_files.append(s_file)
                        #print("\tDeleting:", s_file)
                        continue
        
                    if not c_include_exts and not c_exclude_exts:
                        os.remove(s_file)
                        a_removed_files.append(s_file)
                        #print("\tDeleting:", s_file)
                        continue
                    
                    #print("\tSkipping:", s_file)
        
        a_remaining_files = os.listdir(s_path)
        if a_remaining_files and b_only_if_empty:
            #print("Files in directory: {}".format(s_path))
            return a_removed_files
        else:
            os.rmdir(s_path)
    return a_removed_files

def get_new_write_filename(s_path, b_dot_format=False):
    """ Get a new, unoccupied filename. If a file already exists, add a number
        to the end of the filename and increment it upwards until no existing file is found.
        Then return new filename to write to."""
    if not isinstance(s_path, str) or not s_path:
        return ""
    #s_folder = folder_from_path(s_path)
    s_folder = os.path.split(s_path)[0]
    s_file = os.path.basename(s_path)

    if not os.path.isdir(s_folder):  # if the folder doesn't exists
        return ""
    if not os.path.isfile(os.path.join(s_folder, s_file)):  # if a file doesn't exist with that name
        # Filepaths were coming out doubled, when only a filename was passed to this function
        return os.path.join(s_folder, s_file) if s_folder != s_file else s_file

    s_base, s_ext = os.path.splitext(s_file)
    i_count = 1
    if b_dot_format:
        s_format = "{}.{}{}"
    else:
        s_format = "{} ({}){}"
    
    while os.path.isfile(os.path.join(s_folder, s_format.format(s_base, str(i_count), s_ext))):
        i_count += 1
    return os.path.join(s_folder, s_format.format(s_base, str(i_count), s_ext))


def generate_temp_filename(ext="", folder="", **kwargs):
    if not folder: folder = os.getcwd()
    
    s_key = binascii.hexlify(os.urandom(8)).decode()
    a_pieces = []
    for i_start in range(0, len(s_key), 4):
        a_pieces.append(s_key[i_start: i_start + 4])
    
    s_name = ""
    if kwargs.get("include_date", True):
        s_name = "{}_".format(datetime.now().strftime("%Y%m%d"))
    s_name += "".join(a_pieces)
    if ext and not ext[0] == ".": ext = f".{ext}"
    s_file = os.path.join(folder, f"_{s_name}{ext}")
    return s_file


def get_file_list(s_folder, c_exts=None, b_include_subfolders=False):
    """ Pass in a folder path and a set of extensions.
        Return a list of files matching any extension as absolute paths.
        Arguments:
            s_folder: {str} folder to get files
            c_exts: {set} only include files with these extensions
            b_include_subfolders: {bool} also walk through subdirectories
        Returns:
            {list} of file paths (folder + filename)
    """
    a_files = []
    if c_exts:
        e_valid_exts = set(
            [
                s_ext if s_ext.startswith(".") else ".{}".format(s_ext)
                for s_ext in c_exts
            ]
        )

        for (dirpath, dirnames, filenames) in os.walk(s_folder):
            a_files.extend(
                [
                    os.path.join(dirpath, f)
                    for f in filenames
                    if os.path.splitext(f)[1] in e_valid_exts
                ]
            )
            if not b_include_subfolders: break
    else:
        for (dirpath, dirnames, filenames) in os.walk(s_folder):
            a_files.extend([os.path.join(dirpath, f) for f in filenames])
            if not b_include_subfolders: break
    return a_files


def get_file_in_path(s_file):
    ''' Check if a file exists in one of the folders specified in the
        system's environment variable "PATH" '''
    a_folders = os.environ.get("PATH", "").split(";")
    for s_folder in a_folders:
        a_files = get_file_list(s_folder)
        e_files = {os.path.split(s.lower())[1]: s for s in a_files}
        #print("e_files", e_files)
        if s_file.lower() in e_files:
            return e_files[s_file.lower()]
        #break
    return ""


def zip_directory(s_directory, s_zip=None, **kwargs):
    ''' zip the contents of a directory '''
    if not s_directory: s_directory = os.getcwd()
    if not os.path.isdir(s_directory): raise Exception(f'Not a valid directory to zip up: "{s_directory}"')
    s_drive, s_dir = os.path.splitdrive(s_directory)
    a_folder = [x for x in re.split(r"[/\\:]", s_dir) if x]
    
    c_include_exts = {str(x).lower() for x in kwargs.get("include_extensions", [])}
    c_exclude_exts = {str(x).lower() for x in kwargs.get("exclude_extensions", [])}
    
    #print(len(c_include_exts), "c_include_exts", c_include_exts)
    #print(len(c_exclude_exts), "c_exclude_exts", c_exclude_exts)
    
    s_date = datetime.now().strftime("%Y-%m-%d")
    if not s_zip:
        if kwargs.get("zip_cwd"):
            s_zip_dir = os.getcwd()
        else:
            if kwargs.get("store_in_directory", True):
                s_zip_dir = os.path.join(*a_folder)
            else:
                s_zip_dir = os.path.join(*a_folder[:-1])
        
        if s_drive and s_drive[-1] != os.path.sep: s_drive += os.path.sep
        if not s_drive: s_zip_dir = os.path.sep + s_zip_dir
        s_zip = os.path.join(s_drive, s_zip_dir, f"{a_folder[-1]}_{s_date}.zip")
    
    a_files = []
    with ZipFile(s_zip, "w") as o_zip:
        for s_folder, a_subfolders, a_filenames in os.walk(s_directory):
            for s_filename in a_filenames:
                _, s_ext = os.path.splitext(s_filename)
                s_file = os.path.join(s_directory, s_filename)
                
                if c_include_exts and s_ext.lower() in c_include_exts:
                    o_zip.write(s_file, os.path.basename(s_file))
                    a_files.append(s_file)
                    continue
                
                if c_exclude_exts and s_ext.lower() not in c_exclude_exts:
                    o_zip.write(s_file, os.path.basename(s_file))
                    a_files.append(s_file)
                    continue
    
                if not c_include_exts and not c_exclude_exts:
                    o_zip.write(s_file, os.path.basename(s_file))
                    a_files.append(s_file)
                    continue
                #print("skipping:", s_filename)
    return s_zip, a_files


def to_string(s):
    try:
        return str(s)
    except:
        #Change the encoding type if needed
        return s.encode('utf-8')


def flatten_key_value(key, value, new_dict=None):
    ''' Recursively flatten the dict '''
    if new_dict is None:
        new_dict = {}
            
    if isinstance(value, list):  # Reduction Condition 1
        i = 0
        for sub_item in value:
            flatten_key_value('{}_{}'.format(key, to_string(i)), sub_item, new_dict)
            i += 1
    elif isinstance(value, set):  # Reduction Condition 1
        i = 0
        for sub_item in sorted(value):
            flatten_key_value('{}_{}'.format(key, to_string(i)), sub_item, new_dict)
            i += 1
    elif isinstance(value, dict):  # Reduction Condition 2
        sub_keys = value.keys()
        for sub_key in sub_keys:
            flatten_key_value('{}_{}'.format(key, to_string(sub_key)), value[sub_key], new_dict)
    else:  # Base Condition
        new_dict[to_string(key)] = to_string(value)


def flatten_dict(e_dict):
    e_flattened = {}
    for k, v in e_dict.items():
        flatten_key_value(k, v, e_flattened)
    return e_flattened


def df_to_sparse(df):
    ''' Convert a dataframe into a dict of sparse dicts, with indexes as key1 and columns as key2
        Returns:
            {dict} of {row_index1: sparse_dict_row1, row_index2: sparse_dict_row2...}
    '''
    if df.index.nlevels == 1:
        e_df = defaultdict(dict)
        for k, v in df.stack().items():
            e_df[k[:-1]][k[-1]] = v
    return dict(e_df)


def json_dict_prep(variable):
    ''' recursively check through a dict to make sure it can be dumped into a json string
        by converting datetime to isoformat, and dropping custom object variables.
        Arguments:
            variable: any python variable
        Returns:
            variable in type that can be jsonified or None
    '''
    if isinstance(variable, (str, float, int)):
        return variable
    elif isinstance(variable, datetime):
        return variable.isoformat()
    elif isinstance(variable, list):
        return [json_dict_prep(x) for x in variable]
    elif isinstance(variable, tuple):
        return tuple(json_dict_prep(x) for x in variable)
    elif isinstance(variable, list):
        return {json_dict_prep(x) for x in variable}
    elif isinstance(variable, dict):
        return {k: json_dict_prep(v) for k, v in variable.items()}
    else:
        return None


def convert_command_line_arg(s_value, b_bool=True, b_int=True, b_float=True):
    ''' Convert the value of a str command line argument value into bool, int, float or list if appropriate '''
    if isinstance(s_value, list):
        return [convert_command_line_arg(x, b_bool=b_bool, b_int=b_int, b_float=b_float) for x in s_value]
    elif isinstance(s_value, tuple):
        return tuple([convert_command_line_arg(x, b_bool=b_bool, b_int=b_int, b_float=b_float) for x in s_value])
    elif not isinstance(s_value, str):
        return s_value
    
    s_value = s_value.strip()
    if s_value.lower() in {'yes', 'true', 'on'}:
        if b_bool: return True
        else: return s_value
    elif s_value.lower() in {'no', 'false', 'off'}:
        if b_bool: return False
        else: return s_value
    elif b_int or b_float and re.search(r"\d", s_value) and not re.search(r"[a-zA-Z]", s_value):
        if b_int and re.match(r"^(\d{1,3},)*\d+$", s_value):
            return int(re.sub(r"[^\d]", "", s_value))
        elif b_float and re.match(r"^((\d{1,3},)*\d+(\.\d*)?|.\d+)$", s_value):
            return float(re.sub(r"[^\d.]", "", s_value))
        else:
            return s_value
    else:
        return s_value
    

def parse_command_line_args(sys_argv):
    ''' parse the command line arguments and keyword arguments into a list and dict
        Arguments:
            {list} from sys.argv
        Returns:
            {tuple} of ({list} arguments, {dict} keyword arguments)
    '''
    a_args, e_kwargs = [], {}
    
    idx = 1
    while idx < len(sys_argv):
        #print(idx, sys_argv[idx])
        if sys_argv[idx].startswith('--'):
            s_key = sys_argv[idx][2:]
            #print(f'sys_argv[{idx}]: "{s_key}"')
            if ":" in s_key:
                if re.search(": *[\"'([]? *$", s_key):  # only the key was captured, not the value
                    s_key = s_key.strip()
                    #print(f'only key in sys_argv[{idx}]: "{s_key}"')
                    a_values = []
                    s_type = ""
                    if s_key[-1] == ":":
                        s_key = s_key[:-1]
                        while idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):  # no indication of start of list, and next item(s) could be value
                            idx += 1
                            val = sys_argv[idx].strip()
                            if val: a_values.append(val)
                    elif s_key[-1] == "[":
                        s_key = re.sub(": *\[$", "", s_key[:-1])
                        s_type = "list"
                        while idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):  # start of list and and next item(s) could be value
                            idx += 1
                            val = sys_argv[idx].strip()
                            if "]" in val:
                                val = re.sub(r"\].*$", "", val)
                                sys_argv[idx] = re.sub(r"^.*\]", "", sys_argv[idx])
                                if val: a_values.append(val)
                                idx -= 1
                                break
                            else:
                                if val: a_values.append(val)
                    elif s_key[-1] == "(":
                        s_key = re.sub(": *\($", "", s_key[:-1])
                        s_type = "tuple"
                        while idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):  # start of list and and next item(s) could be value
                            idx += 1
                            val = sys_argv[idx].strip()
                            if ")" in val:
                                val = re.sub(r"\).*$", "", val)
                                sys_argv[idx] = re.sub(r"^.*\)", "", sys_argv[idx])
                                if val: a_values.append(val)
                                idx -= 1
                                break
                            else:
                                if val: a_values.append(val)
                    #else:
                    #    if idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):
                        
                        
                    if a_values:
                        if a_values[0].startswith("["):
                            a_values[0] = a_values[0][1:]
                            s_type = "list"
                        elif a_values[0].startswith("("):
                            a_values[0] = a_values[0][1:]
                            s_type = "tuple"
                        for i_val, s_val in enumerate(a_values):
                            #print(f'a_values[{i_val}] == "{s_val}"')
                            if s_val.endswith("]"):
                                a_values[i_val] = s_val[:-1]
                                if i_val < len(a_values) - 1:
                                    idx -= ((len(a_values) - 1) - i_val)
                            elif s_val.endswith(")"):
                                a_values[i_val] = s_val[:-1]
                                if i_val < len(a_values) - 1:
                                    idx -= ((len(a_values) - 1) - i_val)
                                    
                    if s_type == "list":
                        val = a_values
                    elif s_type == "tuple":
                        val = tuple(a_values)
                    elif s_type == "" and len(a_values) == 1:
                        val = a_values[0]
                    else:
                        val = a_values
                    
                    e_kwargs[s_key] = val
                        
                else:
                    s_key, val = s_key.split(':', 1)
                    val = val.strip()
                    if val.startswith(("[", "(")):
                        s_start = val[0]
                        if s_start == "[" and val.endswith("]"):
                            a_values = val[1:-1].split(",")
                            s_type = "list"
                        elif s_start == "(" and val.endswith(")"):
                            a_values = tuple(val[1:-1].split(","))
                            s_type = "tuple"
                        else:
                            if s_start == "(":
                                s_type = "tuple"
                            elif s_start == "[":
                                s_type = "list"
                            
                            #print("val", val)
                            a_values = [x.strip() for x in val[1:].split(",") if x.strip()]
                            
                            while idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):  # next item(s) could be more values
                                idx += 1
                                val = sys_argv[idx].strip()
                                if s_type == "tuple" and ")" in val:
                                    val = re.sub(r"\).*$", "", val)
                                    sys_argv[idx] = re.sub(r"^.*\)", "", sys_argv[idx])
                                    if val: a_values.append(val)
                                    idx -= 1
                                    break
                                elif s_type == "list" and "]" in val:
                                    val = re.sub(r"\].*$", "", val)
                                    sys_argv[idx] = re.sub(r"^.*\]", "", sys_argv[idx])
                                    if val: a_values.append(val)
                                    idx -= 1
                                    break
                                else:
                                    if val: a_values.append(val)
                        
                        if s_type == "list":
                            val = a_values
                        elif s_type == "tuple":
                            val = tuple(a_values)
                        elif s_type == "" and len(a_values) == 1:
                            val = a_values[0]
                        else:
                            val = a_values
                    
                    if s_key in e_kwargs:
                        if isinstance(e_kwargs[s_key], list):
                            e_kwargs[s_key].append(val)
                        else:
                            e_kwargs[s_key] = [e_kwargs[s_key], val]
                    else:
                        e_kwargs[s_key] = val
                    
            elif idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):
                val = sys_argv[idx + 1]
                if s_key in e_kwargs:
                    if isinstance(e_kwargs[s_key], list):
                        e_kwargs[s_key].append(val)
                    else:
                        e_kwargs[s_key] = [e_kwargs[s_key], val]
                else:
                    e_kwargs[s_key] = sys_argv[idx + 1]
                idx += 1
            
            elif s_key not in e_kwargs:
                if ':' in s_key:
                    s_key, val = s_key.split(':', 1)
                else:
                    val = None
                
                if s_key in e_kwargs and val:
                    if isinstance(e_kwargs[s_key], list):
                        e_kwargs[s_key].append(val)
                    else:
                        e_kwargs[s_key] = [e_kwargs[s_key], val]
                else:
                    e_kwargs[s_key] = val
                
        else:
            a_args.append(convert_command_line_arg(sys_argv[idx]))
        idx += 1
        
    e_kwargs = {key.lower(): convert_command_line_arg(val) for key, val in e_kwargs.items()}
    
    return a_args, e_kwargs
