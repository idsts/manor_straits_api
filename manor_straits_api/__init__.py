import pyximport; pyximport.install()

try:
    from .utils import *
except:
    from utils import *
try:
    from . import flask_idsts
except:
    import flask_idsts
try:
    from . import hubspot_api
except:
    import hubspot_api
try:
    from . import sakari_api
except:
    import sakari_api
try:
    from . import handwrite_api
except:
    import handwrite_api
try:
    from . import test_idsts_api
except:
    import test_idsts_api

__version__ = "0.3.5"