﻿import re, os, sys, datetime
import requests, json, urllib


class HandwriteAPI:
    _base_url =  "https://api.handwrite.io"
    _get_header = {
        "Content-type": "application/json",
    }
    _post_header = {
        "Content-type": "application/json",
        "Accept": "application/json",
    }
    def __init__(self, api_key=None, **kwargs):
        self._handwriting, self._stationery = {}, {}
        if api_key:
            self._set_api_key(api_key)
        else:
            self._set_api_key(
                os.environ.get("IDSTS_HANDWRITE_API_KEY", "test_hw_c202072a1eaa06d225db"))  # default is development key for testing only
        #elif kwargs.get("status", "").lower() == "live":
        print("Handwrite API Key:", self._api_key)
    
    def _set_api_key(self, api_key):
        assert api_key
        self._api_key = api_key
        self._get_header["Authorization"] = api_key
        self._post_header["Authorization"] = api_key
    
    def get_handwriting(self, **kwargs):
        s_url = f"{self._base_url}/v1/handwriting"
        
        o_response = requests.get(url=s_url, headers=self._post_header)
        if o_response.ok:
            a_response = json.loads(o_response.text)
            self._handwriting = {e_hw["_id"]: e_hw for e_hw in a_response if "_id" in e_hw}
            return o_response
        else:
            if kwargs.get("b_debug"):
                return o_response
            else:
                return {}
    
    def get_stationery(self, **kwargs):
        s_url = f"{self._base_url}/v1/stationery"
        
        o_response = requests.get(url=s_url, headers=self._post_header)
        if o_response.ok:
            a_response = json.loads(o_response.text)
            self._stationery = {e_cd["_id"]: e_cd for e_cd in a_response if "_id" in e_cd}
            return o_response
        else:
            if kwargs.get("b_debug"):
                return o_response
            else:
                return {}
            
    def send_message(self, a_recipients, e_from, s_message, s_card_id, s_handwriting_id, **kwargs):
        ''' Send a message to up to 1,000 recipients
            Arguments:
                a_recipients: {list} of recipient dictionaries like
                    {
                        "firstName": "The",
                        "lastName": "Dude",
                        "company": "Unemployed",
                        "street1": "25 Main Street",
                        "city": "Los Angeles",
                        "state": "CA",
                        "zip": "90210"
                    }
                e_from: {dict} of the contact information of the sender like
                    {
                        "firstName": "Jackie",
                        "lastName": "Treehorn",
                        "street1": "1 Random Street",
                        "street2": "Apt 33A",
                        "city": "Malibu",
                        "state": "CA",
                        "zip": "90263"
                    }
                s_message: {str} of message up to 320 characters in length.
                           The message can use the fields in the recipients as variables in the
                           format of "Hi {firstName}, this is Tim at IDSTS. How are things in {city}?"
                s_card_id: {str} id of the stationery to be used
                s_handwriting_id: {str} id of the style of handwriting
        '''
        s_url = f"{self._base_url}/v1/send"
        
        e_parameters = {}
        
        if isinstance(a_recipients, dict) and a_recipients.get("mobile"):
            a_recipients = [a_recipients]
        
        a_contacts = []
        for cont in a_recipients:
            if not cont: continue
            if isinstance(cont, dict):
                e_cont = {
                    k: v.strip() for k, v in cont.items()
                    if v.strip() and k in {"firstName", "lastName", "company", "street1", "street2", "city", "state", "zip"}}
                if e_cont and all(k in e_cont for k in ["street1", "city", "state", "zip"]):
                    a_contacts.append(e_cont)
                else:
                    if kwargs.get("b_debug"): print("Skipping contact in unrecognized format:", cont)
            else:
                if kwargs.get("b_debug"): print("Skipping contact in unrecognized format:", cont)
        
        e_from = {
            k: v.strip() for k, v in e_from.items()
            if v.strip() and k in {"firstName", "lastName", "street1", "street2", "city", "state", "zip"}}
          
        if not a_contacts: raise Exception('No valid contacts specified to receive the message. Needs at least one contact with "street1", "city", "state", "zip".')
        if not e_from or not all(k in e_from for k in ["firstName", "lastName", "street1", "city", "state", "zip"]):
            print("e_from", e_from)
            raise Exception('Not a valid "from" address. Needs "firstName", "lastName", "street1", "city", "state", "zip".')
        if not s_message: raise Exception("No message to send to recipients. Must be a non-blank text. Also for testing, Handwrite may return errors if the exact same message is sent to the same contact.")
        
        if not self._handwriting: self.get_handwriting()
        if not self._stationery: self.get_stationery()
        
        if not s_handwriting_id or s_handwriting_id not in self._handwriting:
            raise Exception(f"Not a recognized handwriting id: {s_handwriting_id}")
        if not s_card_id or s_card_id not in self._stationery:
            raise Exception(f"Not a recognized stationery card id: {s_card_id}")
        
        e_data = {
            "recipients": a_contacts,
            "message": s_message,
            "handwriting": s_handwriting_id,
            "card": s_card_id,
            "from": e_from,
        }

        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_endpoint}?{s_parameters}"

        if kwargs.get("b_debug"):
            print(s_url)
            print(e_data)

        o_response = requests.post(url=s_url, json=e_data, headers=self._post_header)
        return o_response