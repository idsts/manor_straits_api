﻿import os, sys, re
from collections import defaultdict, Counter

try:
    from . import utils, standardization, mysql_db, db_tables
except:
    import utils, standardization, mysql_db, db_tables


AWS_DB_USER = os.environ.get("IDSTS_AWS_DB_USER", "")
AWS_DB_PASSWORD = os.environ.get("IDSTS_AWS_DB_PASS", "")

a_RETURN_COLS = ["id", "owner_first_name", "owner_last_name", "owner_email", "hubspot_vid"]

a_UNIQUE_COLS = [  # columns that contain non-null values that should be completely unique to 1 owner
    "hubspot_vid",
    "owner_email", "owner_email_2", "owner_email_3",
    "owner_phone", "owner_phone_2", "owner_phone_3"]
c_UNIQUE_COLS = set(a_UNIQUE_COLS)

a_COMBO_COLS = [  # columns that contain values that should be completely unique to 1 owner when grouped as a combination of non-null values
    ("owner_address", "owner_first_name", "owner_last_name", "owner_city", "owner_state"),
    ("owner_address", "owner_first_name", "owner_last_name", "owner_zip"),
    ("owner_first_name", "owner_last_name", "owner_city", "owner_state"), ("owner_name", "owner_city", "owner_state"),
    ("owner_first_name", "owner_last_name", "owner_zip"), ("owner_name", "owner_zip"),
    #("owner_address", "owner_city", "owner_state"), #("owner_address", "owner_zip"),
    #("owner_first_name", "owner_last_name", "owner_state"), #("owner_first_name", "owner_last_name"),
]

o_DB = None
s_DB = db_tables.s_DB  # "SandboxKE"

s_FIELD_MAPS_TABLE =  db_tables.s_FIELD_MAPS_TABLE  # "field_maps"
s_OWNERS_TABLE =  db_tables.s_OWNERS_TABLE  # "owners"
s_OWNER_MAPS_TABLE =   db_tables.s_OWNER_MAPS_TABLE  # "owner_maps"
#s_PROPERTIES_TABLE =  db_tables.s_PROPERTIES_TABLE  # "properties"
#s_PROCESS_TABLE =  db_tables.s_PROCESS_TABLE  # "python_scripts"

e_SOURCE_STANDARDIZATION =  db_tables.e_SOURCE_STANDARDIZATION  # {
#    "hubspot": "Hubspot", "hubspot_vid": "Hubspot", "hubspot_id": "Hubspot", "hubspotvid": "Hubspot",
#    "prospectnow": "ProspectNow", "prospect_now": "ProspectNow", "prospect-now": "ProspectNow", "prospect now": "ProspectNow",
#    "realtymole": "RealtyMole", "realty_mole": "RealtyMole", "realty mole": "RealtyMole", "realty-mole": "RealtyMole",
#}


def sufficient_owner_information(e_owner):
    ''' Check if a contact contains enough information to be add '''
    #if any(col in find_contact.c_UNIQUE_COLS for col in e_owner.keys()):
    if any(e_owner.get(col) for col in a_UNIQUE_COLS): return True
    for t_combo in a_COMBO_COLS:
        if all(e_owner.get(col) for col in t_combo): return True
    return False


class ContactFinder:
    def __init__(self, **kwargs):
        self._db = None
        self._load_dbs(**kwargs)
        
        self._unique_cols = kwargs.get("unique_cols", a_UNIQUE_COLS)
        self._combo_cols = kwargs.get("combo_cols", a_COMBO_COLS)
        
        self._db.load()
        self._owner_cols = self._db.get_table_cols(s_OWNERS_TABLE)
        self._map_tables = self._db.unique_column_values(s_OWNER_MAPS_TABLE, "other_table")
        self._map_table_aliases = {source.lower(): source for source in self._map_tables.keys()}
        self._db.close()
    
    def _load_dbs(self, **kwargs):
        global o_DB
        
        if kwargs.get("o_db"):
            self._db = kwargs["o_db"]
            if o_DB is None: o_DB = self._db
        elif o_DB is not None:
            self._db = o_DB
        else:
            self._db = mysql_db.mySqlDbConn(
                s_host = kwargs.get("aws_db_host", mysql_db._host),
                i_port = kwargs.get("aws_db_port", mysql_db._port),
                s_user = kwargs.get("aws_db_user", AWS_DB_USER),
                s_pass = kwargs.get("aws_db_pass", AWS_DB_PASSWORD),
                s_database = kwargs.get("aws_main_db", s_DB),
                chunk_size = 50000,
            )
            o_DB = self._db
    
    def get_other_ids(self, owner_id, **kwargs):
        ''' for a given owner id, find the other sources it may be mapped to
            Arguments:
                owner_id: {int} a valid owner id
            Returns:
                {dict} of {"source_a": [source_a_id_1, source_a_id_2...], "source_b": [source_b_id_1], ...}
        '''
        a_mappings = self._db.search_row(s_OWNER_MAPS_TABLE, {"owner_id": owner_id})
        e_mappings = defaultdict(list)
        for source, other_id, owner in a_mappings:
            e_mappings[source].append(other_id)
        return dict(e_mappings)
        
    def get_owner_id(self, source, other_id, **kwargs):
        ''' for a given other_id, find the owner id(s)
            Arguments:
                source: {str} a table like "ProspectNow" or a connected API like "Hubspot"
                other_id: {int} a valid other id for that source
            Returns:
                {list} of owner_ids (hopefully just one)
        '''
        if source not in self._map_tables: return []
        
        a_map_rows = self._db.search_row(s_OWNER_MAPS_TABLE, {"other_table": source, "other_id": other_id})
        if not a_map_rows: return []
        
        return [t_row[2] for t_row in a_map_rows]
    
    def delete_contact(self, owner_id=None, source=None, source_id=None, **kwargs):
        ''' delete an owner and any mappings given an owner id or one of the source/source_ids that links to it
            Does NOT delete the associated records in other tables/apis, or prevent it from being re-added during the next scheduled update
            Arguments:
                owner_id: {int} a valid owner id
                ---OR---
                source: {str} a table like "ProspectNow" or a connected API like "Hubspot"
                other_id: {int} a valid other id for that source
        '''
        assert owner_id or (source and source_id)
        if owner_id:
            a_owner_ids = [owner_id]
        else:
            a_owner_ids = get_owner_id(source=source, other_id=source_id, **kwargs)
        
        #for owner_id in a_owner_ids:
        #    #e_mappings = self.get_other_ids(owner_id)
        #    self._db.delete_row(s_OWNER_MAPS_TABLE, {"owner_id": owner_id})
        self._db.delete_rows(s_OWNER_MAPS_TABLE, "owner_id", a_owner_ids)
    
    def add_new_ids(self, owner_id, mappings, **kwargs):
        ''' given one or mappings between owner id and other sources, add those as maps to the owner_maps table
            Arguments:
                mappings:
                    {list} of tuples like [("source_a", "source_a_id_1"), ("source_a", "source_a_id_2", "different_owner"), ("source_b", "source_b_id_1") ...]
                    -OR-
                    {dict} of lists like {"source_a": ["source_a_id_1", "source_a_id_2"], "source_b": ["source_b_id_1"], ...}
        '''
        a_mappings = []
        if isinstance(mappings, dict):
            for s_source, a_maps in mappings.items():
                a_mappings.extend([
                    (s_source, v_id, owner_id)
                    for v_id in a_maps])
        elif isinstance(mappings, list):
            assert all([isinstance(x, tuple) for x in mappings])
            a_mappings.extend([
                t_map if len(t_map) == 3 else
                (t_map[0], t_map[1], owner_id)
                for t_map in mappings])
        
        self._db.insert_batch(
            s_OWNER_MAPS_TABLE,
            ["other_table", "other_id", "owner_id",],
            a_mappings)
        
    
    def find_contact(self, e_contact, return_records=False, max_matches=1, b_thorough=True, **kwargs):
        ''' Look in the master owners tables for a contact matching key elements in the search
            Arguments:
                e_contact: {dict} with standard or at least common field names
                return_records: {bool} return the full row records instead of just the id
            Returns:
                {list} of master owner records matching or just their ids
        '''
        b_full_records = return_records #  kwargs.get("return_records", True)
        i_max_matches = max_matches #  kwargs.get("max_matches", 1)
        
        a_return_cols = None
        if kwargs.get("return_cols"):
            if kwargs["return_cols"] == True:
                a_return_cols = a_RETURN_COLS
            else:
                a_return_cols = kwargs["return_cols"]
        
        e_matches = {}
        
        self._db.load()
        
        # try looking for any ids from other tables that are mapped to an owner record
        a_other_ids = []
        for k, v in e_contact.items():
            s_source = e_SOURCE_STANDARDIZATION.get(k.lower(), k)
            if s_source not in self._map_tables: continue
            v_other_id = v
            
            if v_other_id:
                a_other_ids.extend(self._db.search_row(s_OWNER_MAPS_TABLE, {"other_table": s_source, "other_id": v_other_id}))
        
        if a_other_ids:
            if return_records:
                a_other_id_matches = self._db.get_rows(s_OWNERS_TABLE, "id", tuple([t[2] for t in a_other_ids]))
                e_matches.update({t_row[0]: t_row for t_row in a_other_id_matches})
            else:
                e_matches.update(dict(zip(tuple([t[2] for t in a_other_ids]), range(len(a_other_ids)))))
            if kwargs.get("b_debug"): print("a_other_id_matches", a_other_id_matches)
        
        if e_matches and not b_thorough:
            if return_records:
                if kwargs.get("as_dicts"):
                    return [dict(zip(self._owner_cols, t_match)) for t_match in e_matches.values()]
                else:
                    return list(e_matches.values())
            else:
                return list(e_matches.keys())
        
                
        # normalize fields to owner fields and standardize values
        e_contact = standardization.standardize_owner(standardization.to_owner_contact(e_contact))
        if kwargs.get("b_debug"): print("e_contact", e_contact)
        
        #try looking for unique ids and fields
        a_unique_cols = kwargs.get("unique_cols", self._unique_cols)
        a_unique_matches = []
        for s_unique_col in a_unique_cols:
            if e_contact.get(s_unique_col):
                #t_row = self._db.search_row(s_OWNERS_TABLE, {s_unique_col: e_contact[s_unique_col]}, columns=a_return_cols, i_max_rows=1)
                a_matches = self._db.search_row(s_OWNERS_TABLE, {s_unique_col: e_contact[s_unique_col]}, columns=a_return_cols, i_max_rows=i_max_matches)
                if a_matches:
                    if isinstance(a_matches, tuple): a_matches = [a_matches]
                    a_unique_matches.extend(a_matches)
                    #if return_records:
                    #    a_unique_matches.extend(a_matches)
                    #else:
                    #    a_unique_matches.extend([t_row[0] for t_row in a_matches])
        
        e_matches.update({t_row[0]: t_row for t_row in a_unique_matches})
        
        if e_matches:
            if len(e_matches) > 1:
                print(f"Found too many matches ({len(e_matches)}) for specified other source ids or unique fields like email/phone")
        
        if e_matches and not b_thorough:
            if return_records:
                if kwargs.get("as_dicts"):
                    return [dict(zip(self._owner_cols, t_match)) for t_match in e_matches.values()]
                else:
                    return list(e_matches.values())
            else:
                return list(e_matches.keys())
        
        # try looking for combinations that should be unique
        a_combo_cols = kwargs.get("combo_cols", self._combo_cols)
        a_combo_matches = []
        e_ambiguous_matches = {}
        for t_combo in a_combo_cols:
            if all([e_contact.get(col) for col in t_combo]):
                a_matches = self._db.search_row(s_OWNERS_TABLE, e_searches={col: e_contact[col] for col in t_combo}, columns=a_return_cols, i_max_rows=10000)
                if not a_matches:
                    continue  # not matched
                elif len(a_matches) == 1:  # unique match
                    a_combo_matches.extend(a_matches)
                    break
                    #if return_records:
                    #    return [a_matches[0]]
                    #else:
                    #    return [a_matches[0][0]]
                else: # ambiguous matches
                    if not e_matches:
                        print(f"Found too many matches ({len(a_matches)}) for combination of fields that should be unique:", t_combo, {col: e_contact[col] for col in t_combo}, a_matches)
                        e_ambiguous_matches.update({t_row[0]: t_row for t_row in a_matches})
        
        e_matches.update({t_row[0]: t_row for t_row in a_combo_matches})
        
        self._db.close()
        
        if e_matches:
            if return_records:
                if kwargs.get("as_dicts"):
                    return [dict(zip(self._owner_cols, t_match)) for t_match in e_matches.values()]
                else:
                    return list(e_matches.values())
            else:
                return list(e_matches.keys())
        
        elif e_ambiguous_matches:
            if return_records:
                if kwargs.get("as_dicts"):
                    return [dict(zip(self._owner_cols, t_match)) for t_match in e_ambiguous_matches.values()]
                else:
                    return list(e_ambiguous_matches.values())
        else:
            return []
    
    
    def search_contact(self, e_contact, return_records=False, max_matches=1000, **kwargs):
        ''' Look in the master owners tables for all contact matching elements in the search
            Arguments:
                e_contact: {dict} with standard or at least common field names
                return_records: {bool} return the full row records instead of just the id
            Returns:
                {list} of master owner records matching or just their ids
        '''
        b_full_records = return_records #  kwargs.get("return_records", True)
        i_max_matches = max_matches #  kwargs.get("max_matches", 1)
        
        a_return_cols = None
        if kwargs.get("return_cols"):
            if kwargs["return_cols"] == True:
                a_return_cols = a_RETURN_COLS
            else:
                a_return_cols = kwargs["return_cols"]
        
        e_matches = {}
        self._db.load()
        
        # try looking for any ids from other tables that are mapped to an owner record
        a_other_ids = []
        for k, v in e_contact.items():
            s_source = e_SOURCE_STANDARDIZATION.get(k.lower(), k)
            if s_source not in self._map_tables: continue
            v_other_id = v
            
            if v_other_id:
                a_other_ids.extend(self._db.search_row(s_OWNER_MAPS_TABLE, {"other_table": s_source, "other_id": v_other_id}))
        
        if a_other_ids:
            if return_records:
                a_other_id_matches = self._db.get_rows(s_OWNERS_TABLE, "id", tuple([t[2] for t in a_other_ids]))
                e_matches.update({t_row[0]: t_row for t_row in a_other_id_matches})
            else:
                e_matches.update(dict(zip(tuple([t[2] for t in a_other_ids]), range(len(a_other_ids)))))
            if kwargs.get("b_debug"): print("a_other_id_matches", a_other_id_matches)
                
        # normalize fields to owner fields and standardize values
        e_contact = standardization.standardize_owner(standardization.to_owner_contact(e_contact))
        if kwargs.get("b_debug"): print("e_contact", e_contact)
        
        #try looking for unique ids and fields
        a_unique_cols = kwargs.get("unique_cols", self._unique_cols)
        a_unique_matches = []
        for s_unique_col in a_unique_cols:
            if e_contact.get(s_unique_col):
                #t_row = self._db.search_row(s_OWNERS_TABLE, {s_unique_col: e_contact[s_unique_col]}, columns=a_return_cols, i_max_rows=1)
                a_matches = self._db.search_row(s_OWNERS_TABLE, {s_unique_col: e_contact[s_unique_col]}, columns=a_return_cols, i_max_rows=i_max_matches)
                if a_matches:
                    if isinstance(a_matches, tuple): a_matches = [a_matches]
                    a_unique_matches.extend(a_matches)
        
        e_matches.update({t_row[0]: t_row for t_row in a_unique_matches})
        
        # try looking for combinations that should be unique
        if len(e_matches) < max_matches:
            a_combo_cols = kwargs.get("combo_cols", self._combo_cols)
            a_combo_matches = []
            for t_combo in a_combo_cols:
                if all([e_contact.get(col) for col in t_combo]):
                    a_matches = self._db.search_row(s_OWNERS_TABLE, e_searches={col: e_contact[col] for col in t_combo}, columns=a_return_cols, i_max_rows=i_max_matches)
                    if not a_matches:
                        continue  # not matched
                    else:
                        a_combo_matches.extend(a_matches)
        
        while len(e_matches) < max_matches and a_combo_matches:
            t_row = a_combo_matches.pop(0)
            e_matches[t_row[0]] = t_row
        
        # try looking at all remaining fields
        if not e_matches: #len(e_matches) < max_matches:
            e_and_conditions = {
                k: v for k, v in e_contact.items() if k not in c_UNIQUE_COLS and v}
            if "owner_name" in e_and_conditions and ("owner_first_name" in e_and_conditions or "owner_last_name" in e_and_conditions):
                del e_and_conditions["owner_name"]
            
            if not e_and_conditions: return []
            #print("e_and_conditions", e_and_conditions)
            a_and_matches = self._db.search_row(s_OWNERS_TABLE, e_searches=e_and_conditions, columns=a_return_cols, operator="AND", i_max_rows=i_max_matches)
            while len(e_matches) < max_matches and a_and_matches:
                t_row = a_and_matches.pop(0)
                e_matches[t_row[0]] = t_row
        
        # try looking at any field
        if kwargs.get("any_overlap") and len(e_matches) < max_matches:
            e_or_conditions = {
                k: v for k, v in e_contact.items() if k not in c_UNIQUE_COLS and v}
            #print("e_or_conditions", e_or_conditions)
            if not e_or_conditions: return []
            a_or_matches = self._db.search_row(s_OWNERS_TABLE, e_searches=e_or_conditions, columns=a_return_cols, operator="OR", i_max_rows=i_max_matches)
            while len(e_matches) < max_matches and a_or_matches:
                t_row = a_or_matches.pop(0)
                e_matches[t_row[0]] = t_row
        
        self._db.close()
        
        if e_matches:
            if return_records:
                if kwargs.get("as_dicts"):
                    return [dict(zip(self._owner_cols, t_match)) for t_match in e_matches.values()]
                else:
                    return list(e_matches.values())
            else:
                return list(e_matches.keys())
        else:
            return []
