﻿import os, sys, re
from operator import itemgetter
import pandas as pd
import pymysql

re_COL_DATATYPES = re.compile(
    r"((var)?char\(\d+\)|(tiny|medium|long)?(text|blob)|(tiny|small|medium|big)?int|"
    "bool(ean)?|(decimal|float|double)(\(\d*,*d\))?|binary|enum|set|date|datetime|timestamp|time|year)", flags=re.I)

############## DB CONNECTION INFORMATION ################
# This could be moved to enviroment variables 
# if being tested/deployed with continuous integration
_host = "rr7l99bygshfm4.cosafxp7zpze.us-east-1.rds.amazonaws.com"
_port = 3306
_user = os.environ.get("IDSTS_AWS_DB_USER", "")
_password = os.environ.get("IDSTS_AWS_DB_PASS", "")

e_db_tables = {
    "turnover": [
        "DataDictionary", #153
        "GeoEnrichOutput1", #121088
        "GeoEnrichPivoted", #115230
        "ProspectNow", #102624
        "RealtyMole", #33470
        "airdna", # 1
        "loopnet", # 1
        "steven", # 33425
        "Acs", # 739
        "CensusFipsMaster", # 43751
        "Infutor", # 4877
        "powerbi_full", # 97814
    ],
    "MLEngine": [
        "Hub_Prediction", 
    ],
    "SandboxKE": [
        'field_maps', #409
        'hubspot_touch_summary', #4
        'owners', #4736
        'property_master_map', #2419
        'python_scripts', #2
    ],
}

############## CONNECTOR CLASS ################
'''
    Example usage:
        o_db = mySqlDbConn(s_host=s_host, s_database=s_db, s_user=s_user, s_pass=s_pass, s_port=s_port)
        o_db.load()
        a_hardware = o_db.get_large_table("hardware")
        
        df_brands = o_db.get_table_as_df("brands")
        o_db.close()
'''

class mySqlDbConn:
    def __init__(self, s_host, s_database, s_user, s_pass, i_port=3306, s_table_prefix="", **kwargs):
        self.host, self.port = s_host, i_port
        self.database, self.user, self.password = s_database, s_user, s_pass
        self.table_prefix = s_table_prefix
        self.conn, self.cursor = None, None
        self._chunk_size = kwargs.get("chunk_size", 100000)  # number of rows to pull when reading large tables in segmentschunks
    
    def __repr__(self):
        s_repr = f'__init__ = {self.host}:{self.port}/{self.database}'
        return s_repr
    
    def load(self):
        ''' Connect to mysql server and db, and start a cursor '''
        if self.conn is None:
            self.conn = pymysql.connect(
                host=self.host,
                port=self.port,
                user=self.user,
                password=self.password,
                db=self.database
            )
            
            self.cursor = self.conn.cursor()
        elif self.cursor is None:
            self.cursor = self.conn.cursor()
    
    def close(self):
        if self.cursor is not None: self.cursor.close()
        if self.conn is not None: self.conn.close()
        self.cursor = None
        self.conn = None
    
    def __enter__(self):
        print('opening connection to postgres')
        self.load()
    
    def __exit__(self, exc_type, exc_value, traceback):
        print('closing connection to postgres')
        self.close()
    
    ########## Class Decorators ##########
    def auto_load(func, *args, **kwargs):
        ''' automatically open and close connection if it already isn't already open '''
        def auto_load_decorator(self, *args, **kwargs):
            b_already_open = self.conn is not None
            if not b_already_open:
                self.load()
            
            output = func(self, *args, **kwargs)
            
            if not b_already_open:
                self.close()
            return output
        return auto_load_decorator
    
    def auto_commit(func, *args, **kwargs):
        ''' automatically commits the queue as well as open and close connection '''
        def auto_commit_decorator(self, *args, **kwargs):
            b_already_open = self.conn is not None
            if not b_already_open:
                self.load()
            
            output = func(self, *args, **kwargs)
            self.conn.commit()
            
            if not b_already_open:
                self.close()
            return output
        return auto_commit_decorator
    
    
    ############ Other ############
    def commit(self, **kwargs):
        if self.conn is not None:
            self.conn.commit()
        return self
        
    @auto_load
    def execute(self, statement, **kwargs):
        result = self.cursor.execute(statement)
        if kwargs.get("commit"):
            self.conn.commit()
        if kwargs.get("fetch", True):
            return self.cursor.fetchall()
        else:
            return result
    
    ############ Table Control ############
    @auto_load
    def list_tables(self, **kwargs):
        self.cursor.execute("show tables")
        a_tables = self.cursor.fetchall()
        return [
            (x[0], self.get_table_row_est(x[0], **kwargs)) for x in a_tables]
    
    @auto_load
    def create_table(self, s_table, a_columns, **kwargs):
        ''' create a new SQL table
            Arguments:
                s_table: {str} name of table to create
                a_columns: {list} of fields in format of
                    [("column_name", "var_type", "other_flags"),...]
                    or
                    ["column_name vartype other_flags",...]
        '''
        a_cols = []
        for col in zip(a_columns):
            #print("col", col)
            if isinstance(col, (tuple, list)):
                if not col or not col[0]: continue
                s_col = ' '.join(col)
                a_cols.append(s_col)
            elif isinstance(col, str):
                s_col = col.strip()
                if not s_col: continue
                a_cols.append(s_col)
    
#            sql = "".join(["CREATE TABLE IF NOT EXISTS {} (\n".format(s_MARKERAPI_TABLE), ", \n".join(a_columns), "\n) ;"])
        s_cols = ', '.join(a_cols)
        s_sql = f"CREATE TABLE {s_table}({s_cols});"
        if kwargs.get("b_debug"): print(s_sql)
        try:
            self.cursor.execute(s_sql)
            return True
        except Exception as e:
            print("Problem occured:{}".format(e))
            return False
    
    @auto_load
    def delete_table(self, s_table, **kwargs):
        try:
            self.cursor.execute(f'DROP TABLE IF EXISTS `{s_table}`')
            #print "Affected: %d" % self.cursor.rowcount
            return True
        except:
            return False
    
    @auto_load
    def rename_tables(self, e_table_renames, **kwargs):
        #try:
        #a_rows = [(s_old, s_new) for s_old, s_new in e_table_renames.items()]
        #s_sql = f'RENAME TABLE %s TO %s IF EXISTS %s;'
        #s_sql = f'RENAME TABLE %s TO %s;'
        #return self.cursor.executemany(s_sql, a_rows)
        #print "Affected: %d" % self.cursor.rowcount
        
        for s_old, s_new in e_table_renames.items():
            s_sql = f"ALTER TABLE {s_old} RENAME {s_new}"
            self.cursor.execute(s_sql)
        
        return True
        #except:
        #    return False
    
    def get_table_row_est(self, s_table_name, **kwargs):
        if s_table_name.startswith(self.table_prefix):
            s_table_name = s_table_name[len(self.table_prefix):]
            
        if kwargs.get("exact"):
            self.cursor.execute(f"SELECT COUNT(*) FROM {s_table_name}")
            t_count = self.cursor.fetchone()
            i_count = int(t_count[0])
                
            return i_count
        else:
            self.cursor.execute(f"SELECT table_name, table_rows FROM information_schema.tables WHERE table_name = '{s_table_name}'")
            t_count = self.cursor.fetchone()
            if t_count:
                i_count = int(t_count[1])
                if i_count == 0:
                    # if estimate is zero, double-check with get direct count
                    self.cursor.execute(f"SELECT COUNT(*) FROM {s_table_name}")
                    t_count = self.cursor.fetchone()
                    i_count = int(t_count[0])
                    
                return i_count
            else:
                return 0
    
    def get_table_cols(self, s_table_name, b_include_data_type=False, **kwargs):
        if s_table_name.startswith(self.table_prefix):
            s_table_name = s_table_name[len(self.table_prefix):]
        if b_include_data_type:
            self.cursor.execute(f"SELECT ordinal_position, column_name,data_type FROM information_schema.columns WHERE table_name = '{s_table_name}'")
            a_cols = list(self.cursor.fetchall())
            a_cols.sort(key=itemgetter(0))
            return [x[1:] for x in a_cols]
        else:
            self.cursor.execute(f"SELECT ordinal_position, column_name FROM information_schema.columns WHERE table_name = '{s_table_name}'")
            a_cols = list(self.cursor.fetchall())
            a_cols.sort(key=itemgetter(0))
            return [x[1] for x in a_cols]
    
    @auto_commit     
    def add_column(self, s_table_name, s_column, s_signature="text", b_first=False, s_after="", **kwargs):
        s_sql = f"ALTER TABLE {s_table_name} ADD {s_column} {s_signature}"
        if b_first:
            s_sql += " FIRST"
        elif s_after:
            s_sql += f" AFTER {s_after}"
        if kwargs.get("b_debug"): print(s_sql)
        return self.cursor.execute(s_sql)

    ############ Data Changes ############
    @auto_commit
    def insert(self, s_table, e_values, **kwargs):
        ''' Insert a row into the specified table
            Arguments:
                s_table: {str}
                e_values: {dict} of {column1: value1...}
        '''
        assert e_values
        t_elements = tuple(e_values.items())
        s_cols = ", ".join([t[0] for t in t_elements])
        s_vals = ", ".join(["%s"] * len(t_elements))
        t_vals = tuple([t[1] for t in t_elements])
        
        s_sql = f"REPLACE INTO {s_table}({s_cols}) VALUES({s_vals});"
        if kwargs.get("b_debug"): print(s_sql)
        result = self.cursor.execute(s_sql, t_vals)
        #self.conn.commit()
        
        if kwargs.get("return_id"):
            return self.cursor.lastrowid
        elif kwargs.get("fetch", True):
            return self.cursor.fetchall()
        else:
            return result
    
    @auto_commit
    def insert_batch(self, s_table, a_column_names, a_rows, **kwargs):
        ''' Insert a batch of rows into the specified table
            Arguments:
                s_table: {str}
                a_column_names: {list} of the names of columns
                a_rows: {list} of tuples of the data for each row
        '''
        assert a_column_names
        if not a_rows: return 0
        assert all(len(t) == len(a_column_names) for t in a_rows)
        s_columns = ", ".join(a_column_names)
        s_val_placeholders = ", ".join(["%s"] * len(a_column_names))
        s_sql = f"REPLACE INTO {s_table}({s_columns}) VALUES({s_val_placeholders});"
        result = self.cursor.executemany(s_sql, a_rows)
        #self.conn.commit()
        return result
    
    @auto_commit
    def update(self, s_table, e_values, e_indexes, **kwargs):
        ''' Update a row values into the specified table
            Arguments:
                s_table: {str}
                e_values: {dict} of {column1: value1...}
                e_indexes: {dict} of {column1: value1...}
        '''
        assert e_values
        a_elements = list(e_values.items())
        s_val_cols = ", ".join([f"{t[0]} = %s" for t in a_elements])
        a_vals = [t[1] for t in a_elements]
        
        a_conditions = list(e_indexes.items())
        s_cond_cols = " AND ".join([f"{t[0]} = %s" for t in a_conditions])
        a_cond = [t[1] for t in a_conditions]
        
        t_row = tuple(a_vals + a_cond)
        if a_conditions:
            s_sql = f"UPDATE {s_table} set {s_val_cols} WHERE {s_cond_cols};"
        else:
            s_sql = f"UPDATE {s_table} set {s_val_cols};"
            
        if kwargs.get("b_debug"): print(s_sql, t_row)
        result = self.cursor.execute(s_sql, t_row)
        #self.conn.commit()
        return result
    
    @auto_commit
    def update_batch(self, s_table, a_value_cols, a_key_cols, a_rows, **kwargs):
        ''' Update a batch of values into the specified table
            Arguments:
                s_table: {str}
                a_value_cols: {list} of columns whose values will be replaced [column2, column3...]
                a_key_cols: {list} of columns whose keys must match [column1...]
                a_rows: {list} of tuples of values for a_value_cols and a_key_cols for each row
                    [
                        (row1_value2, row1_value3, row1_value1),
                        (row2_value2, row2_value3, row2_value1),
                    ]
        '''
        assert a_value_cols
        assert a_key_cols
        if not a_rows: return 0
        
        s_val_cols = ", ".join([f"{col} = %s" for col in a_value_cols])
        s_cond_cols = " AND ".join([f"{col} = %s" for col in a_key_cols])
        
        i_cols = len(a_value_cols) + len(a_key_cols)
        assert all(len(t_row) == i_cols for t_row in a_rows)
        
        s_sql = f"UPDATE {s_table} SET {s_val_cols} WHERE {s_cond_cols} ;"
        if kwargs.get("b_debug"): print(s_sql, a_rows[0])
        result = self.cursor.executemany(s_sql, a_rows)
        return result
    
    @auto_commit
    def delete_all_rows(self, s_table, b_fast=False, **kwargs):
        ''' Delete all the rows from a table
            Arguments:
                s_table: {str} the table to erase all rows from
                b_fast: {boolean} faster operation, but cannot be rolled back
        '''
        if b_fast:
            s_sql = f"TRUNCATE TABLE {s_table}"
        else:
            s_sql = f"DELETE FROM {s_table}"
        result = self.cursor.execute(s_sql,)
        return result
    
    @auto_commit
    def delete_batch(self, s_table, a_ids, **kwargs):
        ''' Delete a batch of rows into the specified table
            Arguments:
                s_table: {str} the table to erase specified rows from
                a_ids: {list} of "id" values to delete
        '''
        s_sql = f"DELETE FROM {s_table} WHERE id = %s"
        a_ids_to_delete = [
            (id,) if isinstance(id, str) else
            (str(int(id)),) if isinstance(id, (int, float)) else
            id
            for id in a_ids]
        result = self.cursor.executemany(s_sql, a_ids_to_delete)
        return result
    
    @auto_commit
    def delete_row(self, s_table, e_conditions, **kwargs):
        ''' Delete a row into the specified table
            Arguments:
                s_table: {str}
                e_conditions: {dict} of {column1: value1...}
        '''
        assert e_conditions
        t_elements = tuple(e_conditions.items())
        t_vals = tuple([t[1] for t in t_elements])
        
        s_priority = kwargs.get("priority", "LOW_PRIORITY")
        assert s_priority in {"LOW_PRIORITY", "QUICK", "IGNORE"}
        
        s_operator = " {} ".format(kwargs.get("operator", "AND"))
        s_cond_cols = s_operator.join([f"{col[0]} = %s" for col in t_elements])
        
        s_sql = f"DELETE {s_priority} FROM {s_table} WHERE {s_cond_cols};"
        if kwargs.get("b_debug"): print(s_sql)
        result = self.cursor.execute(s_sql, t_vals)
        
        if kwargs.get("return_id"):
            return self.cursor.lastrowid
        elif kwargs.get("fetch", True):
            return self.cursor.fetchall()
        else:
            return result
    
    @auto_commit
    def delete_rows(self, s_table, a_key_cols, a_rows, **kwargs):
        ''' Delete rows matching specified conditions into the specified table
            Arguments:
                s_table: {str} the table to erase specified rows from
                a_value_cols: {list} of columns whose values will be replaced [column2, column3...]
                a_key_cols: {list} of columns whose keys must match [column1...]
                a_rows: {list} of tuples of values for a_value_cols and a_key_cols for each row
                    [
                        (row1_value2, row1_value3, row1_value1),
                        (row2_value2, row2_value3, row2_value1),
                    ]
        '''
        assert a_key_cols
        if not a_rows: return 0
        
        s_priority = kwargs.get("priority", "LOW_PRIORITY")
        assert s_priority in {"LOW_PRIORITY", "QUICK", "IGNORE"}
        
        s_operator = " {} ".format(kwargs.get("operator", "AND"))
        s_cond_cols = s_operator.join([f"{col} = %s" for col in a_key_cols])
        
        i_cols = len(a_key_cols)
        assert all(len(t_row) == i_cols for t_row in a_rows)
        
        s_sql = f"DELETE {s_priority} FROM {s_table} WHERE {s_cond_cols} ;"
        if kwargs.get("b_debug"): print(s_sql, a_rows[0])
        result = self.cursor.executemany(s_sql, a_rows)
        return result
    
    ############ Data Access ############
    @auto_load
    def get_large_table(self, s_table, i_start=0, i_batch_size=None, **kwargs):
        ''' iterative read, but returning single large python list of rows '''
        if i_batch_size is None or i_batch_size < 1:
            i_batch_size = self._chunk_size
        
        i_total_rows = self.get_table_row_est(s_table, **kwargs)
        
        if kwargs.get("maximum"):
            i_total_rows = min(i_total_rows, kwargs["maximum"])
            i_batch_size = min(i_batch_size, kwargs["maximum"])
        
        if not s_table.startswith(self.table_prefix):
            s_table = f"{self.table_prefix}{s_table}"
        a_rows = []
        
        s_cols = "*"
        if kwargs.get("cols"):
            if isinstance(kwargs["cols"], (list, tuple)):
                s_cols = ", ".join(kwargs["cols"])
            else:
                s_cols = kwargs["cols"]
        
        s_where = kwargs.get("where", "").strip()
        if s_where and not s_where.upper().startswith("WHERE "): s_where = f"WHERE {s_where}"
        
        i_offset = i_start
        i_batch = 0
        while i_offset < i_total_rows:
            i_batch += 1
            print(f"\tbatch {i_batch:,}, {i_offset:,}-{min(i_total_rows, i_offset + i_batch_size):,}")
            if kwargs.get("b_debug"): print(f"SELECT {s_cols} from {s_table} {s_where} LIMIT {i_batch_size} OFFSET {i_offset}")
            self.cursor.execute(f"SELECT {s_cols} from {s_table} {s_where} LIMIT {i_batch_size} OFFSET {i_offset}")
            a_matches = self.cursor.fetchall()
            a_rows.extend(a_matches)
            i_offset += i_batch_size
        return a_rows
    
    @auto_load
    def get_table(self, s_table, **kwargs):
        ''' Return a table as a single list. Only suitable for smallish tables. '''
        if not s_table.startswith(self.table_prefix):
            s_table = f"{self.table_prefix}{s_table}"
        
        s_cols = "*"
        if kwargs.get("cols"):
            if isinstance(kwargs["cols"], (list, tuple)):
                s_cols = ", ".join(kwargs["cols"])
            else:
                s_cols = kwargs["cols"]
        
        s_where = kwargs.get("where", "").strip()
        if s_where and not s_where.upper().startswith("WHERE "): s_where = f"WHERE {s_where}"
        
        self.cursor.execute(f"SELECT {s_cols} from {s_table} {s_where};")
        return self.cursor.fetchall()
    
    @auto_load
    def get_table_as_df(self, s_table, **kwargs):
        a_cols = self.get_table_cols(s_table, **kwargs)
        a_rows = self.get_large_table(s_table, **kwargs)
        
        return pd.DataFrame(a_rows, columns=a_cols)
    
    @auto_load
    def write_table_to_csv(self, s_table, s_path, to_excel_kwargs={}, **kwargs):
        a_cols = self.get_table_cols(s_table)
        a_rows = self.get_large_table(s_table)
        
        df_table = pd.DataFrame(a_rows, columns=a_cols)
        df_table.to_csv(s_path, **to_excel_kwargs)
    
    def yield_table(self, s_table, i_start=0, i_max_rows=None, i_batch_size=None, **kwargs):
        ''' iterative read, yielding chunks of table as lists of rows '''
        if i_batch_size is None or i_batch_size < 1:
            i_batch_size = self._chunk_size
        
        i_total_rows = self.get_table_row_est(s_table, **kwargs)
        if i_max_rows is not None: i_total_rows = min(i_total_rows, i_start + i_max_rows)
        
        if not s_table.startswith(self.table_prefix):
            s_table = f"{self.table_prefix}{s_table}"
        a_rows = []
        
        i_offset = i_start
        i_batch = (i_start // i_batch_size)
        s_where = kwargs.get("where", "").strip()
        if s_where and not s_where.upper().startswith("WHERE "): s_where = f"WHERE {s_where}"
        
        while i_offset < i_total_rows:
            if i_batch >= 25000: self.load()
            
            i_batch += 1
            print(f"\tbatch {i_batch:,}, {i_offset:,}-{i_offset + i_batch_size:,}")
            self.cursor.execute(f"SELECT * from {s_table} {s_where} LIMIT {i_batch_size} OFFSET {i_offset}")
            a_matches = self.cursor.fetchall()
            yield a_matches
            i_offset += i_batch_size

            if i_batch >= 25000: self.close()
    
    ############ Data Access ############
    @auto_commit
    def search_row(self, s_table, e_searches, **kwargs):
        ''' Find rows matching specified conditions into the specified table
            Arguments:
                s_table: {str}
                e_searches: {dict} of {column1: value1...}
            Returns:
                {list} of matches
        '''
        assert e_searches
        a_conditions = list(e_searches.items())
        a_cond = [t[1] for t in a_conditions]
        
        s_operator = kwargs.get("operator", "AND")
        s_cond_cols = f" {s_operator} ".join([f"{t[0]} = %s" for t in a_conditions])
        
        s_return = "*"
        if kwargs.get("columns"):
            if isinstance(kwargs["columns"], str):
                s_return = kwargs["columns"]
            elif isinstance(kwargs["columns"], (list, tuple)):
                s_return = ",".join(kwargs["columns"])
        
        s_sql = f"SELECT {s_return} FROM {s_table} WHERE {s_cond_cols} ;"
        if kwargs.get("b_debug"): print(s_sql, a_cond)
        result = self.cursor.execute(s_sql, a_cond)
        if kwargs.get("i_max_rows") == 1:
            return self.cursor.fetchone()
        else:
            return list(self.cursor.fetchall())
    
    
    @auto_commit
    def get_rows(self, s_table, s_index_col, t_indexes, **kwargs):
        ''' Find rows with an index in the specified list
            Arguments:
                s_table: {str}
                s_index_col: {str} the column to look for the indexes in
                t_indexes: {tuple} the index values to match
            Returns:
                {list} of matches
        '''
        assert s_index_col
        assert t_indexes
        t_indexes = tuple(t_indexes)
        s_return = "*"
        if kwargs.get("columns"):
            if isinstance(kwargs["columns"], str):
                s_return = kwargs["columns"]
            elif isinstance(kwargs["columns"], (list, tuple)):
                s_return = ",".join(kwargs["columns"])
        
        s_sql = "SELECT {} FROM {} WHERE {} in ({}) ;".format(s_return, s_table, s_index_col, ','.join(r'%s' for _ in t_indexes))
        #s_sql = f"SELECT {s_return} FROM {s_table} WHERE {s_index_col} in (%s) ;"
        
        if kwargs.get("b_debug"): print(s_sql, t_indexes)
        result = self.cursor.execute(s_sql, t_indexes)
        return list(self.cursor.fetchall())
    
    """
    EXECUTEMANY DOESN'T WORK WITH SELECT
    @auto_commit
    def search_rows(self, s_table, a_key_cols, a_rows, i_max_rows=None, **kwargs):
        ''' Find rows matching specified conditions into the specified table
            Arguments:
                s_table: {str}
                a_value_cols: {list} of columns whose values will be replaced [column2, column3...]
                a_key_cols: {list} of columns whose keys must match [column1...]
                a_rows: {list} of tuples of values for a_value_cols and a_key_cols for each row
                    [
                        (row1_value2, row1_value3, row1_value1),
                        (row2_value2, row2_value3, row2_value1),
                    ]
            Returns:
                {list} of matches
        '''
        assert a_key_cols
        if not a_rows: return 0
        
        i_total_rows = self.get_table_row_est(s_table)
        i_max_rows = min(int(i_max_rows), i_total_rows) if isinstance(i_max_rows, (int, float)) and i_max_rows > 0 else i_total_rows
        
        s_cond_cols = " AND ".join([f"{col} = %s" for col in a_key_cols])
        i_cols = len(a_key_cols)
        assert all(len(t_row) == i_cols for t_row in a_rows)
        
        s_return = "*"
        if kwargs.get("columns"):
            if isinstance(kwargs["columns"], str):
                s_return = kwargs["columns"]
            elif isinstance(kwargs["columns"], (list, tuple)):
                s_return = ",".join(kwargs["columns"])
        
        s_sql = f"SELECT {s_return} FROM {s_table} WHERE {s_cond_cols} LIMIT {i_max_rows};"
        if kwargs.get("b_debug"): print(s_sql, a_rows[0])
        result = self.cursor.executemany(s_sql, a_rows)
        #self.conn.commit()
        return list(self.cursor.fetchall())
    """
    
    ############ Analysis/Statistics ############
    @auto_load
    def unique_column_values(self, s_table, s_column, e_searches=None, **kwargs):
        ''' Count the unique values that appear in a column of a table
            Arguments:
                s_table: {str}
                s_column: {str} the column to analyze
                e_searches: {dict} of {column1: value1...}
            Returns:
                {dict} of frequencies
        '''
        if e_searches:
            a_conditions = list(e_searches.items())
            a_cond = [t[1] for t in a_conditions]
            
            s_cond_cols = " AND ".join([f"{t[0]} = %s" for t in a_conditions])
        
            s_sql = f"SELECT {s_column}, COUNT(*) FROM {s_table} GROUP BY {s_column} WHERE {s_cond_cols} ;"
            if kwargs.get("b_debug"): print(s_sql, a_cond)
            result = self.cursor.execute(s_sql, a_cond)
        else:
            s_sql = f"SELECT {s_column}, COUNT(*) FROM {s_table} GROUP BY {s_column};"
            result = self.cursor.execute(s_sql,)
        
        
        return dict(self.cursor.fetchall())

    @auto_load
    def unique_table_values(self, s_table, **kwargs):
        ''' Count the unique values that appear in a column of a table
            Arguments:
                s_table: {str}
                s_column: {str} the column to analyze
                e_searches: {dict} of {column1: value1...}
            Returns:
                {dict} of frequencies
        '''
        e_column_frequencies = {}
        a_columns = self.get_table_cols(s_table)
        c_excludes = set(kwargs.get("exclude_cols", []))
        
        for s_col in a_columns:
            if s_col in c_excludes: continue
            e_column_frequencies[s_col] = self.unique_column_values(s_table, s_col, **kwargs)
        
        
        
        return e_column_frequencies