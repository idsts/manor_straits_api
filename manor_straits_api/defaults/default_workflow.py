﻿default_workflow = {
    'enabled': True,
    'type': 'DRIP_DELAY',
    'name': 'Test workflow created from API',
    'description': '',
    
    'listening': False,
    'onlyEnrollsManually': False,
    'segmentCriteria': [
        [
            {'filterFamily': 'PropertyValue',
            'withinTimeMode': 'PAST',
            'operator': 'EQ',
            'property': 'number_of_emails',
            'value': 0,
            'type': 'number'}
        ]
    ],
    'goalCriteria': [],
    
    'allowContactToTriggerMultipleTimes': False,
    'onlyExecOnBizDays': False,
    
    'actions': [
        {
            'type': 'BRANCH',
            'filtersListId': 7,
            'actionId': 7,
            'info': [],
            'errors': [],
            'warnings': [],
            'filters': [[{'filterFamily': 'Subscriptions',
              'withinTimeMode': 'PAST',
              'subscriptionsFilter': {'subscriptionIds': [9662393],
               'optStatuses': ['OPT_IN']}}]],
            
            'rejectActions': [],
            'acceptActions': [
                {
                    'type': 'DELAY',
                    'stepListCount': 0,
                    'delayMillis': 0,
                    'actionId': 4,
                    'stepId': 1},
                {
                    'type': 'EMAIL',
                    'emailContentId': 30028434909,
                    'emailCampaignId': 89781207,
                    'emailCampaignGroupId': 89780788,
                    'actionId': 1,
                    'stepId': 1,
                    'goalListCount': 0
                },
                {
                    'type': 'ADD_SUBTRACT_PROPERTY',
                    'propertyName': 'number_of_emails',
                    'valueToModifyPropertyBy': 1,
                    'actionId': 2,
                    'stepId': 1
                },
                {
                    'type': 'DELAY',
                    'stepListCount': 0,
                    'delayMillis': 172800000,
                    'actionId': 3,
                    'stepId': 2
                },
                {
                    'type': 'SET_CONTACT_PROPERTY',
                    'propertyName': 'hs_lead_status',
                    'newValue': 'IN_PROGRESS',
                    'actionId': 5,
                    'stepId': 2
                },
            ],
        },
    ],
    
    'nurtureTimeRange': {'enabled': False, 'startHour': 9, 'stopHour': 10},
    'unenrollmentSetting': {'type': 'NONE', 'excludedWorkflows': []},
}

''' You can create a workflow with any of the available
workflow actions. When in doubt, create a workflow in
HubSpot with the desired actions first and inspect the API
response from the workflow request.

The body below will create a workflow with an action 
to set the contact property Company to HubSpot, and then
immediately trigger a webhook to the URL shown with the
given auth credentials. "Delay" actions are optional and if
no delay is desired, it can be left out. Delay values are
required to be in milliseconds; this workflow with execute
the actions 1 hour after each contact is enrolled. Delays
can be added between groups of actions as well. '''


default_workflow_webhook = {
    "name": "Test Workflow",
    "type": "DRIP_DELAY",
    "onlyEnrollsManually": True,
    "actions": [
        {
            "type": "DELAY",
            "delayMillis": 3600000
        },
        {
            "newValue": "HubSpot",
            "propertyName": "company",
            "type": "SET_CONTACT_PROPERTY"
        },
        {
            "type": "WEBHOOK",
            "url": "https://www.myintegration.com/webhook.php",
            "method": "POST",
            "authCreds": {
                "user": "user",
                "password": "password"
            }
        }
    ]
}


''' Changing the "type" field and setting the "eventAnchor"
field lets you create date-based workflows. For example,
this workflow is based on a static date of October 9, 2014
for all contacts enrolled and will execute the property
action at 12:00pm on that date. '''

default_workflow_date = {
    "name": "Test Workflow",
    "type": "STATIC_ANCHOR",
    "onlyEnrollsManually": True,
    "eventAnchor": {
        "staticDateAnchor": "10/09/2014"
    },
    "actions": [
        {
            "type": "DELAY",
            "delayMillis": 0,
            "anchorSetting": {
                "boundary": "ON",
                "execTimeOfDay": "12:00 PM"
            }
        },
        {
            "type": "SET_CONTACT_PROPERTY",
            "propertyName": "company",
            "newValue": "HubSpot"
        }
    ]
}


''' The workflows above don't have any enrollment criteria, so
contacts will only be enrolled manually. To specify
enrollment criteria so the workflow automatically enrolls
contacts, use the same format as list "filters" described
in the Contact Lists API. Put these filters in the
workflow's "segmentCriteria" field. For example, this
workflow enrolls contacts that have a Lead, SQL or MQL
lifecycle stage. '''

default_workflow_manual_enrollment = {
    "name": "Test Workflow",
    "type": "DRIP_DELAY",
    "segmentCriteria": [
        [
            {
                "operator": "SET_ANY",
                "value": "lead;salesqualifiedlead;marketingqualifiedlead",
                "property": "lifecyclestage",
                "type": "string"
            }
        ]
    ],
    "actions": [
    ]
}

'''
To create a workflow with branching logic, add a branch
action as shown below with a group of actions nested within
the branch's "acceptActions" and "rejectActions" fields,
which correspond to the YES/NO branches, respectively. To
specify branch criteria, put the desired list filters in
the branch action's "filters" field. You can nest branches
up to 15 levels deep in workflows. The body below creates a
workflow that sets the HubSpot owner ID to one value or the
other based on whether the contact's industry is "Marketing
Software." '''

default_workflow_branching = {
    "name": "Test Workflow",
    "type": "DRIP_DELAY",
    "onlyEnrollsManually": True,
    "actions": [
        {
            "type": "BRANCH",
            "filters": [
                [
                    {
                        "operator": "EQ",
                        "value": "Marketing Software",
                        "property": "industry",
                        "type": "string"
                    }
                ]
            ],
            "acceptActions": [
                {
                    "type": "SET_CONTACT_PROPERTY",
                    "propertyName": "hubspot_owner_id",
                    "newValue": "12345"
                }
            ],
            "rejectActions": [
                {
                    "type": "SET_CONTACT_PROPERTY",
                    "propertyName": "hubspot_owner_id",
                    "newValue": "67890"
                }
            ]
        }
    ]
}
      