﻿default_contact_property = {
    "name": "newcustomproperty",
    "label": "A New Custom Property",
    "description": "A new property for you",
    "groupName": "contactinformation",
        # contactinformation
        # conversioninformation
        # analyticsinformation
        # emailinformation
    "type": "string",
        # string - string data
        # number - number value, can be any positive or negative integer or decimal number
        # date - A specific date, stored as a millisecond Unix timestamp. See this page for more details about date and datetime properties
        # datetime - A specific time, stored as a millisecond Unix timestamp. See this page for more details about date and datetime properties
        # enumeration - one of a set of values that are set in the 'options' field. If this type is used, at least one option must be included.
    "fieldType": "text",
        # booleancheckbox
        # calculation_equation
        # calculation_read_time
        # calculation_score
        # checkbox
        # date
        # number
        # phonenumber
        # radio
        # select
        # text
        # textare
    "formField": True,
    "displayOrder": 6,
    "options": [],
    "ownerId": "47656206",
}

default_contact_property_ml = {
    "name": "newmlproperty",
    "label": "A New ML Score Property",
    "description": "A new ML property for you",
    "groupName": "machinelearning",
        # contactinformation
        # conversioninformation
        # analyticsinformation
        # emailinformation
    "type": "number",
        # string - string data
        # number - number value, can be any positive or negative integer or decimal number
        # date - A specific date, stored as a millisecond Unix timestamp. See this page for more details about date and datetime properties
        # datetime - A specific time, stored as a millisecond Unix timestamp. See this page for more details about date and datetime properties
        # enumeration - one of a set of values that are set in the 'options' field. If this type is used, at least one option must be included.
    "fieldType": "number",
        # booleancheckbox
        # calculation_equation
        # calculation_read_time
        # calculation_score
        # checkbox
        # date
        # number
        # phonenumber
        # radio
        # select
        # text
        # textarea
    "formField": False,
    "displayOrder": 6,
    "options": [],
    "ownerId": "47656206",
}
