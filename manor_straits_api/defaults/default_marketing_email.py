﻿default_marketing_email = {
    "ab": False,
    # Boolean; Whether or not the page is part of an AB test.
    "abHoursToWait": 4,
    # integer; on AB emails, if test results are inconclusive after 4 hours, variation A will be sent.
    "abVariation": False,
    # Boolean; Whether or not the page is the variation (as opposed to the master) if the page is part of an AB test
    "abSampleSizeDefault": "MASTER",
    # String; ["MASTER", "VARIANT"] if there are less than 1000 recipients, only one variation will be sent.
    "abSamplingDefault": "MASTER", 
    # String; ["MASTER", "VARIANT"] if AB test results are inconclusive in the test group, choose a variation to send (resp. A or B) to the remaining contacts.
    "abStatus": "MASTER",
    # String; ["MASTER", "VARIANT"] determines if the current email is variation A or variation B.
    "abSuccessMetric": "CLICKS_BY_OPENS",
    # String; [ CLICKS_BY_OPENS, CLICKS_BY_DELIVERED, OPENS_BY_DELIVERED ] metric that will be used to determine the winning variation.
    "abTestId": "1234567890",
    # String; id shared between variation A and B
    "abTestPercentage": 40,
    # Integer; the size of the test group (40% will receive variation A, the other 60% variation B).
    "abVariation": False,
    # Boolean; Determines whether the email is a variant or not.
    "absoluteUrl":'http://7691721.hs-sites.com/-temporary-slug-0ec6743f-48f7-44b4-9ef0-99d80f6b5332',
    # String; The URL of the web version of the email.
    "allEmailCampaignIds": [88878340, 88879396],
    # A list of email IDs that are associated with the email. 
    "analyticsPageId": "30028434909",
    # String; the id used to access the analytics page of the email(in most cases, the same as the email ID)
    "analyticsPageType": "email",
    # String["blog-post", "email", "knowledge-article", "site-page", "landing-page"] always "email" for an email
    "archived": False,
    # Boolean; determines whether the email is archived or not.
    "author": "timothy@idstst.com",
    # String; the email of the user who made the last update to the email. 
    #"authorAt": 1516383316944,
    # Integer; timestamp of the last update to the email in milliseconds.
    #"authorEmail": "timothy@idstst.com",
    # String; the email of the user who made the last update to the email.
    "authorName": "Tim Gilbert",
    # String; the name of the user who made the last update to the email.
    #"authorUserId": 1234567,
    # Integer; id of the user who made the last update to the email.
    "blogEmailType": "daily",
    # String; ["instant", "daily", "weekly", "monthly"] the cadence for how often blog emails should be sent.
    "blogRssSettings": None,
    # { blogLayout: [0=summary, 1=summary with featured image, 2=full post} ] additional settings for blog subscription emails.
    "campaign": "e254bc38-25ca-4782-9f8e-d7bbeed33027",
    # String; the ID of an email's marketing campaign.
    "campaignName": "Test Campaign 01",
    # String; the name of the email's marketing campaign.
    "canSpamSettingsId": 29234950837,
    # Integer; ID used to retrieve the company address, shown in the footer.
    "categoryId": 2,
    # Integer; the category ID value, which is always 2 for emails (read only).
    #"clonedFrom": 1234567890,
    # Integer; if the email was cloned, ID of the email it was cloned from.
    "contentTypeCategory": 2,
    # Integer; the category ID value, which is always 2 for emails (read only).
    "createPage": False,
    # Boolean; enables a web version of the email when set to True. 
    #"created": 1514293429917,
    # Integer; the timestamp of the email's creation, in milliseconds. 
    "currentlyPublished": False,
    # Boolean; determines the publish status of the email. 
    "domain": "",
    # String; the domain of the web version of the email. Defaults to the primary domain. 
    "emailBody": "Example email body",
    'emailBody': '{% content_attribute "email_body" %}{{ default_email_body }}{% end_content_attribute %}',
    # String; the main content of the email within the 'main email body' module.
    "emailNote": "Example email note",
    # String; optional email notes, included in the details page of the email. 
    "emailTemplateMode": "DESIGN_MANAGER",
    "emailType": "AUTOMATED_EMAIL",
    # String;[ BATCH_EMAIL, AB_EMAIL, AUTOMATED_EMAIL, BLOG_EMAIL, BLOG_EMAIL_CHILD, FOLLOWUP_EMAIL, LOCALTIME_EMAIL, OPTIN_EMAIL, OPTIN_FOLLOWUP_EMAIL, RESUBSCRIBE_EMAIL,
    # RSS_EMAIL, RSS_EMAIL_CHILD, SINGLE_SEND_API, SMTP_TOKEN, LEADFLOW_EMAIL, FEEDBACK_CES_EMAIL, FEEDBACK_NPS_EMAIL, FEEDBACK_CUSTOM_EMAIL, TICKET_EMAIL ]
    "feedbackEmailCategory": None,
    # String; [ "NPS, "CES", "CUSTOM" ] If the email is a feedback email, determines type of feedback email.
    "feedbackSurveyId": None,
    # Integer; the id of the feedback survey that is linked to the email.
    'flexAreas': {'main': {'boxFirstElementIndex': 0,
    'boxLastElementIndex': 0,
    'boxed': False,
    'isSingleColumnFullWidth': False,
    'sections': [{'columns': [{'id': 'builtin_column_0-0',
        'widgets': ['module_15911926642472', 'builtin_module_0_0_0'],
        'width': 12}],
     'id': 'builtin_section-0',
     'style': {'backgroundColor': '#EAF0F6',
        'backgroundType': 'CONTENT',
        'paddingBottom': '10px',
        'paddingTop': '10px'}},
    {'columns': [{'id': 'builtin_column_2-0',
        'widgets': ['builtin_module_2_0_0', 'module_153970326731935'],
        'width': 12}],
     'id': 'builtin_section-2',
     'style': {'backgroundType': 'CONTENT',
        'paddingBottom': '0px',
        'paddingTop': '0px'}},
    {'columns': [{'id': 'builtin_column_4-0',
        'widgets': ['module_153978175731681'],
        'width': 12}],
     'id': 'builtin_section-4',
     'style': {'backgroundType': 'CONTENT',
        'paddingBottom': '0px',
        'paddingTop': '0px'}},
    {'columns': [{'id': 'builtin_column_5-0',
        'widgets': ['builtin_module_5_0_0'],
        'width': 12}],
     'id': 'builtin_section-5',
     'style': {'backgroundType': 'CONTENT',
        'paddingBottom': '20px',
        'paddingTop': '20px'}},
    {'columns': [{'id': 'builtin_column_6-0',
        'widgets': ['builtin_module_6_0_0'],
        'width': 12}],
     'id': 'builtin_section-6',
     'style': {'backgroundColor': '',
        'backgroundType': 'CONTENT',
        'paddingBottom': '0px',
        'paddingTop': '30px'}}]}},
    # Describes the layout of the drag and drop email template.
    #"folderId": 1234567890,
    # Integer; if the email is in a folder, id of that folder.
    #"freezeDate": 1516383316944,
    # Integer; The publish date or updated date if the email is not published.
    "fromName": "Manor Straits",
    # String; the sender name recipients will see (linked to the replyTo address).
    "htmlTitle": "",
    # String; the page title of the web version of the email.
    #"id": 1234567890,
    # Integer; the id of the email.
    "isGraymailSuppressionEnabled": False,
    # Boolean; if True, the email will not send to unengaged contacts. 
    "isLocalTimezoneSend": False,
    # Boolean; if True, the email will adjust its send time relative to the recipients timezone. 
    "isPublished": False,
    #Boolean; if True, the email is in a published state. 
    "isRecipientFatigueSuppressionEnabled": None,
    # Boolean; if True, enables a send frequency cap (a feature available to enterprise accounts).
    "leadFlowId": None,
    # Integer; the id of the parent leadflow if the email is a leadflow email. 
    "liveDomain": "your-website.hs-sites.com",
    # String; domain actually used in the web version (read only)
    "mailingListsExcluded": [],
    # A list of all contact lists to exclude from the email send. 
    "mailingListsIncluded": [],
    #A list of all contact lists included in the email send. 
    "maxRssEntries": 5,
    # in blog and recurring emails, the max number of entries to include. 
    "metaDescription": "",
    # String; meta description of the web version of the email, to drive search engine traffic to your page
    "name": "Email created from API",
    # String; the name of the email, as displayed on the email dashboard. 
    #"pageExpiryDate": 1548464400000,
    # Integer; the expiration date of the web version of an email, in milliseconds.
    "pageExpiryRedirectId": 0,
    # String; the url of the page the user will be redirected to after the web version of the email expires.
    "pageRedirected": False,
    # Boolean; indicates if the email's web version has already been set to redirect
    "portalId": 7691721,
    # Integer; the id of the parent portal. 
    "previewKey": "hSCgPqgH",
    # String; the preview key used to generate the preview url before the email is published
    "processingStatus": "",
    # String; [ UNDEFINED, PUBLISHED, PUBLISHED_OR_SCHEDULED, SCHEDULED, PROCESSING,
    # PRE_PROCESSING, ERROR, CANCELED_FORCIBLY, CANCELED_ABUSE ]
    # the email's processing status.
    #"publishDate": 1514293429917,
    # Integer; the timestamp in milliseconds that the email has been published at, or scheduled to send at. 
    #"publishedAt": 1514293429917,
    # Integer; if the email has been published, the time when the publish button has been pressed.
    #"publishedById": 1234567,
    # Integer; if the email has been published, email of the user that pressed the publish button (read only).
    #"publishedByName": "Test Testerson",
    # String; if the email has been published, name of the user that pressed the publish button (read only).
    #"publishImmediately" : True,
    # Boolean; True if the email is not scheduled but will send at publish time.
    #"publishedUrl": "",
    # String; absoluteUrl, only if the email is currentlyPublished (read-only),
    "replyTo": "tim@findwatt.com",
    # String; The email address the recipient will see and reply to (linked to fromName).
    "resolvedDomain": "findwatt.com",
    # String; the domain used in the web version: either the primary one or the one set in the domain field (read only)
    "rssEmailAuthorLineTemplate": "By , ",
    # String; text shown before the "author_line" tag in blog & RSS email's items.
    "rssEmailBlogImageMaxWidth" : 300,
    # Integer; the max width for blog post images in RSS emails. 
    "rssEmailByText": "By",
    # String; if rssEmailAuthorLineTemplate is not set, word before the author name in blog & RSS email's items.
    "rssEmailClickThroughText": "Read more",
    # String; text shown on the link to see the full post in blog & RSS email's items.
    "rssEmailCommentText": "Comment;",
    # String; text shown on the link to comment the post in blog & RSS email's items.
    "rssEmailEntryTemplate": "",
    # String; optional, custom template for every RSS entry.
    "rssEmailEntryTemplateEnabled": False,
    # Boolean; determines if the Entry Template is used for an RSS email. 
    "rssEmailUrl": "",
    # String; URL used for social sharing. 
    "rssToEmailTiming": {
     "repeats": "instant", # [ "instant", "daily", "weekly", "monthly" ]
     "repeats_on_monthly": 1, # what day of the month should the monthly email be sent [1-31]
     "repeats_on_weekly": 1, # what day of the week should the weekly email be sent [1=monday - 7=sunday]
     "summary": "Once", # description, not used
     "time": "9:00 am" # time the email should be sent at
    },
    # A dictionary that determines what time the RSS email should be sent out. 
    "slug": "web-version",
    # String; path of the web version URL. 
    "smartEmailFields": None,
    # String; lists the smart objects in email fields (from address, subject..)
    "styleSettings": None,
    # String; Custom email style settings (background color, primary font);
    "subcategory": "automated",
    # [ ab_master, ab_variant, automated, automated_for_deal, automated_for_form, automated_for_form_legacy, automated_for_form_buffer, automated_for_form_draft,
    # rss_to_email, rss_to_email_child, blog_email, blog_email_child, optin_email, optin_followup_email, batch, resubscribe_email, single_send_api, smtp_token,
    # localtime, automated_for_ticket, automated_for_leadflow, automated_for_feedback_ces, automated_for_feedback_nps, automated_for_feedback_custom ]
    "subject": "New Posts today for the blog Default HubSpot Blog",
    # String; the subject of the email. 
    "subscription": 9525265,
    # Integer; the id of the email's subscription type.
    #"subscriptionBlogId": 1234567890,
    # Integer; for blog emails, id of the linked blog.
    "subscription_name": "Test Subscription 01",
    # String; the name of the email's subscription type.
    "templatePath": "@hubspot/email/dnd/promotion.html",
    # String; the path of the email's body template within the design manager. 
    "transactional": False,
    # Boolean; determines whether the email is a transactional email or not. 
    "unpublishedAt": 0,
    # Integer; the timestamp in milliseconds of when the email was unpublished.
    "updated": 0,
    # Integer; timestamp of the last update in milliseconds.
    "updatedById": 0,
    # Integer; the ID of the last user who updated the email. 
    "url": "http:#your-website.hs-sites.com/web-version",
    # String; the web version URL (read-only).
    "useRssHeadlineAsSubject": False,
    # Boolean; Setting for RSS emails, uses the latest RSS entry as the email subject.
    "vidsExcluded": [],
    # A list of contact IDs to exclude from being sent the email. 
    "vidsIncluded": [],
    # A list of contacts IDs to include in the email send. 
    "widgets": { # The content of layout sections of the email (widgets).
        'builtin_module_0_0_0':{
            'body': {
                'alignment': 'center',
                'hs_enable_module_padding': True,
                'img': {
                    'alt': 'MS_large',
                    'height': 256,
                    'src': 'https://cdn2.hubspot.net/hubfs/7691721/MS_large.png',
                    'width': 256},
                'module_id': 1367093,
                'path': '@hubspot/image_email'},
           'child_css': {},
           'css': {},
           'id': 'builtin_module_0_0_0',
           'module_id': 1367093,
           'name': 'builtin_module_0_0_0',
           'order': 1,
           'type': 'module'},
        'builtin_module_2_0_0': {
            'body': {
                'html': "<h1>Is your property an untapped gold mine?</h1>\n<p>You might be sitting on valuable real estate that isn't earning you the money it should. Based on an analysis of the social media trends in your neighborhood, this is near peak market for property values.</p>",
                'i18nKey': 'richText.promotion.primary',
                'module_id': 1155639,
                'path': '@hubspot/rich_text'},
            'child_css': {},
            'css': {},
            'id': 'builtin_module_2_0_0',
            'module_id': 1155639,
            'name': 'builtin_module_2_0_0',
            'order': 4,
            'type': 'module'},
      'builtin_module_5_0_0': {
        'body': {
            'html': '<p style="text-align: center; margin-bottom: 10px;">Have questions? Either respond to this email or contact the sender on <a href="mailto:youremail@example.com">timothy@idsts.com</a></p>',
            'i18nKey': 'richText.promotion.footer',
            'module_id': 1155639,
            'path': '@hubspot/rich_text'},
            'child_css': {},
            'css': {},
            'id': 'builtin_module_5_0_0',
            'module_id': 1155639,
            'name': 'builtin_module_5_0_0',
            'order': 8,
            'type': 'module'},
      'builtin_module_6_0_0': {
        'body': {
            'align': 'center',
            'font': {
                'color': '#23496d',
                'font': 'Arial, sans-serif',
                'size': {'units': 'px', 'value': 12}},
            'link_font': {
                'color': '#00a4bd',
                'font': 'Helvetica,Arial,sans-serif',
                'size': {'units': 'px', 'value': 12},
                'styles': {'bold': False, 'italic': False, 'underline': True}},
            'module_id': 2869621,
            'path': '@hubspot/email_footer',
            'unsubscribe_link_type': 'both'},
            'child_css': {},
            'css': {},
            'id': 'builtin_module_6_0_0',
            'module_id': 2869621,
            'name': 'builtin_module_6_0_0',
            'order': 9,
            'type': 'module'},
        'module_153970326731935': {
            'body': {
                'alignment': 'center',
                'background_color': '#00a4bd',
                'corner_radius': 8,
                'destination': 'https://manorstraits.com/',
                'font_style': {
                    'color': '#ffffff',
                     'font': 'Arial, sans-serif',
                     'size': {'units': 'px', 'value': 16},
                     'styles': {'bold': False, 'italic': False, 'underline': False}},
                'make_full_width': True,
                'module_id': 1976948,
                'path': '@hubspot/button_email',
                'text': 'Explore your real estate options'},
                'child_css': {},
                'css': {},
                'id': 'module_153970326731935',
                'module_id': 1976948,
                'name': 'module_153970326731935',
                'order': 5,
                'type': 'module'},
        'module_153978175731681': {
            'body': {
                'alignment': 'center',
                'color': {'color': '#99acc2', 'opacity': 100},
                'css': {},
                'height': 1,
                'hs_enable_module_padding': False,
                'line_type': 'solid',
                'module_id': 2191110,
                'path': '@hubspot/email_divider',
                'width': 100},
                'child_css': {},
                'css': {},
                'id': 'module_153978175731681',
                'module_id': 2191110,
                'name': 'module_153978175731681',
                'order': 6,
                'type': 'module'},
        'module_15911926642472': {
            'body': {
                'hs_enable_module_padding': True,
                'hs_wrapper_css': {
                    'padding-bottom': '10px',
                    'padding-left': '20px',
                    'padding-right': '20px',
                    'padding-top': '10px'},
                'html': '<h2>An invitation from your friends at Manor Straits</h2>'},
                'child_css': {},
                'css': {},
                'id': 'module_15911926642472',
                'module_id': 1155639,
                'name': 'module_15911926642472',
                'type': 'module'},
        'preview_text': {
            'body': {'value': ''},
            'child_css': {},
            'css': {},
            'id': 'preview_text',
            'label': 'Preview Text <span class=help-text>This will be used as the preview text that displays in some email clients</span>',
            'name': 'preview_text',
            'order': 0,
            'type': 'text'}
    }
}



default_marketing_email_minimal = {
    "allEmailCampaignIds": [], # A list of email IDs that are associated with the email. 
    "archived": False, # Boolean; determines whether the email is archived or not.
    "author": "tim@findwatt.com",  # String; the email of the user who made the last update to the email. 
    #"authorEmail": "timothy@idstst.com",  # String; the email of the user who made the last update to the email.
    "authorName": "Tim Gilbert",  # String; the name of the user who made the last update to the email.
    #"authorUserId": 1234567,  # Integer; id of the user who made the last update to the email.
    "campaign": "e254bc38-25ca-4782-9f8e-d7bbeed33027",  # String; the ID of an email's marketing campaign.
    "campaignName": "Test Campaign 01",
    # String; the name of the email's marketing campaign.
    "canSpamSettingsId": 29234950837,  # Integer; ID used to retrieve the company address, shown in the footer.
    "categoryId": 2,  # Integer; the category ID value, which is always 2 for emails (read only).
    "contentTypeCategory": 2,  # Integer; the category ID value, which is always 2 for emails (read only).
    "createPage": False,  # Boolean; enables a web version of the email when set to True. 
    "currentlyPublished": False,  # Boolean; determines the publish status of the email. 
    'emailBody': '{% content_attribute "email_body" %}{{ default_email_body }}{% end_content_attribute %}',  # String; the main content of the email within the 'main email body' module.
    "emailNote": "Example email note",  # String; optional email notes, included in the details page of the email. 
    "emailTemplateMode": "DESIGN_MANAGER",
    "emailType": "AUTOMATED_EMAIL",
    # String;[ BATCH_EMAIL, AB_EMAIL, AUTOMATED_EMAIL, BLOG_EMAIL, BLOG_EMAIL_CHILD, FOLLOWUP_EMAIL, LOCALTIME_EMAIL, OPTIN_EMAIL, OPTIN_FOLLOWUP_EMAIL, RESUBSCRIBE_EMAIL,
    # RSS_EMAIL, RSS_EMAIL_CHILD, SINGLE_SEND_API, SMTP_TOKEN, LEADFLOW_EMAIL, FEEDBACK_CES_EMAIL, FEEDBACK_NPS_EMAIL, FEEDBACK_CUSTOM_EMAIL, TICKET_EMAIL ]
    "feedbackEmailCategory": None,  # String; [ "NPS, "CES", "CUSTOM" ] If the email is a feedback email, determines type of feedback email.
    'flexAreas': {'main': {'boxFirstElementIndex': 0,
        'boxLastElementIndex': 0,
        'boxed': False,
        'isSingleColumnFullWidth': False,
        'sections': [{'columns': [{'id': 'builtin_column_0-0',
            'widgets': ['module_15911926642472', 'builtin_module_0_0_0'],
            'width': 12}],
         'id': 'builtin_section-0',
         'style': {'backgroundColor': '#EAF0F6',
            'backgroundType': 'CONTENT',
            'paddingBottom': '10px',
            'paddingTop': '10px'}},
        {'columns': [{'id': 'builtin_column_2-0',
            'widgets': ['builtin_module_2_0_0', 'module_153970326731935'],
            'width': 12}],
         'id': 'builtin_section-2',
         'style': {'backgroundType': 'CONTENT',
            'paddingBottom': '0px',
            'paddingTop': '0px'}},
        {'columns': [{'id': 'builtin_column_4-0',
            'widgets': ['module_153978175731681'],
            'width': 12}],
         'id': 'builtin_section-4',
         'style': {'backgroundType': 'CONTENT',
            'paddingBottom': '0px',
            'paddingTop': '0px'}},
        {'columns': [{'id': 'builtin_column_5-0',
            'widgets': ['builtin_module_5_0_0'],
            'width': 12}],
         'id': 'builtin_section-5',
         'style': {'backgroundType': 'CONTENT',
            'paddingBottom': '20px',
            'paddingTop': '20px'}},
        {'columns': [{'id': 'builtin_column_6-0',
            'widgets': ['builtin_module_6_0_0'],
            'width': 12}],
         'id': 'builtin_section-6',
         'style': {'backgroundColor': '',
            'backgroundType': 'CONTENT',
            'paddingBottom': '0px',
            'paddingTop': '30px'}}]}
    }, # Describes the layout of the drag and drop email template.
    "fromName": "Manor Straits",  # String; the sender name recipients will see (linked to the replyTo address).
    
    "isLocalTimezoneSend": True,  # Boolean; if True, the email will adjust its send time relative to the recipients timezone. 
    "isPublished": False,  #Boolean; if True, the email is in a published state. 
    #"mailingListsExcluded": [],  # A list of all contact lists to exclude from the email send. 
    #"mailingListsIncluded": [],  #A list of all contact lists included in the email send. 
    "name": "Email created from API",  # String; the name of the email, as displayed on the email dashboard. 
    "portalId": 7691721,  # Integer; the id of the parent portal. 
    #"previewKey": "hSCgPqgH",
    # String; the preview key used to generate the preview url before the email is published
    "processingStatus": "UNDEFINED",  # the email's processing status.
    # String; [ UNDEFINED, PUBLISHED, PUBLISHED_OR_SCHEDULED, SCHEDULED, PROCESSING,
    # PRE_PROCESSING, ERROR, CANCELED_FORCIBLY, CANCELED_ABUSE ]
  
    "publishImmediately" : True,  # Boolean; True if the email is not scheduled but will send at publish time.
    "replyTo": "tim@findwatt.com",  # String; The email address the recipient will see and reply to (linked to fromName).
    "resolvedDomain": "findwatt.com",  # String; the domain used in the web version: either the primary one or the one set in the domain field (read only)
    "slug": "web-version",  # String; path of the web version URL. 
    "smartEmailFields": None,  # String; lists the smart objects in email fields (from address, subject..)
    "styleSettings": None,  # String; Custom email style settings (background color, primary font);
    "subcategory": "automated",
    # [ ab_master, ab_variant, automated, automated_for_deal, automated_for_form, automated_for_form_legacy, automated_for_form_buffer, automated_for_form_draft,
    # rss_to_email, rss_to_email_child, blog_email, blog_email_child, optin_email, optin_followup_email, batch, resubscribe_email, single_send_api, smtp_token,
    # localtime, automated_for_ticket, automated_for_leadflow, automated_for_feedback_ces, automated_for_feedback_nps, automated_for_feedback_custom ]
    "subject": "New Posts today for the blog Default HubSpot Blog",
    # String; the subject of the email. 
    "subscription": 9525265,
    # Integer; the id of the email's subscription type.
    #"subscriptionBlogId": 1234567890,
    # Integer; for blog emails, id of the linked blog.
    "subscription_name": "Test Subscription 01",
    # String; the name of the email's subscription type.
    "templatePath": "@hubspot/email/dnd/promotion.html",
    # String; the path of the email's body template within the design manager. 
    "transactional": False, # Boolean; determines whether the email is a transactional email or not. 
    
    #"vidsExcluded": [],  # A list of contact IDs to exclude from being sent the email. 
    #"vidsIncluded": [],  # A list of contacts IDs to include in the email send. 
    "widgets": { # The content of layout sections of the email (widgets).
        'builtin_module_0_0_0':{
            'body': {
                'alignment': 'center',
                'hs_enable_module_padding': True,
                'img': {
                    'alt': 'MS_large',
                    'height': 256,
                    'src': 'https://cdn2.hubspot.net/hubfs/7691721/MS_large.png',
                    'width': 256},
                'module_id': 1367093,
                'path': '@hubspot/image_email'},
           'child_css': {},
           'css': {},
           'id': 'builtin_module_0_0_0',
           'module_id': 1367093,
           'name': 'builtin_module_0_0_0',
           'order': 1,
           'type': 'module'},
        'builtin_module_2_0_0': {
            'body': {
                'html': "<h1>Is your property an untapped gold mine?</h1>\n<p>You might be sitting on valuable real estate that isn't earning you the money it should. Based on an analysis of the social media trends in your neighborhood, this is near peak market for property values.</p>",
                'i18nKey': 'richText.promotion.primary',
                'module_id': 1155639,
                'path': '@hubspot/rich_text'},
            'child_css': {},
            'css': {},
            'id': 'builtin_module_2_0_0',
            'module_id': 1155639,
            'name': 'builtin_module_2_0_0',
            'order': 4,
            'type': 'module'},
      'builtin_module_5_0_0': {
        'body': {
            'html': '<p style="text-align: center; margin-bottom: 10px;">Have questions? Either respond to this email or contact the sender on <a href="mailto:youremail@example.com">timothy@idsts.com</a></p>',
            'i18nKey': 'richText.promotion.footer',
            'module_id': 1155639,
            'path': '@hubspot/rich_text'},
            'child_css': {},
            'css': {},
            'id': 'builtin_module_5_0_0',
            'module_id': 1155639,
            'name': 'builtin_module_5_0_0',
            'order': 8,
            'type': 'module'},
      'builtin_module_6_0_0': {
        'body': {
            'align': 'center',
            'font': {
                'color': '#23496d',
                'font': 'Arial, sans-serif',
                'size': {'units': 'px', 'value': 12}},
            'link_font': {
                'color': '#00a4bd',
                'font': 'Helvetica,Arial,sans-serif',
                'size': {'units': 'px', 'value': 12},
                'styles': {'bold': False, 'italic': False, 'underline': True}},
            'module_id': 2869621,
            'path': '@hubspot/email_footer',
            'unsubscribe_link_type': 'both'},
            'child_css': {},
            'css': {},
            'id': 'builtin_module_6_0_0',
            'module_id': 2869621,
            'name': 'builtin_module_6_0_0',
            'order': 9,
            'type': 'module'},
        'module_153970326731935': {
            'body': {
                'alignment': 'center',
                'background_color': '#00a4bd',
                'corner_radius': 8,
                'destination': 'https://manorstraits.com/',
                'font_style': {
                    'color': '#ffffff',
                     'font': 'Arial, sans-serif',
                     'size': {'units': 'px', 'value': 16},
                     'styles': {'bold': False, 'italic': False, 'underline': False}},
                'make_full_width': True,
                'module_id': 1976948,
                'path': '@hubspot/button_email',
                'text': 'Explore your real estate options'},
                'child_css': {},
                'css': {},
                'id': 'module_153970326731935',
                'module_id': 1976948,
                'name': 'module_153970326731935',
                'order': 5,
                'type': 'module'},
        'module_153978175731681': {
            'body': {
                'alignment': 'center',
                'color': {'color': '#99acc2', 'opacity': 100},
                'css': {},
                'height': 1,
                'hs_enable_module_padding': False,
                'line_type': 'solid',
                'module_id': 2191110,
                'path': '@hubspot/email_divider',
                'width': 100},
                'child_css': {},
                'css': {},
                'id': 'module_153978175731681',
                'module_id': 2191110,
                'name': 'module_153978175731681',
                'order': 6,
                'type': 'module'},
        'module_15911926642472': {
            'body': {
                'hs_enable_module_padding': True,
                'hs_wrapper_css': {
                    'padding-bottom': '10px',
                    'padding-left': '20px',
                    'padding-right': '20px',
                    'padding-top': '10px'},
                'html': '<h2>An invitation from your friends at Manor Straits</h2>'},
                'child_css': {},
                'css': {},
                'id': 'module_15911926642472',
                'module_id': 1155639,
                'name': 'module_15911926642472',
                'type': 'module'},
        'preview_text': {
            'body': {'value': ''},
            'child_css': {},
            'css': {},
            'id': 'preview_text',
            'label': 'Preview Text <span class=help-text>This will be used as the preview text that displays in some email clients</span>',
            'name': 'preview_text',
            'order': 0,
            'type': 'text'}
    }
}