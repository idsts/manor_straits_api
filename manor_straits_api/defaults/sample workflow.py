﻿{
    'enabled': True,
    'type': 'DRIP_DELAY',
    'name': 'Test workflow - 01',
    'description': '',
    
    'listening': False,
    'onlyEnrollsManually': False,
    'segmentCriteria': [
        [
            {'filterFamily': 'PropertyValue',
            'withinTimeMode': 'PAST',
            'operator': 'EQ',
            'property': 'number_of_emails',
            'value': 0,
            'type': 'number'}
        ]
    ],
    'goalCriteria': [],
    
    'allowContactToTriggerMultipleTimes': False,
    'onlyExecOnBizDays': False,
    
    'actions': [
        {
            'type': 'DELAY',
            'stepListCount': 0,
            'delayMillis': 0,
            'actionId': 4,
            'stepId': 1
        },
        {
            'type': 'EMAIL',
            'emailContentId': 30028434909,
            'emailCampaignId': 88879396,
            'emailCampaignGroupId': 88878341,
            'actionId': 1,
            'stepId': 1,
            'goalListCount': 0
        },
        {
            'type': 'ADD_SUBTRACT_PROPERTY',
            'valueToModifyPropertyBy': 1,
            'propertyName': 'number_of_emails',
            'actionId': 2,
            'stepId': 1
        },
        {
            'type': 'DELAY',
            'stepListCount': 0,
            'delayMillis': 172800000,
            'actionId': 3,
            'stepId': 2
        },
        {
            'type': 'SET_CONTACT_PROPERTY',
            'newValue': 'IN_PROGRESS',
            'propertyName': 'hs_lead_status',
            'actionId': 5,
            'stepId': 2
        }
    ],
    
    
    
    
    
    'migrationStatus': {
        'portalId': 7691721,
        'flowId': 31113573,
        'workflowId': 13402683,
        'migrationStatus': 'EXECUTION_MIGRATED',
        'platformOwnsActions': True,
        'lastSuccessfulMigrationTimestamp': None},
    
    
    
    'nurtureTimeRange': {'enabled': False, 'startHour': 9, 'stopHour': 10},
    
    'unenrollmentSetting': {'type': 'NONE', 'excludedWorkflows': []},
    
    'creationSource': {
        'sourceApplication': {
            'source': 'WORKFLOWS_APP',
            'serviceName': 'https://app.hubspot.com/workflows/7691721/platform/create/new?flowTypeId=0-1&scrollToElementId=scroll-to-new-action-config-0-1&templateId=1&searchFilter='},
        'createdByUser': {'userId': 6467784, 'userEmail': 'tim@findwatt.com'},
        'createdAt': 1591133147326},
    'updateSource': {
        'sourceApplication': {
            'source': 'WORKFLOWS_APP',
            'serviceName': 'https://app.hubspot.com/workflows/7691721/platform/flow/31113573/review'},
        'updatedByUser': {'userId': 6467784, 'userEmail': 'tim@findwatt.com'},
        'updatedAt': 1591367744647},
        
    'allowEnrollmentFromMerge': True,
    
    
    'recurringSetting': {'type': 'NONE'},
    
    'originalAuthorUserId': 6467784,
    
    'internalStartingListId': 5,
    
    'isSegmentBased': True,
    
    'enrollOnCriteriaUpdate': True,
    
    'reEnrollmentTriggerSets': [],
    
    'triggerSets': [],
    
    'suppressionListIds': [],
    
    'lastUpdatedBy': 'tim@findwatt.com',
    
    'id': 13402683,
    
    'warnings': ['SHOULD_ADD_OPT_OUT_CONDITION'],
    
    'portalId': 7691721,
    'insertedAt': 1591133147411,
    'updatedAt': 1591367744647,
    
    'metaData': {
        'triggeredByWorkflowIds': [],
        'contactListCounts': {
            'enrolledListCount': 1,
            'activeListCount': 0,
            'completedListCount': 1,
            'succeededListCount': 0,
            'startingListCount': 1,
            'startingListDoneProcessing': True
        },
        'succeededListId': 4,
        'contactListIds': {
            'active': 2,
            'completed': 3,
            'succeeded': 4,
            'enrolled': 1
        }
    },
    'validation': {
        'source': 'API',
        'actionIdErrorMap': {},
        'actionIdWarningMap': {},
        'actionIdInfoMap': {},
        'stepIdErrorMap': {},
        'stepIdWarningMap': {},
        'eventAnchorErrors': [],
        'errorsWithoutFrontendCounterpart': [],
        'triggerErrors': {},
        'branchErrors': {},
        'branchWarnings': {},
        'branchInfo': {},
        'triggerSetWarnings': {},
        'miscellaneousWarnings': ['SHOULD_ADD_OPT_OUT_CONDITION'],
        'fatalErrors': [],
        'enrollmentWarnings': [],
        'enrollmentErrors': [],
        'miscellaneousErrors': []
    }
}