﻿import pyximport; pyximport.install()

import os, sys, re, json, shutil
import binascii, requests
from datetime import datetime, timedelta
from collections import defaultdict
from functools import wraps
import logging
import pandas as pd
from flask import Flask, request, send_from_directory, send_file, redirect, flash, url_for
import flask_restful
from flask_restful import Api, reqparse
from flask_cors import CORS, cross_origin
from flask_restful.utils import cors
from werkzeug.utils import secure_filename


try:
    from . import utils, standardization, db_tables
    from . import mysql_db
    from .find_contact import ContactFinder, sufficient_owner_information
    from . import touch_utils, aws_ses
except:
    import utils, standardization, db_tables
    import mysql_db
    from find_contact import ContactFinder, sufficient_owner_information
    import touch_utils, aws_ses

try:
    from .hubspot_api import HubspotAPI, sufficient_for_new_hubspot_contact
    from .sakari_api import SakariAPI
    from .handwrite_api import HandwriteAPI
except:
    from hubspot_api import HubspotAPI, sufficient_for_new_hubspot_contact
    from sakari_api import SakariAPI
    from handwrite_api import HandwriteAPI


e_API_KEYS = {
    "tim": 'f14f-3db6-49e4-2d69-9bf5-cd82-ac20-9c1c',
    "arkadiusz": 'd309-4993-30ff-9fa9-745d-1204-df90-bc97',
    "ralph": '16a8-0d47-77a0-ae0b-0f36-7675-6832-c0ed',
}
s_USER = "unknown"

o_AWS_DB = None

o_HUBSPOT = None
o_SAKARI = None
o_HANDWRITE = None

o_CONTACT_FINDER = None


s_DB = db_tables.s_DB  # "SandboxKE"
s_FIELD_MAPS_TABLE = db_tables.s_FIELD_MAPS_TABLE  # "field_maps"
s_OWNERS_TABLE = db_tables.s_OWNERS_TABLE  # "owners"
s_OWNER_MAPS_TABLE =  db_tables.s_OWNER_MAPS_TABLE  # "owner_maps"
s_TOUCH_SUMMARY_TABLE =  db_tables.s_TOUCH_SUMMARY_TABLE  # "hubspot_touch_summary"
s_TOUCH_TABLE =  db_tables.s_TOUCH_TABLE  # "hubspot_touches"
s_PROPERTIES_TABLE = db_tables.s_PROPERTIES_TABLE  # "properties"
s_PROCESS_TABLE = db_tables.s_PROCESS_TABLE  # "python_scripts"

s_DB_SOURCES = db_tables.s_DB_SOURCES  # "turnover"
e_SOURCE_TABLES = db_tables.e_SOURCE_TABLES  # {
#    "ProspectNow": "ProspectNow",
#    "Infutor": "Infutor",
#    "RealtyMole": "RealtyMole",
#}

AWS_DB_USER = os.environ.get("IDSTS_AWS_DB_USER", "")
AWS_DB_PASSWORD = os.environ.get("IDSTS_AWS_DB_PASS", "")

# resources that don't need a valid API key to access
c_OPEN_RESOURCES = {"EndPoints.get", "DevWebhook.post", "DevWebhook.put", "EmailNotification.post"}

re_EMAIL = re.compile(r"[^<>@]+< *(?P<email>.+@([^.]+\.)+.+) *>")

e_DEFAULT_HEADER = {'Access-Control-Allow-Origin': '*'}

b_REQUIRE_MINIMUM_CONTACT_INFO = True

a_IMPORTANT_WORKFLOW_PROPERTIES = ["id", "type", "name", "description", "segmentCriteria", "enabled", "suppressionListIds"]
a_WORKFLOWS, dt_WORKFLOWS = None, datetime.now()
td_WORKFLOW_CACHE = timedelta(days=0, hours=2, minutes=0)  # how long to cache workflow details

t_CORS_OPTIONS_RESPONSE = (
    {'Allow' : 'PUT,GET,POST,OPTIONS,DELETE'},
    200, 
    {
        'Access-Control-Allow-Origin': "*",
        'Access-Control-Allow-Methods' : 'PUT,GET,POST,OPTIONS,DELETE',
        'Access-Control-Allow-Headers': "*",
    })

s_MEDIA_PATH = os.path.join(os.getcwd(), "media")
ALLOWED_EXTENSIONS = {'.txt', '.pdf', '.png', '.jpg', '.jpeg', '.gif'}


# when a contact is enrolled in a workflow, set their contact properties to these values first to meet their criteria
e_WORKFLOW_CRITERIA = {
    # Drip Order 1 (Email, SMS)
    "15931997": {"drip_campaign_order": 11},
    # Drip Order 2 (SMS, Email)
    "15963713": {"drip_campaign_order": 12},
    # Drip Order 3 (Email, SMS, Postcard)
    "15964815": {"drip_campaign_order": 13},
    # Drip Order 4 (SMS, Email, Call)
    "15965100": {"drip_campaign_order": 14},
    # Drip Order 5 (Email only)
    "15965650": {"drip_campaign_order": 15},
    # Drip Order 6 (SMS, Call)
    "16002601": {"drip_campaign_order": 16},
}

def _load_db(s_reason="record engagements in real time"):
    global o_AWS_DB
    try:
        if o_AWS_DB is None:
            o_AWS_DB = mysql_db.mySqlDbConn(
                s_host = mysql_db._host, i_port = mysql_db._port,
                s_user = AWS_DB_USER, s_pass = AWS_DB_PASSWORD,
                s_database = s_DB, chunk_size = 50000,
            )
    except Exception as e:
        logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load AWS DB in order to {s_reason}: {e}")

def generate_key():
    s_key = binascii.hexlify(os.urandom(16)).decode()
    a_pieces = []
    for i_start in range(0, len(s_key), 4):
        a_pieces.append(s_key[i_start: i_start + 4])
    return "-".join(a_pieces)


def check_log_file(s_log_file, i_max_size=32767, i_days_old=-1, b_monthly=False, b_force=False, **kwargs):
    ''' If log file is over a certain size or was created too long ago, copy it to another file and start a new log. '''
    if not os.path.isfile(s_log_file): return
    b_start_new_file = False
    i_size = os.path.getsize(s_log_file)
    
    dt_created = datetime.fromtimestamp(os.path.getctime(s_log_file))
    dt_current = datetime.now()
    i_age = (dt_current - dt_created).days
    
    if (i_max_size > 0 and i_size > i_max_size) or (i_days_old > 0 and i_age > i_days_old) or (b_monthly and dt_current.month != dt_created.month) or b_force:
        s_copy_to = utils.get_new_write_filename(s_log_file, b_dot_format=True)
        shutil.copy2(s_log_file, s_copy_to)
        
        try:
            logging.shutdown()
            os.remove(s_log_file)
        except:
            with open(s_log_file, "w") as f: pass


def verify_contact_exists(e_contact, **kwargs):
    ''' Check if a contact already exists as a master owner record, and whether it is on hubspot or not.
        If it isn't in master owners db yet, add it.
        If it isn't on hubspot yet, add it and map it to owner record.
    '''
    global o_CONTACT_FINDER, o_HUBSPOT
    
    if o_CONTACT_FINDER is None:
        try:
            o_CONTACT_FINDER = ContactFinder()
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Contact Finder in order to verify/add contacts owners: {e}")
            #return {"message": f"Could not load Contact Finder in order to verify/add contacts owners: {e}"}, 500
            return None, None
    
    if o_HUBSPOT is None:
        try:
            o_HUBSPOT = HubspotAPI()
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Contact Finder in order to verify/add contacts owners: {e}")
            #return {"message": f"Could not load Hubspot API in order to verify/add contacts owners: {e}"}, 500
            return None, None
    
    e_owner = standardization.standardize_owner(standardization.to_owner_contact(e_contact))
    
    if kwargs.get("require_sufficient") and not sufficient_owner_information(e_owner):
        return None, None
    
    #a_current_owner_records = o_CONTACT_FINDER.find_contact(e_contact, b_thorough=False, return_records=True, as_dicts=True)
    a_current_owner_ids = o_CONTACT_FINDER.find_contact(e_contact, b_thorough=False, return_records=False)
    
    if not a_current_owner_ids:
        # contact needs to be added to master owners table
        a_current_owner_ids = [o_CONTACT_FINDER._db.insert(s_OWNERS_TABLE, e_owner, return_id=True)]
        
        # so obviously it doesn't already have mappings
        e_mappings = {}
    else:
        # find any mappings of the existing ids to other sources
        e_mappings = defaultdict(list)
        for owner_id in a_current_owner_ids:
            e_m = o_CONTACT_FINDER.get_other_ids(owner_id)
            for source, a_source_ids in e_m.items():
                e_mappings[source].extend(a_source_ids)
        print("e_mappings", e_mappings)
    
    if a_current_owner_ids and not e_mappings.get("Hubspot"):
        # contact needs to be added to Hubspot or mapped to existing hubspot contact
        e_hubspot_contact = standardization.to_hubspot_contact(e_contact)
        
        print("e_hubspot_contact", e_hubspot_contact)
        if e_hubspot_contact.get("vid"):
            print(f"Already has hubspot id: {e_hubspot_contact['vid']}")
            e_mappings["Hubspot"] = [e_hubspot_contact["vid"]]
            o_CONTACT_FINDER.add_new_ids(
                a_current_owner_ids[0],
                [("Hubspot", e_hubspot_contact.get("vid"), owner_id) for owner_id in a_current_owner_ids])
        else:
            # make sure the contact isn't just unmapped
            a_existing_matches = o_HUBSPOT.find_contacts([e_hubspot_contact])
            if kwargs.get("b_debug"): print("a_existing_matches", a_existing_matches)
            if a_existing_matches and a_existing_matches[0]:
                if len(a_existing_matches[0]) == 1:
                    print(f"Found Hubspot contact that matches ({a_existing_matches[0][0]}). Creating mapping to owner_ids {a_current_owner_ids}).")
                    #print("a_existing_matches", a_existing_matches)
                    e_mappings["Hubspot"] = [a_existing_matches[0][0]]
                    o_CONTACT_FINDER.add_new_ids(
                        a_current_owner_ids[0],
                        [("Hubspot", e_mappings["Hubspot"], owner_id) for owner_id in a_current_owner_ids])
                else:
                    if sufficient_for_new_hubspot_contact(e_hubspot_contact):
                        logger.warn(f"{utils.get_time()}\tAmbiguous unmapped owner that matches multiple hubspot contacts using limited info. Trying to adding new hubspot contact.\n\te_owner: {e_owner}\n\te_hubspot_contact: {e_hubspot_contact}\n")
                        
                        print(f"Found multiple Hubspot contacts that match ({a_existing_matches[0]}). Creating new hubspot contact and mapping to owner_ids {a_current_owner_ids}).")
                    
                        e_confirmation = o_HUBSPOT.create_contact(e_hubspot_contact)
                        if kwargs.get("b_debug"): print("hubspot contact creation confirmation:", e_confirmation)
                        if isinstance(e_confirmation, dict) and e_confirmation.get("vid"):
                            #print('e_confirmation["vid"]', e_confirmation["vid"])
                            e_mappings["Hubspot"] = [e_confirmation["vid"]]
                            o_CONTACT_FINDER.add_new_ids(
                                a_current_owner_ids[0],
                                [("Hubspot", e_confirmation["vid"], owner_id) for owner_id in a_current_owner_ids])
                    else:
                        logger.error(f"{utils.get_time()}\tAmbiguous unmapped owner that matches multiple hubspot contacts. Don't add as new contact.\n\te_owner: {e_owner}\n\te_hubspot_contact: {e_hubspot_contact}\n")
                    
            elif not kwargs.get("require_sufficient") or sufficient_for_new_hubspot_contact(e_hubspot_contact):
                print(f"No Hubspot contacts that match. Creating new hubspot contact and mapping to owner_ids {a_current_owner_ids}).")
                    
                e_confirmation = o_HUBSPOT.create_contact(e_hubspot_contact)
                if kwargs.get("b_debug"): print("hubspot contact creation confirmation:", e_confirmation)
                if isinstance(e_confirmation, dict) and e_confirmation.get("vid"):
                    #print('e_confirmation["vid"]', e_confirmation["vid"])
                    e_mappings["Hubspot"] = [e_confirmation["vid"]]
                    o_CONTACT_FINDER.add_new_ids(
                        a_current_owner_ids[0],
                        [("Hubspot", e_confirmation["vid"], owner_id) for owner_id in a_current_owner_ids])
        
    return a_current_owner_ids, e_mappings


def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        #print("func.__name__", func.__name__,  func.__qualname__)
        if not getattr(func, 'authenticated', True) or func.__qualname__ in c_OPEN_RESOURCES or func.__qualname__.endswith("options"):
            return func(*args, **kwargs)
        
        e_header = {
            k.lower().replace("-", "_"): v
            for k, v in request.headers.items()}
        #print('e_header', e_header)
        s_key = e_header.get("api_key", "")
        if not s_key: s_key = e_header.get("api key", "")
        
        if not s_key:
            s_key = kwargs.get("api_key", "")
            #print("kwargs", kwargs)
            
            parser = reqparse.RequestParser()
            parser.add_argument("api_key", required=False)
            req_args = parser.parse_args()
            #print("req_args", req_args)
            if not s_key: s_key = req_args.get("api_key", "")
        
        if s_key:
            s_key = re.sub(r"(^['\" ]+|['\" ]+$)", "", s_key)
            for user, key in e_API_KEYS.items():
                if s_key == key:
                    global s_USER
                    s_USER = user
                    #print(f"API User: {s_USER}")
                    logger.info(f"{utils.get_time()}\tAPI User: {s_USER}")
                    return func(*args, **kwargs)
            print("The endpoint %s requires a valid {'api_key': 'your_api_key_here'} in the header. '%s' is not valid." % (request.path, s_key))
            logger.error("The endpoint %s requires a valid {'api_key': 'your_api_key_here'} in the header. '%s' is not valid." % (request.path, s_key))
            return {"message": "The endpoint %s requires a valid {'api_key': 'your_api_key_here'} in the header. '%s' is not valid." % (request.path, s_key)}, 401
        else:
            print("The endpoint %s requires a valid {'api_key': 'your_api_key_here'} in the header." % (request.path), request.headers)
            logger.error("The endpoint %s requires a valid {'api_key': 'your_api_key_here'} in the header." % (request.path))
            return {"message": "The endpoint %s requires a valid  {'api_key': 'your_api_key_here'} in the header." % (request.path)}, 401
        
        #print("will abort via flask_restful.abort(401)")
        flask_restful.abort(401)
    return wrapper

def log_touch_to_db(o_hubspot_confirmation, **kwargs):
    ''' take a confirmation from hubspot_api.create_engagement() and log it immediately the AWS DB for hubspot touches ''' 
    print("Log touch to DB", type(o_hubspot_confirmation))
    if isinstance(o_hubspot_confirmation, dict):
        _load_db("record engagements in real time")
        
        a_vids = o_hubspot_confirmation.get("associations", {}).get("contactIds", [])
        e_hubspot_contacts = o_HUBSPOT._fetch_contacts_by_id(a_vids)
        e_vid_email, e_email_vid = o_HUBSPOT.get_contact_emails(list(e_hubspot_contacts.values()))
        
        a_converted_touches = touch_utils.engagement_to_touch(o_hubspot_confirmation, e_vid_email)
        #print("a_converted_touches", len(a_converted_touches), a_converted_touches)
        a_touch_rows = [touch_utils.touch_to_db_row(e_row) for e_row in a_converted_touches] if a_converted_touches else []
        #print("a_touch_rows", len(a_touch_rows), a_touch_rows)
        if a_touch_rows and o_AWS_DB:
            o_db_confirm = o_AWS_DB.insert_batch(s_TOUCH_TABLE, touch_utils.a_TOUCH_COLS, a_touch_rows)
            #print('o_db_confirm', o_db_confirm)

class Resource(flask_restful.Resource):
    method_decorators = [  # applies to all inherited resources
        authenticate,
    ]
    decorators = [
    ]


class EndPoints(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, headers=None, **kwargs):
        #logger.info(f"{utils.get_time()}\tEndPoints.get")
        e_end_points = {
            "EndPoints": {
                "url": "/endpoints",
                "method": "GET",
                "description": "To list the available endpoints",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {},
            },
            "Hubspot Users": {
                "url": "/owners/",
                "method": "GET",
                "description": "To list the available hubspot owner ids (these are hubspot users, not property owners.",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "fields": "optional list of comma separate hubspot fields to show for each other. Default is email, firstName, lastName, isActive",
                },
            },
            "SMS": {
                "url": "/sms",
                "method": "POST",
                "description": "To send a text message. Needs a list of one or more 'contacts' and a text 'message'",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "contacts": "List of phone numbers or list of contact jsons. Can include {\"hubspot\": hubspot_vid} in the contact json to use exact matching to a hubspot contact instead of trying to fuzzy match.",
                    "message": "Text of the message. If contacts are jsons, can include wildcard properties like {{ firstName }} if they are present in the contacts",
                }
            },
            "Postcard": {
                "url": "/postcard",
                "method": "POST",
                "description": "To send a post. Needs a list of one or more 'contacts' and a text 'message'",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "contacts": "list of contact jsons including at least firstName, lastName, street1, city, state, zip, and preferably email or phone. Can include {\"hubspot\": hubspot_vid} in the contact json to use exact matching to a hubspot contact instead of trying to fuzzy match.",
                    "from": "json of the sender's contact including at least firstName, lastName, street1, city, state, zip",
                    "message": "Text of the message. Can include wildcard properties like {firstName} if they are present in the contacts",
                    "card_id": "The Handwrite.io ID of the stationery to use. Default is '5dc304cfbc08d20016f1ec2f' if card_id is omitted from request.",
                    "handwriting_id": "The Handwrite.io ID of the handwriting style to use. Default is '5dc30652bc08d20016f1ec33' if card_id is omitted from request.",
                }
            },
            "Log Phone Call": {
                "url": "/call/log",
                "method": "POST",
                "description": "To log details about a phone call. THIS DOES NOT ACTUALLY MAKE A CALL.",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "contact": "The phone number of the person who was called. Can include {\"hubspot\": hubspot_vid} in the contact json to use exact matching to a hubspot contact instead of trying to fuzzy match.",
                    "caller": "The phone number of the person who placed the call",
                    "message": "Notes or transcripts about the phone call.",
                    "status": "Optional status of the phone call",
                    "duration": "Optional length of the call in milliseconds",
                    "recording_url": "An optional http link to a recording of the call",
                }
            },
            "Email": {
                "url": "/email",
                "method": "POST",
                "description": """
To send an email via AWS SES and log it on hubspot.
Can only send from pre-approved email/addresses and domains. Right now there is only 'info@manorstraits.com'.
Contact email addresses that have previously bounced or marked an email from us as spam cannot be re-sent to.
""",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "contacts": "list of emails or list of contact jsons with 'email' values. Can include {\"hubspot\": hubspot_vid} in the contact json to use exact matching to a hubspot contact instead of trying to fuzzy match.",
                    "from": 'contact json of sender like {"email": "email@domain.com", "firstName": "First", "lastName": "Last"}',
                    "cc": "optional list of emails or list of contact jsons with 'email' values",
                    "bcc": "optional list of emails or list of contact jsons with 'email' values",
                    "subject": "the subject of the email",
                    "text": "The plaintext version of the email body",
                    "html": "The html version of the email body."
                }
            },
            "Log Email": {
                "url": "/email/log",
                "method": "POST",
                "description": "To log an already email sent to hubspot. THIS DOES NOT ACTUALLY SEND AN EMAIL.",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "contacts": "list of emails or list of contact jsons with 'email' values",
                    "from": 'contact json of sender like {"email": "email@domain.com", "firstName": "First", "lastName": "Last"}',
                    "cc": "optional list of emails or list of contact jsons with 'email' values",
                    "bcc": "optional list of emails or list of contact jsons with 'email' values",
                    "subject": "the subject of the email",
                    "text": "The plaintext version of the email body",
                    "html": "The html version of the email body."
                }
            },
            "Contact Touch Summary for All Contacts": {
                "url": "/touches/summary",
                "method": "GET",
                "description": "To list the summary of touch information for all contacts.",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "by_hubspot_vid": "Key json dictionary by hubspot_vid instead of owner_id",
                },
            },
            "Contact Touch Summary for One Contact": {
                "url": ("/touches/summary/{owner_id}", "/touches/summary/hubspot/{hubspot_vid}"),
                "method": "GET",
                "description": """
To list the summary of touch information for just one contact by appending the master 'owners' id like '/touches/summary/4375' to the endpoint.
Or use the hubspot_vid like '/touches/summary/hubspot/203401' """,
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {},
            },
            "Contact Touch Details for All Contacts": {
                "url": "/touches/details",
                "method": "GET",
                "description": "To list the individual touches for all contacts.",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "event_type": """Optionally filter down to the types of events to get the details for.
                    Can be included in the request multiple times to include different types.
                    Values can include ["CALL", "EMAIL", "NOTE", "MARKETING EMAIL OPEN", "MARKETING EMAIL CLICK"]. """},
            },
            "Contact Touch Details for One Contact": {
                "url": ("/touches/details/{owner_id}", "/touches/details/hubspot/{hubspot_vid}"),
                "method": "GET",
                "description": """
To list the individual touches for just one contact by appending the master 'owners' id like '/touches/summary/4375' to the endpoint.
Or use the hubspot_vid like '/touches/summary/hubspot/203401' """,
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "event_type": """Optionally filter down to the types of events to get the details for.
                    Can be included in the request multiple times to include different types.
                    Values can include ["CALL", "EMAIL", "NOTE", "MARKETING EMAIL OPEN", "MARKETING EMAIL CLICK"]. """},
            },
            "Get Contact": {
                "url": "/contact/{owner_id}",
                "method": "GET",
                "description": "To get an owner's contact record and any mappings to other sources",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {"owner_id": "The unique 'id' from the 'owners' table on the AWS MYSQL DB."},
            },
            "Get Contact by Source": {
                "url": "/contact/{source}/{other_id}",
                "method": "GET",
                "description": "To get an owner's contact record and any mappings to other sources by looking up its mapped id from another source",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "source": "Recognized data source from a db table like 'ProspectNow' or from a connected API like 'Hubspot'.",
                    "other_id": "The unique id from the specified source that should map to one contact owner.",
                },
            },
            "Find Contact": {
                "url": "/contact/find",
                "method": "POST",
                "description": """
                    To search for an owner's contact record using unique identifiers like 'email' or phone',
                    or a combination of identifiers like 'firstname' + 'lastname' + 'city' + 'state' that should lead to a unique record.
                    If none of those work, then fall back on trying to match all the fields in the contact JSON.
                """,
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {"contact": "A JSON dictionary containing enough information to find a unique owner."},
            },
            "Workflows": {
                "url": "/workflows/",
                "method": "GET",
                "description": "To list the available workflows/campaigns to enroll contacts in.",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "all_fields": "optionally return all detailed hubspot workflow descriptor fields rather than just the important ones",
                    "include_disabled": "include workflows that are not currently enabled in hubspot",
                    "force_refresh": f"re-download workflows from hubspot even if the cached request is newer than {utils.timedelta_to_str(td_WORKFLOW_CACHE)}",
                },
            },
            "List Workflow Enrollments": {
                "url": "/workflows/enroll/{workflow_id}",
                "method": "GET",
                "description": "To list the contacts enrolled in the workflow campaign (by hubspot_vid and email).",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "all_enrolled": "include all enrolled contacts (including ones who previously finished the workflow), not just the contacts who are actively going the workflow currently."
                },
            },
            "List Contact's Workflows": {
                "url": "/workflows/enroll",
                "method": "GET",
                "description": "To list the workflows that the specified contact is enrolled.",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "email": "the email address of the hubspot contact to list the workflows for",
                    "hubspot_vid": "the unique hubspot contact id instead of email",
                },
            },
            "Enroll in Workflow": {
                "url": "/workflows/enroll/{workflow_id}",
                "method": "POST",
                "description": "To enroll an existing hubspot contact in a workflows/campaign.",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "email": "the email address of the hubspot contact to enroll in the workflow",
                    "hubspot_vid": "optionally can specify the unique hubspot contact id instead of email, but the hubspot contact's record must still contain an email.",
                },
            },
            "Un-enroll from Workflow": {
                "url": "/workflows/enroll/{workflow_id}",
                "method": "DELETE",
                "description": "To un-enroll a contact in a workflows/campaign",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "email": "the email address of the hubspot contact to un-enroll in the workflow",
                    "hubspot_vid": "optionally can specify the unique hubspot contact id instead of email, but the hubspot contact's record must still contain an email.",
                },
            },
            "Un-enroll from All Workflows": {
                "url": "/workflows/enroll",
                "method": "DELETE",
                "description": "To un-enroll a contact in a workflows/campaign",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "email": "the email address of the hubspot contact to un-enroll in the workflow",
                    "hubspot_vid": "optionally can specify the unique hubspot contact id instead of email, but the hubspot contact's record must still contain an email.",
                    "unenroll_from_all": "unenroll contact from any workflows they are enrolled in and leave \"/workflow_id\" out of the url",
                },
            },
            
        }
        return e_end_points, 200


##### Owners and Contacts #####

class HubspotOwners(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, headers=None, **kwargs):
        ''' Get the list of owners in order to figure out who to attribute logged sms/call/email/postcard engagements, or to generate mailto links '''
        logger.info(f"{utils.get_time()}\tHubspotOwners.get")
        parser = reqparse.RequestParser()
        parser.add_argument("fields", required=False)
        args = parser.parse_args()
        
        a_fields = ["ownerId", "email"]
        if args["fields"]:
            a_fields.extend([x.strip() for x in args["fields"].split(",")])
        else:
            a_fields.extend([
                "firstName", "lastName", "isActive"
                "firstname", "lastname", "company",
                "email", "website", "phone", "mobilephone", "fax",
                "address", "street_address_2", "city", "state", "zip", "country"
            ])
            
        global o_HUBSPOT
        o_hubspot = o_HUBSPOT
        try:
            if o_hubspot is None:
                o_hubspot = HubspotAPI()
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Hubspot API in order to list owners: {e}")
            return {"message": f"Could not load Hubspot API in order to list owners: {e}"}, 500
        
        a_owners = o_hubspot.get_owners()
        
        if not isinstance(a_owners, list):
            if a_owners is None:
                logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. Not finding any hubspot owners (users)." + f"\nArgs: {args}")
                return {"Error": "Not finding any hubspot owners (users) is returning None"}, 404
            else:
                return a_owners.text, a_owners.status_code
        
        e_owner_contacts = o_hubspot._fetch_contacts_by_email(a_owners, properties="")
        if e_owner_contacts:
            e_owner_props = {}
            for vid, e_vid in e_owner_contacts.items():
                e_vid_props = e_vid.get("properties", {})
                a_props = [("hubspot_vid", int(vid))] + [
                    (k, e_vid.get(k, e_vid_props.get(k, {}).get("value")))
                    for k in o_hubspot._important_owner_properties
                ]
                e_vid = {k: v for k, v in a_props if v}
                e_owner_props[e_vid.get("email")] = e_vid
        
        e_owners = {
            "count": len(a_owners) + 1,
            "owners": [
                {
                    "firstname": "Manor",
                    "lastname": "Straits",
                    "email": "info@manorstraits.com",
                    "website": "manorstraits.com",
                    "phone": "847-287-6021",
                    "address": "",
                    "city": "Chicago",
                    "state": "IL",
                    "zip": "",
                    "country": "US",
                    "ownerid": -1,
                    "hubspot_vid": -1,
                },
            ],
        }
        
        for e_owner in a_owners:
            e_owner_filtered = {k.lower(): v for k, v in e_owner.items() if k in a_fields and v}
            e_p = e_owner_props.get(e_owner.get("email"), {})
            e_p.update(e_owner_filtered)
            e_owner_filtered = e_p
            
            e_owners["owners"].append(e_owner_filtered)
        return e_owners, 200


class GetContact(Resource):
    def options(self,  owner_id=None, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, owner_id=None, **kwargs):
        ''' Try to get a contact's record in the owners table along with the mappings to other sources
            Arguments:
                owner_id: {int} owner id record
            Returns:
                {tuple} of Message, Status Code
        '''
        logger.info(f"{utils.get_time()}\tGetContact.get")
        global o_CONTACT_FINDER
        if o_CONTACT_FINDER is None:
            try:
                o_CONTACT_FINDER = ContactFinder()
            except Exception as e:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Contact Finder in order to find contact: {e}")
                return {"message": f"Could not load Contact Finder in order to find contact: {e}"}, 500
        
        try:
            i_owner_id = int(owner_id)
            a_matches = [
                dict(zip(o_CONTACT_FINDER._owner_cols, t_row))
                for t_row in o_CONTACT_FINDER._db.search_row(s_OWNERS_TABLE, {"id": i_owner_id})]
            if not a_matches:
                logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. No matching contacts found in owners table for owner_id: {i_owner_id}")
                return {"message": f"No matching contacts found in owners table for owner_id: {i_owner_id}"}, 404
            e_matches = {
                e_row["id"]: {k: v for k, v in e_row.items() if v}
                for e_row in a_matches}
            
            e_mappings = {
                owner_id: o_CONTACT_FINDER.get_other_ids(owner_id)
                for owner_id in e_matches.keys()}
            return {"owners": e_matches, "mappings": e_mappings}, 200
        except Exception as e:
            logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. Failed to process contact search using owner_id '{i_owner_id}': {e}")
            return {"message": f"Failed to process contact search using owner_id '{i_owner_id}': {e}"}, 400
        
        logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. No owner_id specified in request.")
        return {"message": "No owner_id specified in request."}, 400


class GetOtherContact(Resource):
    def options(self, source=None, other_id=None, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, source, other_id, **kwargs):
        ''' Try to get a contact's record in the owners table along with the mappings to other sources
            Arguments:
                source: {str} name of recognized source DB table or API
                other_id: {int} id from source
            Returns:
                {tuple} of Message, Status Code
        '''
        logger.info(f"{utils.get_time()}\tGetOtherContact.get")
        global o_CONTACT_FINDER
        if o_CONTACT_FINDER is None:
            try:
                o_CONTACT_FINDER = ContactFinder()
            except Exception as e:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Contact Finder in order to find contact: {e}")
                return {"message": f"Could not load Contact Finder in order to find contact: {e}"}, 500
        
        try:
            s_source = o_CONTACT_FINDER._map_table_aliases.get(str(source).lower())
        
            if s_source not in o_CONTACT_FINDER._map_tables:
                logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. Unrecognized source '{source}' for looking up id: '{other_id}'")
                return {"message": f"Unrecognized source '{source}' for looking up id: '{other_id}'"}, 400
            
        
            i_other_id = int(other_id)
            a_owner_ids = o_CONTACT_FINDER.get_owner_id(s_source, i_other_id)
            
            if not a_owner_ids:
                logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. No matching contacts found from {s_source} for id: {i_other_id}")
                return {"message": f"No matching contacts found from {s_source} for id: {i_other_id}"}, 404
            
            a_matches = [
                dict(zip(o_CONTACT_FINDER._owner_cols, t_row))
                for t_row in o_CONTACT_FINDER._db.get_rows(s_OWNERS_TABLE, "id", tuple(a_owner_ids))]
            e_matches = {
                e_row["id"]: {k: v for k, v in e_row.items() if v}
                for e_row in a_matches}
        
            e_mappings = {
                owner_id: o_CONTACT_FINDER.get_other_ids(owner_id)
                for owner_id in a_owner_ids}
            return {"owners": e_matches, "mappings": e_mappings}, 200
        
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Failed to process contact search using {s_source} id '{i_other_id}': {e}")
            return {"message": f"Failed to process contact search using {s_source} id '{i_other_id}': {e}"}, 400


class PostGetContact(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        ''' Try to get a contact's record in the owners table along with the mappings to other sources
            Arguments:
                owner_id: {int} optional owner record id
                hubspot_vid: {int} optional hubspot_vid
            Returns:
                {tuple} of Message, Status Code
        '''
        logger.info(f"{utils.get_time()}\tPostGetContact.post")
        parser = reqparse.RequestParser()
        parser.add_argument("owner_id", type=int)
        parser.add_argument("hubspot_vid", type=int)
        args = parser.parse_args()
        
        global o_CONTACT_FINDER
        if o_CONTACT_FINDER is None:
            try:
                o_CONTACT_FINDER = ContactFinder()
            except Exception as e:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Contact Finder in order to find contact: {e}")
                return {"message": f"Could not load Contact Finder in order to find contact: {e}"}, 500
        
        if args["owner_id"]:
            try:
                i_owner_id = int(args["owner_id"])
                a_matches = [
                    dict(zip(o_CONTACT_FINDER._owner_cols, t_row))
                    for t_row in o_CONTACT_FINDER._db.search_row(s_OWNERS_TABLE, {"id": i_owner_id})]
                if not a_matches:
                    logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. No matching contacts found in owners table for owner_id: {i_owner_id}" + f"\nArgs: {args}")
                    return {"message": f"No matching contacts found in owners table for owner_id: {i_owner_id}"}, 404
                e_matches = {
                    e_row["id"]: {k: v for k, v in e_row.items() if v}
                    for e_row in a_matches}
                
                e_mappings = {
                    owner_id: o_CONTACT_FINDER.get_other_ids(owner_id)
                    for owner_id in e_matches.keys()}
                return {"owners": e_matches, "mappings": e_mappings}, 200
            except Exception as e:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Failed to process contact search using owner_id '{i_owner_id}': {e}")
                return {"message": f"Failed to process contact search using owner_id '{i_owner_id}': {e}"}, 400
                
        if args["hubspot_vid"]:
            try:
                i_hubspot_vid = int(args["hubspot_vid"])
                a_owner_ids = o_CONTACT_FINDER.get_owner_id("Hubspot", i_hubspot_vid)
                
                a_owner_maps = o_CONTACT_FINDER._db.search_row(s_OWNER_MAPS_TABLE, {"other_table": "Hubspot", "other_id": i_hubspot_vid})
                if not a_owner_maps:
                    logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. No matching contacts found in owners table for hubspot_vid: {i_hubspot_vid}" + f"\nArgs: {args}")
                    return {"message": f"No matching contacts found in owners table for hubspot_vid: {i_hubspot_vid}"}, 404
                
                a_matches = o_CONTACT_FINDER._db.get_rows(s_OWNERS_TABLE, "id", tuple(a_owner_maps), **kwargs)
                e_matches = {
                    e_row["id"]: {k: v for k, v in e_row.items() if v}
                    for e_row in a_matches}
            
                e_mappings = {
                    owner_id: o_CONTACT_FINDER.get_other_ids(owner_id)
                    for owner_id in a_owner_ids}
                return {"owners": e_matches, "mappings": e_mappings}, 200
            
            except Exception as e:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Failed to process contact search using hubspot_vid '{i_hubspot_vid}': {e}")
                return {"message": f"Failed to process contact search using hubspot_vid '{i_hubspot_vid}': {e}"}, 400
        
        logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. No owner_id or hubspot_vid specified in request." + f"\nArgs: {args}")
        return {"message": "No owner_id or hubspot_vid specified in request."}, 400


class FindContact(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        ''' Try to get a contact's record in the owners table along with the mappings to other sources
            Arguments:
                contact: {dict} of contact "email" or "phone" or enough name and address information to narrow it down
            Returns:
                {tuple} of Message, Status Code
        '''
        logger.info(f"{utils.get_time()}\tFindContact.post")
        parser = reqparse.RequestParser()
        parser.add_argument("contact", type=dict, location='json', required=True)
        args = parser.parse_args()
        
        global o_CONTACT_FINDER
        if o_CONTACT_FINDER is None:
            try:
                o_CONTACT_FINDER = ContactFinder()
            except Exception as e:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Contact Finder in order to find contact: {e}")
                return {"message": f"Could not load Contact Finder in order to find contact: {e}"}, 500
            
        try:
            e_contact = args["contact"]
            #rint('e_contact', e_contact)
            if not isinstance(e_contact, dict) or not e_contact:
                logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. No valid contact specified in request." + f"\nArgs: {args}")
                return {"message": "No valid contact specified in request."}, 400
            
            #a_matches = o_CONTACT_FINDER.find_contact(e_contact, b_thorough=True, return_records=True, as_dicts=True)
            a_matches = o_CONTACT_FINDER.search_contact(e_contact, return_records=True, as_dicts=True, max_matches=100)
            if not a_matches:
                logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. No matching contacts found in owners table for contact: {e_contact}" + f"\nArgs: {args}")
                return {"message": f"No matching contacts found in owners table for contact: {e_contact}"}, 404
                
            #print('a_matches', a_matches)
            e_matches = {
                e_row["id"]: {k: v for k, v in e_row.items() if v}
                for e_row in a_matches}
            
            e_mappings = {
                owner_id: o_CONTACT_FINDER.get_other_ids(owner_id)
                for owner_id in e_matches.keys()}
            
            return {"owners": e_matches, "mappings": e_mappings}, 200
        
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Failed to process contact search using contact json '{e_contact}': \n\n{e}")
            return {"message": f"Failed to process contact search using contact json '{e_contact}': \n\n{e}"}, 400


##### Get Information about Workflows and enroll contacts in them #####

class Workflows(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, headers=None, **kwargs):
        ''' Get the list of workflows '''
        logger.info(f"{utils.get_time()}\tWorkflows.get")
        parser = reqparse.RequestParser()
        parser.add_argument("all_fields", type=str, required=False)
        parser.add_argument("include_disabled", type=str, required=False)
        parser.add_argument("force_refresh", type=str, required=False)
        args = parser.parse_args()
        
        b_all_fields = str(args["all_fields"]).lower() in {"true", "yes", "on", "1", "all", "all-fields", "all_fields"}
        b_include_disabled = str(args["include_disabled"]).lower() in {"true", "yes", "on", "1", "include", "include-disabled", "include_disabled"}
        b_force_refresh = str(args["force_refresh"]).lower() in {"true", "yes", "on", "1", "force", "refresh", "force-refresh", "force_refresh"}
        
        global o_HUBSPOT
        try:
            if o_HUBSPOT is None: o_HUBSPOT = HubspotAPI()
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Hubspot API in order to list owners: {e}")
            return {"message": f"Could not load Hubspot API in order to list owners: {e}"}, 500
        
        global a_WORKFLOWS, dt_WORKFLOWS
        # when running as multiple WSGI threads, this may not access the cached version every time
        if b_force_refresh or not a_WORKFLOWS or (datetime.now() - dt_WORKFLOWS) > td_WORKFLOW_CACHE:
            logger.info(f"{utils.get_time()}\tAPI User: {s_USER}. Getting workflows from current hubspot.")
            print(f"Getting workflows from hubspot. b_force_refresh:{b_force_refresh}, a_WORKFLOWS:{type(a_WORKFLOWS)}, td_WORKFLOW_CACHE:{td_WORKFLOW_CACHE}, datetime.now() - dt_WORKFLOWS:{(datetime.now() - dt_WORKFLOWS)}, (datetime.now() - dt_WORKFLOWS) > td_WORKFLOW_CACHEL:{(datetime.now() - dt_WORKFLOWS) > td_WORKFLOW_CACHE}", )
            logger.info(f"Getting workflows from hubspot. b_force_refresh:{b_force_refresh}, a_WORKFLOWS:{type(a_WORKFLOWS)}, td_WORKFLOW_CACHE:{td_WORKFLOW_CACHE}, datetime.now() - dt_WORKFLOWS:{(datetime.now() - dt_WORKFLOWS)}, (datetime.now() - dt_WORKFLOWS) > td_WORKFLOW_CACHEL:{(datetime.now() - dt_WORKFLOWS) > td_WORKFLOW_CACHE}", )
            a_WORKFLOWS = o_HUBSPOT.get_workflows(with_details=True, disabled_details=b_include_disabled)
            dt_WORKFLOWS = datetime.now()
        else:
            logger.info(f"{utils.get_time()}\tAPI User: {s_USER}. Using cached workflows from {dt_WORKFLOWS.isoformat()}.")
            print(f"Using cached workflows from {dt_WORKFLOWS.isoformat()}")
        
        if not isinstance(a_WORKFLOWS, list):
            if a_WORKFLOWS is None:
                logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. Not finding any hubspot workflows/campaigns." + f"\nArgs: {args}")
                return {"Error": "Not finding any hubspot workflows/campaigns"}, 404
            else:
                return a_owners.text, a_owners.status_code
        
        e_workflows = {}
        for e_wf in a_WORKFLOWS:
            if not isinstance(e_wf, dict): print("Improper workflow", e_wf)
            s_name = e_wf.get("name")
            if s_name in e_workflows:
                s_name = re.sub(" (\d+)$", "", s_name)
                i = 1
                while f"{s_name} ({i})" in e_workflows: i += 1
                s_name = f"{s_name} ({i})"
            if not e_wf.get("enabled") and not b_include_disabled:
                #print(f"Skipping disabled workflow: '{s_name}'")
                continue
            
            if b_all_fields:
                e_workflows[s_name] = e_wf
            else:
                e_workflows[s_name] = {k: e_wf[k] for k in a_IMPORTANT_WORKFLOW_PROPERTIES if e_wf.get(k)}
                
                e_workflows[s_name]["contactListCounts"] = e_wf.get("metaData", {}).get("contactListCounts")
                e_workflows[s_name]["contactListIds"] = e_wf.get("metaData", {}).get("contactListIds")
                
        
        return e_workflows, 200


class WorkflowEnroll(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
        
    def get(self, workflow_id=None, headers=None, **kwargs):
        ''' list the workflows that a contact is in if no workflow id, or the enrolled contacts if there is a workflow id '''
        logger.info(f"{utils.get_time()}\WorkflowEnroll.get")
        parser = reqparse.RequestParser()
        parser.add_argument("workflow_id", type=str, required=False)
        parser.add_argument("email", type=str, required=False)
        parser.add_argument("hubspot_vid", type=str, required=False)
        parser.add_argument("all_enrolled", required=False)
        args = parser.parse_args()
        
        b_all_enrolled = False
        if args["all_enrolled"]:
            if isinstance(args["all_enrolled"], bool):
                b_all_enrolled = args["all_enrolled"]
            elif isinstance(args["all_enrolled"], (int, float)):
                b_all_enrolled = args["all_enrolled"] > 0
            elif isinstance(args["all_enrolled"], str):
                b_all_enrolled = str(args["all_enrolled"]).lower() in {"true", "yes", "keep", "save", "1", "all", "enrolled", "all-enrolled"}
        
        global o_HUBSPOT
        try:
            if o_HUBSPOT is None: o_HUBSPOT = HubspotAPI()
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Hubspot API in order to list owners: {e}")
            return {"message": f"Could not load Hubspot API in order to list owners: {e}"}, 500
        
        if workflow_id or args["workflow_id"]:
            if not workflow_id: workflow_id = args["workflow_id"]
            e_wf = o_HUBSPOT.get_workflow(workflow_id)
            if not isinstance(e_wf, dict):
                s_error = e_wf.text if isinstance(e_wf, requests.models.Response) else str(e_wf)
                logger.warn(f"{utils.get_time()}\tAPI User: {s_USER}. Could not access workflow {workflow_id} in order to list its contacts: '{s_error}'")
                return {"message": f"Could not access workflow {workflow_id} in order to list its contacts: '{s_error}'"}, 400
            
            if b_all_enrolled:
                i_list = e_wf.get("metaData", {}).get("contactListIds", {}).get("enrolled", -1)
            else:
                i_list = e_wf.get("metaData", {}).get("contactListIds", {}).get("active", -1)
            
            try:
                a_workflow_contacts = o_HUBSPOT.get_lists_contacts(i_list, ids=True)
            except Exception as e:
                logger.warn(f"{utils.get_time()}\tAPI User: {s_USER}. Couldn't read contacts from the list of workflow {workflow_id}: {e}")
                return {"message": f"Couldn't read contacts from the list of workflow {workflow_id}: {e}"}, 400
            
            return {f"workflow_{workflow_id}_contacts": a_workflow_contacts}, 200
        
        elif args["hubspot_vid"] or args["email"]:
            contact_id = args["hubspot_vid"] if args["hubspot_vid"] else args["email"]
            try:
                a_contact_workflows = o_HUBSPOT.get_contact_workflows(contact_id, ids=True)
                a_contact_workflow_ids = [e_wf["id"] for e_wf in a_contact_workflows]
                return {f"contact_{contact_id}_workflows": a_contact_workflow_ids}, 200
            except Exception as e:
                logger.warn(f"{utils.get_time()}\tAPI User: {s_USER}. Couldn't read workflows for contact {contact_id}: {e}")
                return {"message": f"Couldn't read workflows for contact {contact_id}: {e}"}, 400
        else:
            logger.warn(f"{utils.get_time()}\tAPI User: {s_USER}. No workflow_id or hubspot_vid or email specified.")
            return {"message": f"No workflow_id or hubspot_vid or email specified. Please specify a workflow to get its enrolled contacts, or a contact's id or email to get its workflows"}, 400
                    
        
    
    def post(self, workflow_id=None, headers=None, **kwargs):
        ''' Enroll a hubspot contact into a workflow '''
        logger.info(f"{utils.get_time()}\WorkflowEnroll.post")
        parser = reqparse.RequestParser()
        parser.add_argument("workflow_id", type=str, required=False)
        parser.add_argument("email", type=str, required=False)
        parser.add_argument("hubspot_vid", type=str, required=False)
        parser.add_argument("set_properties", type=str, required=False)
        args = parser.parse_args()
        
        s_contact = str(args["email"]).strip()
        if not s_contact: s_contact = str(args["hubspot_vid"])
        
        if not workflow_id: workflow_id = str(args["workflow_id"]).strip()
        if not workflow_id:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. No workflow in the url or in the request.")
            return {"message": "You must specify either the 'workflow_id' as part of the url or in the request."}, 412
        
        if not s_contact:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. No 'email' or 'hubspot_vid' in the request.")
            return {"message": "You must specify either the 'email' or 'hubspot_vid' in the enroll request"}, 412
        
        global o_HUBSPOT
        try:
            if o_HUBSPOT is None: o_HUBSPOT = HubspotAPI()
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Hubspot API in order to list owners: {e}")
            return {"message": f"Could not load Hubspot API in order to list owners: {e}"}, 500
        
        #e_WORKFLOW_CRITERIA
        #print('args["set_properties"]', args["set_properties"])
        if (
            e_WORKFLOW_CRITERIA.get(str(workflow_id)) and
            (args["set_properties"] is None or str(args["set_properties"]).lower() in {"true", "yes", "1", "set", "set-properties"})
        ):
            #print(f"Setting required contact properties for workflow {workflow_id} to contact {s_contact}")
            logger.info(f"{utils.get_time()}\tAPI User: {s_USER}. Setting required contact properties for workflow {workflow_id} to contact {s_contact}")
            e_props = e_WORKFLOW_CRITERIA[str(workflow_id)]
            o_HUBSPOT.update_contact(s_contact, e_props)
        
        logger.info(f"{utils.get_time()}\tAPI User: {s_USER}. Trying to enroll contact {s_contact} in workflow {workflow_id}.")
        o_response = o_HUBSPOT.enroll_in_workflow(workflow_id, s_contact)
        if isinstance(o_response, tuple):
            status_code = "200" if o_response[1] else o_response[2]
            
            if not o_response[1]:
                logger.warn(f"{utils.get_time()}\tAPI User: {s_USER}. Couldn't enroll contact {s_contact} in workflow {workflow_id}: {o_response}")
            
            return {"message": o_response[0]}, status_code
        else:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Unrecognized enrollment problem: {o_response}")
            return {"message": f"Unrecognized enrollment problem: {o_response}. Contact Tim."}, 400
    
    def delete(self, workflow_id=None, headers=None, **kwargs):
        ''' UneEnroll a hubspot contact from a workflow '''
        logger.info(f"{utils.get_time()}\WorkflowEnroll.delete")
        parser = reqparse.RequestParser()
        parser.add_argument("workflow_id", type=str, required=False)
        parser.add_argument("email", type=str, required=False)
        parser.add_argument("hubspot_vid", type=str, required=False)
        parser.add_argument("unenroll_from_all", type=str, required=False)
        args = parser.parse_args()
        
        b_force = False
        if args["unenroll_from_all"]:
            if isinstance(args["unenroll_from_all"], bool):
                b_force = args["unenroll_from_all"]
            elif isinstance(args["unenroll_from_all"], (int, float)):
                b_force = args["unenroll_from_all"] > 0
            elif isinstance(args["unenroll_from_all"], str):
                b_force = str(args["unenroll_from_all"]).lower() in {"true", "yes", "1", "all", "force", "unenroll-from-all"}
        
        s_contact = str(args["email"]).strip()
        if not s_contact: s_contact = str(args["hubspot_vid"])
        
        if not s_contact:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. No 'email' or 'hubspot_vid' in the request.")
            return {"message": "You must specify either the 'email' or 'hubspot_vid' in the enroll request"}, 412
        
        global o_HUBSPOT
        try:
            if o_HUBSPOT is None: o_HUBSPOT = HubspotAPI()
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Hubspot API in order to unenroll contact: {e}")
            return {"message": f"Could not load Hubspot API in order to unenroll contact: {e}"}, 500
        
        if not workflow_id: workflow_id = str(args["workflow_id"]).strip()
        if not workflow_id or workflow_id in {"None", "", None, -1, "-1"}:
            if b_force:
                a_contact_workflows = o_HUBSPOT.get_contact_workflows(s_contact)
                
                if not a_contact_workflows:
                    return {"message": f"Contact {s_contact} is not enrolled in any workflows."}, 404
                
                a_success_responses, a_fail_responses = [], []
                for workflow_id in a_contact_workflows:
                    if isinstance(workflow_id, dict):
                        workflow_id = workflow_id["id"]
                    if not workflow_id or workflow_id in {"None", "", None, -1, "-1"}: continue
                    o_response = o_HUBSPOT.unenroll_from_workflow(workflow_id, s_contact)
                    if isinstance(o_response, tuple):
                        a_success_responses.append(o_response[0])
                    else:
                        logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Unrecognized enrollment problem for contact {s_contact} in workflow {workflow_id}: {o_response}")
                        a_fail_responses.append(o_response)
                
                e_message, status = {}, 500
                if a_success_responses and not a_fail_responses:
                    e_message["message"] = "successful unenrollment"
                    e_message["unenroll_successes"] = a_success_responses
                    status = 200
                elif a_success_responses and a_fail_responses:
                    e_message["message"] = "partial unenrollment"
                    e_message["unenroll_successes"] = a_success_responses
                    e_message["unenroll_failures"] = a_fail_responses
                    status = 400
                elif not a_success_responses and a_fail_responses:
                    e_message["message"] = "failed unenrollment"
                    e_message["unenroll_failures"] = a_fail_responses
                    status = 400
                else: # not a_success_responses and not a_fail_responses:
                    e_message["message"] = "no unenrollment"
                    status = 404
                return e_message, status
                
            else:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. No workflow in the url or in the request.")
                return {"message": "You must specify either the 'workflow_id' as part of the url or in the request."}, 412
        
        else:
            o_response = o_HUBSPOT.unenroll_from_workflow(workflow_id, s_contact)
            if isinstance(o_response, tuple):
                status_code = "200" if o_response[1] else o_response[2]
                return {"message": o_response[0]}, status_code
            else:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Unrecognized enrollment problem: {o_response}")
                return {"message": f"Unrecognized enrollment problem: {o_response}. Contact Tim."}, 400

##### Get Information about Touches/Engagements with Contacts #####

class HubspotTouchSummary(Resource):
    def options(self, owner_id=None, hubspot_vid=None, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, owner_id=None, hubspot_vid=None, headers=None, **kwargs):
        ''' Get the list of touch summaries '''
        logger.info(f"{utils.get_time()}\tHubspotTouchSummary.get")
        parser = reqparse.RequestParser()
        parser.add_argument("by_hubspot_vid", required=False)
        args = parser.parse_args()
        
        b_by_hubspot_vid = str(args["by_hubspot_vid"]).lower() in {"true", "yes", "hubspot", "vid", "hubspot_vid"}
        
        _load_db("list Hubspot contact touches")
        if not o_AWS_DB:
            return {"message": f"Could not load AWS DB in order to list hubspot contact touches: {e}"}, 500
        
        try:
            e_summaries = touch_utils.get_touch_summaries(
                o_AWS_DB,
                hubspot_vid=hubspot_vid,
                owner_id=owner_id,
                b_by_hubspot_vid=b_by_hubspot_vid)
            if hubspot_vid or owner_id:
                if not e_summaries:
                    if hubspot_vid:
                        return {"message": f"No engagements found for hubspot_vid {hubspot_vid}"}, 400
                    if owner_id:
                        return {"message": f"No engagements found for owner_id {owner_id}"}, 400
                return list(e_summaries.values())[0], 200
            else:
                return e_summaries, 200
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Failed to get contact touch summaries: {e}")
            return {"message": f"Failed to get contact touch summaries: {e}"}, 400


class HubspotTouchDetails(Resource):
    def options(self, owner_id=None, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, owner_id=None, hubspot_vid=None, headers=None, **kwargs):
        ''' Get the list of touch summaries '''
        logger.info(f"{utils.get_time()}\tHubspotTouchDetails.get")
        parser = reqparse.RequestParser()
        parser.add_argument("event_type", type=str, required=False, location='args', action='append')
        parser.add_argument("keep_non_mapped", required=False, location='args')
        parser.add_argument("by_hubspot_vid", required=False)
        args = parser.parse_args()
        
        _load_db("list Hubspot contact touches")
        if not o_AWS_DB:
            return {"message": f"Could not load AWS DB in order to list Hubspot contact touches: {e}"}, 500
        
        c_match_types = {str(event_type).upper().strip() for event_type in args["event_type"]} if args["event_type"] else set()
        
        b_keep_non_mapped = str(args["keep_non_mapped"]).lower() in {"true", "yes", "keep", "save", "1"}
        b_by_hubspot_vid = str(args["by_hubspot_vid"]).lower() in {"true", "yes", "hubspot", "vid", "hubspot_vid"}
        
        try:
            e_touches = touch_utils.get_touch_details(
                o_AWS_DB,
                c_match_types,
                hubspot_vid=hubspot_vid,
                owner_id=owner_id,
                b_keep_non_mapped=b_keep_non_mapped,
                b_by_hubspot_vid=b_by_hubspot_vid)
            
            if not e_touches:
                if hubspot_vid or owner_id:
                    if not e_touches:
                        if hubspot_vid:
                            return {"message": f"No engagements found for hubspot_vid {hubspot_vid}"}, 400
                        if owner_id:
                            return {"message": f"No engagements found for owner_id {owner_id}"}, 400
                    return e_touches, 200
                else:
                    logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. No contact touches found." + f"\nArgs: {args}")
                    return {"Error": "No contact touches found"}, 404
            else:
                if hubspot_vid or owner_id:
                    return list(e_touches.values())[0], 200
                else:
                    return list(e_touches.values()), 200
        
        except Exception as e:
            if hubspot_vid:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Failed to get contact touches for hubspot_vid '{hubspot_vid}': {e}")
                return {"message": f"Failed to get contact touches for hubspot_vid '{hubspot_vid}': {e}"}, 400
            elif owner_id:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Failed to get contact touches for owner_id '{owner_id}': {e}")
                return {"message": f"Failed to get contact touches for owner_id '{owner_id}': {e}"}, 400
            else:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Failed to get contact touches: {e}")
                return {"message": f"Failed to get contact touches: {e}"}, 400
        


##### Send messages via text/postcard/etc #####

class SMS(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        ''' send a text message to one or more contacts
            Arguments:
                contacts: {list} of plain cell numbers as {str} OR contacts in dict format with the cell number as "mobile"
                          ["555-0101", "555-7777", ...]
                            _OR_
                          [
                            {"mobile": "555-0101", "firstName": "Sam", "lastName": "Pull",},
                            {"mobile": "555-7777", "firstName": "Tess", "lastName": "Ting",},
                          ]
                message: {str} of the content to be sent. Can contain wild cards like "{{ firstName }}" if the values are present in the contacts.
            Returns:
                {tuple} of Message, Status Code
        '''
        parser = reqparse.RequestParser()
        parser.add_argument("contacts", required=True, type=list, location='json')
        parser.add_argument("message", required=True, type=str, location='json')
        parser.add_argument("hubspot_owner", type=int)
        args = parser.parse_args()
        assert isinstance(args["contacts"], list)
        contacts = args["contacts"]
        s_message = args["message"]
        
        logger.info(f"{utils.get_time()}\tSMS.post to {len(contacts)} contacts")
        
        i_owner = -1
        if args["hubspot_owner"]:
            try: i_owner = int(args["hubspot_owner"])
            except: print("Unknown hubspot_owner:", args["hubspot_owner"])
        
        global o_SAKARI
        try:
            if o_SAKARI is None:
                o_SAKARI = SakariAPI()
        except:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Sakari API in order to send messages")
            return {"message": "Could not load Sakari API in order to send messages"}, 500
        
        global o_HUBSPOT
        try:
            if o_HUBSPOT is None:
                if i_owner >= 0:
                    o_HUBSPOT = HubspotAPI(owner_id=i_owner)
                else:
                    o_HUBSPOT = HubspotAPI()
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Hubspot API in order to record engagement: {e}")
            return {"message": f"Could not load Hubspot API in order to record engagement: {e}"}, 500
        
        try:
            o_response = o_SAKARI._send_message(contacts, s_message)
        except Exception as e:
            logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. Error in attempting to pass request on to Saraki: {e}" + f"\nArgs: {args}")
            return {"message": f"Error in attempting to pass request on to Saraki: {e}"}, 400
        #print("o_response", o_response.text)

        if not o_response or not o_response.ok:
            logger.warn(f"SMS Message Failed: {o_response.status_code}, {o_response.text}")
            return o_response.text, o_response.status_code
        
        else:
            try:
                e_data = json.loads(o_response.text).get("data", {})
                c_invalid_numbers = {
                    e["mobile"]
                    for e in e_data.get("invalid", [])
                    if e.get("mobile")}
                i_valid_numbers = e_data.get("valid", 0)
                
                #print("contacts", contacts)
                c_hubspot_ids = set()
                s_hubspot = "No valid contacts exist in hubspot to log SMS messages"
                
                if i_valid_numbers > 0:
                    a_valid_conts = [
                        cont for cont in contacts
                        if (
                            isinstance(cont, dict) and
                            (cont.get("phone", cont.get("mobile", cont.get("cell"))) or cont.get("email", cont.get("work_email")))
                            and cont.get("phone", cont.get("mobile", cont.get("cell"))) not in c_invalid_numbers
                        ) or
                        (
                            isinstance(cont, str) and re.search(r"\d{3,}", cont) and cont not in c_invalid_numbers
                        )
                    ]
                    #print("a_valid_conts", a_valid_conts)
                    a_numbers = [
                        cont.get("phone", cont.get("cell", cont.get("mobile"))) if isinstance(cont, dict) else cont
                        for cont in contacts]
                    
                    a_matching_hubspot_ids = o_HUBSPOT.find_contacts(a_valid_conts, b_thorough=False)
                    #print("a_matching_hubspot_ids", a_matching_hubspot_ids)
                    for idx, (cont, matches) in enumerate(zip(a_valid_conts, a_matching_hubspot_ids)):
                        #print(idx, (cont, matches))
                        if not cont: continue
                        if not matches:
                            if isinstance(cont, dict):
                                a_current_owner_ids, e_mappings = verify_contact_exists(cont)
                                if e_mappings: a_matching_hubspot_ids[idx] = e_mappings.get("Hubspot", [])
                            elif isinstance(cont, str):
                                a_current_owner_ids, e_mappings = verify_contact_exists({"mobilephone": cont})
                                if e_mappings: a_matching_hubspot_ids[idx] = e_mappings.get("Hubspot", [])
                    c_hubspot_ids = {
                        vid for number_vids in a_matching_hubspot_ids
                        for vid in number_vids}
                    
                    if c_hubspot_ids:
                        e_engagement = {
                            "active": True,
                            "ownerId": i_owner if i_owner >= 0 else o_HUBSPOT.owner_id,
                            "type": "NOTE"}
                        e_metadata = {"body": f"SMS: {s_message}"}
                        
                        a_associated_owners = [int(x) for x in [o_HUBSPOT.owner_id] if int(x) >= 0]
                        e_association = {
                            "contactIds": list(c_hubspot_ids),
                            "ownerIds": a_associated_owners,}
                        
                        o_note_confirm = o_HUBSPOT.create_engagement(
                            e_engagement = e_engagement,
                            e_association = e_association,
                            e_metadata=e_metadata,)
                        s_hubspot = json.dumps(o_note_confirm) if isinstance(o_note_confirm, dict) else o_note_confirm.text
                        
                        # Log immediately to hubspot_touches
                        log_touch_to_db(o_note_confirm)
                        
                    else:
                        s_hubspot = "No valid contacts exist in hubspot to log SMS messages"
                
                i_status = o_response.status_code
                if c_invalid_numbers or i_valid_numbers <= 0:
                    i_status = 400
                
                if i_valid_numbers > 0 and not c_invalid_numbers:
                    s_message = f"SMS sent via Sakari to {i_valid_numbers} numbers and logged on Hubspot to {len(c_hubspot_ids)} hubspot contacts."
                elif i_valid_numbers > 0 and c_invalid_numbers:
                    s_message = f"SMS sent via Sakari to {i_valid_numbers} numbers and logged on Hubspot to {len(c_hubspot_ids)} hubspot contacts. Failed to send SMS to {c_invalid_numbers} contacts."
                elif i_valid_numbers == 0 or not c_hubspot_ids:
                    s_message = f"Failed to send SMS to {c_invalid_numbers} contacts."
                
                return (
                    {
                        "message": s_message,
                        "sakari message": o_response.text,
                        "hubspot message": s_hubspot
                    },
                    i_status,
                    e_DEFAULT_HEADER)
                
            except Exception as e:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Error with code when trying to log SMS engagements with Hubspot API: {e}")
                return {"message": f"Error with code when trying to log SMS engagements with Hubspot API: {e}"}, 500


class Postcard(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        ''' send a postcard message to one or more contacts
            Arguments:
                contacts: {list} of contacts in dict format
                          [
                            {
                                "firstName": "Sam",
                                "lastName": "Pull",
                                "company": "Tester Inc.",
                                "street1": "123 Main Street",
                                "street2": "Suite 2",
                                "city": "Anywhere",
                                "state": "IL",
                                "zip": "01234"},
                          ]
                message: {str} of the content to be sent. Can contain wild cards like "{firstName}" if the values are present in the contacts.
                         Limit 320 characters
                         
                from: {dict} of the contact information of the sender like
                    {
                        "firstName": "Jackie",
                        "lastName": "Treehorn",
                        "street1": "1 Random Street",
                        "street2": "Apt 33A",
                        "city": "Malibu",
                        "state": "CA",
                        "zip": "90263"
                    }
                    
                message: {str} of message up to 320 characters in length.
                           The message can use the fields in the recipients as variables in the
                           format of "Hi {firstName}, this is Tim at IDSTS. How are things in {city}?"
                s_card_id: {str} id of the stationery to be used
                s_handwriting_id: {str} id of the style of handwriting
            Returns:
                {tuple} of Message, Status Code
        '''
        parser = reqparse.RequestParser()
        parser.add_argument("contacts", required=True, type=list, location='json')
        parser.add_argument("from", required=True, type=dict, location='json')
        parser.add_argument("message", required=True, type=str, location='json')
        parser.add_argument("card_id", required=False, location='json')
        parser.add_argument("handwriting_id", required=False, location='json')
        parser.add_argument("hubspot_owner", type=int)
        args = parser.parse_args()
        
        assert isinstance(args["contacts"], list)
        contacts = args["contacts"]
        
        logger.info(f"{utils.get_time()}\tPostcard.post to {len(contacts)} contacts")
        
        i_owner = -1
        if args["hubspot_owner"]:
            try: i_owner = int(args["hubspot_owner"])
            except: print("Unknown hubspot_owner:", args["hubspot_owner"])
        
        global o_HANDWRITE
        try:
            if o_HANDWRITE is None:
                o_HANDWRITE = HandwriteAPI()
        except:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Handwrite API in order to send messages")
            return {"message": "Could not load Handwrite API in order to send messages"}, 500
        
        global o_HUBSPOT
        try:
            if o_HUBSPOT is None:
                if i_owner >= 0:
                    o_HUBSPOT = HubspotAPI(owner_id=i_owner)
                else:
                    o_HUBSPOT = HubspotAPI()
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Hubspot API in order to record engagement: {e}")
            return {"message": f"Could not load Hubspot API in order to record engagement: {e}"}, 500
        
        e_from = args["from"]
        if not isinstance(e_from, dict): e_from = {}
        
        if "firstName" not in e_from: e_from["firstName"] = "Your Friends at"
        if "lastName" not in e_from: e_from["lastName"] = "Manor Straits"
        if "firstName" not in e_from: e_from["firstName"] = ""
        if "street1" not in e_from: e_from["street1"] = ""
        if "street2" not in e_from: e_from["street2"] = ""
        if "city" not in e_from: e_from["city"] = "Chicago"
        if "state" not in e_from: e_from["state"] = "IL"
        if "zip" not in e_from: e_from["zip"] = ""
        
        s_message = args["message"]
        s_card_id = args["card_id"] if args["card_id"] else "5dc304cfbc08d20016f1ec2f"
        s_handwriting_id = args["handwriting_id"] if args["handwriting_id"] else "5dc30652bc08d20016f1ec33"
        
        try:
            o_response = o_HANDWRITE.send_message(contacts, e_from, s_message, s_card_id, s_handwriting_id)
        except Exception as e:
            logger.error(f"{utils.get_time()}\tAPI User: {s_USER}. Error in attempting to pass request on to Handwrite.io: {e}" + f"\nArgs: {args}")
            return {"message": f"Error in attempting to pass request on to Handwrite.io: {e}"}, 400
        
        if not o_response.ok:
            logger.warn(f"Postcard Message Failed: {o_response.status_code}, {o_response.text}")
            return o_response.text, o_response.status_code
        else:
            try:
                a_valid_conts = [
                    cont for cont in contacts
                    if isinstance(cont, dict) and
                        (cont.get("firstName", cont.get("lastName")) and cont.get("address")) or
                        cont.get("phone", cont.get("cell", cont.get("mobile"))) or
                        cont.get("email", cont.get("work_email"))]
                
                a_matching_hubspot_ids = o_HUBSPOT.find_contacts(a_valid_conts, b_thorough=False)
                for idx, (cont, matches) in enumerate(zip(a_valid_conts, a_matching_hubspot_ids)):
                    if not cont: continue
                    if not matches:
                        a_current_owner_ids, e_mappings = verify_contact_exists(cont)
                        if e_mappings: a_matching_hubspot_ids[idx] = e_mappings.get("Hubspot", [])
                c_hubspot_ids = {
                    vid for number_vids in a_matching_hubspot_ids
                    for vid in number_vids}
                
                e_engagement = {
                    "active": True,
                    "ownerId": i_owner if i_owner >= 0 else o_HUBSPOT.owner_id,
                    "type": "NOTE"}
                e_metadata = {"body": f"POSTCARD: {s_message}"}
                
                a_associated_owners = [int(x) for x in [o_HUBSPOT.owner_id] if int(x) >= 0]
                e_association = {
                    "contactIds": list(c_hubspot_ids),
                    "ownerIds": a_associated_owners,}
            
                o_note_confirm = o_HUBSPOT.create_engagement(
                    e_engagement = e_engagement,
                    e_association = e_association,
                    e_metadata=e_metadata,)
            
                # Log immediately to hubspot_touches
                log_touch_to_db(o_note_confirm)
                
                return (
                    {
                        "message": f"Postcard sent via Handwrite and logged on Hubspot.",
                        "handwrite message": o_response.text,
                        "hubspot message": json.dumps(o_note_confirm) if isinstance(o_note_confirm, dict) else o_note_confirm.text
                    },
                    o_response.status_code,
                    e_DEFAULT_HEADER)
            
            except Exception as e:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Error with code when trying to log postcard engagements with Hubspot API: {e}")
                return {"message": f"Error with code when trying to log postcard engagements with Hubspot API: {e}"}, 500


class LogPhone(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        ''' log a phone call and notes about it into hubspot
            Arguments:
                contact: {str} recipients phone number
                caller: {str} the caller's number
                body: {str} text notes of the call
                status: {str} status of the call. Default is "COMPLETED".
                duration: {int} length in milliseconds
                recording_url: {str} link to record of call
            Returns:
                {tuple} of Message, Status Code
        '''
        parser = reqparse.RequestParser()
        parser.add_argument("contact", required=True, location='json')
        parser.add_argument("caller", required=True, type=str, location='json')
        parser.add_argument("message", required=False, type=str, location='json')
        parser.add_argument("status", required=False, type=str, location='json')
        parser.add_argument("duration", required=False, type=int, location='json')
        parser.add_argument("recording_url", required=False, type=str, location='json')
        parser.add_argument("hubspot_owner", type=int)
        args = parser.parse_args()
        
        s_to_number, contact = "", None
        #print(type(args["contact"]), args["contact"])
        if args["contact"]:
            if isinstance(args["contact"], str):
                if not args["contact"].startswith(("{", "[")):
                    s_to_number = args["contact"]
                    contact = {"phone": s_to_number}
                else:
                    contact = json.loads(args["contact"])
                    s_to_number = contact.get("phone", contact.get("cell", contact.get("mobile", contact.get("mobilephone", ""))))
            elif isinstance(args["contact"], dict):
                contact = args["contact"]
                s_to_number = contact.get("phone", contact.get("cell", contact.get("mobile", contact.get("mobilephone", ""))))
        #print("contact", contact, "s_to_number", s_to_number)
        
        if not s_to_number:
            return {"message": f'"contact" parameter in unknown format or blank. Please pass in a either a phone number or contact json dict with a key like "phone", "mobile", "cell", "mobilephone".'}, 400
        
        s_from_number = args["caller"]
        s_message = args["message"]
        s_status = args["status"] if args["status"] else "COMPLETED"
        i_duration = args["duration"]
        s_recording_url = args["recording_url"]
        
        logger.info(f"{utils.get_time()}\tLogPhone.post to {s_to_number} from {s_from_number}")
        
        i_owner = -1
        if args["hubspot_owner"]:
            try: i_owner = int(args["hubspot_owner"])
            except: print("Unknown hubspot_owner:", args["hubspot_owner"])
        
        global o_HUBSPOT
        try:
            if o_HUBSPOT is None:
                if i_owner >= 0:
                    o_HUBSPOT = HubspotAPI(owner_id=i_owner)
                else:
                    o_HUBSPOT = HubspotAPI()
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Hubspot API in order to record engagement: {e}")
            return {"message": f"Could not load Hubspot API in order to record engagement: {e}"}, 500
        
        try:
            a_matching_hubspot_ids = o_HUBSPOT.find_contacts([contact], b_thorough=False)
            for idx, (cont, matches) in enumerate(zip([contact], a_matching_hubspot_ids)):
                if not cont: continue
                if not matches:
                    #print('cont', cont)
                    a_current_owner_ids, e_mappings = verify_contact_exists(cont)
                    if e_mappings: a_matching_hubspot_ids[idx] = e_mappings.get("Hubspot", [])
            
            c_hubspot_ids = {
                vid for number_vids in a_matching_hubspot_ids
                for vid in number_vids}
            
            e_engagement = {
                "active": True,
                "ownerId": i_owner if i_owner >= 0 else o_HUBSPOT.owner_id,
                "type": "CALL"}
            e_metadata = {
                "toNumber": s_to_number,
                "fromNumber": s_from_number,
                "body": f"CALL: {s_message}",
                "status": s_status,
            }
            if i_duration: e_metadata["durationMilliseconds"] = i_duration
            if s_recording_url: e_metadata["recordingUrl"] = s_recording_url
            
            a_associated_owners = [int(x) for x in [o_HUBSPOT.owner_id] if int(x) >= 0]
            e_association = {
                "contactIds": list(c_hubspot_ids),
                "ownerIds": a_associated_owners,}
        
            o_note_confirm = o_HUBSPOT.create_engagement(
                e_engagement = e_engagement,
                e_association = e_association,
                e_metadata=e_metadata,)
            
            # Log immediately to hubspot_touches
            log_touch_to_db(o_note_confirm)
            
            if isinstance(o_note_confirm, dict) or o_note_confirm.ok:
                return (
                    {
                        "message": f"Call logged on Hubspot.",
                        "hubspot message": json.dumps(o_note_confirm) if isinstance(o_note_confirm, dict) else o_note_confirm.text
                    },
                    200 if isinstance(o_note_confirm, dict) else o_note_confirm.status_code)
            else:
                logger.warn(f"LogPhone Failed: {o_note_confirm.status_code}, {o_note_confirm.text}")
                return o_note_confirm.text, o_note_confirm.status_code
            
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Error with code when trying to log phone engagements with Hubspot API: {e}")
            return {"message": f"Error with code when trying to log phone engagements with Hubspot API: {e}"}, 500


class Email(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        ''' log an email and notes about it into hubspot. Send email if request.path is "/email" rather than "/email/log"
            Arguments:
                contacts: {list} of contact dicts with "email" in them or text email addresses like "contact name <test@test.com>"
                from: {dict} like {"email": "email@domain.com", "firstName": "First", "lastName": "Last"}
                cc: optional {list} of contact dicts with "email" in them or text email addresses like "contact name <test@test.com>"
                bcc: optional {list} of contact dicts with "email" in them or text email addresses like "contact name <test@test.com>"
                subject: {str} the subject of the email
                text: {str} raw text of the body of the email
                html: {str} html version of the email
            Returns:
                {tuple} of Message, Status Code
        '''
        b_send_email = "log" not in request.path.lower()
        
        parser = reqparse.RequestParser()
        parser.add_argument("contacts", required=True, type=list, location='json')
        parser.add_argument("from", required=True, type=dict, location='json')
        parser.add_argument("cc", required=False, type=list, location='json')
        parser.add_argument("bcc", required=False, type=list, location='json')
        parser.add_argument("subject", required=True, type=str, location='json')
        parser.add_argument("html", required=False, type=str, location='json')
        parser.add_argument("text", required=False, type=str, location='json')
        parser.add_argument("hubspot_owner", type=int)
        args = parser.parse_args()
        
        a_to = [
            {"email": v_cont} if isinstance(v_cont, str) else v_cont
            for v_cont in args["contacts"]
            if v_cont and ((isinstance(v_cont, str)and "@" in v_cont) or (isinstance(v_cont, dict)and "@" in v_cont.get("email", "")))]
        a_cc, a_bcc = [], []
        if args["cc"]:
            a_cc = [
                {"email": v_cont} if isinstance(v_cont, str) else v_cont
                for v_cont in args["cc"]
                if v_cont and ((isinstance(v_cont, str)and "@" in v_cont) or (isinstance(v_cont, dict)and "@" in v_cont.get("email", "")))]
        if args["bcc"]:
            a_bcc = [
                {"email": v_cont} if isinstance(v_cont, str) else v_cont
                for v_cont in args["bcc"]
                if v_cont and ((isinstance(v_cont, str)and "@" in v_cont) or (isinstance(v_cont, dict)and "@" in v_cont.get("email", "")))]
        
        e_from = args["from"]
        if "@" not in e_from.get("email", ""):
            logger.error(f'{utils.get_time()}\tAPI User: {s_USER}. The input "from" doesn\'t include a valid email address: "{e_from.get("email", "")}"' + f"\nArgs: {args}")
            return {"message": f'The input "from" doesn\'t include a valid email address: "{e_from.get("email", "")}"'}, 400
                    
        s_subject = args["subject"]
        s_raw = args["text"]
        s_html = args["html"]
        if not s_raw and not s_html:
            logger.error(f'{utils.get_time()}\tAPI User: {s_USER}. Must contain at least either raw "text" or "html" body of email' + f"\nArgs: {args}")
            return {"message": 'Must contain at least either raw "text" or "html" body of email'}, 400
        
        s_recipients = ", ".join([v["email"] for v in a_to if v.get("email")])
        
        e_ses_response = None
        if b_send_email:
            try:
                logger.info(f"{utils.get_time()}\tSending Email to {len(a_to)} contacts from {e_from.get('email')}: {s_recipients}")
                e_ses_response = aws_ses.send_email(
                    [x["email"] for x in a_to if x.get("email")],
                    s_subject,
                    s_raw,
                    s_html=s_html,
                    s_from=e_from.get("email") if isinstance(e_from, dict) else None,
                    v_cc=[x["email"] for x in a_cc if x.get("email")],
                    v_bcc=[x["email"] for x in a_bcc if x.get("email")],)
                
                if e_ses_response.get("email", {}).get("Code") == "MessageRejected":
                    return {"message": "Failed to send email", "amazon_ses": e_ses_response}, 400
                
            except Exception as e:
                logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Failed to send email: {e}")
                if e_ses_response:
                    return {"message": "Failed to send email", "amazon_ses": e_ses_response}, 400
                else:
                    return {"message": f'Failed to send email: "{e}"'}, 500
        else:
            logger.info(f"{utils.get_time()}\tLogging email to {len(a_to)} contacts from {e_from.get('email')}: {s_recipients}")
        
        i_owner = -1
        if args["hubspot_owner"]:
            try: i_owner = int(args["hubspot_owner"])
            except: print("Unknown hubspot_owner:", args["hubspot_owner"])
        
        global o_HUBSPOT
        try:
            if o_HUBSPOT is None:
                if i_owner >= 0:
                    o_HUBSPOT = HubspotAPI(owner_id=i_owner)
                else:
                    o_HUBSPOT = HubspotAPI()
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not load Hubspot API in order to record engagement: {e}")
            return {"message": f"Could not load Hubspot API in order to record email engagement: {e}"}, 500
        
        #try:
        if kwargs.get("add_cc_bbc", False):
            a_valid_conts = a_to + a_cc + a_bcc
        else:
            a_valid_conts = a_to[:]
        
        a_matching_hubspot_ids = o_HUBSPOT.find_contacts(a_valid_conts, b_thorough=False)
        
        for idx, (cont, matches) in enumerate(zip(a_valid_conts, a_matching_hubspot_ids)):
            if not cont: continue
            if not matches:
                a_current_owner_ids, e_mappings = verify_contact_exists(cont)
                if e_mappings: a_matching_hubspot_ids[idx] = e_mappings.get("Hubspot", [])
        c_hubspot_ids = {
            vid for number_vids in a_matching_hubspot_ids
            for vid in number_vids}
        
        if not kwargs.get("add_cc_bbc", False):
            # if not automatically adding cc and bcc recipients as full contacts, still try to associate them with the email if they already exist
            a_other_emails = []
            for e_cont in a_cc + a_bcc:
                if e_cont.get("hubspot_id"):
                    c_hubspot_ids.add(e_cont["hubspot_id"])
                elif e_cont.get("hubspot_vid"):
                    c_hubspot_ids.add(e_cont["hubspot_vid"])
                elif e_cont.get("email"):
                    s_email = e_cont["email"].strip()
                    re_email = re_EMAIL.search(s_email)
                    if re_email:
                        a_other_emails.append(re_email.group("email"))
            #print("a_other_emails", a_other_emails)
        
            if a_other_emails:
                c_hubspot_ids.update({
                    vid for number_vids in o_HUBSPOT.find_contacts(a_other_emails)
                    for vid in number_vids})
            #print("c_hubspot_ids", c_hubspot_ids)
        
        e_engagement = {
            "active": True,
            "ownerId": i_owner if i_owner >= 0 else o_HUBSPOT.owner_id,
            "type": "EMAIL"}
        #print("e_engagement", e_engagement)
        e_metadata = {
            "from": e_from,
            "to": a_to,
            "subject": s_subject,
        }
        if a_cc: e_metadata["cc"] = a_cc
        if a_bcc: e_metadata["bcc"] = a_bcc
        if s_raw: e_metadata["text"] = s_raw
        if s_html: e_metadata["html"] = s_html
        #print("e_metadata", e_metadata)
        
        a_associated_owners = [int(x) for x in [o_HUBSPOT.owner_id] if int(x) >= 0]
        e_association = {
            "contactIds": list(c_hubspot_ids),
            "ownerIds": a_associated_owners,}
        #print("e_association", e_association)
    
        o_note_confirm = o_HUBSPOT.create_engagement(
            e_engagement = e_engagement,
            e_association = e_association,
            e_metadata=e_metadata,)
        
        # Log immediately to hubspot_touches
        log_touch_to_db(o_note_confirm)
        
        if isinstance(o_note_confirm, dict) or o_note_confirm.ok:
            e_response = {
                    "message": f"Email logged on Hubspot.",
                    "hubspot message": json.dumps(o_note_confirm) if isinstance(o_note_confirm, dict) else o_note_confirm.text
                }
            if e_ses_response: e_response["ses message"] = e_ses_response
            return (
                e_response,
                200 if isinstance(o_note_confirm, dict) else o_note_confirm.status_code)
        else:
            logger.warn(f"Email Failed: {o_note_confirm.status_code}, {o_note_confirm.text}")
            return o_note_confirm.text, o_note_confirm.status_code
        
        #except Exception as e:
        #    logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Error with code when trying to log email engagements with Hubspot API: {e}")
        #    return {"message": f"Error with code when trying to log email engagements with Hubspot API: {e}"}, 500

class EmailNotification(Resource):
    ''' listen for SNS email statuses to look for addresses that need to be blocked for bouncing/complaining '''
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        ''' Get 
            Arguments:
            Returns:
                {tuple} of Message, Status Code
        '''
        try:
            e_data = json.loads(request.get_data(as_text=True))
            s_type = e_data.get('Type', 'Unknown Type')
            
            if s_type == 'SubscriptionConfirmation':
                s_confirm_url = e_data.get("SubscribeURL", "")
                try:
                    o_result = requests.get(s_confirm_url)
                    logger.info(f"{utils.get_time()}\tAPI User: {s_USER}. SES SNS confirmation: {s_confirm_url}")
                    return {"message": f"Accessed confirmation url {s_confirm_url}", "result": o_result.text}, o_result.status_code
                except Exception as e:
                    logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. SES SNS failed confirmation {s_confirm_url}:", e)
                    return {"message": f"Couldn't access confirmation url {s_confirm_url}"}, 400
            
            message = e_data.get("Message", {})
            if isinstance(message, str): message = json.loads(message)
            if not message: message = e_data
            s_notification_type = message.get("notificationType", "Unknown").title()
            if s_notification_type == "Unknown":
                logger.warn(f"{utils.get_time()}\tAPI User: {s_USER}. SES SNS {s_type} Data: {request.get_data(as_text=True)}")
                
            a_block = aws_ses.get_sns_block_recipient(e_data)
            
            if a_block and s_notification_type.lower() in {"complaint", "bounce"}:
                logger.info(f"{utils.get_time()}\tAPI User: {s_USER}. SES SNS {s_notification_type} Notification of {len(a_block)} addresses to block.")
                aws_ses.block_emails(a_block)
            else:
                logger.info(f"{utils.get_time()}\tAPI User: {s_USER}. SES SNS {s_notification_type} Notification.")
            
            return {"message": f"SES SNS {s_notification_type} Notification received."}, 200
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Error trying to process SES SNS Notification: {e}")
            return {"message": f"Error with code when trying to process SES SNS Notification: {e}"}, 500


##### DEVELOPMENT WEBHOOK LISTENER #####

class DevWebhook(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, api_key="", **kwargs):
        s_folder = os.path.join(os.getcwd(), "webhook_payloads")
        if not os.path.isdir(s_folder):
            return {"message": 'There are currently no webhook payloads'}, 404
        
        s_webhook_payloads, a_payloads = utils.zip_directory(s_folder, store_in_directory=True, exclude_extensions=[".zip"])
        
        logger.info(f'DevWebhook.get: zipped {len(a_payloads)} webhook payloads into "{s_webhook_payloads}"')
        
        try:
            return send_file(s_webhook_payloads, as_attachment=True)
        except FileNotFoundError:
            return {"message": "Couldn't find any webhook payload files stored"}, 404
    
    def delete(self, api_key="", **kwargs):
        s_folder = os.path.join(os.getcwd(), "webhook_payloads")
        if not os.path.isdir(s_folder):
            return {"message": 'There are currently no webhook payloads'}, 404
        try:
            a_removed = utils.clear_sub_folders("webhook_payloads", os.getcwd())
            i_payloads = len([s for x in a_removed if s and s.lower().endswith(".txt")])
            logger.info(f'DevWebhook.delete: removed {i_payloads} webhook payloads')
            return {"message": f'removed {len(a_removed)} webhook payloads'}, 200
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Could not clear out webhook payloads: {e}")
            return {"message": f"Could not clear out webhook payloads: {e}"}, 500
    
    def post(self, **kwargs):
        ''' to see what a webhook is sending, record it's output to a txt file
            Arguments:
            Returns:
                {tuple} of Message, Status Code
        '''
        try:
            print(request)
            e_json = request.json
            if isinstance(e_json, str):
                print("Loading json payload")
                e_json = json.loads(e_json)
            
            print(f"{utils.get_time()}\tAPI User: {s_USER}. Webhook payload")
            logger.info(f"{utils.get_time()}\tAPI User: {s_USER}. Webhook payload")
            if e_json is None: e_json = {}
            
            s_folder = os.path.join(os.getcwd(), "webhook_payloads")
            s_file = "webhook_payload_{}.txt".format(datetime.now().strftime("%Y%m%d_%H%M%S"))
            if not utils.create_folder(os.path.join(s_folder, s_file)):
                raise Exception(f"Couldn't create folder to store webhook payloads: \"{s_folder}\"")
            
            with open(os.path.join(s_folder, s_file), "w", encoding="utf-8-sig") as o_file:
                e_req = {
                    'source_ip': request.remote_addr,
                    #'url': request.url,
                    'path': request.path,
                    'method': request.method,
                    'user_agent': request.user_agent.string,
                    'args': dict(request.args),
                    'view_args': request.view_args,
                    'query_string': request.query_string.decode("utf-8"),
                    'data': request.get_data(as_text=True),
                }
                
                o_file.write('{\r\n    "request": {')
                for i, (k, v) in enumerate(e_req.items()):
                    o_file.write(f'\r\n        "{k}": {json.dumps(v)}')
                    if i < len(e_req) - 1: o_file.write(",")
                o_file.write('\r\n},\r\n"payload": {')
                for k, v in e_json.items():
                    o_file.write(f'\r\n        "{k}": {json.dumps(v)}')
                    if i < len(e_req) - 1: o_file.write(",")
                o_file.write('\r\n    }\r\n}')
            
            return {"message": f"Webhook payload recorded to \"{s_file}\""}, 200
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Error trying to record sample webhook payload: {e}")
            return {"message": f"Error with code when trying to record sample webhook payload: {e}"}, 500
    
    def put(self, **kwargs):
        ''' to see what a webhook is sending, record it's output to a txt file
            Arguments:
            Returns:
                {tuple} of Message, Status Code
        '''
        try:
            print(request)
            e_json = request.json
            if isinstance(e_json, str):
                print("Loading json payload")
                e_json = json.loads(e_json)
            
            print(f"{utils.get_time()}\tAPI User: {s_USER}. Webhook payload")
            logger.info(f"{utils.get_time()}\tAPI User: {s_USER}. Webhook payload")
            if e_json is None: e_json = {}
            
            s_folder = os.path.join(os.getcwd(), "webhook_payloads")
            s_file = "webhook_payload_{}.txt".format(datetime.now().strftime("%Y%m%d_%H%M%S"))
            if not utils.create_folder(os.path.join(s_folder, s_file)):
                raise Exception(f"Couldn't create folder to store webhook payloads: \"{s_folder}\"")
            
            with open(os.path.join(s_folder, s_file), "w", encoding="utf-8-sig") as o_file:
                e_req = {
                    'source_ip': request.remote_addr,
                    #'url': request.url,
                    'path': request.path,
                    'method': request.method,
                    'user_agent': request.user_agent.string,
                    'args': dict(request.args),
                    'view_args': request.view_args,
                    'query_string': request.query_string.decode("utf-8"),
                    'data': request.get_data(as_text=True),
                }
                
                o_file.write('{\r\n    "request": {')
                for i, (k, v) in enumerate(e_req.items()):
                    o_file.write(f'\r\n        "{k}": {json.dumps(v)}')
                    if i < len(e_req) - 1: o_file.write(",")
                o_file.write('\r\n},\r\n"payload": {')
                for k, v in e_json.items():
                    o_file.write(f'\r\n        "{k}": {json.dumps(v)}')
                    if i < len(e_req) - 1: o_file.write(",")
                o_file.write('\r\n    }\r\n}')
            
            return {"message": f"Webhook payload recorded to \"{s_file}\""}, 200
        except Exception as e:
            logger.exception(f"{utils.get_time()}\tAPI User: {s_USER}. Error trying to record sample webhook payload: {e}")
            return {"message": f"Error with code when trying to record sample webhook payload: {e}"}, 500


##### LOAD APP #####

app = Flask(__name__)
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['UPLOAD_FOLDER'] = s_MEDIA_PATH
app.config['SECRET_KEY'] = generate_key()
app.config['MAX_CONTENT_LENGTH'] = 5 * (1024 * 1024)  # 5 megabyte filesize limit

api = Api(app)
cors = CORS(
    app,
    resources=r"/*",
    allow_headers=[
        "Content-Type",
        "Authorization",
        "Access-Control-Allow-Credentials",
        "api_key",
    ],
    supports_credentials=True)

#app.config['CORS_HEADERS'] = 'Content-Type Access-Control-Allow-Credentials api_key'

#logging.getLogger('flask').level = logging.DEBUG
#logging.getLogger('flask_restful').level = logging.DEBUG
#logging.getLogger('flask_cors').level = logging.DEBUG

s_log_file = os.path.join(os.getcwd(), f'{__file__}.log')
check_log_file(s_log_file)

logging.basicConfig(filename=s_log_file, level=os.environ.get("LOGLEVEL", "INFO"))
logger = logging.getLogger("IDSTS_API")

                    
api.add_resource(EndPoints, "/", "/endpoints", "/endpoints/")
api.add_resource(HubspotOwners, "/owners", "/owners/", "/users", "/users/")

api.add_resource(PostGetContact, "/contact")
api.add_resource(GetContact, "/contact/<int:owner_id>")
api.add_resource(GetOtherContact, "/contact/<string:source>/<int:other_id>")
api.add_resource(FindContact, "/contact/find", "/contact/find/")

api.add_resource(Workflows, "/workflows", "/workflows/")
api.add_resource(WorkflowEnroll, "/workflows/enroll", "/workflows/enroll/<int:workflow_id>")

api.add_resource(SMS, "/sms", "/sms/")
api.add_resource(Postcard, "/postcard", "/postcard/")
api.add_resource(LogPhone, "/call/log", "/call/log/")

api.add_resource(Email, "/email", "/email/", "/email/log", "/email/log/")
api.add_resource(EmailNotification, "/email/ses_notification", "/email/ses_sns")

api.add_resource(HubspotTouchSummary, "/touches/summary", "/touches/summary/<int:owner_id>", "/touches/summary/hubspot/<int:hubspot_vid>")
api.add_resource(HubspotTouchDetails, "/touches/details", "/touches/details/<int:owner_id>", "/touches/details/hubspot/<int:hubspot_vid>")


api.add_resource(DevWebhook, "/dev/webhook")


@app.route('/tests/<path:path>')
@authenticate
def send_test(path):
    return send_from_directory('tests', path)


def allowed_file(filename):
    return os.path.splitext(filename)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/media/upload', methods=['GET', 'POST'])
@authenticate
def upload_file(**kwargs):

    e_header = { k.lower().replace("-", "_"): v for k, v in request.headers.items()}
    s_key = e_header.get("api_key", "")
    if not s_key: s_key = e_header.get("api key", "")
    
    if not s_key: s_key = kwargs.get("api_key", "")
    if not s_key: 
        parser = reqparse.RequestParser()
        parser.add_argument("api_key", required=False)
        req_args = parser.parse_args()
        s_key = req_args.get("api_key", "")
    
    if s_key:
        s_key = re.sub(r"(^['\" ]+|['\" ]+$)", "", s_key)

    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            logger.warn(f"{utils.get_time()}\t'No file part'")
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        
        logger.info(f"{utils.get_time()}\t'Received file: {file.filename}.")
        
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            logger.warn(f"{utils.get_time()}\t'No selected file'")
            flash('No selected file')
            return redirect(request.url)
        
        if not allowed_file(file.filename):
            logger.warn(f"{utils.get_time()}\t'Invalid file extension: {os.path.splitext(file.filename)[1]}'")
            flash('Invalid file extension: {os.path.splitext(file.filename)[1]}')
            return redirect(request.url)
            
        
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            logger.info(f"{utils.get_time()}\t'Saving file to: {os.path.join(app.config['UPLOAD_FOLDER'], filename)}'")
            logger.info(f"redirect to '{url_for('get_media', path=filename)}'")
            
            return redirect(url_for('get_media', path=filename, api_key=s_key))
    return '''
        <!doctype html>
        <title>Upload new File</title>
        <h1>Upload new File</h1>
        <form method=post enctype=multipart/form-data>
          <input type=file name=file>
          <input type=submit value=Upload>
        </form>
        '''

@app.route('/media/<path:path>')
#@authenticate
def get_media(path):
    return send_from_directory('media', path)


if __name__ == "__main__":
    args, kwargs = utils.parse_command_line_args(sys.argv)
    
    b_debug = kwargs.get("debug", True)
    s_host = kwargs.get("host", '0.0.0.0')
    i_port = kwargs.get("port", 5000)
    
    logger.info(f"{utils.get_time()}\tStart flask_idsts.py on {s_host}:{i_port}")
    app.run(host=s_host, port=i_port, debug=b_debug)
