﻿#!/opt/anaconda3/bin/python
import os, sys, re
from collections import Counter
from datetime import datetime
import dateutil.parser
import pandas as pd

try:
    from . import utils, db_tables, mysql_db, touch_utils
except:
    import utils, db_tables, mysql_db, touch_utils

try:
    from . import hubspot_api
except:
    import hubspot_api


s_DB = db_tables.s_DB  # "SandboxKE"
s_TOUCH_SUMMARY_TABLE = db_tables.s_TOUCH_SUMMARY_TABLE  # "hubspot_touch_summary"
s_PROCESS_TABLE = db_tables.s_PROCESS_TABLE # "python_scripts"

HUBSPOT_API_KEY = os.environ.get("IDSTS_HUBSPOT_API_KEY", "")
AWS_DB_USER = os.environ.get("IDSTS_AWS_DB_USER", "")
AWS_DB_PASSWORD = os.environ.get("IDSTS_AWS_DB_PASS", "")

o_DB = None
o_HUBSPOT = None

a_KEY_COLS = [
    "hubspot_vid",
    "email",
]
a_VALUE_COLS = [
#    "phonenumber",
    "last_update",
    "call_count",
    "call_last",
    "email_count",
    "email_last",
    "sms_count",
    "sms_last",
    "postcard_count",
    "postcard_last",
    "marketing_sent_count",
    "marketing_sent_last",
    "marketing_delivered_count",
    "marketing_delivered_last",
    "marketing_open_count",
    "marketing_open_last",
    "marketing_click_count",
    "marketing_click_last",
    "marketing_print_count",
    "marketing_print_last",
    "marketing_forward_count",
    "marketing_forward_last",
]


@utils.timer
def update_touch_summary_db(**kwargs):
    load_db()
    load_api()
    
    print("Getting last script run times")
    df_python_scripts = o_DB.get_table_as_df(s_PROCESS_TABLE)
    e_scripts = df_python_scripts.set_index("process").to_dict(orient="index")
    
    last_run = e_scripts.get("count_touches", {}).get("last_run", None)
    if last_run: print("Last update was run on:", last_run.isoformat())
    since = last_run
    
    if kwargs.get("since"):
        if not isinstance(kwargs["since"], datetime):
            kwargs["since"] = dateutil.parser.parse(str(kwargs["since"]))
        since = kwargs["since"]
        if last_run: print(f"Using specified 'since': {since} rather than last update run {last_run}")
    
    e_vid_email, e_email_vid = o_HUBSPOT.get_contact_emails(kwargs.get("a_all_contacts"))
    
    e_tables = dict(o_DB.list_tables())
    if s_TOUCH_SUMMARY_TABLE not in e_tables:
        create_table()
        since = None
    elif kwargs.get("overwrite") == True:
        print(f"Re-counting summaries from scratch for table: {s_TOUCH_SUMMARY_TABLE}")
        since = None
        e_existing_summaries = {}
    else:
        df_existing_touches = o_DB.get_table_as_df(s_TOUCH_SUMMARY_TABLE)
        e_existing_summaries = df_existing_touches.set_index(a_KEY_COLS).to_dict(orient="index")
    
    e_summary_rows = o_HUBSPOT.get_touch_summaries(since=since, e_vid_email=e_vid_email, e_email_vid=e_email_vid)
    
    if not e_summary_rows: return None
    # upload new or changed rows
    a_update_rows = []
    for t_id, e_touches in e_summary_rows.items():
        if not t_id or pd.isnull(t_id[0]) or not t_id[0]: continue
        if not e_touches: continue
        
        if e_existing_summaries.get(t_id):  # if person has prior counts, update those with the new counts
            e_new_counts = touch_utils.update_existing_record(e_touches, e_existing_summaries[t_id])
        
        t_row = (
            #None, # phonenumber
            datetime.now(), # last_update
            e_touches.get("call_count"), # call_count
            e_touches.get("call_last"), # call_last
            e_touches.get("email_count"), # email_count
            e_touches.get("email_last"), # email_last
            e_touches.get("sms_count"), # sms_count
            e_touches.get("sms_last"), # sms_last
            e_touches.get("postcard_count"), # postcard_count
            e_touches.get("postcard_last"), # postcard_last
            e_touches.get("marketing_sent_count"), # marketing_sent_count
            e_touches.get("marketing_sent_last"), # marketing_sent_last
            e_touches.get("marketing_delivered_count"), # marketing_delivered_count
            e_touches.get("marketing_delivered_last"), # marketing_delivered_last
            e_touches.get("marketing_open_count"), # marketing_open_count
            e_touches.get("marketing_open_last"), # marketing_open_last
            e_touches.get("marketing_click_count"), # marketing_click_count
            e_touches.get("marketing_click_last"), # marketing_click_last
            e_touches.get("marketing_print_count"), # marketing_print_count
            e_touches.get("marketing_print_last"), # marketing_print_last
            e_touches.get("marketing_forward_count"), # marketing_forward_count
            e_touches.get("marketing_forward_last"), # marketing_forward_last
            t_id[0], # hubspot_vid
            t_id[1], # email
        )
        a_update_rows.append(t_row)
    
    #o_DB.update_batch(s_TOUCH_SUMMARY_TABLE, a_VALUE_COLS, [a_KEY_COLS[0]], a_update_rows)
    o_DB.insert_batch(s_TOUCH_SUMMARY_TABLE, a_VALUE_COLS + a_KEY_COLS, a_update_rows)
    
    #o_DB.update(s_PROCESS_TABLE, {"last_run": datetime.now().isoformat()}, {"process": "count_touches"})
    o_DB.insert(s_PROCESS_TABLE, {"process": "count_touches", "script": "count_engagements.py", "last_run": datetime.now().isoformat()})

def load_db():
    global o_DB
    #print("AWS_DB", AWS_DB_USER, AWS_DB_PASSWORD)
    
    o_DB = mysql_db.mySqlDbConn(
        s_host = mysql_db._host,
        i_port = mysql_db._port,
        s_user = AWS_DB_USER,
        s_pass = AWS_DB_PASSWORD,
        s_database = s_DB,
        chunk_size = 50000,
    )
    return o_DB

def load_api():
    global o_HUBSPOT
    o_HUBSPOT = hubspot_api.HubspotAPI(api_key=HUBSPOT_API_KEY)

def create_table():
    o_DB.load()
    o_DB.create_table(
        s_TOUCH_SUMMARY_TABLE,
        [
            "hubspot_vid int PRIMARY KEY",
            "email varchar(255) UNIQUE",
            #"phonenumber text",
            "last_update datetime",
            "call_count int",
            "call_last datetime",
            "email_count int",
            "email_last datetime",
            "sms_count int",
            "sms_last datetime",
            "postcard_count int",
            "postcard_last datetime",
            "marketing_sent_count int",
            "marketing_sent_last datetime",
            "marketing_delivered_count int",
            "marketing_delivered_last datetime",
            "marketing_open_count int",
            "marketing_open_last datetime",
            "marketing_click_count int",
            "marketing_click_last datetime",
            "marketing_print_count int",
            "marketing_print_last datetime",
            "marketing_forward_count int",
            "marketing_forward_last datetime",
        ])
    o_DB.cursor.execute(f"CREATE INDEX id_index ON {s_TOUCH_SUMMARY_TABLE}(email(128))")
    o_DB.cursor.execute(f"ALTER TABLE {s_TOUCH_SUMMARY_TABLE} ADD CONSTRAINT unique_contacts UNIQUE KEY(hubspot_vid, email(128))")
    o_DB.close()


if __name__ == "__main__":  
    ''' This gets run when count_engagements.py is run from the command prompt
        Args:
            args: {list} of the positional command line options.
                [0] 
            kwargs:
                --overwrite: {bool}. Do complete analysis of all engagements/activity instead of only most recent.
                --since: {str} of date. Override last run date in db to specify datetime to begin analysis.
    '''
    #print('sys.argv', sys.argv, '\n')
    args, kwargs = utils.parse_command_line_args(sys.argv)
    
    if "since" in kwargs:
        #print(kwargs["since"])
        kwargs["since"] = dt_since = dateutil.parser.parse(str(kwargs["since"]))
        print("Update touch summaries since:", kwargs["since"])
        
    if not HUBSPOT_API_KEY:
        raise Exception("""
            The environment variable for the hubspot API key hasn't been set up.
            Remember to run 'IDSTS_HUBSPOT_API_KEY=your_hubspot_api_key' from shell or add it to your /etc/environment file.""")
    if not AWS_DB_USER:
        raise Exception("""
            The environment variable for the AWS MYSQL Db Username hasn't been set up.
            Remember to run 'AWS_DB_USER=your_aws_db_username' from shell or add it to your /etc/environment file.""")
    if not AWS_DB_PASSWORD:
        raise Exception("""
            The environment variable for the AWS MYSQL Db Password hasn't been set up.
            Remember to run 'AWS_DB_PASSWORD=your_aws_db_password' from shell or add it to your /etc/environment file.""")

    #print('args', args, '\n')
    #print('kwargs', kwargs, '\n')
    update_touch_summary_db(**kwargs)
    