﻿import re, os, sys, datetime
import requests, json, urllib
import binascii

s_API_KEY = 'f14f-3db6-49e4-2d69-9bf5-cd82-ac20-9c1c'

def generate_key(length=16):
    s_key = binascii.hexlify(os.urandom(length)).decode()
    a_pieces = []
    for i_start in range(0, len(s_key), 4):
        a_pieces.append(s_key[i_start: i_start + 4])
    return "-".join(a_pieces)


class TestIDSTSAPI:
    
    _get_parameters = {}
    _get_header = {
        "api_key": s_API_KEY
    }
    _post_header = {
        "Content-type": "application/json",
        "Accept": "application/json",
        "api_key": s_API_KEY,
    }
    def __init__(self, **kwargs):
        if kwargs.get("base_url"):
            self._base_url = kwargs["base_url"]
        else:
            s_host = kwargs.get("host", "http://localhost")
            if not s_host.lower().startswith("http"): s_host = f"http://{s_host}"
            s_port = kwargs.get("port", "5000")
            if s_port:
                self._base_url = f"{s_host}:{s_port}"
            else:
                self._base_url = f"{s_host}"
        
        self._api_key = kwargs.get("api_key", s_API_KEY)
        self._get_header["api_key"] = self._api_key
        self._get_header["_post_header"] = self._api_key
    
    def list_end_points(self, **kwargs):
        s_url = f"{self._base_url}/endpoints"
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
        
        if kwargs.get("b_debug"):
            print(s_url)
        
        o_response = requests.get(url=s_url, headers=self._get_header)
        return o_response
        
    def confirm_bad_api_key(self, **kwargs):
        s_good_url = f"{self._base_url}/sms"
        e_header = self._post_header.copy()
        e_header["api_key"] = "bad-key"
        e_data = {"contacts": [], "message": ""}
        o_response = requests.post(url=s_good_url, json=e_data, headers=e_header)
        assert o_response.status_code == 401
        return o_response
        
    def confirm_bad_url(self, **kwargs):
        s_bad_url = f"{self._base_url}/bad_url"
        e_data = {}
        o_response = requests.post(url=s_bad_url, json=e_data, headers=self._post_header)
        #print(o_response.text, o_response.status_code)
        assert o_response.status_code == 404
        return o_response
        
    def confirm_bad_method(self, **kwargs):
        s_good_url = f"{self._base_url}/sms"
        o_response = requests.get(url=s_good_url, headers=self._post_header)
        #print(o_response.text, o_response.status_code)
        assert o_response.status_code == 405
        return o_response
    
    ##### OWNERS #####
    def list_owners(self, **kwargs):
        s_url = f"{self._base_url}/owners"
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
        
        if kwargs.get("b_debug"):
            print(s_url)
        
        o_response = requests.get(url=s_url, headers=self._get_header)
        return o_response
    
    ##### WORKFLOWS #####
    def list_workflows(self, **kwargs):
        s_url = f"{self._base_url}/workflows/"
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
    
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
    
        if kwargs.get("b_debug"):
            print(s_url)
    
        o_response = requests.get(url=s_url, headers=self._get_header)
        return o_response
    
    def enroll_contact(self, **kwargs):
        workflow_id =  kwargs.get("workflow_id", 16400333)
        
        if kwargs.get("test_data"):
            s_url = f"{self._base_url}/workflows/enroll"
        else:
            s_url = f"{self._base_url}/workflows/enroll/{workflow_id}"
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
    
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
    
        e_data = {}
        
        if kwargs.get("test_data"):
            e_data["workflow_id"] = workflow_id
        
        if kwargs.get("hubspot_vid"):
            e_data["hubspot_vid"] = kwargs["hubspot_vid"]
        elif kwargs.get("test_id"):
            kwargs["hubspot_vid"] = "203401"
        
        if not e_data.get("hubspot_vid"):
            e_data["email"] = kwargs.get("email", "timothy+market@idsts.com")
        
        if kwargs.get("b_debug"):
            print(s_url)
            print(e_data)
    
        o_response = requests.post(url=s_url, json=e_data, headers=self._get_header)
        return o_response
    
    def unenroll_contact(self, **kwargs):
        workflow_id =  kwargs.get("workflow_id", 16400333)
        
        if kwargs.get("workflow_id") and not kwargs.get("test_data"):
            s_url = f"{self._base_url}/workflows/enroll/{workflow_id}"
        else:
            s_url = f"{self._base_url}/workflows/enroll"
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
    
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
    
        e_data = {}
        
        if kwargs.get("test_data"):
            e_data["workflow_id"] = workflow_id
        
        if kwargs.get("hubspot_vid"):
            e_data["hubspot_vid"] = kwargs["hubspot_vid"]
        elif kwargs.get("test_id"):
            kwargs["hubspot_vid"] = "203401"
        
        if not e_data.get("hubspot_vid"):
            e_data["email"] = kwargs.get("email", "timothy+market@idsts.com")
        
        if kwargs.get("b_debug"):
            print(s_url)
            print(e_data)
    
        o_response = requests.delete(url=s_url, json=e_data, headers=self._get_header)
        return o_response
    
    ##### MESSAGES #####
    def send_test_sms(self, **kwargs):
        s_url = f"{self._base_url}/sms"
        
        s_key = generate_key(length=8)
        e_parameters = {}
        
        e_data = {
            "contacts": kwargs.get("contacts", [
                #{"mobile": "9132652386", "firstName": "Kwame", "lastName": "Everett"},
                {"mobile": "650-814-0020", "firstName": "Tim", "lastName": "Gilbert"},
            ]),
            "message": kwargs.get("message", "Hello {{ firstName }}, this is a test via the IDSTS, Sakari, and Hubspot APIs %s" % s_key),
        }
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_endpoint}?{s_parameters}"

        if kwargs.get("b_debug"):
            print(s_url)
            print(e_data)

        o_response = requests.post(url=s_url, json=e_data, headers=self._post_header)
        return o_response
    
    def send_test_postcard(self, **kwargs):
        s_url = f"{self._base_url}/postcard"
        
        s_key = generate_key(length=8)
        e_parameters = {}
        e_data = {
            "contacts": kwargs.get("contacts", [
                {
                    "firstName": "Timothy",
                    "lastName": "Gilbert",
                    "company": "IDSTS",
                    "street1": "398 E Eaglewood Ln",
                    "city": "Mt. Jackson",
                    "state": "VA",
                    "zip": "22842",
                    "phone": "650-814-0020",
                },
            ]),
            "message": kwargs.get("message", "Hi {{ firstName }}, this is a test postcard sent via the IDSTS and Handwrite, and logged on Hubspot APIs %s" % s_key),
            "from": kwargs.get("from",
                {
                    "firstName": "The",
                    "lastName": "Dude",
                    "company": "Unemployed",
                    "street1": "25 Main Street",
                    "city": "Los Angeles",
                    "state": "CA",
                    "zip": "90210"
                }),
            "handwriting_id": "5dc30652bc08d20016f1ec33",
            "card_id": "5dc304cfbc08d20016f1ec2f",
        }
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_endpoint}?{s_parameters}"

        if kwargs.get("b_debug"):
            print(s_url)
            print(e_data)

        o_response = requests.post(url=s_url, json=e_data, headers=self._post_header)
        return o_response
    
    ##### LOGGING #####
    def log_test_call(self, **kwargs):
        s_url = f"{self._base_url}/call/log"
        
        e_parameters = {}
        e_data = {
            #"contact": "913-265-2386",
            "contact": json.dumps(kwargs.get("contact", "650-814-0020")),
            "caller": kwargs.get("caller", "555-1212"),
            "message": kwargs.get("message", "These are notes about a test call for hubspot API"),
            #"status": "COMPLETED",
            #"duration": 60000,
            #"recording_url": "",
        }
        if kwargs.get("status"): e_data["status"] = kwargs["status"]
        if kwargs.get("duration"): e_data["duration"] = kwargs["duration"]
        if kwargs.get("recording_url"): e_data["recording_url"] = kwargs["recording_url"]
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_endpoint}?{s_parameters}"

        if kwargs.get("b_debug"):
            print(s_url)
            print(e_data)

        o_response = requests.post(url=s_url, json=e_data, headers=self._post_header)
        return o_response
    
    def test_email(self, **kwargs):
        if kwargs.get("send"):
            s_url = f"{self._base_url}/email"
        else:
            s_url = f"{self._base_url}/email/log"
        
        e_parameters = {}
        e_data = {
            "contacts": kwargs.get("contacts", ["Kwame <kwame@idsts.com>", {"email": "timothy@idsts.com", "firstName": "Tim", "lastName": "Gilbert"}]),
            "cc": kwargs.get("cc", ["timothy+market@idsts.com"]),
            "from": kwargs.get("sender", {"email": "Manor Straits Info <info@manorstraits.com>"}),
            "subject": kwargs.get("subject", "This is a test email from IDSTS API"),
            "text": kwargs.get("text", "This is the raw text of the email"),
            #"html": "<div>This is the <i>HTML</i> version of the email</div>",
        }
        if kwargs.get("html"): e_data["html"] = kwargs["html"]
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_endpoint}?{s_parameters}"

        if kwargs.get("b_debug"):
            print(s_url)
            print(e_data)

        o_response = requests.post(url=s_url, json=e_data, headers=self._post_header)
        return o_response
    
    ##### TOUCHES #####
    def list_all_touch_summaries(self, **kwargs):
        s_url = f"{self._base_url}/touches/summary"
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
        
        if kwargs.get("b_debug"):
            print(s_url)
        
        o_response = requests.get(url=s_url, headers=self._get_header)
        return o_response
    
    def list_contact_touch_summary(self, **kwargs):
        owner_id = kwargs.get("owner_id", 1)
        s_url = f"{self._base_url}/touches/summary/{owner_id}"
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
        
        if kwargs.get("b_debug"):
            print(s_url)
        
        o_response = requests.get(url=s_url, headers=self._get_header)
        return o_response
    
    def list_all_touches(self, **kwargs):
        s_url = f"{self._base_url}/touches/details"
        
        #s_url = f"{self._base_url}/touches/details?event_type=CALL"
        #s_url = f"{self._base_url}/touches/details?event_type=EMAIL&event_type=CALL&keep_non_mapped=False"
        #s_url = f"{self._base_url}/touches/details?event_type=POSTCARD&keep_non_mapped=True"
        
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
        
        if kwargs.get("b_debug"):
            print(s_url)
        
        o_response = requests.get(url=s_url, headers=self._get_header)
        return o_response
    
    def list_contact_touches(self, **kwargs):
        owner_id = kwargs.get("owner_id", 1)
        s_url = f"{self._base_url}/touches/details/{owner_id}"
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
        
        if kwargs.get("b_debug"):
            print(s_url)
        
        o_response = requests.get(url=s_url, headers=self._get_header)
        return o_response
    
    
    ##### CONTACTS #####
    def get_contact(self, **kwargs):
        owner_id = kwargs.get("owner_id", 1)
        s_url = f"{self._base_url}/contact/{owner_id}"
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
        
        if kwargs.get("b_debug"):
            print(s_url)
        
        o_response = requests.get(url=s_url, headers=self._get_header)
        return o_response
    
    def get_contact_by_hubspot_id(self, **kwargs):
        hubspot_vid = kwargs.get("hubspot_vid", 1)
        s_url = f"{self._base_url}/contact/Hubspot/{hubspot_vid}"
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
        
        if kwargs.get("b_debug"):
            print(s_url)
        
        o_response = requests.get(url=s_url, headers=self._get_header)
        return o_response
    
    def get_contact_by_prospectnow_id(self, **kwargs):
        prospectnow_id = kwargs.get("prospectnow_id", 1)
        s_url = f"{self._base_url}/contact/ProspectNow/{prospectnow_id}"
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
        
        if kwargs.get("b_debug"):
            print(s_url)
        
        o_response = requests.get(url=s_url, headers=self._get_header)
        return o_response
    
    def find_uniqe_contact(self, **kwargs):
        s_url = f"{self._base_url}/contact/find"
        e_contact = kwargs.get(
            "contact", 
            {
                "firstname": "Tim",
                "lastname": "Gilbert",
                "email": "timothy@idsts.com",
                "phone":"6508140020"})
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
        
        e_data = {"contact": e_contact}
        if kwargs.get("b_debug"):
            print(s_url)
        
        o_response = requests.post(url=s_url, json=e_data, headers=self._get_header)
        return o_response
    
    def find_matching_contacts(self, **kwargs):
        s_url = f"{self._base_url}/contact/find"
        e_contact = kwargs.get(
            "contact", 
            {
                "last name": "Gilbert",
                "email": "bogus@fake.com",
                "phone":"555-800-1212"})
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
        
        e_data = {"contact": e_contact}
        if kwargs.get("b_debug"):
            print(s_url)
        
        o_response = requests.post(url=s_url, json=e_data, headers=self._get_header)
        return o_response
    
    
    def no_matching_contacts(self, **kwargs):
        s_url = f"{self._base_url}/contact/find"
        e_contact = kwargs.get(
            "contact", 
            {
                "last name": "Nothing",
                "email": "bogus@fake.com",
                "phone":"555-800-1212"})
        e_parameters = self._get_parameters.copy()
        if kwargs.get("e_parameters"): e_parameters.update(kwargs["e_parameters"])
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_url}?{s_parameters}"
        
        e_data = {"contact": e_contact}
        if kwargs.get("b_debug"):
            print(s_url)
        
        o_response = requests.post(url=s_url, json=e_data, headers=self._get_header)
        return o_response
    
    ##### WEBHOOKS #####
    def record_webhook(self, e_data, **kwargs):
        s_url = f"{self._base_url}/dev/webhook"
        
        e_parameters = {}
        
        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_endpoint}?{s_parameters}"
        
        if not e_data:
            e_data = {"sample data": True}
        
        if kwargs.get("b_debug"):
            print(s_url)
            print(e_data)
        
        e_post_header = {
            "Content-type": "application/json",
            "Accept": "application/json",
            #"api_key": s_API_KEY,
        }
        if kwargs.get("method", "str").lower() == "put":
            o_response = requests.put(url=s_url, json=e_data, headers=e_post_header)
        else:
            o_response = requests.post(url=s_url, json=e_data, headers=e_post_header)
        return o_response
