﻿try:
    from .utils import get_time
    from .flask_idsts import app, logger
except:
    from utils import get_time
    from flask_idsts import app, logger

if __name__ == "__main__":
    app.run()
    logger.info(f"{get_time()}\tStart flask_idsts.py")