﻿import re, os, sys, datetime
import requests, json, urllib

i_LIFESPAN_MINUTES, i_LIFESPAN_DAYS = 120, 0

e_EVENT_TYPES = {
    "Message Received": "message-received",
    "Message Sent": "message-sent",
    "Message Status": "message-status",
    "Contact Created": "contact-created",
    "Contact Updated": "contact-updated",
    "Contact Removed": "contact-removed",
    "Contact Opt Out": "contact-opt-out",
    "Contact Opt In": "contact-opt-in",
    "Conversation Started": "conversation-started",
    "Conversation Closed": "conversation-closed"
}

class SakariAPI:
    _base_url =  "https://api.sakari.io"
    _post_header = {
        "Content-type": "application/json",
        "Accept": "application/json",
    }
    def __init__(self, **kwargs):
        
        self._account_id = kwargs.get("account_id", os.environ.get("SAKARI_ACCOUNT_ID", ""))
        self._client_id = kwargs.get("client_id", os.environ.get("SAKARI_CLIENT_ID", ""))
        self._client_secret = kwargs.get("client_secret", os.environ.get("SAKARI_CLIENT_SECRET", ""))

        self._access_token, self._expiry = None, None
        self._access_token = self.get_access_token(**kwargs)
        print("Sakari Account ID:", self._account_id)
        print("Sakari Client ID:", self._client_id)
    
    def get_access_token(self, **kwargs):
        if self._access_token and self._expiry is not None and self._expiry > datetime.datetime.now():
            self._post_header["Authorization"] = f"{self._access_token['token_type']} {self._access_token['access_token']}"
            if kwargs.get("b_debug"): print("Using existing token.")
            return self._access_token
        elif self._access_token:
            if kwargs.get("b_debug"): print("Existing token has expired.")
            self._access_token, self._expiry = None, None
            if "Authorization" in self._post_header: del self._post_header["Authorization"]
        
        s_url = f"{self._base_url}/oauth2/token"

        e_parameters = {}
        e_header = {k:v for k,v in self._post_header.items() if k != "Authorization"}
        e_data = {
            "grant_type": "client_credentials",
            "client_id": self._client_id,
            "client_secret": self._client_secret}

        if e_parameters:
            s_parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{s_endpoint}?{s_parameters}"

        if kwargs.get("b_debug"): print("Get new access_token from:", s_url)
        
        o_response = requests.post(url=s_url, json=e_data, headers=e_header)
        if o_response.ok:
            self._access_token = json.loads(o_response.text)
            self._expiry = datetime.datetime.now() + datetime.timedelta(i_LIFESPAN_DAYS, i_LIFESPAN_MINUTES * 60)
            
            self._post_header["Authorization"] = f"{self._access_token['token_type']} {self._access_token['access_token']}"
        else:
            raise Exception("Couldn't get access_token from Sakari", o_response.status_code, o_response.text)
        
        return self._access_token
    
    ##### CONNECTION #####
    def _get_response(self, s_endpoint, e_parameters, **kwargs):
        self.get_access_token(**kwargs)
        s_parameters = urllib.parse.urlencode(e_parameters)
        s_url = f"{s_endpoint}?{s_parameters}"
        e_header = self._post_header
        if kwargs.get("header"):
            e_header = e_header.copy()
            e_header.update(kwargs["header"])
        
        return requests.get(url=s_url, headers=e_header)
    
    def _delete_response(self, s_endpoint, e_parameters, **kwargs):
        self.get_access_token(**kwargs)
        s_parameters = urllib.parse.urlencode(e_parameters)
        s_url = f"{s_endpoint}?{s_parameters}"
        e_header = self._post_header
        if kwargs.get("header"):
            e_header = e_header.copy()
            e_header.update(kwargs["header"])
        
        return requests.delete(url=s_url, headers=e_header)
    
    def _post_response(self, s_endpoint, e_data, e_parameters, **kwargs):
        self.get_access_token(**kwargs)
        s_parameters = urllib.parse.urlencode(e_parameters)
        s_url = f"{s_endpoint}?{s_parameters}"
        if kwargs.get("b_debug"): print(s_url)
        
        e_header = self._post_header
        if kwargs.get("header"):
            e_header = e_header.copy()
            e_header.update(kwargs["header"])
        
        return requests.post(url=s_url, json=e_data, headers=e_header)
    
    def _put_response(self, s_endpoint, e_data, e_parameters, **kwargs):
        self.get_access_token(**kwargs)
        s_parameters = urllib.parse.urlencode(e_parameters)
        s_url = f"{s_endpoint}?{s_parameters}"
        if kwargs.get("b_debug"): print(s_url)
        
        e_header = self._post_header
        if kwargs.get("header"):
            e_header = e_header.copy()
            e_header.update(kwargs["header"])
        
        return requests.put(url=s_url, json=e_data, headers=e_header)
    
    ##### CONTACTS #####
    def _get_contacts(self, **kwargs):
        s_url = f"{self._base_url}/v1/accounts/{self._account_id}/contacts"
        e_parameters = kwargs.get("parameters", {})
        
        if kwargs.get("offset"): e_parameters["offset"] = kwargs["offset"]
        if kwargs.get("limit"): e_parameters["limit"] = kwargs["limit"]
        if kwargs.get("contact_id"): e_parameters["contactId"] = kwargs["contact_id"]
        if kwargs.get("first_name"): e_parameters["firstName"] = kwargs["first_name"]
        if kwargs.get("last_name"): e_parameters["lastName"] = kwargs["last_name"]
        if kwargs.get("mobile"): e_parameters["mobile"] = kwargs["mobile"]
        if kwargs.get("email"): e_parameters["email"] = kwargs["email"]
        if kwargs.get("tags"): e_parameters["tags"] = kwargs["tags"]
        
        return self._get_response(s_url, e_parameters, **kwargs)

    def get_contacts(self, **kwargs):
        a_contacts = []
    
        i_batch, i_batch_size = 1, kwargs.get("batch_size", 250)
        
        o_cnts = self._get_contacts(offset=0, limit=i_batch_size, **kwargs)
        if not o_cnts.ok: return a_contacts
        e_response = json.loads(o_cnts.text)
        b_success = e_response.get("success", False)
        e_pagination = e_response.get("pagination", {})
        a_contacts.extend(e_response.get("data", []))
        
        assert i_batch_size > 0
        i_total, i_offset = e_pagination["totalCount"], len(e_response.get("data", []))
        #print("i_total", i_total)
        print(f"\tSakari Contacts Batch {i_batch}, Offset {i_offset}, Contacts {len(a_contacts)}")
        
        while i_offset < i_total:
            i_batch += 1
            print(f"\tSakari Contacts Batch {i_batch}, Offset {i_offset} Contacts {len(a_contacts)}")
            o_cnts = self._get_contacts(offset=i_offset, limit=i_batch_size, **kwargs)
            if o_cnts.ok:
                e_response = json.loads(o_cnts.text)
                e_pagination = e_response.get("pagination", {})
                i_total = e_pagination["totalCount"]
                a_contacts.extend(e_response.get("data", []))
            i_offset += i_batch_size
        
        print(f"{len(a_contacts)} Sakari Contacts")
        return a_contacts
    
    def _get_contact(self, contact_id, **kwargs):
        if not contact_id: return None
        s_url = f"{self._base_url}/v1/accounts/{self._account_id}/contacts/{contact_id}"
        e_parameters = {}
        return self._get_response(s_url, e_parameters, **kwargs)
    
    def get_contact(self, contact_id, **kwargs):
        o_cnt = self._get_contact(contact_id, **kwargs)
        if not o_cnt.ok: return {}
        e_response = json.loads(o_cnt.text)
        e_cont = e_response["data"]
        return e_cont
    
    def _update_contact(self, contact_id, e_fields, **kwargs):
        if not contact_id: return None
        s_url = f"{self._base_url}/v1/accounts/{self._account_id}/contacts/{contact_id}"
        e_parameters = {}
        return self._put_response(s_url, e_fields, e_parameters, **kwargs)
    
    def update_contact(self, contact_id, e_fields, **kwargs):
        o_cnt = self._update_contact(contact_id, e_fields, **kwargs)
        if not o_cnt.ok: return {}
        e_response = json.loads(o_cnt.text)
        e_cont = e_response["data"]
        return e_cont
    
    def _delete_contact(self, contact_id, **kwargs):
        if not contact_id: return None
        s_url = f"{self._base_url}/v1/accounts/{self._account_id}/contacts/{contact_id}"
        e_parameters = {}
        return self._delete_response(s_url, e_parameters, **kwargs)
    
    def delete_contact(self, contact_id, **kwargs):
        o_cnt = self._delete_contact(contact_id, **kwargs)
        if not o_cnt.ok: return False
        e_response = json.loads(o_cnt.text)
        return e_response.get("success", False)
    
    ##### MESSAGES #####
    def _send_message(self, a_recipients, s_message=None, **kwargs):
        s_url = f"{self._base_url}/v1/accounts/{self._account_id}/messages"
        e_parameters = kwargs.get("parameters", {})
        
        if isinstance(a_recipients, dict) and a_recipients.get("mobile"):
            a_recipients = [a_recipients]
        
        a_contacts = []
        for cont in a_recipients:
            if not cont: continue
            if isinstance(cont, str):
                a_contacts.append({"mobile": {"number": cont}})
            elif isinstance(cont, dict):
                e_cont = {
                    k: v for k, v in cont.items()
                    if k in {"id", "email", "firstName", "lastName", "mobile", "tags", "attributes"}}
                if e_cont.get("mobile"):
                    a_contacts.append(e_cont)
                else:
                    if kwargs.get("b_debug"): print("Skipping contact in unrecognized format:", cont)
            else:
                if kwargs.get("b_debug"): print("Skipping contact in unrecognized format:", cont)
        
        if not a_contacts:
            raise Exception("No valid contacts.")
        if not s_message:
            raise Exception("Missing message to be sent.")
        
        e_data = {
            "contacts": a_contacts,
            "template": s_message,
        }
        if kwargs.get("message_type", "").upper() in {"SMS", "MMS"}:
            e_data["type"] = kwargs["message_type"].upper()
        
        if kwargs.get("b_debug"): print(e_data)
        
        o_response = self._post_response(s_url, e_data, e_parameters, **kwargs)
        return o_response
    
    def send_message(self, a_recipients, s_message=None, **kwargs):
        o_response = self._send_message(a_recipients, s_message, **kwargs)
        if o_response.ok:
            return json.loads(o_response.text)
        else:
            return o_response
    
    def _get_message(self, message_id, **kwargs):
        if not message_id: return {}
        s_url = f"{self._base_url}/v1/accounts/{self._account_id}/messages/{message_id}"
        e_parameters = kwargs.get("parameters", {})
        
        o_response = self._get_response(s_url, e_parameters, **kwargs)
        return o_response
    
    def get_message(self, message_id, **kwargs):
        o_response = self._get_message(message_id, **kwargs)
        if o_response.ok:
            e_response = json.loads(o_response.text)
            return e_response.get("data", {})
        else:
            return o_response
    
    def _get_messages(self, **kwargs):
        s_url = f"{self._base_url}/v1/accounts/{self._account_id}/messages"
        e_parameters = kwargs.get("parameters", {})
        
        if kwargs.get("offset"): e_parameters["offset"] = kwargs["offset"]
        if kwargs.get("limit"): e_parameters["limit"] = kwargs["limit"]
        if kwargs.get("contact_id"): e_parameters["contactId"] = kwargs["contact_id"]
        if kwargs.get("conversation_id"): e_parameters["conversationId"] = kwargs["conversation_id"]
        
        o_response = self._get_response(s_url, e_parameters, **kwargs)
        return o_response

    def get_messages(self, **kwargs):
        a_messages = []
    
        i_batch, i_batch_size = 1, kwargs.get("batch_size", 250)
        
        o_msgs = self._get_messages(offset=0, limit=i_batch_size, **kwargs)
        if not o_msgs.ok: return a_messages
        e_response = json.loads(o_msgs.text)
        b_success = e_response.get("success", False)
        e_pagination = e_response.get("pagination", {})
        a_messages.extend(e_response.get("data", []))
        
        assert i_batch_size > 0
        i_total, i_offset = e_pagination["totalCount"], len(e_response.get("data", []))
        #print("i_total", i_total)
        print(f"\tSakari Messages Batch {i_batch}, Offset {i_offset}, Messages {len(a_messages)}")
        
        while i_offset < i_total:
            i_batch += 1
            print(f"\tSakari Messages Batch {i_batch}, Offset {i_offset} Messages {len(a_messages)}")
            o_msgs = self._get_messages(offset=i_offset, limit=i_batch_size, **kwargs)
            if o_msgs.ok:
                e_response = json.loads(o_msgs.text)
                e_pagination = e_response.get("pagination", {})
                i_total = e_pagination["totalCount"]
                a_messages.extend(e_response.get("data", []))
            i_offset += i_batch_size
        
        print(f"{len(a_messages)} Sakari Messages")
        
        return a_messages

    ##### CONVERSATIONS #####
    
    def _get_conversation(self, conversation_id, **kwargs):
        if not conversation_id: return {}
        s_url = f"{self._base_url}/v1/accounts/{self._account_id}/conversations/{conversation_id}"
        e_parameters = kwargs.get("parameters", {})
        
        o_response = self._get_response(s_url, e_parameters, **kwargs)
        return o_response
    
    def get_conversation(self, conversation_id, **kwargs):
        o_response = self._get_conversation(conversation_id, **kwargs)
        if o_response.ok:
            e_response = json.loads(o_response.text)
            return e_response.get("data", {})
        else:
            return o_response
    
    def _get_conversations(self, **kwargs):
        s_url = f"{self._base_url}/v1/accounts/{self._account_id}/conversations"
        e_parameters = kwargs.get("parameters", {})
        
        if kwargs.get("offset"): e_parameters["offset"] = kwargs["offset"]
        if kwargs.get("limit"): e_parameters["limit"] = kwargs["limit"]
        if kwargs.get("contact_id"): e_parameters["contactId"] = kwargs["contact_id"]
        if kwargs.get("conversation_id"): e_parameters["conversationId"] = kwargs["conversation_id"]
        
        o_response = self._get_response(s_url, e_parameters, **kwargs)
        return o_response

    def get_conversations(self, **kwargs):
        a_conversations = []
    
        i_batch, i_batch_size = 1, kwargs.get("batch_size", 250)
        
        o_msgs = self._get_conversations(offset=0, limit=i_batch_size, **kwargs)
        if not o_msgs.ok: return a_conversations
        e_response = json.loads(o_msgs.text)
        b_success = e_response.get("success", False)
        e_pagination = e_response.get("pagination", {})
        a_conversations.extend(e_response.get("data", []))
        
        assert i_batch_size > 0
        i_total, i_offset = e_pagination["totalCount"], len(e_response.get("data", []))
        #print("i_total", i_total)
        print(f"\tSakari Conversations Batch {i_batch}, Offset {i_offset}, Conversations {len(a_conversations)}")
        
        while i_offset < i_total:
            i_batch += 1
            print(f"\tSakari Conversations Batch {i_batch}, Offset {i_offset} Conversations {len(a_conversations)}")
            o_msgs = self._get_conversations(offset=i_offset, limit=i_batch_size, **kwargs)
            if o_msgs.ok:
                e_response = json.loads(o_msgs.text)
                e_pagination = e_response.get("pagination", {})
                i_total = e_pagination["totalCount"]
                a_conversations.extend(e_response.get("data", []))
            i_offset += i_batch_size
        
        print(f"{len(a_conversations)} Sakari Conversations")
        
        return conversations

    ##### WEBHOOKS #####
    
    def _get_webhooks(self, **kwargs):
        s_url = f"{self._base_url}/v1/accounts/{self._account_id}/webhooks"
        e_parameters = kwargs.get("parameters", {})
        
        #if kwargs.get("offset"): e_parameters["offset"] = kwargs["offset"]
        #if kwargs.get("limit"): e_parameters["limit"] = kwargs["limit"]
        
        o_response = self._get_response(s_url, e_parameters, **kwargs)
        return o_response

    def get_webhooks(self, **kwargs):
        a_webhooks = []
        
        o_response = self._get_webhooks(**kwargs)
        if not o_response.ok: return a_webhooks
        
        
        e_response = json.loads(o_response.text)
        b_success = e_response.get("success", False)
        e_pagination = e_response.get("pagination", {})
        a_webhooks.extend(e_response.get("data", []))
        
        return a_webhooks
    
    def add_webhook(self, target_url, a_event_types=None, **kwargs):
        s_url = f"{self._base_url}/v1/accounts/{self._account_id}/webhooks"
        if kwargs.get("b_debug"): print("s_url", s_url)
        e_data = {"url": target_url}
        if kwargs.get("b_debug"): print("e_data", e_data)
        #a_event_types = ["message-received"]
        if a_event_types:
            e_data["eventTypes"] = [str(x).lower() for x in a_event_types if x]
        else:
            e_data["eventTypes"] = [
                'message-received',
                'message-sent',
                'message-status',
                'contact-created',
                'contact-updated',
                'contact-removed',
                'contact-opt-out',
                'contact-opt-in',
                'conversation-started',
                'conversation-closed']
        
        assert all(x in e_EVENT_TYPES.values() for x in e_data["eventTypes"])
        
        e_parameters = kwargs.get("parameters", {})
        
        o_response = self._post_response(s_url, e_data, e_parameters, **kwargs)
        return o_response
    
    def remove_webhook(self, target_url, **kwargs):
        s_target_url = urllib.parse.quote_plus(target_url)
        s_url = f"{self._base_url}/v1/accounts/{self._account_id}/webhooks/{s_target_url}"
        if kwargs.get("b_debug"): print("s_url", s_url)
        e_parameters = kwargs.get("parameters", {})
        
        o_response = self._delete_response(s_url, e_parameters)
        return o_response
    