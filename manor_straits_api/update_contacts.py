﻿import pyximport; pyximport.install()

import os, sys, re
from collections import Counter, defaultdict
from datetime import datetime
from time import time
import dateutil.parser
import pandas as pd
import numpy as np

try:
    from . import utils, standardization, mysql_db, db_tables, find_contact
except:
    import utils, standardization, mysql_db, db_tables, find_contact

try:
    from . import hubspot_api
except:
    import hubspot_api

s_DB = db_tables.s_DB  # "SandboxKE"

s_FIELD_MAPS_TABLE = db_tables.s_FIELD_MAPS_TABLE  # "field_maps"
s_OWNERS_TABLE = db_tables.s_OWNERS_TABLE  # "test_owners"
s_OWNER_MAPS_TABLE =  db_tables.s_OWNER_MAPS_TABLE  # "test_owner_maps"
s_PROPERTIES_TABLE = db_tables.s_PROPERTIES_TABLE  # "properties"
s_PROCESS_TABLE = db_tables.s_PROCESS_TABLE  # "python_scripts"


s_DB_SOURCES = "turnover"
e_SOURCE_TABLES = {
    "ProspectNow": "ProspectNow",
    "Infutor": "Infutor",
    "RealtyMole": "RealtyMole",
}

AWS_DB_USER = os.environ.get("IDSTS_AWS_DB_USER", "")
AWS_DB_PASSWORD = os.environ.get("IDSTS_AWS_DB_PASS", "")

HUBSPOT_API_KEY = os.environ.get("IDSTS_HUBSPOT_API_KEY", "")

o_UPDATER = None
o_DB = None
o_DB_SOURCES = None
o_HUBSPOT = None

b_USE_LOW_MEMORY = True


def add_contacts_to_master(**kwargs):
    n_start = time()
    global o_UPDATER
    if o_UPDATER is None:
        o_UPDATER = ContactUpdater(**kwargs)
    
    # add new contacts from Hubspot to master "owners" table
    if kwargs.get("hubspot", True):
        o_UPDATER.hubspot_to_owners(**kwargs)
    
    # add new contacts or match existing ones from ProspectNow to master "owners" table
    if kwargs.get("prospectnow", True):
        o_UPDATER.prospectnow_to_owners(**kwargs)
    
    # add new contacts or match existing ones from ProspectNow to master "owners" table
    if kwargs.get("infutor", True):
        o_UPDATER.infutor_to_owners(**kwargs)
    
    if o_DB is not None:
        o_DB.insert(s_PROCESS_TABLE, {"process": "update_contacts", "script": "update_contacts.py", "last_run": datetime.now().isoformat()})
    
    print("Updating took:", utils.seconds_to_str(time() - n_start))

class ContactUpdater():
    def __init__(self, **kwargs):
    
        self._hubspot = None
        self._db, self._db_sources, self._db_tables = None, None, {}
        
        self._low_memory = kwargs.get("low_memory", b_USE_LOW_MEMORY)
        
        if self._low_memory: assert not kwargs.get("overwrite")
        
        self._owner_maps, self._maps_owners = {}, {}
        self._existing_owners, self._owner_cols = None, []
        self._next_owner_id = 1
        
        #self._owner_ids
        self._owner_names, self._owner_emails = {}, {}
        self._owner_phones, self._owner_addresses = {}, {}
        
        self._hubspot_fields, self._prospectnow_fields, self._infutor_fields = {}, {}, {}
        
        n_start = time()
        self._load_dbs(**kwargs)
        self._load_apis(**kwargs)
        
        self._contact_finder = find_contact.ContactFinder()
        
        self.read_field_maps(**kwargs)
        self.read_owner_maps(**kwargs)
        
        self.read_existing_owners(**kwargs)
        print("Loading ContactUpdater:", utils.seconds_to_str(time() - n_start), "\n")
    
    ##### Read, process, and search existing owner records #####
    
    def read_field_maps(self, **kwargs):
        self._df_field_maps = o_DB.get_table_as_df("field_maps", exact=True)
    
    def read_owner_maps(self, **kwargs):
        ''' Read the table of that maps owner records to other source tables or external APIs
            Create dictionary indexes for looking up by owner id then source name, or by source name and then source id)
        '''
        print("Getting owner maps")
        df_owner_maps = self._db.get_table_as_df(s_OWNER_MAPS_TABLE, exact=True)
        i_rows, i_owners = len(df_owner_maps), len(set(df_owner_maps["owner_id"]))
        i_sources, i_source_ids = len(set(df_owner_maps["other_table"])),  len(set(df_owner_maps["other_id"]))
        print(f"There are {i_rows:,} maps between {i_owners:,} owner records and {i_source_ids:,} other records across {i_sources:,} sources")
        
        self._owner_maps, self._maps_owners = defaultdict(lambda: defaultdict(set)), defaultdict(lambda: defaultdict(set))
        for idx, other_table, other_id, owner_id in df_owner_maps.itertuples():
            self._owner_maps[owner_id][other_table].add(other_id)
            self._maps_owners[other_table][other_id].add(owner_id)
    
    def read_existing_owners(self, **kwargs):
        ''' Read the table of existing owner records in a DataFrame,
            and also create some pre-indexes of names, emails, phones, and addresses for fast lookups '''
        print("Getting existing owners")
        
        self._db.load()
        if not self._owner_cols:
            self._owner_cols = self._db.get_table_cols(s_OWNERS_TABLE)
        
        if self._low_memory:
            i_owners = self._db.get_table_row_est(s_OWNERS_TABLE, exact=True)
            print(f"There are {i_owners:,} existing owners")
        else:
            self._existing_owners = self._db.get_table_as_df(s_OWNERS_TABLE, exact=True)
            #self._owner_cols = list(self._existing_owners.columns)
            print(f"There are {len(self._existing_owners):,} existing owners")
            
            if "index" not in self._existing_owners.columns: self._existing_owners.insert(0, "index", self._existing_owners["id"])
            self._existing_owners = self._existing_owners.set_index("index")
            self._owners = self._existing_owners.to_dict(orient="index")
            
            a_full_names = self._existing_owners["owner_first_name"] + " " + self._existing_owners["owner_last_name"]
            if "owner_name" in self._existing_owners.columns:
                self._existing_owners["owner_name"] = self._existing_owners["owner_name"].fillna(a_full_names)
            else:
                self._existing_owners.insert(1, "owner_name", a_full_names)
            
            self._existing_owners["owner_name"] = self._existing_owners["owner_name"].str.strip().fillna("")
        
            #a_owner_rows = self._existing_owners.to_dict(orient="records")
            self.update_owner_lookups(self._owners, clear_old=True)
    
    def update_owner_lookups(self, owners, clear_old=False, **kwargs):
        ''' Update the quick lookup indexes for existing owners
            Arguments:
                owners: {list} of row dicts containing standardized owner values or
                        {pd.DataFrame} with standard-name owner columns from master contacts table
                clear_old: {boolean} to clear any already loaded lookup indexes
        '''
        if clear_old:
            #self._owner_ids = {}
            self._owner_names, self._owner_emails  = {}, {}
            self._owner_phones, self._owner_addresses = {}, {}
            self._owner_hubspot_vids = {}
        
        if self._low_memory:
            self._next_owner_id = o_DB.execute(f"SELECT Auto_increment FROM information_schema.tables WHERE table_name='{s_OWNERS_TABLE}'")[0][0]
            return
        
        b_print_counts = kwargs.get("print_counts", True)
        if isinstance(owners, list):
            e_owner_rows = {e_row["id"]: {k: v for k, v in e_row.items() if not pd.isnull(v)} for e_row in owners}
        elif isinstance(owners, pd.DataFrame):
            e_owner_rows = df_to_sparse(owners)
        elif isinstance(owners, dict):
            e_owner_rows = {e_row["id"]: {k: v for k, v in e_row.items() if not pd.isnull(v)} for e_row in list(owners.values())}
        else:
            raise Exception("Unknown/unsupported owner input data", type(owners))
        print("Updating lookup indexes")
        
        
        self._owners.update(e_owner_rows)
        #if b_print_counts: print("Remove empty fields and make sure rows are in self._owners")
        
        self._owner_hubspot_vids.update({e_row["hubspot_vid"]: e_row["id"] for e_row in e_owner_rows.values() if e_row.get("hubspot_vid")})
        if b_print_counts: print(f"There are {len(self._owner_hubspot_vids):,} existing hubspot_vids to match on")
        
        self._owner_emails.update({e_row["owner_email"].lower(): e_row["id"] for e_row in e_owner_rows.values() if e_row.get("owner_email")})
        self._owner_emails.update({e_row["owner_email_2"].lower(): e_row["id"] for e_row in e_owner_rows.values() if e_row.get("owner_email_2")})
        self._owner_emails.update({e_row["owner_email_3"].lower(): e_row["id"] for e_row in e_owner_rows.values() if e_row.get("owner_email_3")})
        if b_print_counts: print(f"There are {len(self._owner_emails):,} existing owner emails to match on")
        
        self._owner_phones.update({e_row["owner_phone"]: e_row["id"] for e_row in e_owner_rows.values() if e_row.get("owner_phone")})
        self._owner_phones.update({e_row["owner_phone_2"]: e_row["id"] for e_row in e_owner_rows.values() if e_row.get("owner_phone_2")})
        self._owner_phones.update({e_row["owner_phone_3"]: e_row["id"] for e_row in e_owner_rows.values() if e_row.get("owner_phone_3")})
        if b_print_counts: print(f"There are {len(self._owner_phones):,} existing owner phone numbers to match on")
        
        self._owner_names.update({e_row["owner_first_name"].lower(): e_row["id"] for e_row in e_owner_rows.values() if e_row.get("owner_first_name")})
        self._owner_names.update({e_row["owner_last_name"].lower(): e_row["id"] for e_row in e_owner_rows.values() if e_row.get("owner_last_name")})
        self._owner_names.update({e_row["owner_name"].lower(): e_row["id"] for e_row in e_owner_rows.values() if e_row.get("owner_name")})
        self._owner_names.update({
            "{}, {}".format(e_row["owner_last_name"].lower(), e_row["owner_last_name"].lower()): e_row["id"]
            for e_row in e_owner_rows.values() if e_row.get("owner_first_name") and e_row.get("owner_last_name")})
        if b_print_counts: print(f"There are {len(self._owner_names):,} existing owner name pieces to match on")
        
        self._owner_addresses.update({
            (
                e_row["owner_address"].lower() if e_row.get("owner_address", "") else "",
                e_row["owner_city"].lower() if e_row.get("owner_city", "") else "",
                e_row["owner_state"].lower() if e_row.get("owner_state", "") else "",
            ): e_row["id"]
            for e_row in e_owner_rows.values()
            if e_row.get("owner_address", "") and (e_row.get("owner_city", "") or e_row.get("owner_state", ""))
        })
        self._owner_addresses.update({
            (
                e_row["owner_address"].lower() if e_row.get("owner_address", "") else "",
                e_row["owner_zip"].lower() if e_row.get("owner_city", "") else "",
            ): e_row["id"]
            for e_row in e_owner_rows.values()
            if e_row.get("owner_address", "") and e_row.get("owner_zip", "")
        })
        if b_print_counts: print(f"There are {len(self._owner_addresses):,} existing owner address pieces to match on")
        
        self._next_owner_id = max(self._owners.keys()) + 1 if self._owners else 1
    
    def find_matching_contact(self, e_row, **kwargs):
        ''' find an existing owner using email, name, phone, or address
            Arguments:
                e_row: {dict} using standard_name fields as keys and containing contact information to try to match against
            Returns:
                {int} an id number of a match owner db record or None
        '''
        if self._low_memory:
            a_matches = self._contact_finder.find_contact(e_row, return_records=False)
            if a_matches and len(a_matches) == 1: return a_matches[0]
        else:
        
            s_email = e_row["owner_email"].lower() if e_row.get("owner_email") else None
            if s_email and s_email in self._owner_emails: return self._owner_emails[s_email]
            s_email = e_row["owner_email_2"].lower() if e_row.get("owner_email_2") else None
            if s_email and s_email in self._owner_emails: return self._owner_emails[s_email]
            s_email = e_row["owner_email_3"].lower() if e_row.get("owner_email_3") else None
            if s_email and s_email in self._owner_emails: return self._owner_emails[s_email]
            
            
            s_phone = e_row["owner_phone"].lower() if e_row.get("owner_phone") else None
            if s_phone and s_phone in self._owner_phones: return self._owner_phones[s_phone]
            s_phone = e_row["owner_phone_2"].lower() if e_row.get("owner_phone_2") else None
            if s_phone and s_phone in self._owner_phones: return self._owner_phones[s_phone]
            s_phone = e_row["owner_phone_3"].lower() if e_row.get("owner_phone_3") else None
            if s_phone and s_phone in self._owner_phones: return self._owner_phones[s_phone]
            
            s_name = e_row["owner_name"].lower() if e_row.get("owner_name") else None
            if s_name and s_name in self._owner_names: return self._owner_names[s_name]
            
            if e_row.get("owner_first_name") and e_row.get("owner_last_name"):
                s_name = "{} {}".format(e_row["owner_first_name"], e_row["owner_last_name"])
                if s_name in self._owner_names: return self._owner_names[s_name]
                s_name = "{}, {}".format(e_row["owner_last_name"], e_row["owner_first_name"])
                if s_name in self._owner_names: return self._owner_names[s_name]
                
            #s_name = e_row.get("owner_last_name").lower()
            #if s_name and s_name in self._owner_names: return self._owner_names[s_name]
            #s_name = e_row.get("owner_first_name").lower()
            #if s_name and s_name in self._owner_names: return self._owner_names[s_name]
            
            t_address = (
                e_row["owner_address"].lower() if e_row.get("owner_address", "") else "",
                e_row["owner_city"].lower() if e_row.get("owner_city", "") else "",
                e_row["owner_state"].lower() if e_row.get("owner_state", "") else "",)
            if t_address[0] and t_address in self._owner_addresses: return self._owner_addresses[t_address]
            
            t_address_partial = (t_address[0], "", t_address[2])
            if t_address[0] and t_address_partial in self._owner_addresses: return self._owner_addresses[t_address_partial]
            t_address_partial = (t_address[0], t_address[1], "")
            if t_address[0] and t_address_partial in self._owner_addresses: return self._owner_addresses[t_address_partial]
            
            t_address = (
                e_row["owner_address"].lower() if e_row.get("owner_address", "") else "",
                e_row["owner_zip"].lower() if e_row.get("owner_zip", "") else "",)
            if all(x for x in t_address) and t_address in self._owner_addresses: return self._owner_addresses[t_address]
            return None
    
    ##### Standardize and Deduplicate #####
    
    def standardize_owner(self, e_owner, **kwargs):
        return standardization.standardize_owner(e_owner, **kwargs)
    
    def deduplicate_rows(
        self,
        a_rows,
        a_unique_fields=[("owner_name", "owner_city"), ("owner_first_name", "owner_last_name", "owner_city"), ("owner_email",), ("owner_phone",)],
        **kwargs
    ):
        '''
            Arguments:
                a_rows: {list} of {dict} rows with standard_name fields
                a_unique_fields: {list} of fields to dedupe rows based on the unique combination of these field's values
        '''
        #print(len(a_rows))
        for t_combo_fields in a_unique_fields:
            e_unique_owners = defaultdict(dict)
            a_empty_index_rows = []
            
            for e_row in a_rows:
                t_key = tuple(e_row.get(k) for k in t_combo_fields)
                if not any(x for x in t_key):
                    a_empty_index_rows.append(e_row)
                    continue
                #print(t_combo_fields, t_key)
                e_unique_owners[t_key].update({k: v for k, v in e_row.items() if not pd.isnull(v)})
            a_rows = list(e_unique_owners.values()) + a_empty_index_rows

            
            #print(t_combo_fields, len(a_rows), a_rows)
        return a_rows
    
    ##### Upload to master owners db #####
    
    def upload_new_contacts(self, a_new_rows, b_make_unique=True, b_add_id=True, b_update_lookups=True, **kwargs):
        ''' update the master contacts table by appending new rows.
            Arguments:
                a_new_rows: {list} of {dict} rows with standard_name fields
                a_unique_fields: {list} of fields to dedupe rows based on the unique combination of these field's values
                b_add_id: {boolean} add owner id numbers rather than 
            Returns:
                {list} of rows dicts with values and optionally added id
        '''
        if not a_new_rows: return []
        
        if b_make_unique:
            if "a_unique_fields" in kwargs:
                a_new_rows = self.deduplicate_rows(a_new_rows, a_unique_fields=kwargs["a_unique_fields"])
            else:
                a_new_rows = self.deduplicate_rows(a_new_rows)
        print(f"There are {len(a_new_rows)} unique contacts after deduplication.")
        a_owner_cols = self._owner_cols if b_add_id else [x for x in self._owner_cols if x != "id"]
        
        # add projected id numbers rather than using MYSQL's AUTO_INCREMENT
        if b_add_id:
            for i_row, e_row in enumerate(a_new_rows):
                e_row["id"] = self._next_owner_id
                self._next_owner_id += 1
        
        # transform and upload batch to DB
        a_new_tuple_rows = [
            tuple(e_row.get(col) for col in a_owner_cols)
            for e_row in a_new_rows]
        #print("new_tuple_rows", len(a_new_tuple_rows), a_new_tuple_rows[:10])
        self._db.insert_batch(s_OWNERS_TABLE, a_owner_cols, a_new_tuple_rows)
        print(f"Added {len(a_new_tuple_rows):,} new contacts to owners table.")
        
        # add the new rows to the lookup indexes
        if b_update_lookups:
            self.update_owner_lookups(a_new_rows, clear_old=False, print_counts=False)
        
        return a_new_rows
    
    def upload_updated_contacts(self, a_updated_rows, b_fill_null=True, b_update_lookups=True, **kwargs):
        ''' update the master contacts table using rows.
            Arguments:
                a_updated_rows: {list} of {dict} rows with new/added values which must contain valid "id" of master contact record
                b_fill_null: {boolean} to only place in values which are blank in the current master contacts rather than change existing values.
            Returns:
                {list} of rows dicts with updated values
        '''
        e_updated_rows = {e_row["id"]: e_row for e_row in a_updated_rows if e_row.get("id") not in {"", None, np.nan}}
        if not e_updated_rows: return []
        
        # filter down the existing to just the ones with updates
        #e_existing_rows = {
        #    owner_id: e_row
        #    for owner_id, e_row in self._owners.items()
        #    if owner_id in e_updated_rows}
        
        a_existing_rows = self._db.get_rows(s_OWNERS_TABLE, "id", list(e_updated_rows.keys()))
        e_existing_rows = {t_row[0]: dict(zip(self._owner_cols, t_row)) for t_row in a_existing_rows} if a_existing_rows else {}
        
        c_table_columns = set(self._owner_cols)
        
        a_output_rows = []
        for row_id, e_updated in e_updated_rows.items():
            e_output = {}
            if row_id not in e_existing_rows:
                print(f'The id {row_id} is missing from the existing records. Either the row was deleted, or the input of existing rows was improperly filtered before being passed in.')
                continue  # have to skip rows that don't actually match to the ids of existing rows
            
            if b_fill_null:
                e_output = {k: v for k, v in e_existing_rows[row_id].items() if not pd.isnull(v) and v not in {""} and k in c_table_columns}
                e_output.update({k: v for k, v in e_updated.items() if k not in e_output and not pd.isnull(v) and v not in {""} and k in c_table_columns})
            else:
                e_output = {k: v for k, v in e_existing_rows[row_id].items() if not pd.isnull(v) and v not in {""} and k in c_table_columns}
                e_output.update({k: v for k, v in e_updated.items() if not pd.isnull(v) and v not in {""} and k in c_table_columns})
            
            a_output_rows.append(e_output)
        
        a_val_cols = [col for col in self._owner_cols if col != "id"]
        a_key_cols = ["id"]
        a_output_tuple_rows = [
            tuple(e_row.get(col) for col in a_val_cols + a_key_cols)
            for e_row in a_output_rows]
        
        self._db.update_batch(s_OWNERS_TABLE, a_val_cols, a_key_cols, a_output_tuple_rows)
        
        # add the updated rows to the lookup indexes
        if b_update_lookups:
            self.update_owner_lookups(a_output_rows, clear_old=False, print_counts=False)
        
        return a_output_rows
    
    def upload_maps(self, a_rows, s_source, s_key, **kwargs):
        ''' update the master contacts table using rows.
            Arguments:
                a_rows: {list} of {dict} rows with new/added values which must contain valid "id" of master contact record
                s_source: {str} the name of the source of the other item
                s_key: {str} the name of the key in each
        '''
        a_owner_maps = [
            (s_source, e_row[s_key], e_row["id"])
            for e_row in a_rows
            if e_row.get(s_key)]
        
        # add the maps for the existing records
        if a_owner_maps:
            self._db.insert_batch(
                s_OWNER_MAPS_TABLE,
                ["other_table", "other_id", "owner_id"],
                a_owner_maps)
        
        for other_table, other_id, owner_id in a_owner_maps:
            self._owner_maps[owner_id][other_table].add(other_id)
            self._maps_owners[other_table][other_id].add(owner_id)
        
        return a_owner_maps
    
    ##### Update from APIs #####
    
    def hubspot_to_owners(self, **kwargs):
        ''' Read the current owners on hubspot and add ones to the owners table if they don't already exist '''
        n_start = time()
        a_properties = [
            "firstname", "lastname", "company",
            "email", "website",
            "mobilephone", "phone", #"fax",
            "address", "city", "state", "zip", "country",
            #"hubspot_owner_id", "num_contacted_notes",
        ]
        a_hubspot_contacts = self._hubspot.get_contacts(properties=a_properties, **kwargs)
        
        df_hubspot_fields = self._df_field_maps[self._df_field_maps["source"].str.contains("Hubspot")].set_index("field_name").dropna(subset=["standard_name"])
        self._hubspot_fields = df_hubspot_fields[df_hubspot_fields["standard_name"] != ""].to_dict(orient="index")
        
        #e_hubspot_owners = self._existing_owners.dropna(subset=["hubspot_vid"]).set_index("hubspot_vid").to_dict(orient="index")
        e_hubspot_owners = self._maps_owners["Hubspot"]
        
        # Find new hubspot contacts that aren't already directly indexed against owners
        a_new_owners = []
        i_already_present = 0
        for e_contact in a_hubspot_contacts:
            e_row = {
                k: v.get("value")
                if isinstance(v, dict) else v
                for k, v in e_contact.items()
                if k != "properties" and k in self._hubspot_fields}
                
            if e_row["vid"] in e_hubspot_owners:  #contact already exists
                i_already_present += 1
                continue # maybe later add update check against properties
            
            e_row.update({
                k: v.get("value") if isinstance(v, dict) else v
                for k, v in e_contact.get("properties", {}).items()
                if k in self._hubspot_fields
            })
            
            e_row = {self._hubspot_fields[k]["standard_name"]: v for k, v in e_row.items() if k in self._hubspot_fields and v}
            e_row = self.standardize_owner(e_row)
            
            a_new_owners.append(e_row)
        
        if a_new_owners:
            # Add any new contacts to owners db
            a_added_owners = self.upload_new_contacts(a_new_owners, b_make_unique=False)
            print(f"{len(a_added_owners):,} new hubspot contacts to add to master contacts. {i_already_present:,} were already present.")
            
            # update maps between owners and Hubspot contacts list
            a_owner_maps = self.upload_maps(a_added_owners, "Hubspot", "hubspot_vid")
        else:
            print("No new hubspot contacts to add to master contacts")
        
        print("Updating Hubspot contacts to owners took:", utils.seconds_to_str(time() - n_start))
        print("\n")
    
    ##### Update from DB Tables #####
    
    def prospectnow_to_owners(self, **kwargs):
        ''' Read the current owners from Prospect Now DB table and add ones to the owners table if they don't already exist '''
        n_start = time()
        df_prospectnow_fields = self._df_field_maps[
            (self._df_field_maps["source"].str.contains("ProspectNow")) &
            (self._df_field_maps["field_type"] == "Owner")
        ].set_index("field_name").dropna(subset=["standard_name"])
        self._prospectnow_fields = df_prospectnow_fields[df_prospectnow_fields["standard_name"] != ""].to_dict(orient="index")
        
        #print("Getting Prospect Now Table")
        #df_prospect_now = self._db_sources.get_table_as_df(e_SOURCE_TABLES["ProspectNow"], exact=True, **kwargs)
        #if kwargs.get("maximum", 0) > 0:
        #    df_prospect_now = df_prospect_now.iloc[:kwargs["maximum"]]
        
        print("Getting Prospect Now Table in batches")
        self._db_sources.load()
        a_cols = self._db_sources.get_table_cols(e_SOURCE_TABLES["ProspectNow"], **kwargs)
        i_batch = 0
        
        s_where = f"""
WHERE {db_tables.s_DB_SOURCES}.{db_tables.e_SOURCE_TABLES["ProspectNow"]}.id NOT IN
(SELECT other_id FROM {db_tables.s_DB}.{s_OWNER_MAPS_TABLE} WHERE other_table = "{db_tables.e_SOURCE_TABLES["ProspectNow"]}")"""

        for a_rows in self._db_sources.yield_table(e_SOURCE_TABLES["ProspectNow"], i_batch_size=kwargs.get("batch_size", 10000), where=s_where, exact=True):
            i_batch += 1
            df_prospect_now = pd.DataFrame(a_rows, columns=a_cols)
            
            # filter down to the important owner columns
            df_prospect_now = df_prospect_now[[col for col in df_prospect_now.columns if col == "id" or col in self._prospectnow_fields.keys()]]
            
            e_pn_owners = self._maps_owners[e_SOURCE_TABLES["ProspectNow"]]
            
            # split the prospectnow owners into ones that already match existing owner records and those that are new to be added
            a_new_owners, a_link_owners, a_incomplete_owners = [], [], []
            i_already_present = 0
            i_batch_size = min(10000, kwargs["maximum"] / 10) if kwargs.get("maximum", 0) > 0 else 10000
            for i_row, (idx, o_pn_row) in enumerate(df_prospect_now.iterrows()):
                if i_row % i_batch_size == 0 and i_row > 0:
                    print(f"Standardizing Prospect Now owners: {i_row:,} of {len(df_prospect_now):,} ({(i_row + 1) / len(df_prospect_now):0.1%})")
                e_pn_row = dict(o_pn_row)
                
                if e_pn_row["id"] in e_pn_owners:
                    i_already_present += 1
                    continue
                
                e_row = {self._prospectnow_fields[k]["standard_name"]: v for k, v in e_pn_row.items() if(k in self._prospectnow_fields) and not pd.isnull(v) and v}
                e_row["id_prospectnow"] = e_pn_row["id"]
                e_row = self.standardize_owner(e_row)
                
                i_existing_match = self.find_matching_contact(e_row)
                if i_existing_match is None:
                    if find_contact.sufficient_owner_information(e_row):
                        a_new_owners.append(e_row)
                    else:
                        a_incomplete_owners.append(e_row)
                else:
                    e_row["id"] = i_existing_match
                    a_link_owners.append(e_row)
            
            # update the existing owner records
            a_updated_owners = self.upload_updated_contacts(a_link_owners)
            if a_updated_owners:
                print(f"Batch {i_batch}: {len(a_updated_owners):,} updated Prospect Now contacts to add to master contacts. {i_already_present:,} were already present and skipped.")
            else:
                print(f"Batch {i_batch}: {i_already_present:,} Prospect Now contacts were already present and skipped.")
                
            if a_new_owners:
                print(f"Batch {i_batch}: {len(a_new_owners):,} Prospect Now contacts are unmapped/unmatched and should be checked for new unique contacts.")
            
            if a_incomplete_owners:
                print(f"Batch {i_batch}: {len(a_incomplete_owners):,} Prospect Now contacts contain too little information to be added to owners table.")
            
            # add new owner records after trying to de-duplicate them
            a_added_owners = self.upload_new_contacts(a_new_owners, b_make_unique=True)
            print(f"Batch {i_batch}: {len(a_added_owners):,} new Prospect Now contacts to add to master contacts")
            
            # update maps between owners and ProspectNow table
            a_owner_maps = self.upload_maps(a_added_owners + a_link_owners, e_SOURCE_TABLES["ProspectNow"], "id_prospectnow")
        
        self._db_sources.close()
        print("Updating ProspectNow contacts to owners took:", utils.seconds_to_str(time() - n_start))
        print("\n")
    
    def infutor_to_owners(self, **kwargs):
        ''' Read the current owners from Infutor DB table and add ones to the owners table if they don't already exist '''
        
        n_start = time()
        df_infutor_fields = self._df_field_maps[
            (self._df_field_maps["source"].str.contains("Infutor")) &
            (self._df_field_maps["field_type"] == "Owner")
        ].set_index("field_name").dropna(subset=["standard_name"])
        self._infutor_fields = df_infutor_fields[df_infutor_fields["standard_name"] != ""].to_dict(orient="index")
        
        #print("Getting Infutor Table")
        #df_infutor = self._db_sources.get_table_as_df(e_SOURCE_TABLES["Infutor"], exact=True, **kwargs)
        #if kwargs.get("maximum", 0) > 0:
        #    df_infutor = df_infutor.iloc[:kwargs["maximum"]]
        
        print("Getting Infutor table in batches")
        
        self._db_sources.load()
        a_cols = self._db_sources.get_table_cols(e_SOURCE_TABLES["Infutor"], **kwargs)
        i_batch = 0
        s_where = f"""
WHERE {db_tables.s_DB_SOURCES}.{db_tables.e_SOURCE_TABLES["Infutor"]}.id NOT IN
(SELECT other_id FROM {db_tables.s_DB}.{s_OWNER_MAPS_TABLE} WHERE other_table = "{db_tables.e_SOURCE_TABLES["Infutor"]}")"""

        for a_rows in self._db_sources.yield_table(e_SOURCE_TABLES["Infutor"], i_batch_size=kwargs.get("batch_size", 10000), s_where=s_where, exact=True):
            i_batch += 1
            df_infutor = pd.DataFrame(a_rows, columns=a_cols)
            
            # filter down to the important owner columns
            df_infutor = df_infutor[[col for col in df_infutor.columns if col == "index" or col in self._infutor_fields.keys()]]
            
            e_inf_owners = self._maps_owners[e_SOURCE_TABLES["Infutor"]]
            
            # split the infutor owners into ones that already match existing owner records and those that are new to be added
            a_new_owners, a_link_owners, a_incomplete_owners = [], [], []
            i_already_present = 0
            i_batch_size = min(10000, kwargs["maximum"] / 10) if kwargs.get("maximum", 0) > 0 else 10000
            for i_row, (idx, o_row) in enumerate(df_infutor.iterrows()):
                if i_row % i_batch_size == 0 and i_row > 0:
                    print(f"Standardizing Infutor owners: {i_row:,} of {len(df_infutor):,} ({(i_row + 1) / len(df_infutor):0.1%})")
                e_infutor_row = dict(o_row)
                
                if e_infutor_row["index"] in e_inf_owners:
                    i_already_present += 1
                    continue
                
                e_row = {self._infutor_fields[k]["standard_name"]: v for k, v in e_infutor_row.items() if(k in self._infutor_fields) and not pd.isnull(v) and v}
                e_row["id_infutor"] = e_infutor_row["index"]
                e_row = self.standardize_owner(e_row)
                
                i_existing_match = self.find_matching_contact(e_row)
                if i_existing_match is None:
                    if find_contact.sufficient_owner_information(e_row):
                        a_new_owners.append(e_row)
                    else:
                        a_incomplete_owners.append(e_row)
                else:
                    e_row["id"] = i_existing_match
                    a_link_owners.append(e_row)
            
            # update the existing owner records
            a_updated_owners = self.upload_updated_contacts(a_link_owners)
            if a_updated_owners:
                print(f"{len(a_updated_owners):,} updated Infutor contacts to add to master contacts. {i_already_present:,} were already present and skipped.")
            else:
                print(f"{i_already_present:,} Infutor contacts were already present and skipped.")
            
            if a_new_owners:
                print(f"{len(a_new_owners):,} Infutor contacts are unmapped/unmatched and should be checked for new unique contacts.")
            
            if a_incomplete_owners:
                print(f"Batch {i_batch}: {len(a_incomplete_owners):,} Prospect Now contacts contain too little information to be added to owners table.")
            
            # add new owner records after trying to de-duplicate them
            a_added_owners = self.upload_new_contacts(a_new_owners, b_make_unique=True)
            print(f"{len(a_added_owners):,} new Infutor contacts to add to master contacts")
            
            # update maps between owners and infutor table
            a_owner_maps = self.upload_maps(a_added_owners + a_link_owners, e_SOURCE_TABLES["Infutor"], "id_infutor")
            
        self._db_sources.close()
        print("Updating Infutor contacts to owners took:", utils.seconds_to_str(time() - n_start))
        print("\n")
    
    ##### Connections #####
    
    def _load_dbs(self, **kwargs):
        global o_DB, o_DB_SOURCES
        
        if kwargs.get("o_db"):
            self._db = kwargs["o_db"]
        elif o_DB is not None:
            self._db = o_DB
        else:
            self._db = mysql_db.mySqlDbConn(
                s_host = kwargs.get("aws_db_host", mysql_db._host),
                i_port = kwargs.get("aws_db_port", mysql_db._port),
                s_user = kwargs.get("aws_db_user", AWS_DB_USER),
                s_pass = kwargs.get("aws_db_pass", AWS_DB_PASSWORD),
                s_database = kwargs.get("aws_main_db", s_DB),
                chunk_size = 50000,
            )
            o_DB = self._db
        
        self._db_tables = dict(self._db.list_tables())
        
        
        if s_FIELD_MAPS_TABLE not in self._db_tables: create_field_maps_table(self._db)
        print("owners_table", s_OWNERS_TABLE)
        if s_OWNERS_TABLE not in self._db_tables: create_owners_table(self._db)
        print("owner_maps_table", s_OWNER_MAPS_TABLE)
        if s_OWNER_MAPS_TABLE not in self._db_tables: create_owners_maps_table(self._db)
        #if s_PROPERTIES_TABLE not in self._db_tables: create_properties_table(self._db)
        
        if kwargs.get("o_db_sources"):
            self._db_sources = kwargs["o_db_sources"]
        elif o_DB_SOURCES is not None:
            self._db_sources = o_DB_SOURCES
        else:
            self._db_sources = mysql_db.mySqlDbConn(
                s_host = kwargs.get("aws_db_host", mysql_db._host),
                i_port = kwargs.get("aws_db_port", mysql_db._port),
                s_user = kwargs.get("aws_db_user", AWS_DB_USER),
                s_pass = kwargs.get("aws_db_pass", AWS_DB_PASSWORD),
                s_database = kwargs.get("aws_source_db", s_DB_SOURCES),
                chunk_size = 50000,
            )
            o_DB_SOURCES = self._db_sources
        
        return self._db, self._db_sources
    
    def _load_apis(self, **kwargs):
        global o_HUBSPOT
        if kwargs.get("o_hubspot"):
            self._hubspot = kwargs["o_hubspot"]
        elif o_HUBSPOT is not None:
            self._hubspot = o_HUBSPOT
        else:
            self._hubspot = hubspot_api.HubspotAPI(api_key=kwargs.get("hubspot_api_key", HUBSPOT_API_KEY))
            o_HUBSPOT = self._hubspot

##### End ContactUpdater Class #####


def create_field_maps_table(o_db=None):
    ''' should only be needed in dev '''
    if o_db is None: o_db = o_DB
    o_db.load()
    o_db.create_table(
        s_FIELD_MAPS_TABLE,
        [
            "id int AUTO_INCREMENT PRIMARY KEY",
            "field_type text",
            "source text",
            "standard_name varchar(128)",
            "field_name varchar(128)",
            "description text",
        ])
    o_db.conn.commit()
    o_db.close()


def create_owners_maps_table(o_db=None):
    ''' should only be needed in dev '''
    if o_db is None: o_db = o_DB
    o_db.load()
    o_db.create_table(
        s_OWNER_MAPS_TABLE,
        [
            #"id int AUTO_INCREMENT PRIMARY KEY",
            "other_table varchar(255)",
            "other_id int",
            "owner_id int",
            "UNIQUE KEY (`other_table`, other_id, owner_id)"
        ])
    o_db.cursor.execute(f"ALTER TABLE {s_OWNER_MAPS_TABLE} ADD INDEX owner_id_index (owner_id)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNER_MAPS_TABLE} ADD INDEX other_id_index (other_id)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNER_MAPS_TABLE} ADD INDEX other_table_index (`other_table`)")
    o_db.conn.commit()
    o_db.close()


def create_owners_table(o_db=None):
    ''' should only be needed in dev '''
    if o_db is None: o_db = o_DB
    o_db.load()
    o_db.create_table(
        s_OWNERS_TABLE,
        [
            "id int AUTO_INCREMENT PRIMARY KEY",
            "owner_name varchar(255)",
            "owner_first_name varchar(255)",
            "owner_last_name varchar(255)",
            "owner_type varchar(255)",
            "owner_company varchar(255)",
            "ownership_status varchar(255)",
            "owner_email varchar(255) UNIQUE",
            "owner_email_2 varchar(255)",
            "owner_email_3 varchar(255)",
            "owner_phone varchar(255) UNIQUE",
            "owner_phone_2 varchar(255)",
            "owner_phone_3 varchar(255)",
            "owner_fax varchar(255)",
            "owner_address text",
            "owner_address_2 text",
            "owner_unit varchar(255)",
            "owner_city varchar(255)",
            "owner_state varchar(64)",
            "owner_zip varchar(32)",
            "owner_country varchar(255)",
            "contact_name varchar(255)",
            "other_address text",
            "owner_website text",
            "hubspot_vid int",
            "owner_number_of_contacts int",
            "owner_company_size int",
            "owner_hubspot_owner_id int",
            "do_not_call bool default false",
            "created datetime default now()",
            "enabled bool default true",
        ]
    )
    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX hubspot_vid_index (hubspot_vid)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX owner_email_index (owner_email)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX owner_email_2_index (owner_email_2)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX owner_email_3_index (owner_email_3)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX owner_phone_index (owner_phone)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX owner_phone_2_index (owner_phone_2)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX owner_phone_3_index (owner_phone_3)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX owner_zip_index (owner_zip)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX owner_state_index (owner_state)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX owner_city_index (owner_city)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX owner_country_index (owner_country)")

    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX owner_name_index (owner_name)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX owner_first_name_index (owner_first_name)")
    o_db.cursor.execute(f"ALTER TABLE {s_OWNERS_TABLE} ADD INDEX owner_last_name_index (owner_last_name)")

    o_db.conn.commit()
    o_db.close()


def create_properties_table(o_db=None):
    ''' should only be needed in dev '''
    if o_db is None: o_db = o_DB
    o_db.load()
    o_db.create_table(
        s_PROPERTIES_TABLE,
        [
            "id int AUTO_INCREMENT PRIMARY KEY",
            "owner_id int",
        
            "property_address text",
            "property_address_2 text",
            "property_unit varchar(255)",
            "property_city varchar(255)",
            "property_state varchar(64)",
            "property_zip varchar(32)",
            "property_country varchar(255)",
            "property_county varchar(255)",
            
            "property_type varchar(255)",
            "property_latitude decimal(11, 8)",
            "property_longitude decimal(11, 8)",
            
            "property_year_built int",
            "property_number_of_units int",
            "property_total_value decimal(16, 2)",
            
            "listing_date datetime",
            "listing_age int",
            "listing_price decimal(16, 2)",
            
            "listing_rent decimal(16, 2)",
            "listing_rent_low decimal(16, 2)",
            "listing_rent_high decimal(16, 2)",

        ]
    )
    o_db.conn.commit()
    o_db.close()


if __name__ == "__main__":  
    ''' This gets run when update_contacts.py is run from the command prompt
        Args:
            args: {list} of the positional command line options.
                [0] 
            kwargs:
                --overwrite: {bool}. Do complete analysis of all engagements/activity instead of only most recent.
                --since: {str} of date. Override last run date in db to specify datetime to begin analysis.
    '''
    #print('sys.argv', sys.argv, '\n')
    args, kwargs = utils.parse_command_line_args(sys.argv)
    
    if "since" in kwargs:
        #print(kwargs["since"])
        kwargs["since"] = dt_since = dateutil.parser.parse(str(kwargs["since"]))
        
    if not HUBSPOT_API_KEY:
        raise Exception("""
            The environment variable for the hubspot API key hasn't been set up.
            Remember to run 'IDSTS_HUBSPOT_API_KEY=your_hubspot_api_key' from shell or add it to your /etc/environment file.""")
    if not AWS_DB_USER:
        raise Exception("""
            The environment variable for the AWS MYSQL Db Username hasn't been set up.
            Remember to run 'AWS_DB_USER=your_aws_db_username' from shell or add it to your /etc/environment file.""")
    if not AWS_DB_PASSWORD:
        raise Exception("""
            The environment variable for the AWS MYSQL Db Password hasn't been set up.
            Remember to run 'AWS_DB_PASSWORD=your_aws_db_password' from shell or add it to your /etc/environment file.""")
    
    #print('args', args, '\n')
    #print('kwargs', kwargs, '\n')
    add_contacts_to_master(**kwargs)
