﻿import os, sys, re, json
from datetime import datetime, timedelta
from collections import defaultdict, Counter
import pandas as pd

from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import GaussianNB, MultinomialNB, ComplementNB, CategoricalNB, BernoulliNB
from sklearn.model_selection import train_test_split
import joblib

try:
    from . import hubspot_api, mysql_db, utils, db_tables, metrics
except:
    import hubspot_api, mysql_db, utils, db_tables, metrics

s_DB = db_tables.s_DB
s_TOUCH_TABLE = db_tables.s_TOUCH_TABLE
s_PROCESS_TABLE = db_tables.s_PROCESS_TABLE

HUBSPOT_API_KEY = os.environ.get("IDSTS_HUBSPOT_API_KEY", "")
AWS_DB_USER = os.environ.get("IDSTS_AWS_DB_USER", "")
AWS_DB_PASSWORD = os.environ.get("IDSTS_AWS_DB_PASS", "")

o_DB = None
o_HUBSPOT = None

e_SUBSCRIPTION_STATUSES = {}
e_CONTACTS, e_CONTACT_STATUSES = {}, {}
e_VID_EMAIL, e_EMAIL_VID = {}, {}

a_HUBSPOT_PROPERTIES = [
    "email",
    "hubspotscore",
    "lifecyclestage",
    "hs_lead_status",
    "sakari_sms_received_qty",
    "sakari_sms_sent_qty",
    "sakari_opt_in", "sakari_opt_out",
]

a_LIFECYCLE_STAGES = [
    "Subscriber", "Lead",
    "Marketing qualified lead", "Sales qualified lead",
    "Opportunity", "Customer", " Evangelist", "Other"
]

a_ID_COLS = ["hubspot_vid", "email"]
a_OUTPUT_COLS = [
    "lifecyclestage",
    "hubspotscore",
    "hs_lead_status",
    #"sakari_sms_received_qty",
    #"sakari_sms_sent_qty",
    "sakari_opt_in",
    "sakari_opt_out",
]

e_ORDER_NAME = {
    "CALL": "CALL",
    "CALL CONNECTED": "CALL",
    "EMAIL": "EMAIL",
    "MARKETING EMAIL": "MARKETING EMAIL",
    "MARKETING EMAIL PROCESSED": "MARKETING EMAIL",
    "MARKETING EMAIL DELIVERED": "MARKETING EMAIL",
    "MARKETING EMAIL OPEN": "MARKETING EMAIL",
    "MARKETING EMAIL CLICK": "MARKETING EMAIL",
    "MARKETING EMAIL PRINT": "MARKETING EMAIL",
    "MARKETING EMAIL PRINTED": "MARKETING EMAIL",
    "MARKETING EMAIL FORWARD": "MARKETING EMAIL",
    "MARKETING EMAIL FORWARDED": "MARKETING EMAIL",
    "POSTCARD": "POSTCARD",
    "SMS": "SMS",
}

a_EVENT_TYPES = ['CALL',
 'CALL CONNECTED',
 'EMAIL',
 'MARKETING EMAIL CLICK',
 'MARKETING EMAIL DELIVERED',
 'MARKETING EMAIL OPEN',
 'MARKETING EMAIL PROCESSED',
 'MARKETING EMAIL PRINT',
 'MARKETING EMAIL FORWARD',
 'POSTCARD',
 'SMS']


@utils.timer
def load_db(**kwargs):
    global o_DB
    #print("AWS_DB", AWS_DB_USER, AWS_DB_PASSWORD)
    if kwargs.get("o_db"):
        o_DB = kwargs["o_db"]
    else:
        o_DB = mysql_db.mySqlDbConn(
            s_host = mysql_db._host,
            i_port = mysql_db._port,
            s_user = AWS_DB_USER,
            s_pass = AWS_DB_PASSWORD,
            s_database = s_DB,
            chunk_size = 50000,
        )
    return o_DB


@utils.timer
def load_api(**kwargs):
    global o_HUBSPOT
    if kwargs.get("o_hubspot"):
        o_HUBSPOT = kwargs["o_hubspot"]
    else:
        o_HUBSPOT = hubspot_api.HubspotAPI(api_key=HUBSPOT_API_KEY, **kwargs)
    
    global e_SUBSCRIPTION_STATUSES
    e_SUBSCRIPTION_STATUSES = o_HUBSPOT.get_subscription_statuses(**kwargs)    

    # get contact and their subscription statuses
    a_contacts = o_HUBSPOT.get_contacts(properties=a_HUBSPOT_PROPERTIES, **kwargs)
    
    global e_CONTACTS, e_CONTACT_STATUSES
    e_CONTACTS = {e["vid"]: e for e in a_contacts}
    e_CONTACT_STATUSES = {}
    
    c_hubspot_properties = {"vid",}.union(set(a_HUBSPOT_PROPERTIES))
    for e_contact in a_contacts:
        e_status = {k: v for k, v in e_contact.items() if k in c_hubspot_properties}
        e_status.update({
            k: e_v["value"]
            for k, e_v in e_contact.get("properties", {}).items()
            if e_v.get("value")
        })
        e_status["list-memberships"] = [
            (e.get("static-list-id", -1), e.get("internal-list-id", -1))
            for e in e_contact.get("list-memberships", [])
            if e.get("is-member")
        ]
        
        e_CONTACT_STATUSES[e_status["vid"]] = e_status
    
    global e_VID_EMAIL, e_EMAIL_VID
    e_VID_EMAIL, e_EMAIL_VID = o_HUBSPOT.get_contact_emails(a_all_contacts=a_contacts, **kwargs)
    return o_HUBSPOT


@utils.timer
def get_model_data(**kwargs):
    if o_DB is None: load_db(**kwargs)
    if o_HUBSPOT is None: load_api(**kwargs)

    # get touch details from DB
    df_touch_details = o_DB.get_table_as_df(s_TOUCH_TABLE, exact=True)
    
    # filter down to unique touches, and separate out CALL event_types by disposition
    df_unique_touches = metrics.filter_duplicate_touches(df_touch_details, **kwargs)
    df_unique_touches = metrics.split_call_dispositions(df_unique_touches, hubspot=o_HUBSPOT, **kwargs)
    
    a_rows = []
    dt_now = datetime.now()
    for (hubspot_vid, email), df_contact in df_unique_touches.groupby(["hubspot_vid", "email"]):
        if not email: email = e_VID_EMAIL.get(hubspot_vid, "")
        #print(hubspot_vid, email)
        
        e_row = {
            "hubspot_vid": hubspot_vid,
            "email": email,
            #"lifecyclestage": e_CONTACT_STATUSES.get(hubspot_vid, {}).get("lifecyclestage", ""),
            #"hubspotscore": e_CONTACT_STATUSES.get(hubspot_vid, {}).get("hubspotscore", ""),
            "subscribed": False,
        }
        e_row.update({
            s_prop: e_CONTACT_STATUSES[hubspot_vid][s_prop]
            for s_prop in a_HUBSPOT_PROPERTIES
            if s_prop not in e_row and e_CONTACT_STATUSES.get(hubspot_vid, {}).get(s_prop, "")})
        
        if email:
            e_subs = e_SUBSCRIPTION_STATUSES.get(email, {})
            e_row["subscribed"] = e_subs.get("portal") == "SUBSCRIBED"
            for i_campaign, s_sub in e_subs.items():
                e_row[f"subscribed_campaign_{i_campaign}"] = s_sub == "SUBSCRIBED"
            
        df_contact_types = df_contact.groupby("event_type")
        e_counts = dict(df_contact_types["hubspot_vid"].count())
        e_lasts = dict(df_contact_types["created"].max())
        
        for s_type, i_count in e_counts.items():
            s_type = "count_of_{}".format(s_type.lower().replace(" ", "_"))
            e_row[s_type] = i_count
        for s_type, dt_last in e_lasts.items():
            s_type = "days_since_last_{}".format(s_type.lower().replace(" ", "_"))
            e_row[s_type] = (dt_now - dt_last).days
        
        a_touch_order = [x for x in df_contact["event_type"] if x not in {"MARKETING EMAIL CLICK"}]
        a_simple_touch_order = metrics.dedupe_sequential(a_touch_order) # order ignoring sequential duplicates
        idx = 0
        for s_type in a_simple_touch_order:
            #print(e_ORDER_NAME.get(s_type, s_type))
            s_order_col = f"order_first_{e_ORDER_NAME.get(s_type, s_type).lower()}"
            if s_order_col not in e_row:
                idx += 1
                e_row[s_order_col] = idx
                #print(s_type, idx, s_order_col)
        
        a_rows.append(e_row)
    
    df_model_inputs = pd.DataFrame(a_rows)
    for col in df_model_inputs.columns:
        if col.startswith("subscribed"):
            df_model_inputs[col] = df_model_inputs[col].fillna(False)
    
    return df_unique_touches, df_model_inputs


def split_dataframe_into_random_sets(df, set_sizes=[0.5, 0.5], **kwargs): 
    ''' Split a dataframe into sub dataframes with a random sampling of rows
        Arguments:
            df: {pandas.DataFrame} the dataframe to split
            set_sizes: {list} of the desired size (as percentage or integer) how many sub-dfs to randomly split the 
        Returns:
            {list} of {pandas.DataFrame}
    '''
    if len(df) == 0: return [df]
    
    a_set_sizes = [x if x >= 1 else round(x * len(df)) for x in set_sizes if x > 0]
    i_diff = len(df) - sum(a_set_sizes)
    #print(i_diff)
    if i_diff:
        d_diff = abs(i_diff / len(df))
        if i_diff > 0:
            # adjust the size of the last batch if it's close in size, otherwise add a new group with the remainder
            if d_diff > kwargs.get("rounding_error", 0.02):
                print(f"Adding remainder group of {i_diff}")
                a_set_sizes.append(i_diff)
            else:
                print(f"Adding remainder of {i_diff} to last group of {a_set_sizes[-1]}")
                a_set_sizes[-1] += i_diff
        elif i_diff < 0:
            while i_diff < 0 and a_set_sizes:
                if a_set_sizes[-1] < abs(i_diff):
                    print(f"Removing last group of {a_set_sizes[-1]} because there are still {abs(i_diff)} to many specified")
                    i_diff += a_set_sizes[-1]
                    a_set_sizes.pop(-1)
                else:
                    print(f"Removing {abs(i_diff)} from last group of {a_set_sizes[-1]}.")
                    a_set_sizes[-1] += i_diff
                    i_diff = 0
                    
            if not a_set_sizes:
                raise Exception(f'Set sizes ({set_sizes}) are broken for df with {len(df)} rows.')
    if kwargs.get("b_debug"): print(a_set_sizes, sum(a_set_sizes), len(df))
        
    a_sub_dfs = []
    df_remaining = df
    for i_subset, set_size in enumerate(a_set_sizes):
        df_sub = df_remaining.sample(set_size)
        a_sub_dfs.append(df_sub)
        df_remaining = df_remaining[~df_remaining.index.isin(df_sub.index)]
        if kwargs.get("b_debug"):
            print(f"Subset {i_subset + 1}: {len(df_sub)} rows. {len(df_remaining)} remaining")
    
    return a_sub_dfs


def prep_data(df_model_inputs, a_data_cols, s_label_col, **kwargs):
    X = df_model_inputs[a_data_cols].fillna(0).values
    Y = df_model_inputs[s_label_col].fillna("")
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, train_size=kwargs.get("train_percent", 0.9))
    return X, Y, X_train, X_test, Y_train, Y_test


def create_model(X, Y, **kwargs):
    if kwargs.get("bayes") == "categorical":  # for text label outputs
        o_model = CategoricalNB()
    elif kwargs.get("bayes") == "multinomial":  # for discrete features/counts, no negative numbers
        o_model = MultinomialNB()
    elif kwargs.get("bayes") == "complement":  # for unbalanced datasets
        o_model = ComplementNB()
    elif kwargs.get("bayes") == "bernoulli":  # for binary features, can handle negative numbers
        o_model = BernoulliNB()
    else:  # Gaussian for normal distributions
        o_model = GaussianNB()
    
    o_model.fit(X, Y)
    return o_model


def pick_model(X, Y, X_train, Y_train, **kwargs):
    ''' try out the different bayes models to see what works best '''
    e_models = {}
    for s_bayes in [
        "multinomial",
        "complement",
        "bernoulli",
        "gaussian",
        "categorical",]:
        try:
            #print("s_bayes", s_bayes)
            o_model = create_model(X_train, Y_train, bayes=s_bayes)
            e_models[s_bayes] = (o_model, o_model.score(X, Y))
        except:
            pass
    if kwargs.get("return_all") or len(e_models) <= 1: return e_models
    
    s_bayes, (o_model, d_score) = max(e_models.items(), key=lambda x: x[1][1])
    #print(f"Using {s_bayes} model: {d_score:0.2%}")
    return s_bayes, o_model, d_score


def main(**kwargs):
    load_db(**kwargs)
    load_api(**kwargs)
    
    df_unique_touches, df_model_inputs = get_model_data(**kwargs)
    
    a_subscribed_columns = [col for col in df_model_inputs.columns if col.startswith("subscribed")]

    metrics.add_ratios(df_model_inputs)
    
    a_prediction_cols = [
        ("lifecyclestage", "predicted_lifecycle"),
        #("hs_lead_status", "predicted_lead_status"),
    ]
    
    e_predictions = defaultdict(dict)
    for s_target_col, s_prediction_property in a_prediction_cols:
        print(f"Analyzing '{s_target_col}'")
        a_data_cols = [col for col in df_model_inputs.columns if col not in a_ID_COLS + a_OUTPUT_COLS + a_subscribed_columns]
        X, Y, X_train, X_test, Y_train, Y_test = prep_data(df_model_inputs, a_data_cols, s_target_col)
        
        if all([isinstance(x, (float, int)) for x in Y]):
            s_data_type = "number"
            s_field_type = "number"
        else:
            s_data_type = "string"
            s_field_type = "text"
        s_description = f'''Predicted value for property "{s_target_col}" using Bayesian Model '''
        o_HUBSPOT.create_contact_property(
	        s_prediction_property, s_data_type, s_field_type, group_name="predictions", description=s_description)
	    
        s_bayes, o_model, d_model_score = pick_model(X, Y, X_train, Y_train)
        print(f"Predicting '{s_prediction_property}' using {s_bayes} model with {d_model_score:0.0%} accuracy.")
        a_predictions = o_model.predict(X)
        for hubspot_vid, prediction in zip(df_model_inputs["hubspot_vid"], a_predictions):
            e_predictions[hubspot_vid][s_prediction_property] = prediction
        
    #update_contacts
    return e_predictions
        