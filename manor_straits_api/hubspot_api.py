﻿import os, sys, re
from time import time
from datetime import datetime
from collections import defaultdict, Counter
from copy import deepcopy
import requests, json, urllib
import pandas as pd

try:
    from .defaults import default_marketing_email, default_contact_property, default_workflow
except:
    from defaults import default_marketing_email, default_contact_property, default_workflow


a_REQUIRED_FOR_NEW_HUBSPOT_CONTACT = [
    ("lastname", "email"),
    ("lastname", "phone"),
    ("firstname", "email"),
    ("firstname", "phone"),
    ("firstname", "lastname", "address", "city", "state"),
    ("firstname", "lastname", "address", "zip"),
    ("email", "address", "city", "state"),
    ("email", "address", "zip"),
    ("phone", "address", "city", "state"),
    ("phone", "address", "zip"),
    ("firstname", "lastname"),
]

e_ASSOCIATION_TYPES = {
    "contact_to_company": 1,
    "company_to_contact": 2,
    "deal_to_contact": 3,
    "contact_to_deal": 4,
    "deal_to_company": 5,
    "company_to_deal": 6,
    "company_to_engagement": 7,
    "engagement_to_company": 8,
    "contact_to_engagement": 9,
    "engagement_to_contact": 10,
    "deal_to_engagement": 11,
    "engagement_to_deal": 12,
    "parent_company_to_child_company": 13,
    "child_company_to_parent_company": 14,
    "contact_to_ticket": 15,
    "ticket_to_contact": 16,
    "ticket_to_engagement": 17,
    "engagement_to_ticket": 18,
    "deal_to_line_item": 19,
    "line_item_to_deal": 20,
    "company_to_ticket": 25,
    "ticket_to_company": 26,
    "deal_to_ticket": 27,
    "ticket_to_deal": 28,
    "advisor_to_company": 33,
    "company_to_advisor": 34,
    "board_member_to_company": 35,
    "company_to_board_member": 36,
    "contractor_to_company": 37,
    "company_to_contractor": 38,
    "manager_to_company": 39,
    "company_to_manager": 40,
    "business_owner_to_company": 41,
    "company_to_business_owner": 42,
    "partner_to_company": 43,
    "company_to_partner": 44,
    "reseller_to_company": 45,
    "company_to_reseller": 46,
}


def sufficient_for_new_hubspot_contact(e_hubspot_contact):
    ''' Check if a hubspot contact has enough information to be added to hubspot as a valid entry
        Arguments:
            e_hubspot_contact: {dict} contact that has been put through standardization.to_hubspot_contact
    '''
    for t_fields in a_REQUIRED_FOR_NEW_HUBSPOT_CONTACT:
        if all([s_field in e_hubspot_contact for s_field in t_fields]):
            return True
    return False


class HubspotAPI:
    _post_header = {"Content-type": "application/json", "Accept": "application/json"}
    
    _base_url = f"https://api.hubapi.com/"
    
    _marketing_emails_url = f"{_base_url}marketing-emails/v1/emails"
    _marketing_emails_with_stats_url = f"{_base_url}marketing-emails/v1/emails/with-statistics"
    _marketing_email_details_url = f"{_base_url}marketing-emails/v1/emails/with-statistics/"
    _campaigns_by_id_url = f"{_base_url}email/public/v1/campaigns/by-id"
    _events_url = f"{_base_url}email/public/v1/events"
    _engagements_url = f"{_base_url}engagements/v1/engagements"
    
    _owners_url = f"{_base_url}owners/v2/owners/"
    
    _contacts_url = f"{_base_url}contacts/v1/lists/all/contacts/all"
    _contact_url = f"{_base_url}contacts/v1/contact"
    _contact_by_email_url = f"{_base_url}contacts/v1/contact/email/"
    _contacts_by_id_url = f"{_base_url}contacts/v1/contact/vids/batch/"
    _contacts_by_emails_url = f"{_base_url}contacts/v1/contact/emails/batch/"
    _contact_workflow_url = f"{_base_url}automation/v2/workflows/enrollments/contacts/"
    _contact_search_url = f"{_base_url}contacts/v1/search/query"
    
    _subscriptions_url = f"{_base_url}email/public/v1/subscriptions"
    _subscription_timelines_url = f"{_base_url}email/public/v1/subscriptions/timeline"
    _contact_lists_url = f"{_base_url}contacts/v1/lists"
    _associations_url = f"{_base_url}crm-associations/v1/associations"
    
    _contact_properties_url =  f"{_base_url}properties/v1/contacts/properties"
    _contact_property_groups_url = f"{_base_url}properties/v1/contacts/groups"
    
    _company_properties_url =  f"{_base_url}properties/v1/companies/properties/"
    _company_property_groups_url = f"{_base_url}properties/v1/companies/groups/"
    
    _create_marketing_email_url = f"{_base_url}marketing-emails/v1/emails/"
    
    _workflows_url = f"{_base_url}automation/v3/workflows"
    _workflow_enroll_url = f"{_base_url}automation/v2/workflows/" + "{workflow_id}/enrollments/contacts/{email}"
    
    def __init__(self, api_key=None, owner_id="48343958", **kwargs):
        self.api_key = api_key if api_key else os.environ.get("IDSTS_HUBSPOT_API_KEY", "")
        print("HubspotAPI.api_key", self.api_key)
        self.owner_id = owner_id
        
        self._parameter_dict = {'hapikey': self.api_key}  #, 'limit': 100}
        #print("HubspotAPI._parameter_dict", self._parameter_dict)
        self.contact_property_groups, self.contact_properties = {}, {}
        self.contacts = None
        
        self.marketing_emails, self.workflows = {}, {}
        
        self._call_dispositions = {}
        
        self._important_owner_properties = kwargs.get(
            "important_owner_properties",
            [
                "firstname", "lastname", "company",
                "email", "website", "phone", "mobilephone", "fax",
                "address", "street_address_2", "city", "state", "zip", "country",
            ]
        )
        
    ##### CONNECTION #####
    def _get_response(self, s_endpoint, e_parameters, **kwargs):
        if 'hapikey' not in e_parameters: e_parameters['hapikey'] = self.api_key
        s_parameters = urllib.parse.urlencode(e_parameters)
        s_url = f"{s_endpoint}?{s_parameters}"
        o_response = requests.get(url=s_url)
        if not o_response.ok:
            #if kwargs.get("b_debug"): 
            #    return o_response
            #else:
            #    return None
            return o_response
        
        if kwargs.get("return_raw", False):
            return o_response
        elif kwargs.get("return_text", False):
            return o_response.text
        else:
            return json.loads(o_response.text)
    
    def _delete_response(self, s_endpoint, e_parameters, **kwargs):
        if 'hapikey' not in e_parameters: e_parameters['hapikey'] = self.api_key
        s_parameters = urllib.parse.urlencode(e_parameters)
        s_url = f"{s_endpoint}?{s_parameters}"
        o_response = requests.delete(url=s_url)
        if not o_response.ok:
            return o_response
        
        if kwargs.get("return_raw", False):
            return o_response
        elif kwargs.get("return_text", False):
            return o_response.text
        else:
            return json.loads(o_response.text)
    
    def _post_response(self, s_endpoint, e_data, e_parameters, **kwargs):
        if 'hapikey' not in e_parameters: e_parameters['hapikey'] = self.api_key
        s_parameters = urllib.parse.urlencode(e_parameters)
        s_url = f"{s_endpoint}?{s_parameters}"
        if kwargs.get("b_debug"): print(s_url)
        
        e_header = self._post_header
        if kwargs.get("header"):
            e_header = e_header.copy()
            e_header.update(kwargs["header"])
        
        o_response = requests.post(url=s_url, json=e_data, headers=e_header)
        if not o_response.ok:
            #if kwargs.get("b_debug"): 
            #    return o_response
            #else:
            #    return None
            return o_response
        
        if kwargs.get("return_raw", False):
            return o_response
        elif kwargs.get("return_text", False):
            return o_response.text
        else:
            return json.loads(o_response.text)
    
    def _put_response(self, s_endpoint, e_data, e_parameters, **kwargs):
        if 'hapikey' not in e_parameters: e_parameters['hapikey'] = self.api_key
        s_parameters = urllib.parse.urlencode(e_parameters)
        s_url = f"{s_endpoint}?{s_parameters}"
        if kwargs.get("b_debug"): print(s_url)
        
        e_header = self._post_header
        if kwargs.get("header"):
            e_header = e_header.copy()
            e_header.update(kwargs["header"])
        
        o_response = requests.put(url=s_url, json=e_data, headers=e_header)
        if not o_response.ok:
            #if kwargs.get("b_debug"): 
            #    return o_response
            #else:
            #    return None
            return o_response
        
        if kwargs.get("return_raw", False):
            return o_response
        elif kwargs.get("return_text", False):
            return o_response.text
        else:
            return json.loads(o_response.text)
    
    ##### HELPERS #####
    def _timestamp_to_date(self, i_hubspot_timestamp):
        return datetime.fromtimestamp(i_hubspot_timestamp / 1000.0)
    
    def _date_to_timestamp(self, dt_date):
        return int(dt_date.timestamp() * 1000)
    
    ##### OWNERS #####
    
    def get_owners(self, email=None, **kwargs):
        e_parameters = self._parameter_dict.copy()
        if email:
            e_parameters["email"] = email
        
        a_owners = self._get_response(
            self._owners_url,
            e_parameters=e_parameters,
            **kwargs)
        #e_contact_property_groups = {e["name"]: e for e in a_contact_property_groups}
        
        #self.contact_property_groups = e_contact_property_groups
        return a_owners
    
    ##### EVENTS #####
    def _read_all_events(self, maximum=10000, **kwargs):
        e_parameters = self._parameter_dict.copy()
        if "campaign" in kwargs:
            e_parameters["campaignId"] = kwargs["campaign"]
        if "event_type" in kwargs:
            e_parameters["eventType"] = kwargs["event_type"]
        if "offset" in kwargs:
            e_parameters["offset"] = kwargs["offset"]
        if "limit" in kwargs:
            e_parameters["limit"] = kwargs["limit"]
        
        if not pd.isnull(kwargs.get("since")):
            since = kwargs["since"]
            if isinstance(since, datetime):
                since = self._date_to_timestamp(since)
            elif isinstance(since, float) and not pd.isnull(since):
                since = int(since)
            if kwargs.get("b_debug"): print("since:", since)
            e_parameters["startTimestamp"] = since
        
        a_events = []
        while True:
            parameters = urllib.parse.urlencode(e_parameters)
            s_events = f"{self._events_url}?{parameters}"
            
            o_events = requests.get(url=s_events)
            if not o_events.ok: break
            
            e_events = json.loads(o_events.text)
            
            a_events.extend(e_events["events"])
            if not e_events.get("hasMore"): break
            e_parameters['offset'] = e_events["offset"]
            if len(a_events) >= maximum and maximum > 0: break
        
        return a_events
    
    def get_event_activity(self, **kwargs):
        e_activity = {}
        
        if isinstance(kwargs.get("event_types"), (list, tuple)):
            a_event_types = kwargs["event_types"]
            if not a_event_types:
                a_event_types = []
        else:
            a_event_types = [
                ("OPEN", "opens"),
                ("CLICK", "clicks"),
                ("FORWARD", "forwards"),
                ("DELIVERED", "deliveries"),
                ("PROCESSED", "processed"),
                ("SENT", "sends"),
            ]
                
        #if isinstance(a_event_types[0], (list, tuple)):
        #    a_types, a_type_names = map(list, zip(*a_event_types))
        if isinstance(a_event_types[0], str):
            a_event_types = [(s_type, s_type.lower()) for s_type in a_event_types]
        
        c_skip_types = kwargs.get("skip_types", [])
        
        for s_type, s_name in a_event_types:
            if s_type in c_skip_types or s_name in c_skip_types: continue
            e_activity[s_name] = self._read_all_events(event_type=s_type, **kwargs)
        
        print(f"Total Events: {sum(len(v) for v in e_activity.values())}")
        for s_name, a_events in e_activity.items():
            print(f"\t{s_name.title()} Events: {len(a_events)}")
        
        return e_activity
    
    ##### MARKETING EMAILS #####
    def _read_all_marketing_emails(self, maximum=10000, **kwargs):
        e_parameters = self._parameter_dict.copy()
        if "offset" in kwargs:
            e_parameters["offset"] = kwargs["offset"]
        if "limit" in kwargs:
            e_parameters["limit"] = kwargs["limit"]
        
        a_emails = []
        while True:
            parameters = urllib.parse.urlencode(e_parameters)
            if kwargs.get("with_stats", True):
                s_emails = f"{self._marketing_emails_with_stats_url}?{parameters}"
            else:
                s_emails = f"{self._marketing_emails_url}?{parameters}"
            
            o_emails = requests.get(url=s_emails)
            if not o_emails.ok:
                if kwargs.get("b_debug"): print(o_emails.text)
                break
            
            e_emails = json.loads(o_emails.text)
            
            a_emails.extend(e_emails["objects"])
            
            if not e_emails.get("offset"): break
            
            e_parameters['offset'] = e_emails["offset"]
            if len(a_emails) >= maximum and maximum > 0: break
        
        
        if not kwargs.get("with_details") or not a_emails:
            return a_emails
        else:
            a_detailed_emails = []
            for e_email in a_emails:
                s_email_id = e_email["id"]
                o_email = self._get_response(
                    f"{self._marketing_email_details_url}/{s_email_id}",
                    {},
                    **kwargs)
                assert o_email
                a_detailed_emails.append(o_email)
            
            return a_detailed_emails
    
    def get_marketing_emails(self, **kwargs):
        a_emails = self._read_all_marketing_emails(**kwargs)
        #print(len(a_emails))
        
        if kwargs.get("by_campaign"):
            e_campaign_emails = defaultdict(list)
            for e_email in a_emails:
                e_campaign_emails[e_email['campaign']].append(e_email)
            return e_campaign_emails
        else:
            return a_emails
    
    def get_marketing_email_details(self, id, **kwargs):
        e_parameters = self._parameter_dict.copy()
        parameters = urllib.parse.urlencode(e_parameters)
        s_email = f"{self._marketing_email_details_url}{id}?{parameters}"
        
        o_email = requests.get(url=s_email)
        if not o_email.ok:
            if kwargs.get("b_debug"): print(o_email.text)
            return None
        
        e_email = json.loads(o_email.text)
        
        return e_email
    
    def create_marketing_email(self, name, subject, body, email_properties, **kwargs):
        #e_email = default_marketing_email.default_marketing_email.copy()
        e_email = default_marketing_email.default_marketing_email_minimal.copy()
        e_email["name"] = name
        e_email["subject"] = subject
        e_email["emailBody"] = body
        
        e_email.update({
            k: v
            for k, v in email_properties.items()
            if not isinstance(v, str) or not k.startswith(("builtin_module_", "module_"))})
        
        for k, v in email_properties.items():
            if not isinstance(v, str): continue
            if k.startswith(("builtin_module_", "module_")):
                e_email["widgets"][k]['body']['html'] = v
        
        o_confirm_response = self._post_response(
            self._create_marketing_email_url,
            e_parameters=self._parameter_dict,
            e_data=e_email,
            **kwargs)
        
        if isinstance(o_confirm_response, dict):
            self.contact_property_groups[name] = o_confirm_response
            return o_confirm_response
        else:
            if kwargs.get("b_debug"):
                return o_confirm_response
            else:
                return None
    
    def get_marketing_recipients(self, e_all_activity=None, **kwargs):
        if not e_all_activity:
            e_all_activity = self.get_event_activity(**kwargs)
        
        a_activity_dfs = []
        for s_activity, a_events in e_all_activity.items():
            a_activity_dfs.append(pd.DataFrame(a_events))
        df_activity = pd.concat(a_activity_dfs, sort=False)
        
        if df_activity is None or len(df_activity) == 0:
            print(f"There is no event activity in relevant time frame.")
            return {}
        
        if kwargs.get("b_debug"): print("df_activity.columns", list(df_activity.columns))
        
        if not pd.isnull(kwargs.get("since")):
            since = kwargs["since"]
            if isinstance(since, datetime):
                since = self._date_to_timestamp(since)
            elif isinstance(since, float) and not pd.isnull(since):
                since = int(since)
            if kwargs.get("b_debug"): print("since:", since)
            df_activity = df_activity[df_activity["created"] >= since]
        
        e_marketing_recipients = {}
        for s_recipient, df in df_activity.groupby("recipient"):
            e_recipient = {}
            for idx, e_row in df.iterrows():
                if kwargs.get("b_debug"): print("marketing_recipients", s_recipient, e_row)
                s_type = e_row["type"]
                dt_created = self._timestamp_to_date(e_row["created"])
                i_campaign_id = e_row.get("emailCampaignId")
                i_campaign_group_id = e_row.get("emailCampaignGroupId")
    
                if s_type in e_recipient:
                    e_type = e_recipient[s_type]
                else:
                    e_type = {
                        "count": 0,
                        "last": None,
                        "email_campaign_ids": set(),
                        "email_campaign_group_ids": set()}
                    e_recipient[s_type] = e_type
    
                e_type["count"] += 1
                e_type["last"] = dt_created if e_type["last"] is None else max(dt_created, e_type["last"])
                if not pd.isnull(i_campaign_id): e_type["email_campaign_ids"].add(i_campaign_id)
                if not pd.isnull(i_campaign_group_id): e_type["email_campaign_group_ids"].add(i_campaign_group_id)
            e_marketing_recipients[s_recipient.lower()] = e_recipient
        
        return e_marketing_recipients
    
    ##### ENGAGEMENTS #####
    def _read_all_engagements(self, since=None, maximum=1000000, **kwargs):
        ''' read engagements from hubspot
            Arguments:
                since: {int} or {datetime} only include engagements since date (unix timestamp in milliseconds or python datetime)
                maximum: {int} maximum number of engagements to return
            Returns:
                {list} of engagement records
        '''
        e_parameters = self._parameter_dict.copy()
        if "offset" in kwargs:
            e_parameters["offset"] = kwargs["offset"]
        if "limit" in kwargs:
            e_parameters["limit"] = kwargs["limit"]
        
        if not pd.isnull(since):
            if isinstance(since, datetime):
                since = self._date_to_timestamp(since)
            elif isinstance(since, float) and not pd.isnull(since):
                since = int(since)
            if kwargs.get("b_debug"): print("since:", since)
            e_parameters["since"] = since
        
        i_batch = 0
        a_engagements = []
        while True:
            parameters = urllib.parse.urlencode(e_parameters)
            if e_parameters.get("since"):
                s_engagements = f"{self._engagements_url}/recent/modified?{parameters}"
            else:
                s_engagements = f"{self._engagements_url}/paged?{parameters}"
            
            o_engagements = requests.get(url=s_engagements)
            if not o_engagements.ok:
                if kwargs.get("b_debug"): print(o_engagements.text)
                break
            i_batch += 1
            
            e_engagements = json.loads(o_engagements.text)
            
            a_engagements.extend(e_engagements["results"])
            if not e_engagements.get("hasMore"): break
            e_parameters['offset'] = e_engagements["offset"]
            print(f"\tbatch: {i_batch}, Engagements: {len(a_engagements):,}, offset: {e_engagements['offset']}")
            if len(a_engagements) >= maximum and maximum > 0: break
        return a_engagements
    
    def get_engagements(self, **kwargs):
        a_engagements = self._read_all_engagements(**kwargs)
        print(f"Total Engagements: {len(a_engagements):,}")
        
        if kwargs.get("by_type"):
            e_interactions = defaultdict(list)
            for e_interact in a_engagements:
                e_interactions[e_interact['type']].append(e_interact)
            return e_interactions
        else:
            return a_engagements
    
    def get_engagement(self, engagement_id, **kwargs):
        s_url = f"{self._engagements_url}/{engagement_id}"
    
        e_parameters = self._parameter_dict.copy()
        o_response = self._get_response(s_url, e_parameters=e_parameters, **kwargs)
        return o_response
    
    def create_engagement(self, e_engagement, e_association, e_metadata={}, a_attachments=None, **kwargs):
        s_url = f"{self._engagements_url}"
        e_parameters = self._parameter_dict.copy()
        
        if not self.contact_properties: self.get_contact_properties()
        
        #assert e_engagement.get("type", "").upper() in {"EMAIL", "CALL", "MEETING", "TASK", "NOTE"}
        
        if e_engagement["type"].upper() == "NOTE":
            if not e_metadata.get("body", ""):
                raise Exception(f'Not a valid note text "{metadata.get("body", "")}"')
        elif e_engagement["type"].upper() == "EMAIL":
            if not "@" in e_metadata.get("from", {}).get("email", ""):
                raise Exception(f'Not a valid from email address "{metadata.get("from", "")}"')
            if not all("@" in e_cont.get("email","") for e_cont in e_metadata.get("to", [])):
                raise Exception(f'Not all contacts have a email address: {e_metadata.get("to", [])}')
            if not e_metadata.get("subject", ""):
                raise Exception(f'Not a valid email subject "{metadata.get("subject", "")}"')
            if not e_metadata.get("text", "") and not e_metadata.get("html", ""):
                raise Exception(f'Not a valid email body.')
        elif e_engagement["type"].upper() == "CALL":
            if not e_metadata.get("toNumber", ""):
                raise Exception("Not a valid contact toNumber.")
            if not e_metadata.get("fromNumber", ""):
                raise Exception("Not a valid caller fromNumber.")
            if e_metadata.get("status", "") not in {"NOT_STARTED", "COMPLETED", "IN_PROGRESS", "WAITING", "DEFERRED"}:
                raise Exception(f'Call status must be one of these options: ["NOT_STARTED", "COMPLETED", "IN_PROGRESS", "WAITING", "DEFERRED"] instead of "{e_metadata.get("status", "")}"')
        
        #if "timestamp" not in e_engagement:
        #    e_engagement["timestamp"] = time() * 1000
        
        e_data = {
            "engagement": e_engagement,
            "associations": e_association,
            "metadata": e_metadata,
            "attachments": a_attachments,
        }
        
        if kwargs.get("b_debug"): print("e_data", e_data)
            
        o_confirm_response = self._post_response(
            s_url,
            e_parameters=e_parameters,
            e_data=e_data,
            return_text=False,
            **kwargs)
        return o_confirm_response
    
    def get_call_dispositions(self, **kwargs):
        s_url = f"{self._base_url}/calling/v1/dispositions"
        o_dispositions = self._get_response(s_url, self._parameter_dict, **kwargs)
        if isinstance(o_dispositions, list):
            self._call_dispositions = {e["id"]: e["label"] for e in o_dispositions}
            return self._call_dispositions
        else:
            return o_dispositions
    
    ##### ASSOCIATIONS #####
    def _read_all_associations(self, object_id, association_type, maximum=1000000, **kwargs):
        ''' read associations from hubspot
            Arguments:
                since: {int} or {datetime} only include engagements since date (unix timestamp in milliseconds or python datetime)
                maximum: {int} maximum number of engagements to return
            Returns:
                {list} of engagement records
        '''
        if not object_id: return None
        if isinstance(association_type, str):
            association_type = association_type.lower().replace(" ", "_")
            if association_type in e_ASSOCIATION_TYPES:
                association_type = e_ASSOCIATION_TYPES[association_type]
            elif re.match("^\d+$") and int(association_type) in e_ASSOCIATION_TYPES.values():
                association_type = int(association_type)
            else:
                raise Exception(f"Unknown association type: '{association_type}'. Specify either a numeric id, or a text label in hubspot_api.e_ASSOCIATION_TYPES")
    
        e_parameters = self._parameter_dict.copy()
        if "offset" in kwargs:
            e_parameters["offset"] = kwargs["offset"]
        if "limit" in kwargs:
            e_parameters["limit"] = kwargs["limit"]
    
        i_batch = 0
        a_associations = []
        while True:
            parameters = urllib.parse.urlencode(e_parameters)
            s_url = f"{self._associations_url}/{object_id}/HUBSPOT_DEFINED/{association_type}?{parameters}"
    
            o_associations = requests.get(url=s_url)
            if not o_associations.ok:
                if kwargs.get("b_debug"):
                    print("s_url:", s_url)
                    print(o_associations.text)
                    return o_associations
                break
            i_batch += 1
    
            e_associations = json.loads(o_associations.text)
            #return e_associations
    
            a_associations.extend(e_associations["results"])
            if not e_associations.get("hasMore"): break
            e_parameters['offset'] = e_associations["offset"]
            print(f"\tbatch: {i_batch}, Associations: {len(a_associations):,}, offset: {e_engagements['offset']}")
            if len(a_associations) >= maximum and maximum > 0: break
        return a_associations
    
    def get_associations(self, object_id, association_type, **kwargs):
        if not object_id: return None
        if isinstance(association_type, str):
            association_type = association_type.lower().replace(" ", "_")
            if association_type in e_ASSOCIATION_TYPES:
                association_type = e_ASSOCIATION_TYPES[association_type]
            elif re.match("^\d+$") and int(association_type) in e_ASSOCIATION_TYPES.values():
                association_type = int(association_type)
            else:
                raise Exception(f"Unknown association type: '{association_type}'. Specify either a numeric id, or a text label in hubspot_api.e_ASSOCIATION_TYPES")
        
        a_associations = self._read_all_associations(object_id, association_type, **kwargs)
        print(f"Total Associations: {len(a_associations):,}")
        
        if kwargs.get("get_objects"):


            if association_type in {7, 9, 11, 17}:  # engagements
                a_objects = [self.get_engagement(id) for id in a_associations]

            elif association_type in {2, 3, 10, 16}:  #contact
                a_objects = [self.get_contact(id) for id in a_associations]
            
            #elif association_type in {1, 5, 8, 13, 14, 26, 33, 35, 37, 39, 41, 43, 45}:  # company
            
            #elif association_type in {4,6,12,20,28}:  # deal
            
            #elif association_type in {15,18,25,27}:  # ticket
            
            else:
                a_objects = []
            
            return a_objects
        else:
            return a_associations
    
    ##### CONTACTS #####
    def _read_all_contacts(self, maximum=10000000, **kwargs):
        if kwargs.get("cache_contacts") and self.contacts:
            return self.contacts
    
        e_parameters =  {
            'hapikey': self.api_key,
            "showListMemberships": True,
            "count": 100,}
        if "offset" in kwargs:
            e_parameters["vidOffset"] = kwargs["offset"]
        if "limit" in kwargs:
            e_parameters["count"] = min(100, kwargs["limit"])
        
        a_properties = []
        if kwargs.get("properties"):
            a_properties = [("property", s_prop) for s_prop in kwargs["properties"] if s_prop]
        
        a_contacts = []
        i_batch = 0
        while True:
            parameters = urllib.parse.urlencode(list(e_parameters.items()) + a_properties)
            s_batch_url = f"{self._contacts_url}?{parameters}"
    
            o_batch = requests.get(url=s_batch_url)
            if not o_batch.ok: break
            i_batch += 1
            e_contacts = json.loads(o_batch.text)
            #print(e_contacts.keys())
            a_contacts.extend(e_contacts["contacts"])
            if not e_contacts.get("has-more"): break
            
            if e_contacts.get("vidOffset"):
                e_parameters['vidOffset'] = e_contacts["vidOffset"]
            elif e_contacts.get("vid-offset"):
                e_parameters['vidOffset'] = e_contacts["vid-offset"]
            else:
                break
            
            
            if kwargs.get("b_debug"): print(f"\tbatch: {i_batch:,}, offset: {e_parameters['vidOffset']}, total contacts: {len(a_contacts):,}")
            if len(a_contacts) >= maximum and maximum > 0: break
        
        if kwargs.get("cache_contacts"):
            self.contacts = a_contacts
        
        return a_contacts[:maximum]
    
    def get_contacts(self, **kwargs):
        print("Reading contacts from hubspot")
        a_all_contacts = self._read_all_contacts(**kwargs)
        print(f"Contacts: {len(a_all_contacts):,}")
    
        if kwargs.get("by_list"):
            e_contact_lists = defaultdict(list)
            for e_contact in a_all_contacts:
                for e_list in e_contact.get("list-memberships", []):
                    s_internal_list_id = e_list["internal-list-id"] if "internal-list-id" in e_list else e_list.get("static-list-id", "")
                    #if s_internal_list_id:
                    e_contact_lists[s_internal_list_id].append(e_contact)
            e_contact_lists["_all_"] = a_all_contacts
            return e_contact_lists
        else:
            return a_all_contacts

    def _read_all_contact_lists(self, maximum=10000, **kwargs):
        e_parameters =  {
            'hapikey': self.api_key,
            "count": 100,}
        if "offset" in kwargs:
            e_parameters["offset"] = kwargs["offset"]
        if "limit" in kwargs:
            e_parameters["count"] = min(100, kwargs["limit"])
        
        a_lists = []
        while True:
            parameters = urllib.parse.urlencode(e_parameters)
            s_batch_url = f"{self._contact_lists_url}?{parameters}"
    
            o_batch = requests.get(url=s_batch_url)
            if not o_batch.ok: break
    
            e_lists = json.loads(o_batch.text)
    
            a_lists.extend(e_lists["lists"])
            if not e_lists.get("has-more"): break
            e_parameters['offset'] = e_lists["offset"]
            
            #print(e_lists["vidOffset"], len(a_lists))
            if len(a_lists) >= maximum and maximum > 0: break
        return a_lists
    
    def get_contact_lists(self, **kwargs):
        a_all_lists = self._read_all_contact_lists(**kwargs)
        print(f"contact lists: {len(a_all_lists)}")
    
        if kwargs.get("by_type"):
            e_list_types = defaultdict(list)
            for e_list in a_all_lists:
                s_type = e_list["listType"]
                
                e_list_types[s_type].append(e_list)
            e_list_types["_all_"] = a_all_lists
            return e_list_types
        else:
            return a_all_lists
    
    def _read_lists_contacts(self, contact_list_id, maximum=-1, **kwargs):
        if not contact_list_id: return None
        s_url = f"{self._contact_lists_url}/{contact_list_id}/contacts/all"
        
        e_parameters =  {
            'hapikey': self.api_key,
            "count": 100,}
        if "offset" in kwargs:
            e_parameters["vidOffset"] = kwargs["offset"]
        if "limit" in kwargs:
            e_parameters["count"] = min(100, kwargs["limit"])
        
        a_properties = []
        if kwargs.get("properties"):
            a_properties = [("property", s_prop) for s_prop in kwargs["properties"] if s_prop]
        
        a_lists_contacts = []
        while True:
            parameters = urllib.parse.urlencode(list(e_parameters.items()) + a_properties)
            s_batch_url = f"{s_url}?{parameters}"
            
            o_batch = requests.get(url=s_batch_url)
            if not o_batch.ok:
                return o_batch
                break
            
            e_contacts = json.loads(o_batch.text)
    
            a_lists_contacts.extend(e_contacts["contacts"])
            if not e_contacts.get("has-more"): break
            e_parameters['vidOffset'] = e_contacts["vid-offset"]
    
            if len(a_lists_contacts) >= maximum and maximum > 0: break
        return a_lists_contacts
    
    def get_lists_contacts(self, contact_list_id, **kwargs):
        if kwargs.get("ids", True):
            if "properties" not in kwargs: kwargs["properties"] = []
            if "email" not in kwargs["properties"]: kwargs["properties"].append("email")
        a_lists_contacts = self._read_lists_contacts(contact_list_id, **kwargs)
        if not isinstance(a_lists_contacts, list):
            raise Exception(f"Couldn't access contact list '{contact_list_id}' on Hubspot")
            #return False
        
        print(f"contacts in list {contact_list_id}: {len(a_lists_contacts)}")
    
        if kwargs.get("ids", True):
            return [
                {"hubspod_vid": e_contact.get("vid"), "email": e_contact.get("properties", {}).get("email", {}).get("value")}
                for e_contact in a_lists_contacts]
        else:
            return a_lists_contacts
    
    def _fetch_contacts_by_id(self, ids, **kwargs):
        ''' get batch of contacts by vid '''
        e_parameters =  {
            'hapikey': self.api_key,
            "showListMemberships": False,
            "count": 100,}
        if "offset" in kwargs:
            e_parameters["vidOffset"] = kwargs["offset"]
        if "limit" in kwargs:
            e_parameters["count"] = min(100, kwargs["limit"])
        
        a_ids = []
        if ids:
            a_ids = [x for x in [
                ("vid", int(contact)) if (isinstance(contact, str) and re.match(r"^\d+$", contact)) or isinstance(contact, (int, float)) else
                ("vid", int(contact["vid"])) if isinstance(contact, dict) and re.match(r"^\d+$", str(contact.get("vid", ""))) else
                ("vid", int(contact["vid"])) if isinstance(contact, dict) and isinstance(contact.get("vid"), float) else ""
                for contact in ids if contact] if x]
        
        assert len(a_ids) <= 100 # later on, need to make this run on larger batches
        
        #print("a_ids", a_ids)
        
        a_properties = []
        if kwargs.get("properties"):
            if kwargs["properties"].lower() in {"important", "important_properties"}:
                a_properties = [("property", s_prop) for s_prop in self._important_owner_properties]
            else:
                a_properties = [("property", s_prop) for s_prop in kwargs["properties"] if s_prop]
        
        #print("a_properties", a_properties)
        e_contacts = {}
        
        parameters = urllib.parse.urlencode(list(e_parameters.items()) + a_ids + a_properties)
        s_url = f"{self._contacts_by_id_url}?{parameters}"

        o_result = requests.get(url=s_url)
        if o_result.ok:
            e_contacts = json.loads(o_result.text)
        
        return e_contacts
    
    def _fetch_contacts_by_email(self, emails, **kwargs):
        ''' get batch of contacts by email '''
        e_parameters =  {
            'hapikey': self.api_key,
            "showListMemberships": False,
            "count": 100,}
        if "offset" in kwargs:
            e_parameters["vidOffset"] = kwargs["offset"]
        if "limit" in kwargs:
            e_parameters["count"] = min(100, kwargs["limit"])
        
        a_emails = []
        if emails:
            a_emails = [x for x in [
                ("email", cont) if isinstance(cont, str) and "@" in cont else
                ("email", cont["email"]) if isinstance(cont, dict) and "@" in cont.get("email", "") else ""
                for cont in emails if cont] if x]
        
        assert len(a_emails) <= 100 # later on, need to make this run on larger batches
        
        a_properties = []
        if kwargs.get("properties"):
            if kwargs["properties"].lower() in {"important", "important_properties"}:
                a_properties = [("property", s_prop) for s_prop in self._important_owner_properties]
            else:
                a_properties = [("property", s_prop) for s_prop in kwargs["properties"] if s_prop]
        
        #print("a_properties", a_properties)
        e_contacts = {}
        
        parameters = urllib.parse.urlencode(list(e_parameters.items()) + a_emails + a_properties)
        s_url = f"{self._contacts_by_emails_url}?{parameters}"

        o_result = requests.get(url=s_url)
        if o_result.ok:
            e_contacts = json.loads(o_result.text)
        
        return e_contacts
    
    def get_contact(self, contact_id, **kwargs):
        if isinstance(contact_id, str) and "@" in contact_id:
            s_url = f"{self._contact_by_email_url}{contact_id}/profile"
        else:
            s_url = f"{self._contact_url}/vid/{contact_id}/profile"
        
        e_parameters = self._parameter_dict.copy()
        o_response = self._get_response(s_url, e_parameters=e_parameters, **kwargs)
        
        return o_response
    
    def delete_contact(self, contact_id, **kwargs):
        if not contact_id: return None
        if isinstance(contact_id, str) and "@" in contact_id:
            e_contact = self.get_contact(contact_id)
            if not e_contact or not isinstance(e_contact, dict): return None
            if not e_contact.get("vid"): return None
            contact_id = e_contact["vid"]
        s_url = f"{self._contact_url}/vid/{contact_id}"
        e_parameters = self._parameter_dict.copy()
        o_response = self._delete_response(s_url, e_parameters=e_parameters, **kwargs)
        return o_response
    
    def create_contact(self, e_contact, **kwargs):
        s_url = f"{self._contact_url}"
        e_parameters = self._parameter_dict.copy()
        
        if not self.contact_properties: self.get_contact_properties()
        
        if "properties" not in e_contact:
            e_contact = {
                "properties": [
                    {"property": k, "value": v}
                    for k, v in e_contact.items()]
            }
        if kwargs.get("b_debug"): print("e_contact", e_contact)
        
        assert all(
            e_prop["property"] in self.contact_properties and e_prop["property"] not in {"HubSpot Score"}
            for e_prop in e_contact["properties"])
            
        o_confirm_response = self._post_response(
            s_url,
            e_parameters=e_parameters,
            e_data=e_contact,
            return_text=False,
            **kwargs)
        return o_confirm_response
    
    def create_contacts_batch(self, a_contacts, **kwargs):
        s_url = f"{self._contact_url}/batch"
        e_parameters = self._parameter_dict.copy()
        if kwargs.get("b_debug"): print("s_url", s_url)
        assert len(a_contacts) < 1000
        
        if not self.contact_properties: self.get_contact_properties()
        
        a_contacts_checked = []
        for e_contact in a_contacts:
            if "properties" not in e_contact:
                e_contact = {
                    "properties": [
                        {"property": k, "value": v}
                        for k, v in e_contact.items()]
            }
            
            for e_prop in e_contact["properties"]:
                s_prop = e_prop["property"]
                if s_prop not in self.contact_properties or s_prop in {"HubSpot Score"}:
                        raise Exception(f'Property "{s_prop}" is missing from contact_properties')
            #assert all(
            #    e_prop["property"] in self.contact_properties and e_prop["property"] not in {"HubSpot Score"}
            #    for e_prop in e_contact["properties"])
                
            if not e_contact.get("email") and not e_contact.get("vid"):
                if any(e_prop["property"] == "vid" for e_prop in e_contact["properties"]):
                    for e_prop in e_contact["properties"]:
                        if e_prop["property"] == "vid":
                            e_contact["vid"] = e_prop["value"]
                            break
                elif any(e_prop["property"] == "email" for e_prop in e_contact["properties"]):
                    for e_prop in e_contact["properties"]:
                        if e_prop["property"] == "email":
                            e_contact["email"] = e_prop["value"]
                            break
                #else:
                #    raise Exception(f'Batch contacts must contain either vid or email address: {e_contact}')
                
            if kwargs.get("b_debug"): print("e_contact", e_contact)
            a_contacts_checked.append(e_contact)
        
        if kwargs.get("b_debug"): print("a_contacts_checked", a_contacts_checked)
        
        o_confirm_response = self._post_response(
            s_url,
            e_parameters=e_parameters,
            e_data=a_contacts_checked,
            return_text=True,
            **kwargs)
        return o_confirm_response
    
    def update_contact(self, contact_id, e_updates, **kwargs):
        if isinstance(contact_id, str) and "@" in contact_id:
            s_url = f"{self._contact_by_email_url}{contact_id}/profile"
        else:
            s_url = f"{self._contact_url}/vid/{contact_id}/profile"
        
        e_parameters = self._parameter_dict.copy()
        
        if not self.contact_properties: self.get_contact_properties()
        
        if "properties" not in e_updates:
            e_updates = {
                "properties": [
                    {"property": k, "value": v}
                    for k, v in e_updates.items()]
            }
        if kwargs.get("b_debug"): print("e_updates", e_updates)
        assert all(
            e_prop["property"] in self.contact_properties and e_prop["property"] not in {"HubSpot Score"}
            for e_prop in e_updates["properties"])
            
        o_confirm_response = self._post_response(
            s_url,
            e_parameters=e_parameters,
            e_data=e_updates,
            return_text=True,
            **kwargs)
        return o_confirm_response
        #if isinstance(o_confirm_response, dict):
        #    self.contact_property_groups[name] = o_confirm_response
        #    return o_confirm_response
        #else:
        #    return None
    
    def update_contacts(self, contact_updates, **kwargs):
        s_url = f"{self._contact_url}/batch"
        
        e_parameters = self._parameter_dict.copy()
        
        if not self.contact_properties: self.get_contact_properties()
        
        if isinstance(contact_updates, dict):
            a_contact_updates = []
            for k, v in contact_updates.items():
                e_row = {}
                
                if isinstance(k, str) and "@" in k:
                    e_row["email"] = k
                else:
                    e_row["vid"] = k
                
                if isinstance(v, dict) and "properties" in v:
                    e_row["properties"] = v["properties"]
                elif isinstance(v, list):
                    e_row["properties"] = v
                else:
                    continue
                a_contact_updates.append(e_row)
            
        if kwargs.get("b_debug"): print("a_contact_updates", len(a_contact_updates))
        #assert all(
        #    e_prop["property"] in self.contact_properties and e_prop["property"] not in {"HubSpot Score"}
        #    for e_prop in e_updates["properties"])
        assert a_contact_updates
        
        i_batch, i_batch_size = 0, kwargs.get("batch_size", 250)
        a_responses = []
        while a_contact_updates:
            i_batch += 1
            a_batch_updates, a_contact_updates = a_contact_updates[:i_batch_size], a_contact_updates[i_batch_size:]
            print(f"Updating batch {i_batch} of {len(a_batch_updates)} contacts. {len(a_contact_updates)} left to update.")
            
            o_confirm_response = self._post_response(
                s_url,
                e_parameters=e_parameters,
                e_data=a_batch_updates,
                return_text=True,
                **kwargs)
            a_responses.append(o_confirm_response)
        
        return a_responses
    
    def get_contact_workflows(self, contact_id, **kwargs):
        i_contact_id = contact_id
        if isinstance(i_contact_id, str) and "@" in i_contact_id:
            e_contact = self.get_contact(i_contact_id)
            if not isinstance(e_contact, dict): return e_contact
            i_contact_id = e_contact["vid"]
        
        e_parameters = self._parameter_dict.copy()
        o_response = self._get_response(
            f"{self._contact_workflow_url}{i_contact_id}",
            e_parameters=e_parameters,
            **kwargs)
        return o_response
        
    def search_contact(self, search, **kwargs):        
        e_parameters = self._parameter_dict.copy()
        if not search: return None
        e_parameters["q"] = search
        
        if "property" in kwargs: e_parameters["property"] = kwargs["properties"]
        if "count" in kwargs: e_parameters["count"] = min(100, kwargs["count"])
        if "offset" in kwargs: e_parameters["offset"] = kwargs["offset"]
        if "sort" in kwargs: e_parameters["sort"] = kwargs["sort"]
        if "order" in kwargs: e_parameters["order"] = kwargs["order"]
        
        o_response = self._get_response(
            self._contact_search_url,
            e_parameters=e_parameters,
            **kwargs)
        return o_response
        
    def find_contacts(self, contacts, **kwargs):
        ''' find a smallish list of contacts by information that may be an email, phone number, name, etc
            Arguments:
                contacts: {list} of contacts that are either
                          {str} like emails or phones
                          -OR-
                          {dict} like {}
                          
            Returns:
                {list} of lists of vids of potential matches ()
        '''
        e_contacts = defaultdict(dict)
        a_searches = []
        if isinstance(contacts, (list, tuple)):
            for idx, cont in enumerate(contacts):
                if isinstance(cont, str):
                    if "@" in cont:
                        e_contacts[cont]["email"] = cont
                        e_contacts[cont]["search_by"] = "email"
                        a_searches.append((cont, "email"))
                    else:
                        e_contacts[cont]["text"] = cont
                        e_contacts[cont]["search_by"] = "text"
                        a_searches.append((cont, "text"))
                elif isinstance(cont, dict):
                    cont = {k.lower(): v for k, v in cont.items()}
                    if cont.get("hubspot_vid", cont.get("hubspot", cont.get("vid", cont.get("hubspotvid")))):
                        hubspot_vid = cont.get("hubspot_vid", cont.get("hubspot", cont.get("vid", cont.get("hubspotvid"))))
                        e_contacts[hubspot_vid]["id"] = hubspot_vid
                        e_contacts[hubspot_vid]["search_by"] = "vid"
                        a_searches.append((hubspot_vid, "vid"))
                    
                    elif cont.get("email"):
                        e_contacts[cont["email"]] = cont
                        e_contacts[cont["email"]]["search_by"] = "email"
                        a_searches.append((cont["email"], "email"))
                    elif cont.get("work_email"):
                        cont["email"] = cont["work_email"]
                        e_contacts[cont["email"]] = cont
                        e_contacts[cont["email"]]["search_by"] = "email"
                        a_searches.append((cont["work_email"], "email"))
                    else:
                        for s_text_key in ["phone", "mobilephone", "cell", "mobile"]:
                            if cont.get(s_text_key):
                                e_contacts[idx]["text"] = cont[s_text_key]
                                a_searches.append((cont[s_text_key], "text"))
                                break
                        else:
                            for t_text_key in [
                                ("firstname", "lastname", "address"),
                                ("firstname", "lastname", "city"),
                                ("firstname", "lastname"),
                                ("name", "address"),
                                ("name", "jobtitle"),
                                ("address", "city", "state"),
                                ("address", "zip")
                            ]:
                                a_vals = [cont.get(x) for x in t_text_key]
                                if all(a_vals):
                                    e_contacts[idx]["text"] = ' '.join(a_vals)
                                    a_searches.append((' '.join(a_vals), "text"))
                                    break
        assert len(contacts) == len(a_searches)
        #print("e_contacts", e_contacts)
        #for e_cont in e_contacts.values():
        #    #print("e_cont", e_cont)
        #    e_matches = {}
        #    if e_cont["search_by"] == "email":
        #        e_matches = self.search_contact(e_cont["email"])
        #    elif e_cont["search_by"] == "text":
        #        e_matches = self.search_contact(e_cont["text"])
        
        a_results = []
        for s_search, s_search_type in a_searches:
            if s_search_type == "vid":
                if isinstance(s_search, (list, tuple)):
                    a_results.append(list(s_search))
                else:
                    a_results.append([s_search])
            else:
                e_matches = self.search_contact(s_search )   
                a_matched_ids = [e_match["vid"] for e_match in e_matches.get("contacts", [])]
                a_results.append(a_matched_ids)
        
        #print("e_contacts", e_contacts)
        #a_results = [
        #    e_contacts.get(cont, {}).get("vids", [])
        #    for cont in contacts
        #]
        return a_results
    
    def get_contact_emails(self, a_all_contacts=None, **kwargs):
        if not a_all_contacts:
            a_all_contacts = self.get_contacts()
        
        #e_email_vids, e_vid_email = defaultdict(list), defaultdict(list)
        e_email_vid, e_vid_email = {}, {}
        for e_contact in a_all_contacts:
            for a_id_profile in e_contact["identity-profiles"]:
                for e_id in a_id_profile["identities"]:
                    if e_id.get("type") ==  "EMAIL" and "@" in e_id.get("value"):
                        #e_email_vids[e_id["value"]].append(a_id_profile["vid"])
                        #e_vid_email[a_id_profile["vid"]].append(e_id["value"])
                        e_email_vid[e_id["value"]] = a_id_profile["vid"]
                        e_vid_email[a_id_profile["vid"]] = e_id["value"]
        
        return e_vid_email, e_email_vid
        
    ##### SUBSCRIPTIONS #####
    
    def get_subscriptions_groups(self, **kwargs):
        e_parameters = self._parameter_dict.copy()
        o_subscriptions = self._get_response(self._subscriptions_url, e_parameters=e_parameters, **kwargs)
        if isinstance(o_subscriptions, dict):
            e_subscriptions = {e_sub.get("id"): e_sub for e_sub in o_subscriptions["subscriptionDefinitions"]}
            return e_subscriptions
        else:
            return o_subscriptions
    
    def _read_all_subscriptions(self, maximum=100, **kwargs):
        e_parameters =  {'hapikey': self.api_key}
        if "change_type" in kwargs: e_parameters["changeType"] = kwargs["change_type"]
        if "offset" in kwargs: e_parameters["offset"] = kwargs["offset"]
        if "limit" in kwargs: e_parameters["limit"] = min(100, kwargs["limit"])
        
        if not pd.isnull(kwargs.get("since")):
            since = kwargs["since"]
            if isinstance(since, datetime):
                since = self._date_to_timestamp(since)
            elif isinstance(since, float) and not pd.isnull(since):
                since = int(since)
            if kwargs.get("b_debug"): print("since:", since)
            e_parameters["startTimestamp"] = since
        
        a_timelines = []
        while True:
            parameters = urllib.parse.urlencode(e_parameters)
            s_batch_url = f"{self._subscription_timelines_url}?{parameters}"
    
            o_batch = requests.get(url=s_batch_url)
            if not o_batch.ok: break
    
            e_timelines = json.loads(o_batch.text)
    
            a_timelines.extend(e_timelines["timeline"])
            if not e_timelines.get("hasMore"): break
            e_parameters['offset'] = e_timelines["offset"]
            
            if len(a_timelines) >= maximum and maximum > 0: break
        return a_timelines
    
    def get_subscription_statuses(self, **kwargs):
        if kwargs.get("subscription_timelines"):
            a_subscription_timelines = kwargs["subscription_timelines"]
        else:
            a_subscription_timelines = self._read_all_subscriptions(**kwargs)
        #print(f"timelines: {len(a_subscription_timelines)}")
        
        e_subscriptions = defaultdict(dict)
        for e_recipient in sorted(a_subscription_timelines, key=lambda e: e["timestamp"]):
            s_email = e_recipient["recipient"]
            for e_change in sorted(e_recipient["changes"], key=lambda e: e["timestamp"]):
                e_subscriptions[s_email][e_change.get("subscriptionId", "portal")] = e_change["change"]
        return e_subscriptions
    
    def get_subscription_status(self, email, **kwargs):
        s_url = f"{self._subscriptions_url}/{email}"
        e_parameters = self._parameter_dict.copy()
        o_response = self._get_response(s_url, e_parameters=e_parameters, **kwargs)
        return o_response
    
    ##### CONTACT PROPERTIES #####
    def get_contact_property_groups(self, **kwargs):
        e_parameters = self._parameter_dict.copy()
        e_parameters["includeProperties"] = True
        
        a_contact_property_groups = self._get_response(
            self._contact_property_groups_url,
            e_parameters=e_parameters,
            **kwargs)
        e_contact_property_groups = {e["name"]: e for e in a_contact_property_groups}
        
        self.contact_property_groups = e_contact_property_groups
        return e_contact_property_groups
    
    def get_contact_properties(self, **kwargs):
        e_parameters = self._parameter_dict.copy()
        
        a_contact_properties = self._get_response(
            self._contact_properties_url,
            e_parameters=e_parameters,
            **kwargs)
        e_contact_properties = {e["name"]: e for e in a_contact_properties}
        
        self.contact_properties = e_contact_properties
        return e_contact_properties
    
    def create_contact_property_group(self, name, **kwargs):
        if not self.contact_property_groups: self.get_contact_property_groups()
        if self.contact_property_groups.get(name):
            return self.contact_property_groups[name]
        
        e_parameters = self._parameter_dict.copy()
        
        e_group = {"name": name}
        e_group["displayName"] = kwargs.get("display_name", name)
        if "display_order" in kwargs:
            e_group["displayOrder"] = kwargs["display_order"]
            
        o_confirm_response = self._post_response(
            self._contact_property_groups_url,
            e_parameters=e_parameters,
            e_data=e_group,
            **kwargs)
        if isinstance(o_confirm_response, dict):
            self.contact_property_groups[name] = o_confirm_response
            return o_confirm_response
        else:
            if kwargs.get("b_debug"):
                return o_confirm_response
            else:
                return None
    
    def create_contact_property(self, name, data_type, field_type, **kwargs):
        assert name
        assert data_type in {"string", "number", "date", "datetime", "enumeration"}
        assert field_type in {
            "checkbox", "booleancheckbox",
            "calculation_equation", "calculation_read_time", "calculation_score",
            "number",
            "date", "phonenumber",
            "radio", "select",
            "text", "textarea",
        }
        if not self.contact_property_groups: self.get_contact_property_groups()
        if not self.contact_properties: self.get_contact_properties()
        
        if self.contact_properties.get(name):
            return self.contact_properties[name]
        
        e_parameters = self._parameter_dict.copy()
        
        e_new_property = default_contact_property.default_contact_property_ml.copy()
        e_new_property.update({
            "name": name,
            "type": data_type,
            "fieldType": field_type,
        })
        e_new_property["label"] = kwargs.get("label", name)
        e_new_property["description"] = kwargs.get("description", name)
        e_new_property["formField"] = kwargs.get("form_field", False)
        e_new_property["groupName"] = kwargs.get("group_name", "contactinformation")
        if "options" in kwargs:
            e_new_property["options"] = kwargs["options"]
        if "display_order" in kwargs:
            e_new_property["displayOrder"] = kwargs["display_order"]
        
        if e_new_property["groupName"] not in self.contact_property_groups:
            self.create_contact_property_group(e_new_property["groupName"])
        
        if kwargs.get("b_debug"): print("e_new_property", e_new_property)
        
        o_confirm_response = self._post_response(
            self._contact_properties_url,
            e_parameters=e_parameters,
            e_data=e_new_property,
            **kwargs)
        
        if isinstance(o_confirm_response, dict):
            print(f"adding {name} to contact properties")
            self.contact_properties[name] = o_confirm_response
            return o_confirm_response
        else:
            if kwargs.get("b_debug"):
                return o_confirm_response
            else:
                return None
    
    
    ##### COMPANY PROSPECTS #####
    def get_company_property_groups(self, **kwargs):
        e_parameters = self._parameter_dict.copy()
        e_parameters["includeProperties"] = True
        
        a_company_property_groups = self._get_response(
            self._company_property_groups_url,
            e_parameters=e_parameters,
            **kwargs)
        e_company_property_groups = {e["name"]: e for e in a_company_property_groups}
        
        self.company_property_groups = e_company_property_groups
        return e_company_property_groups
    
    def get_company_properties(self, **kwargs):
        e_parameters = self._parameter_dict.copy()
        
        a_company_properties = self._get_response(
            self._company_properties_url,
            e_parameters=e_parameters,
            **kwargs)
        e_company_properties = {e["name"]: e for e in a_company_properties}
        
        self.company_properties = e_company_properties
        return e_company_properties
    
    def create_company_property_group(self, name, **kwargs):
        if not self.company_property_groups: self.get_company_property_groups()
        if self.company_property_groups.get(name):
            return self.company_property_groups[name]
        
        e_parameters = self._parameter_dict.copy()
        
        e_group = {"name": name}
        e_group["displayName"] = kwargs.get("display_name", name)
        if "display_order" in kwargs:
            e_group["displayOrder"] = kwargs["display_order"]
            
        o_confirm_response = self._post_response(
            self._company_property_groups_url,
            e_parameters=e_parameters,
            e_data=e_group,
            **kwargs)
        if isinstance(o_confirm_response, dict):
            self.company_property_groups[name] = o_confirm_response
            return o_confirm_response
        else:
            if kwargs.get("b_debug"):
                return o_confirm_response
            else:
                return None
    
    def create_company_property(self, name, data_type, field_type, **kwargs):
        assert name
        assert data_type in {"string", "number", "date", "datetime", "enumeration"}
        assert field_type in {
            "checkbox", "booleancheckbox"
            "calculation_equation", "calculation_read_time", "calculation_score",
            "number",
            "date", "phonenumber",
            "radio", "select",
            "text", "textarea",
        }
        if not self.company_property_groups: self.get_company_property_groups()
        if not self.company_properties: self.get_company_properties()
        
        if self.company_properties.get(name):
            return self.company_properties[name]
        
        e_parameters = self._parameter_dict.copy()
        
        e_new_property = default_company_property.default_company_property_ml.copy()
        e_new_property.update({
            "name": name,
            "type": data_type,
            "fieldType": field_type,
        })
        e_new_property["label"] = kwargs.get("label", name)
        e_new_property["description"] = kwargs.get("description", name)
        e_new_property["formField"] = kwargs.get("form_field", False)
        e_new_property["groupName"] = kwargs.get("group_name", "companyinformation")
        if "options" in kwargs:
            e_new_property["options"] = kwargs["options"]
        if "display_order" in kwargs:
            e_new_property["displayOrder"] = kwargs["display_order"]
        
        if e_new_property["groupName"] not in self.company_property_groups:
            self.create_company_property_group(e_new_property["groupName"])
        
        if kwargs.get("b_debug"): print("e_new_property", e_new_property)
        
        o_confirm_response = self._post_response(
            self._company_properties_url,
            e_parameters=e_parameters,
            e_data=e_new_property,
            **kwargs)
        
        if isinstance(o_confirm_response, dict):
            print(f"adding {name} to company properties")
            self.company_properties[name] = o_confirm_response
            return o_confirm_response
        else:
            if kwargs.get("b_debug"):
                return o_confirm_response
            else:
                return None
    
    ##### WORKFLOWS #####
    def _read_all_workflows(self, maximum=10000, **kwargs):
        e_parameters = self._parameter_dict.copy()
        
        a_workflows = []
        while True:
            parameters = urllib.parse.urlencode(e_parameters)
            o_workflows = requests.get(url= f"{self._workflows_url}?{parameters}")
            
            if not o_workflows.ok:
                if kwargs.get("b_debug"): print(o_workflows.text)
                break
            
            e_workflows = json.loads(o_workflows.text)
            
            a_workflows.extend(e_workflows["workflows"])
            
            if not e_workflows.get("offset"): break
            e_parameters['offset'] = e_workflows["offset"]
            
            if len(a_workflows) >= maximum and maximum > 0: break
        
        if not kwargs.get("with_details") or not a_workflows:
            return a_workflows
        else:
            a_detailed_workflows = []
            for e_wf in a_workflows:
                s_wf_id = e_wf["id"]
                if e_wf["enabled"] or kwargs.get("disabled_details"):
                    o_workflow = self._get_response(
                        f"{self._workflows_url}/{s_wf_id}",
                        {"errors": True, "stats": True},
                        **kwargs)
                    assert o_workflow and isinstance(o_workflow, dict)
                    a_detailed_workflows.append(o_workflow)
                else:
                    a_detailed_workflows.append(e_wf)
            
            return a_detailed_workflows
    
    def get_workflows(self, **kwargs):
        a_workflows = self._read_all_workflows(**kwargs)
        if kwargs.get("b_debug"): print(len(a_workflows))
        
        if kwargs.get("by_type"):
            e_workflow_types = defaultdict(list)
            for e_wf in a_workflows:
                e_workflow_types[e_wf['type']].append(e_wf)
            return dict(e_workflow_types)
        else:
            return a_workflows
    
    def get_workflow(self, workflow_id, **kwargs):
        s_url = f"{self._workflows_url}/{workflow_id}"
    
        e_parameters = self._parameter_dict.copy()
        e_parameters["include_errors"] = bool(kwargs.get("include_errors", False))
        e_parameters["stats"] = bool(kwargs.get("include_stats", True))
        
        o_response = self._get_response(s_url, e_parameters=e_parameters, **kwargs)
        return o_response
    
    def create_workflow(self, name, workflow_type, workflow_properties, **kwargs):
        e_wf = default_workflow.default_workflow.copy()
        
        assert workflow_type in {"DRIP_DELAY", "STATIC_ANCHOR", "PROPERTY_ANCHOR"}
        
        e_wf["type"] = workflow_type
        e_wf["name"] = name
        
        if "actions" in workflow_properties:
            for e_action in workflow_properties["actions"]:
                assert "type" in e_action
                assert e_action["type"] in {"DELAY", "EMAIL", "SET_CONTACT_PROPERTY", "ADD_SUBTRACT_PROPERTY", "BRANCH"}
            
        e_wf.update(workflow_properties)
        
        o_confirm_response = self._post_response(
            self._workflows_url,
            e_parameters=self._parameter_dict,
            e_data=e_wf,
            **kwargs)
        
        if isinstance(o_confirm_response, dict):
            return o_confirm_response
        else:
            if kwargs.get("b_debug"):
                return o_confirm_response
            else:
                return None
    
    def enroll_in_workflow(self, workflow_id, contact, **kwargs):
        if not isinstance(contact, str) or "@" not in contact:
            e_contact = self.get_contact(contact)
            if not isinstance(e_contact, dict):
                if e_contact.status_code == 404: return (f'Contact "{contact}" not found', False, 404)
            s_email = e_contact.get("properties", {}).get("email", {}).get("value", "")
            if not s_email:
                return (f'Contact "{contact}" doesn\'t have an email address in their hubspot profile.', False, 412)
        else:
            s_email = contact
        s_url = self._workflow_enroll_url.format(workflow_id=workflow_id, email=s_email)
        
        o_confirm_response = self._post_response(
            s_url,
            e_parameters=self._parameter_dict,
            e_data={},
            return_raw=True,
            **kwargs)
        
        try:
            if o_confirm_response.status_code == 204:
                return (f'Contact "{contact}" successfully enrolled in workflow {workflow_id}.', True, 204)
            elif o_confirm_response.status_code == 404:
                return (f'Either "{contact}" is not a valid hubspot contact or workflow {workflow_id} is not a valid workflow.', False, 404)
            elif o_confirm_response.status_code == 412:
                return (f'"{contact}" does not meet the prerequisites for workflow {workflow_id} or is already enrolled.', False, 412)
            else:
                return (f'Unknown problem enrolling "{contact}" in workflow {workflow_id}', False, 400)
        except:
            return o_confirm_response
    
    def unenroll_from_workflow(self, workflow_id, contact, **kwargs):
        if not isinstance(contact, str) or "@" not in contact:
            e_contact = self.get_contact(contact)
            if not isinstance(e_contact, dict):
                if e_contact.status_code == 404: return (f'Contact "{contact}" not found', False, 400)
            s_email = e_contact.get("properties", {}).get("email", {}).get("value", "")
            if not s_email:
                return (f'Contact "{contact}" doesn\'t have an email address in their hubspot profile.', False, 412)
        else:
            s_email = contact
        s_url = self._workflow_enroll_url.format(workflow_id=workflow_id, email=s_email)
        
        o_confirm_response = self._delete_response(
            s_url,
            e_parameters=self._parameter_dict,
            return_raw=True,
            **kwargs)
        
        try:
            if o_confirm_response.status_code == 204:
                return (f'Contact "{contact}" successfully un-enrolled from workflow {workflow_id}.', True, 204)
            elif o_confirm_response.status_code == 404:
                return (f'Either "{contact}" is not a valid hubspot contact or workflow {workflow_id} is not a valid workflow.', False, 404)
            else:
                return (f'Unknown problem un-enrolling "{contact}" in workflow {workflow_id}', False, 400)
        except:
            return o_confirm_response
    
    
    ##### OTHER #####
    def get_touch_summaries(self, **kwargs):
        ''' Get the summaries of engagements and activity per contact (hubspot vid and email) '''
        if not self._call_dispositions: self.get_call_dispositions()
        e_vid_email, e_email_vid = kwargs.get("e_vid_email", {}), kwargs.get("e_email_vid", {})
        if not e_vid_email or not e_email_vid:
            e_vid_email, e_email_vid = self.get_contact_emails(kwargs.get("a_all_contacts"))
        
        e_marketing_recipients = self.get_marketing_recipients(**kwargs)
        a_engagements = self.get_engagements(**kwargs)
        
        e_engagement_recipients = defaultdict(lambda: defaultdict(lambda: {"count": 0, "last": None}))
        e_engagement_type_counts = Counter()
        for e_row in a_engagements:
            s_type = e_row.get("engagement", {})["type"]
            if s_type == "NOTE":
                s_body = e_row["metadata"].get("body")
                re_start = re.search(r"^[-_ A-Za-z0-9]{,50}(?=:)", s_body)
                if re_start:
                    s_type = re_start.group()
            elif s_type == "CALL" and kwargs.get("call_disposition_types"):
                s_disposition = self._call_dispositions.get(e_eng.get("metadata", {}).get("disposition"), "")
                if s_disposition: s_type = f"CALL {s_disposition.upper()}"
            
            dt_created = self._timestamp_to_date(e_row.get("engagement", {})["createdAt"])
    
            for vid in e_row.get("associations", {}).get("contactIds", []):
                e_type = e_engagement_recipients[vid][s_type]
                e_engagement_type_counts[s_type] += 1
                e_type["count"] += 1
                e_type["last"] = dt_created if e_type["last"] is None else max(dt_created, e_type["last"])
        
        for s_type, i_count in e_engagement_type_counts.items():
            print(f"\t{s_type} Engagements: {i_count}")
        
        e_summary_rows = defaultdict(dict)
    
        #for (vid, email), e_types in e_marketing_recipients.items():
        for s_email, e_types in e_marketing_recipients.items():
            e_row_marketing_types = {}
            for s_type, e_info in e_types.items():
                e_row_marketing_types[f"marketing_{s_type.lower()}_count"] = e_info["count"]
                e_row_marketing_types[f"marketing_{s_type.lower()}_last"] = e_info["last"]
            e_summary_rows[(e_email_vid.get(s_email), s_email)].update(e_row_marketing_types)
        
        for vid, e_types in e_engagement_recipients.items():
            e_row_direct_types = {}
            for s_type, e_info in e_types.items():
                e_row_direct_types[f"{s_type.lower()}_count"] = e_info["count"]
                e_row_direct_types[f"{s_type.lower()}_last"] = e_info["last"]
            e_summary_rows[(vid, e_vid_email.get(vid))].update(e_row_direct_types)
        
        return e_summary_rows
    
    
    def get_touches(self, **kwargs):
        ''' Get all the engagements and activity per contact (hubspot vid and email) '''
        if not self._call_dispositions: self.get_call_dispositions()
        e_vid_email, e_email_vid = kwargs.get("e_vid_email", {}), kwargs.get("e_email_vid", {})
        if not e_vid_email or not e_email_vid:
            e_vid_email, e_email_vid = self.get_contact_emails(kwargs.get("a_all_contacts"))
        
        since = kwargs.get("since")
        if isinstance(since, datetime):
            since = self._date_to_timestamp(since)
        elif isinstance(since, float) and not pd.isnull(since):
            since = int(since)
        if since and kwargs.get("b_debug"): print("since:", since)
        
        # get list of all marketing activities/events
        
        a_emails = self.get_marketing_emails(with_details=True, **kwargs)
        e_marketing_emails = {
            (e_em["emailCampaignGroupId"], i_email_campaign): e_em
            for e_em in a_emails
            for i_email_campaign in e_em.get("allEmailCampaignIds", [])}
        
        #print("e_marketing_emails", e_marketing_emails.keys())
        
        e_activity_keys = {
            "id": "item_id",
            "created": "created",
            "recipient": "email",
            "type": "event_type",
            "appId": "workflow_id",
            "emailCampaignId": "email_campaign",
            "emailCampaignGroupId": "email_campaign_group",}
        
        c_skip_events = {s_type.lower() for s_type in kwargs.get("skip_event_types", ["sends"])}
        
        e_all_activity = self.get_event_activity(skip_types=c_skip_events, **kwargs)
        a_marketing_events = []
        for s_activity, a_events in e_all_activity.items():
            if s_activity in c_skip_events: continue
            for idx, e_event in enumerate(a_events):
                e_ev = {e_activity_keys[k]: v for k, v in e_event.items() if k in e_activity_keys}
                if "created" in e_ev: e_ev["created"] = self._timestamp_to_date(e_ev["created"])
                e_ev["event_type"] = "MARKETING EMAIL {}".format(e_ev.get("event_type", "")).strip()
                e_ev["hubspot_vid"] = e_email_vid.get(e_ev.get("email"))
                
                t_email_id = (e_ev.get("email_campaign_group"), e_ev.get("email_campaign"))
                #print("t_email_id", t_email_id)
                if t_email_id in e_marketing_emails:
                    e_ev["metadata"] = e_marketing_emails[t_email_id]
                
                a_marketing_events.append(e_ev)
        
        # get list of direct engagements
        a_engagements = self.get_engagements(**kwargs)
        e_engagement_keys = {
            "id": "item_id",
            "ownerId": "hubspot_owner",
            "createdAt": "created",
            "recipient": "email",
            "type": "event_type",
            "metadata": "metadata",}
        
        a_direct_engagements = []
        e_unique_engagements = {}
        for idx, e_engagement in enumerate(a_engagements):
            a_contact_ids = e_engagement.get("associations", {}).get("contactIds", [])
            e_eng = {
                e_engagement_keys[k]: v
                for k, v in e_engagement.get("engagement", {}).items()
                if k in e_engagement_keys}
            
            if "created" in e_eng: e_eng["created"] = self._timestamp_to_date(e_eng["created"])
            if e_engagement.get("metadata"):
                e_eng["metadata"] = e_engagement["metadata"]
            
            s_event_type = e_eng.get("event_type", "")
            if s_event_type == "NOTE":
                re_start = None
                if isinstance(e_eng["metadata"], dict):
                    re_start = re.search(r"^[-_ A-Za-z0-9]{,50}(?=:)", e_eng["metadata"].get("body", ""))
                elif isinstance(e_eng["metadata"], str):
                    re_start = re.search(r"^[-_ A-Za-z0-9]{,50}(?=:)", e_eng["metadata"])
                if re_start:
                    s_event_type = re_start.group()
            elif s_event_type == "CALL" and kwargs.get("call_disposition_types"):
                s_disposition = self._call_dispositions.get(e_eng.get("metadata", {}).get("disposition"), "")
                if s_disposition: s_event_type = f"CALL {s_disposition.upper()}"
            
            dt_created = e_eng.get("created", "")
            i_item_id = e_eng.get("item_id", 0)
            
            # should we store touches without any contact ids?
            for i_contact in set(a_contact_ids):
                #if kwargs.get("b_debug"): print(idx, i_contact)
                e_touch = e_eng.copy()
                e_touch["hubspot_vid"] = i_contact
                e_touch["email"] = e_vid_email.get(i_contact, "")
                
                i_hubspot_vid = e_touch.get("hubspot_vid", 0)
                assert i_contact == i_hubspot_vid
                #print("s_event_type", s_event_type, type(s_event_type))
                #print("dt_created", dt_created, type(dt_created))
                #print("i_item_id", i_item_id, type(s_item_id))
                #print("i_hubspot_vid", i_hubspot_vid, type(i_hubspot_vid))
                #print((s_event_type, dt_created, i_item_id, i_hubspot_vid))
                
                a_direct_engagements.append(e_touch)
                e_unique_engagements[(s_event_type, dt_created, i_item_id, i_hubspot_vid)] = e_touch
        
        if kwargs.get("dedupe_engagements", True):
            #print("Return unique", len(a_direct_engagements), len(e_unique_engagements))
            return a_marketing_events + list(e_unique_engagements.values())
        else:
            return a_marketing_events + a_direct_engagements
        