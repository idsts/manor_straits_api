﻿import re
from operator import itemgetter
from collections import defaultdict
import pandas as pd
import phonenumbers
from nameparser import HumanName

try:
    from .utils import *
except:
    from utils import *

cdef dict e_DIRECTION_ABBREVIATIONS = {
    'north': "N",
    'south': "S",
    'east': "E",
    'west': "W",
    'northeast': "NE",
    'northwest': "NW",
    'southeast': "SE",
    'southwest': "SW",
    'ne': "NE",
    'nw': "NW",
    'se': "SE",
    'sw': "SW",
}

cdef dict e_COUNTRY_ABBREVIATIONS = {
    "afghanistan": "AF", "afg": "AF",
    "albania": "AL", "alb": "AL",
    "algeria": "DZ", "dza": "DZ",
    "american samoa": "AS", "asm": "AS",
    "andorra": "AD", "and": "AD",
    "angola": "AO", "ago": "AO",
    "anguilla": "AI", "aia": "AI",
    "antarctica": "AQ", "ata": "AQ",
    "antigua and barbuda": "AG", "atg": "AG",
    "argentina": "AR", "arg": "AR",
    "armenia": "AM", "arm": "AM",
    "aruba": "AW", "abw": "AW",
    "australia": "AU", "aus": "AU",
    "austria": "AT", "aut": "AT",
    "azerbaijan": "AZ", "aze": "AZ",
    "bahamas": "BS", "bhs": "BS",
    "bahrain": "BH", "bhr": "BH",
    "bangladesh": "BD", "bgd": "BD",
    "barbados": "BB", "brb": "BB",
    "belarus": "BY", "blr": "BY",
    "belgium": "BE", "bel": "BE",
    "belize": "BZ", "blz": "BZ",
    "benin": "BJ", "ben": "BJ",
    "bermuda": "BM", "bmu": "BM",
    "bhutan": "BT", "btn": "BT",
    "bolivia": "BO", "bol": "BO",
    "bonaire": "BQ", "bes": "BQ",
    "bosnia and herzegovina": "BA", "bih": "BA",
    "botswana": "BW", "bwa": "BW",
    "bouvet island": "BV", "bvt": "BV",
    "brazil": "BR", "bra": "BR",
    "british indian ocean territory": "IO", "iot": "IO",
    "brunei darussalam": "BN", "brn": "BN",
    "bulgaria": "BG", "bgr": "BG",
    "burkina faso": "BF", "bfa": "BF",
    "burundi": "BI", "bdi": "BI",
    "cambodia": "KH", "khm": "KH",
    "cameroon": "CM", "cmr": "CM",
    "canada": "CA", "can": "CA",
    "cape verde": "CV", "cpv": "CV",
    "cayman islands": "KY", "cym": "KY",
    "central african republic": "CF", "caf": "CF",
    "chad": "TD", "tcd": "TD",
    "chile": "CL", "chl": "CL",
    "china": "CN", "chn": "CN",
    "christmas island": "CX", "cxr": "CX",
    "cocos (keeling) islands": "CC", "cck": "CC",
    "colombia": "CO", "col": "CO",
    "comoros": "KM", "com": "KM",
    "congo": "CG", "cog": "CG",
    "democratic republic of the congo": "CD", "cod": "CD",
    "cook islands": "CK", "cok": "CK",
    "costa rica": "CR", "cri": "CR",
    "croatia": "HR", "hrv": "HR",
    "cuba": "CU", "cub": "CU",
    "curacao": "CW", "cuw": "CW",
    "cyprus": "CY", "cyp": "CY",
    "czech republic": "CZ", "cze": "CZ",
    "cote d'ivoire": "CI", "civ": "CI",
    "denmark": "DK", "dnk": "DK",
    "djibouti": "DJ", "dji": "DJ",
    "dominica": "DM", "dma": "DM",
    "dominican republic": "DO", "dom": "DO",
    "ecuador": "EC", "ecu": "EC",
    "egypt": "EG", "egy": "EG",
    "el salvador": "SV", "slv": "SV",
    "equatorial guinea": "GQ", "gnq": "GQ",
    "eritrea": "ER", "eri": "ER",
    "estonia": "EE", "est": "EE",
    "ethiopia": "ET", "eth": "ET",
    "falkland islands (malvinas)": "FK", "flk": "FK",
    "faroe islands": "FO", "fro": "FO",
    "fiji": "FJ", "fji": "FJ",
    "finland": "FI", "fin": "FI",
    "france": "FR", "fra": "FR",
    "french guiana": "GF", "guf": "GF",
    "french polynesia": "PF", "pyf": "PF",
    "french southern territories": "TF", "atf": "TF",
    "gabon": "GA", "gab": "GA",
    "gambia": "GM", "gmb": "GM",
    "georgia": "GE", "geo": "GE",
    "germany": "DE", "deu": "DE",
    "ghana": "GH", "gha": "GH",
    "gibraltar": "GI", "gib": "GI",
    "greece": "GR", "grc": "GR",
    "greenland": "GL", "grl": "GL",
    "grenada": "GD", "grd": "GD",
    "guadeloupe": "GP", "glp": "GP",
    "guam": "GU", "gum": "GU",
    "guatemala": "GT", "gtm": "GT",
    "guernsey": "GG", "ggy": "GG",
    "guinea": "GN", "gin": "GN",
    "guinea-bissau": "GW", "gnb": "GW",
    "guyana": "GY", "guy": "GY",
    "haiti": "HT", "hti": "HT",
    "heard island and mcdonald islands": "HM", "hmd": "HM",
    "holy see (vatican city state)": "VA", "holy see": "VA", "vatican city state": "VA", "vat": "VA",
    "honduras": "HN", "hnd": "HN",
    "hong kong": "HK", "hkg": "HK",
    "hungary": "HU", "hun": "HU",
    "iceland": "IS", "isl": "IS",
    "india": "IN", "ind": "IN",
    "indonesia": "ID", "idn": "ID",
    "iran, islamic republic of": "IR", "irn": "IR",
    "iraq": "IQ", "irq": "IQ",
    "ireland": "IE", "irl": "IE",
    "isle of man": "IM", "imn": "IM",
    "israel": "IL", "isr": "IL",
    "italy": "IT", "ita": "IT",
    "jamaica": "JM", "jam": "JM",
    "japan": "JP", "jpn": "JP",
    "jersey": "JE", "jey": "JE",
    "jordan": "JO", "jor": "JO",
    "kazakhstan": "KZ", "kaz": "KZ",
    "kenya": "KE", "ken": "KE",
    "kiribati": "KI", "kir": "KI",
    "korea, democratic people's republic of": "KP", "prk": "KP",
    "korea, republic of": "KR", "kor": "KR",
    "kuwait": "KW", "kwt": "KW",
    "kyrgyzstan": "KG", "kgz": "KG",
    "lao people's democratic republic": "LA", "lao": "LA",
    "latvia": "LV", "lva": "LV",
    "lebanon": "LB", "lbn": "LB",
    "lesotho": "LS", "lso": "LS",
    "liberia": "LR", "lbr": "LR",
    "libya": "LY", "lby": "LY",
    "liechtenstein": "LI", "lie": "LI",
    "lithuania": "LT", "ltu": "LT",
    "luxembourg": "LU", "lux": "LU",
    "macao": "MO", "mac": "MO",
    "macedonia, the former yugoslav republic of": "MK", "mkd": "MK",
    "madagascar": "MG", "mdg": "MG",
    "malawi": "MW", "mwi": "MW",
    "malaysia": "MY", "mys": "MY",
    "maldives": "MV", "mdv": "MV",
    "mali": "ML", "mli": "ML",
    "malta": "MT", "mlt": "MT",
    "marshall islands": "MH", "mhl": "MH",
    "martinique": "MQ", "mtq": "MQ",
    "mauritania": "MR", "mrt": "MR",
    "mauritius": "MU", "mus": "MU",
    "mayotte": "YT", "myt": "YT",
    "mexico": "MX", "mex": "MX",
    "micronesia, federated states of": "FM", "fsm": "FM",
    "moldova, republic of": "MD", "mda": "MD",
    "monaco": "MC", "mco": "MC",
    "mongolia": "MN", "mng": "MN",
    "montenegro": "ME", "mne": "ME",
    "montserrat": "MS", "msr": "MS",
    "morocco": "MA", "mar": "MA",
    "mozambique": "MZ", "moz": "MZ",
    "myanmar": "MM", "mmr": "MM",
    "namibia": "NA", "nam": "NA",
    "nauru": "NR", "nru": "NR",
    "nepal": "NP", "npl": "NP",
    "netherlands": "NL", "nld": "NL",
    "new caledonia": "NC", "ncl": "NC",
    "new zealand": "NZ", "nzl": "NZ",
    "nicaragua": "NI", "nic": "NI",
    "niger": "NE", "ner": "NE",
    "nigeria": "NG", "nga": "NG",
    "niue": "NU", "niu": "NU",
    "norfolk island": "NF", "nfk": "NF",
    "northern mariana islands": "MP", "mnp": "MP",
    "norway": "NO", "nor": "NO",
    "oman": "OM", "omn": "OM",
    "pakistan": "PK", "pak": "PK",
    "palau": "PW", "plw": "PW",
    "palestine, state of": "PS", "pse": "PS",
    "panama": "PA", "pan": "PA",
    "papua new guinea": "PG", "png": "PG",
    "paraguay": "PY", "pry": "PY",
    "peru": "PE", "per": "PE",
    "philippines": "PH", "phl": "PH",
    "pitcairn": "PN", "pcn": "PN",
    "poland": "PL", "pol": "PL",
    "portugal": "PT", "prt": "PT",
    "puerto rico": "PR", "pri": "PR",
    "qatar": "QA", "qat": "QA",
    "romania": "RO", "rou": "RO",
    "russian federation": "RU", "rus": "RU",
    "rwanda": "RW", "rwa": "RW",
    "reunion": "RE", "reu": "RE",
    "saint barthelemy": "BL", "blm": "BL",
    "saint helena": "SH", "shn": "SH",
    "saint kitts and nevis": "KN", "kna": "KN",
    "saint lucia": "LC", "lca": "LC",
    "saint martin (french part)": "MF", "saint martin": "MF", "maf": "MF",
    "saint pierre and miquelon": "PM", "spm": "PM",
    "saint vincent and the grenadines": "VC", "vct": "VC",
    "samoa": "WS", "wsm": "WS",
    "san marino": "SM", "smr": "SM",
    "sao tome and principe": "ST", "stp": "ST",
    "saudi arabia": "SA", "sau": "SA",
    "senegal": "SN", "sen": "SN",
    "serbia": "RS", "srb": "RS",
    "seychelles": "SC", "syc": "SC",
    "sierra leone": "SL", "sle": "SL",
    "singapore": "SG", "sgp": "SG",
    "sint maarten (dutch part)": "SX", "sxm": "SX",
    "slovakia": "SK", "svk": "SK",
    "slovenia": "SI", "svn": "SI",
    "solomon islands": "SB", "slb": "SB",
    "somalia": "SO", "som": "SO",
    "south africa": "ZA", "zaf": "ZA",
    "south georgia and the south sandwich islands": "GS", "sgs": "GS",
    "south sudan": "SS", "ssd": "SS",
    "spain": "ES", "esp": "ES",
    "sri lanka": "LK", "lka": "LK",
    "sudan": "SD", "sdn": "SD",
    "suriname": "SR", "sur": "SR",
    "svalbard and jan mayen": "SJ", "sjm": "SJ",
    "swaziland": "SZ", "swz": "SZ",
    "sweden": "SE", "swe": "SE",
    "switzerland": "CH", "che": "CH",
    "syrian arab republic": "SY", "syr": "SY",
    "taiwan": "TW", "twn": "TW",
    "tajikistan": "TJ", "tjk": "TJ",
    "united republic of tanzania": "TZ", "tza": "TZ",
    "thailand": "TH", "tha": "TH",
    "timor-leste": "TL", "tls": "TL",
    "togo": "TG", "tgo": "TG",
    "tokelau": "TK", "tkl": "TK",
    "tonga": "TO", "ton": "TO",
    "trinidad and tobago": "TT", "tto": "TT",
    "tunisia": "TN", "tun": "TN",
    "turkey": "TR", "tur": "TR",
    "turkmenistan": "TM", "tkm": "TM",
    "turks and caicos islands": "TC", "tca": "TC",
    "tuvalu": "TV", "tuv": "TV",
    "uganda": "UG", "uga": "UG",
    "ukraine": "UA", "ukr": "UA",
    "united arab emirates": "AE", "are": "AE",
    "united kingdom": "GB", "great britain": "GB", "britain": "GB", "gbr": "GB",
    "united states": "US", "united states of america": "US", "usa": "US", "us of a": "US",
    "united states minor outlying islands": "UM", "umi": "UM",
    "uruguay": "UY", "ury": "UY",
    "uzbekistan": "UZ", "uzb": "UZ",
    "vanuatu": "VU", "vut": "VU",
    "venezuela": "VE", "ven": "VE",
    "viet nam": "VN", "vnm": "VN",
    "british virgin islands": "VG", "vgb": "VG",
    "us virgin islands": "VI", "vir": "VI",
    "wallis and futuna": "WF", "wlf": "WF",
    "western sahara": "EH", "esh": "EH",
    "yemen": "YE", "yem": "YE",
    "zambia": "ZM", "zmb": "ZM",
    "zimbabwe": "ZW", "zwe": "ZW",
}

cdef dict e_STATE_ABBREVIATIONS = {
    'alaska': "AK",
    'alabama': "AL",
    'arkansas': "AR",
    'arizona': "AZ",
    'california': "CA",
    'colorado': "CO",
    'connecticut': "CT",
    'district of columbia': "DC",
    'delaware': "DE",
    'florida': "FL",
    'georgia': "GA",
    'hawaii': "GU",
    'iowa': "IA",
    'idaho': "ID",
    'illinois': "IL",
    'indiana': "IN",
    'kansas': "KS",
    'kentucky': "KY",
    'louisiana': "LA",
    'massachusetts': "MA",
    'maryland': "MD",
    'maine': "ME",
    'michigan': "MI",
    'minnesota': "MN",
    'missouri': "MO",
    'mississippi': "MS",
    'montana': "MT",
    'north carolina': "NC",
    'north dakota': "ND",
    'nebraska': "NE",
    'new hampshire': "NH",
    'new jersey': "NJ",
    'new mexico': "NM",
    'nevada': "NV",
    'new york': "NY",
    'ohio': "OH",
    'oklahoma': "OK",
    'oregon': "OR",
    'pennsylvania': "PA",
    'rhode island': "RI",
    'south carolina': "SC",
    'south dakota': "SD",
    'tennessee': "TN",
    'texas': "TX",
    'utah': "UT",
    'virginia': "VA",
    'vermont': "VT",
    'washington': "WA",
    'wisconsin': "WI",
    'west virginia': "WV",
    'wyoming': "WY"
}

cdef dict e_STREET_WORDS_TO_ABBREVIATE = {
    "street": "St",
    "streets": "St",
    "sts": "St",
    "str": "St",
    "strts": "St",
    "route": "Rt",
    "routes": "Rt",
    "rte": "Rt",
    "rts": "Rt",
    "rtes": "Rt",
    "avenue": "Ave",
    "avenues": "Ave",
    "court": "Ct",
    "courts": "Ct",
    "crt": "Ct",
    "drive": "Dr",
    "drives": "Dr",
    "drv": "Dr",
    "road": "Rd",
    "roads": "Rd",
    "highway": "Hwy",
    "highways": "Hwy",
    "boulevard": "Blvd",
    "boulevards": "Blvd",
    "circle": "Cir",
    "circles": "Cir",
    "circ": "Cir",
    "lane": "Ln",
    "lanes": "Ln",
    "place": "Pl",
    "places": "Pl",
    "plaza": "Plz",
    "plazas": "Plz",
    "terrace": "Ter",
    "terraces": "Ter",
    "trail": "Trl",
    "trails": "Trl",
    "turnpike": "Tnpk",
    "turnpikes": "Tnpk",
    "way": "Way",
    "ways": "Way",
    "annex": "Anx",
    "brook": "Brk",
    "brooks": "Brk",
    "bypass": "Byp",
    "canyon": "Cyn",
    "canyons": "Cyn",
    "corner": "Cor",
    "corners": "Cor",
    "cove": "Cv",
    "coves": "Cv",
    "creek": "Crk",
    "creeks": "Crk",
    "crossing": "Xing",
    "crossings": "Xing",
    "expressway": "Expy",
    "expressways": "Expy",
    "fall": "Fls",
    "falls": "Fls",
    "flat": "Flt",
    "flats": "Flt",
    "fork": "Frk",
    "forks": "Frk",
    "freeway": "Fwy",
    "freeways": "Fwy",
    "garden": "Gdn",
    "gardens": "Gdn",
    "glen": "Gln",
    "glens": "Gln",
    "harbor": "Hbr",
    "harbors": "Hbr",
    "heights": "Hts",
    "hill": "Hls",
    "hills": "Hls",
    "hollow": "Holw",
    "hollows": "Holw",
    "interstate": "I",
    "island": "Is",
    "islands": "Is",
    "junction": "Jnct",
    "junctions": "Jnct",
    "key": "Ky",
    "keys": "Ky",
    "lake": "Lk",
    "lakes": "Lk",
    "landing": "Lndg",
    "landings": "Lndg",
    "light": "Lgt",
    "lights": "Lgt",
    "loop": "Lp",
    "manor": "Mnr",
    "manors": "Mnr",
    "meadow": "Mdw",
    "meadows": "Mdw",
    "mill": "Ml",
    "mills": "Ml",
    "motorway": "Mtwy",
    "motorways": "Mtwy",
    "mount": "Mt",
    "mountain": "Mtn",
    "mountains": "Mtn",
    "parks": "Park",
    "parkway": "Pkwy",
    "parkways": "Pkwy",
    "plain": "pln",
    "plains": "pln",
    "point": "Pt",
    "points": "Pt",
    "port": "Prt",
    "ports": "Prt",
    "prairie": "Pr",
    "prairies": "Pr",
    "rapid": "Rpd",
    "rapids": "Rpd",
    "ridge": "Rdg",
    "ridges": "Rdg",
    "river": "Riv",
    "rivers": "Riv",
    "shoal": "Shl",
    "shoals": "Shl",
    "shore": "Shr",
    "shores": "Shr",
    "skyway": "Skwy",
    "spurs": "Spur",
    "square": "Sq",
    "squares": "Sq",
    "station": "Sta",
    "stations": "Sta",
    "summit": "Smt",
    "summits": "Smt",
    "throughway": "Trwy",
    "throughways": "Trwy",
    "trace": "Trce",
    "traces": "Trce",
    "track": "Trak",
    "tracks": "Trak",
    "trail": "Trl",
    "trails": "Trl",
    "tunnel": "Tunl",
    "tunnels": "Tunl",
    "union": "Un",
    "unions": "Un",
    "valley": "Vly",
    "valleys": "Vly",
    "view": "Vw",
    "views": "Vw",
    "village": "Vlg",
    "villages": "Vlg",
    "ville": "Vl",
    "vista": "Vis",
    "vistas": "Vis",
    "walks": "Walk",
    "well": "Wl",
    "wells": "Wl",
}

cdef dict e_ROOM_WORDS_TO_ABBREVIATE = {
    "suite": "Ste",
    "apartment": "Apt",
    "room": "Rm",
    "floor": "Fl",
    "unit": "#",
    "#": "#",
    "basement": "Bsmt",
    "building": "Bldg",
    "department": "Dept",
    "front": "Frnt",
    "hangar": "Hngr",
    "lobby": "Lbby",
    "lot": "Lot",
    "lower": "Lowr",
    "office": "Ofc",
    "penthouse": "Ph",
    "space": "Spc",
    "trailer": "Trlr",
    "upper": "uppr",
}

cdef dict e_CITY_WORDS_TO_ABBREVIATE = {
    "mount": "Mt",
    "saint": "St",
    "heights": "Hts",
    "ct": "City",
    "twn": "Town",
    "township": "Twp",
    "junction": "Jct",
    "village": "Vill",
    "point": "Pt",
    "beach": "bch",
    "": "",
    "va": "Virginia",
    "okc": "Oklahoma City",
    "ftc": "Ft Collins",
    "southfiled": "Southfield",
    "sh village": "Southampton Vill",
    "sh vill": "Southampton Vill",
    "sh": "Southampton Vill",
    "": "",
}

cdef dict e_COMPANY_ABBREVIATIONS = {
    'company': "Co",
    'corporation': "Corp",
    'incorporated': "Inc",
    'limited': "Ltd",
    'international': "Intl",
    'association': "Assn",
}

re_DIRECTIONS_ABBR = re.compile(r"\b({})\b".format('|'.join(e_DIRECTION_ABBREVIATIONS.keys())), flags=re.I)
re_STREET_WORDS_ABBR = re.compile(r"\b({})\b".format('|'.join(e_STREET_WORDS_TO_ABBREVIATE.keys())), flags=re.I)
re_ROOM_WORDS_ABBR = re.compile(r"\b({})\b".format('|'.join(e_ROOM_WORDS_TO_ABBREVIATE.keys())), flags=re.I)
re_CITY_WORDS_ABBR = re.compile(r"\b({})\b".format('|'.join(e_CITY_WORDS_TO_ABBREVIATE.keys())), flags=re.I)

re_ADDR_EXTRA_PUNCT = re.compile(r"[-._/\\]+")
re_ADDR_EXTRA_SPACES = re.compile(r" +(?=[,. ])")
re_ZIP_CODE = re.compile(r"^([^\d]+)?(?P<zip>\d{4,5})([- ./](?P<ext>\d{4,4}))?")

re_PO_BOX = re.compile(r"P[. ]?O[. ]?[- _]Box *((#? *)?\d+)?", flags=re.I)

re_ROOM_ABBR = re.compile(
    r",? *({})[-.# ]*[0-9A-Z]+(-[0-9A-Z]+)*".format(
    '|'.join([re.escape(x) for x in e_ROOM_WORDS_TO_ABBREVIATE.values()])), flags=re.I)

re_STREET_NUMBER = re.compile(r"^ *\d+\b( *\d+\b)*")
re_PHONE_LABEL = re.compile("^(cell|phone|mobile|tele|telephone|telefono|fax|work|office|home)[-:. ]*", flags=re.I)
re_COMPANY_ABBR = re.compile(r"\b({})\b".format('|'.join(e_COMPANY_ABBREVIATIONS.keys())), flags=re.I)
re_ORDINALS = re.compile("(?<=\d)(st|nd|rd|th)(?=[^0-9a-z])", flags=re.I)


cdef dict e_FIELDS = {
    "hubspot_vid": [
        "hubspot_vid", "hubspot", "vid"],
    
    "owner_email": [
        "owner_email", "email", "e-mail", "e_mail",
        "email_address", "e-mail-address", "e_mail_address",
        "personal_email", "personal email", "personal e-mail",
        "personal_email_address", "personal email address", "personal e-mail address",
    ],
    "owner_email_2": [
        "owner_email_2", "work_email", "work email", "work e-mail",
        "business_email", "business email", "business e-mail", "office_email", "office email", "office e-mail",
        "email_2", "e-mail_2", "e_mail_2", "email2", "e-mail-2", "e_mail2",
        "email_address_2", "e-mail-address_2", "e_mail_address_2",
        "email_address2", "e-mail-address2", "e_mail_address2"],
    "owner_email_3": [
        "owner_email_3", "email_3", "e-mail_3", "e_mail_3", "email3", "e-mail-3", "e_mail3",
        "email_address_3", "e-mail-address_3", "e_mail_address_3",
        "email_address3", "e-mail-address3", "e_mail_address3"],
    
    "owner_phone": [
        "owner_phone", "phone", "number", "phone_number", "phonenumber",
        "mobile", "mobilephone", "mobilenumber", "mobile_phone", "mobile_number", "mobile-phone", "mobile-number", "mobile phone", "mobile number",
        "cell", "cellphone", "cellnumber", "cell_phone", "cell_number", "cell-phone", "cell-number", "cell phone", "cell number",
    ],
    "owner_phone_2": [
        "owner_phone_2",  "phone2", "phone_2", "phone_number_2", "phonenumber_2", "phone-number-2", "phonenumber-2",
        "mobile_2", "mobilephone_2", "mobilenumber_2", "mobile_phone_2", "mobile_number_2", "mobile-phone_2", "mobile-number_2", "mobile phone_2", "mobile number_2",
        "cell_2", "cellphone_2", "cellnumber_2", "cell_phone_2", "cell_number_2", "cell-phone_2", "cell-number_2", "cell phone_2", "cell number_2",
    ],
    "owner_phone_3": [
        "owner_phone_3",  "phone3", "phone_3", "phone_number_3", "phonenumber_3", "phone-number-3", "phonenumber-3",
        "mobile_3", "mobilephone_3", "mobilenumber_3", "mobile_phone_3", "mobile_number_3", "mobile-phone_3", "mobile-number_3", "mobile phone_3", "mobile number_3",
        "cell_3", "cellphone_3", "cellnumber_3", "cell_phone_3", "cell_number_3", "cell-phone_3", "cell-number_3", "cell phone_3", "cell number_3",
    ],
    "owner_fax": [
        "owner_fax", "fax", "fax_number", "facsimile", "fax_machine"],
    
    "owner_first_name": [
        "owner_first_name", "first_name", "firstname", "first-name", "first name", "first"],
    "owner_last_name": [
        "owner_last_name", "last_name", "lastname", "last-name", "last name", "last",
        "surname", "sur-name", "sur_name",
        "familyname", "family name", "family_name", "family-name"],
    "owner_name": [
        "owner_name", "name", "full_name", "full-name", "full_Name"],
    "owner_company": [
        "owner_company", "company", "organization", "comp.", "co."],
    "ownership_status": [
        "ownership_status", "ownership-status", "ownership status"],
    
    "owner_unit": [
        "owner_unit", "unit", "suite", "apartment", "apt", "ste"],
    
    "owner_address": [
        "owner_address", "address", "address1", "address_1", "address-1",
        "street_address", "street_address1", "street_address_1", "street_address-1",
        "street address", "street address1", "street address_1", "street address-1",
        "street-address", "street-address1", "street-address_1", "street-address-1",
        "address_line", "address_line1", "address_line_1", "address_line-1",
        "address line", "address line1", "address line_1", "address line-1",
        "address-line", "address-line1", "address-line_1", "address-line-1",
        "street1",
    ],
    "owner_address_2": [
        "owner_address_2", "address2", "address_2", "address-2",
        "street_address2", "street_address_2", "street_address-2",
        "street address2", "street address_2", "street address-2",
        "street-address2", "street-address_2", "street-address-2"
        "address_line2", "address_line_2", "address_line-2",
        "address line2", "address line_2", "address line-2",
        "address-line2", "address-line_2", "address-line-2",
        "street2",
    ],
    
    "owner_city": ["owner_city", "city", "town", "city_name", "cityname", "city-name"],
    "owner_state": ["owner_state", "state", "state/territory", "territory", "state/province", "province"],
    "owner_zip": [
        "owner_zip", "zip", "zipcode", "zip_code", "zip-code", "zip code", "zip5",
        "postal_code", "postal code", "postal-code", "post_code", "post code", "post-code"],
    "owner_country": ["owner_country", "country"],
    
    "owner_website": [
        "owner_website", "website", "web site", "url", "site",
        "company_website", "company-website", "company website",
        "company_site", "company-site", "company site",
    ],
    
    "other_address": [
        "other_address", "other-address", "other address",
        "alternate_address", "alternate-address", "alternate address",
        "address3", "address_3", "address-3",
        "p.o. box", "po. box", "po box", "pobox", "po_box", "po box", "po-box",
    ],
    
    
    "contact_name": [
        "contact_name", "contact_full_name",
        "other_contact", "other-contact", "other contact", "other_name", "other-name", "other name",
        "alternate_contact", "alternate-contact", "alternate contact", "alternate_name", "alternate-name", "alternate name",
    ],
}

e_HUBSPOT_FIELDS = {
    # Contact
    "vid": ["vid", "hubspot_vid", "hubspot"],
    "firstname": [
        "owner_first_name", "first_name", "firstname", "first-name", "first name", "first"],
    "lastname": [
        "owner_last_name", "last_name", "lastname", "last-name", "last name", "last",
        "surname", "sur-name", "sur_name",
        "familyname", "family name", "family_name", "family-name"],
    "salutation": ["salutation"],
    
    "email": [
        "owner_email", "email", "e-mail", "e_mail",
        "email_address", "e-mail-address", "e_mail_address",
        "personal_email", "personal email", "personal e-mail",
        "personal_email_address", "personal email address", "personal e-mail address"],
    "work_email": [
        "owner_email_2", "work_email", "work email", "work e-mail",
        "business_email", "business email", "business e-mail", "office_email", "office email", "office e-mail",
        "email_2", "e-mail_2", "e_mail_2", "email2", "e-mail-2", "e_mail2",
        "email_address_2", "e-mail-address_2", "e_mail_address_2",
        "email_address2", "e-mail-address2", "e_mail_address2"],
    
    "phone": [
        "owner_phone", "phone", "number", "phone_number", "phonenumber",
    ],
    "mobilephone": [
        "mobile", "mobilephone", "mobilenumber", "mobile_phone", "mobile_number", "mobile-phone", "mobile-number", "mobile phone", "mobile number",
        "cell", "cellphone", "cellnumber", "cell_phone", "cell_number", "cell-phone", "cell-number", "cell phone", "cell number",
        "owner_phone_2",  "phone2", "phone_2", "phone_number_2", "phonenumber_2", "phone-number-2", "phonenumber-2",
        "mobile_2", "mobilephone_2", "mobilenumber_2", "mobile_phone_2", "mobile_number_2", "mobile-phone_2", "mobile-number_2", "mobile phone_2", "mobile number_2",
        "cell_2", "cellphone_2", "cellnumber_2", "cell_phone_2", "cell_number_2", "cell-phone_2", "cell-number_2", "cell phone_2", "cell number_2",
    ],
    "fax": [
        "owner_fax", "fax", "fax_number", "facsimile", "fax_machine"],
    
    "address": [
        "owner_address", "address", "address1", "address_1", "address-1",
        "street_address", "street_address1", "street_address_1", "street_address-1",
        "street address", "street address1", "street address_1", "street address-1",
        "street-address", "street-address1", "street-address_1", "street-address-1",
        "address_line", "address_line1", "address_line_1", "address_line-1",
        "address line", "address line1", "address line_1", "address line-1",
        "address-line", "address-line1", "address-line_1", "address-line-1",],
    "street_address_2": [
        "owner_address_2", "address2", "address_2", "address-2",
        "street_address2", "street_address_2", "street_address-2",
        "street address2", "street address_2", "street address-2",
        "street-address2", "street-address_2", "street-address-2"
        "address_line2", "address_line_2", "address_line-2",
        "address line2", "address line_2", "address line-2",
        "address-line2", "address-line_2", "address-line-2",
        "owner_unit", "unit", "suite", "apartment", "apt", "ste"],
    "city": ["owner_city", "city", "town", "city_name", "cityname", "city-name"],
    "state": ["owner_state", "state", "state/territory", "territory", "state/province", "province"],
    "zip": ["owner_zip", "zip", "zipcode", "zip_code", "zip-code", "zip code", "zip5",
        "postal_code", "postal code", "postal-code", "post_code", "post code", "post-code"],
    "country": ["owner_country", "country"],
    
    "twitterhandle": ["twitterhandle", "twitter", "twitter_id"],
    
    # Company
    "company": ["owner_company", "company", "organization", "comp.", "co."],
    "website": [
        "owner_website", "website", "web site", "url", "site",
        "company_website", "company-website", "company website",
        "company_site", "company-site", "company site",],
    "numemployees": [
        "employees", "numemployees", "num_employees", "num-employees" "num employees",
        "companyemployees", "company_employees", "company-employees" "company employees",
        "numofemployees", "num_of_employees", "num-of-employees", "num of employees", "num. of employees",
        "numberofemployees", "number_of_employees", "number-of-employees", "number of employees",
    ],
    "annualrevenue": [
        "annualrevenue", "annual_revenue", "annual-revenue", "annual revenue"],
    "industry": ["industry", ""],
    
    "owner_type": ["owner_type", ""],
    "company_size": ["company_size", "owner_company_size"],
    "total_revenue": ["total_revenue", ""],
    
    # Job
    "jobtitle": ["jobtitle", ""],
    "job_function": ["job_function", ""],
    "seniority": ["seniority", ""],
    "start_date": ["start_date", ""],
    
    # Personal
    "gender": ["gender", "sex"],
    "date_of_birth": ["date_of_birth", "d.o.b.", "dob", "birthdate", "born_on"],
    "marital_status": ["marital_status", "married"],
    "relationship_status": ["relationship_status", ""],
    "military_status": ["military_status", ""],
    
    # Education
    "degree": ["degree", "diploma", "level_of_education"],
    "field_of_study": ["field_of_study", ""],
    "graduation_date": ["graduation_date", ""],
    "school": ["school", "university", "college"],
    
    
    # Metadata
    "days_to_close": ["days_to_close", ""],
    "first_conversion_date": ["first_conversion_date", ""],
    "first_conversion_event_name": ["first_conversion_event_name", ""],
    "first_deal_created_date": ["first_deal_created_date", ""],
    
    "hubspot_owner_assigneddate": ["hubspot_owner_assigneddate", ""],
    "lastmodifieddate": ["lastmodifieddate", ""],
    "num_associated_deals": ["num_associated_deals", ""],
    "num_conversion_events": ["num_conversion_events", ""],
    "num_unique_conversion_events": ["num_unique_conversion_events", ""],
    "recent_conversion_date": ["recent_conversion_date", ""],
    "recent_conversion_event_name": ["recent_conversion_event_name", ""],
    "recent_deal_amount": ["recent_deal_amount", ""],
    "recent_deal_close_date": ["recent_deal_close_date", ""],
    
    "currentlyinworkflow": ["currentlyinworkflow", ""],
    "engagements_last_meeting_booked": ["engagements_last_meeting_booked", ""],
    "engagements_last_meeting_booked_campaign": ["engagements_last_meeting_booked_campaign", ""],
    "engagements_last_meeting_booked_medium": ["engagements_last_meeting_booked_medium", ""],
    "engagements_last_meeting_booked_source": ["engagements_last_meeting_booked_source", ""],
    
    "hubspot_owner_id": ["hubspot_owner_id", "owner_hubspot_owner_id"],
    "hubspot_team_id": ["hubspot_team_id", ""],
    "notes_last_contacted": ["notes_last_contacted", ""],
    "notes_last_updated": ["notes_last_updated", ""],
    "notes_next_activity_date": ["notes_next_activity_date", ""],
    "num_contacted_notes": ["num_contacted_notes", "owner_number_of_contacts"],
    "num_notes": ["num_notes", ""],
    "number_of_emails": ["number_of_emails", ""],
    
    
    "surveymonkeyeventlastupdated": ["surveymonkeyeventlastupdated", ""],
    "webinareventlastupdated": ["webinareventlastupdated", ""],
    "message": ["message", ""],
    "closedate": ["closedate", ""],
    "lifecyclestage": ["lifecyclestage", ""],
    "createdate": ["createdate", ""],
    "hubspotscore": ["hubspotscore", ""],
    "associatedcompanyid": ["associatedcompanyid", ""],
    "associatedcompanylastupdated": ["associatedcompanylastupdated", ""],
}


def load_cities_and_zips(s_path="us_cities_and_zips.csv", **kwargs):
    s_path = get_local_path(s_path)
    
    df_cities_and_zips = pd.read_csv(s_path)
    
    df_cities = df_cities_and_zips[df_cities_and_zips["Target Type"].isin(["Municipality", "City", "City Region", "Borough", "Neighborhood"])].copy()
    print(f"{len(df_cities):,} cities")
    if kwargs.get("normalize_cities"):
        df_cities["Name"] = df_cities["Name"].apply(lambda x: standardize_city_names(x, **kwargs))
    
    df_zips = df_cities_and_zips[df_cities_and_zips["Target Type"] == "Postal Code"].copy()
    print(f"{len(df_zips):,} zip codes")
    
    if kwargs.get("by_state"):
        e_state_cities = defaultdict(set)
        for s_state, s_city in zip(df_cities["ST"], df_cities["Name"]):
            e_state_cities[s_state].add(s_city)
        
        e_state_zips = defaultdict(set)
        for s_state, s_city in zip(df_zips["ST"], df_zips["Name"]):
            e_state_zips[s_state].add(s_city)
        
        return e_state_cities, e_state_zips
    else:
        return df_cities, df_zips


cdef sort_regex_end(re_match):
    if re_match is None: return -1
    return re_match.end()


cpdef standardize_street_addresses(
    str s_address,
    bint b_remove_po_box=True,
    bint b_remove_unit = False,
    bint b_remove_number=False,
    bint b_lowercase=False,
    bint b_titlecase=False,
    bint b_debug=False
):
    ''' try to standardize abbreviations and punctuations for street type words '''
    cdef list a_street_abbreviations, a_room_abbreviations, a_direction_abbreviations, a_ordinals
    cdef str s_direction, s_street_type, s_replacement, s_room_type
    
    if not isinstance(s_address, str) or not s_address: return None
    s_address = re_ADDR_EXTRA_PUNCT.sub(" ", s_address)
    if b_debug: print("extra_punct:", s_address)
    
    a_street_abbreviations = list(re_STREET_WORDS_ABBR.finditer(s_address))
    if a_street_abbreviations:
        a_street_abbreviations.sort(key=sort_regex_end)
        
        re_match = a_street_abbreviations[-1]
        s_street_type = re_match.group()
        s_replacement = e_STREET_WORDS_TO_ABBREVIATE[s_street_type.lower()]
        s_address = "".join([s_address[:re_match.start()], s_replacement, s_address[re_match.end():]])
        if b_debug: print("abbreviate_street:", s_address)
    
    a_room_abbreviations = list(re_ROOM_WORDS_ABBR.finditer(s_address))
    if a_room_abbreviations:
        a_room_abbreviations.sort(key=sort_regex_end)
        
        for re_match in reversed(a_room_abbreviations):
            s_room_type = re_match.group()
            s_replacement = e_ROOM_WORDS_TO_ABBREVIATE[s_room_type.lower()]
            s_address = "".join([s_address[:re_match.start()], s_replacement, s_address[re_match.end():]])
        if b_debug: print("abbreviate_unit:", s_address)
    
    a_direction_abbreviations = list(re_DIRECTIONS_ABBR.finditer(s_address))
    if a_direction_abbreviations:
        a_direction_abbreviations.sort(key=sort_regex_end)
        
        for re_match in reversed(a_direction_abbreviations):
            s_direction = re_match.group()
            s_replacement = e_DIRECTION_ABBREVIATIONS[s_direction.lower()]
            s_address = "".join([s_address[:re_match.start()], s_replacement, s_address[re_match.end():]])
        if b_debug: print("abbreviate_directions:", s_address)
    
    a_ordinals = list(re_ORDINALS.finditer(s_address))
    if a_ordinals:
        a_ordinals.sort(key=sort_regex_end)
        
        for re_match in reversed(a_ordinals):
            s_replacement = re_match.group().lower()
            s_address = "".join([s_address[:re_match.start()], s_replacement, s_address[re_match.end():]])
        if b_debug: print("lowercase_ordinals:", s_address)
    
    if b_remove_po_box:
        s_address = re_PO_BOX.sub(" ", s_address)
        if b_debug: print("remove_po_box:", s_address)
    
    if b_remove_unit:
        #print(re_ROOM_ABBR.findall(s_address))
        s_address = re_ROOM_ABBR.sub(" ", s_address)
        if b_debug: print("remove_unit:", s_address)
    
    if b_remove_number:
        s_address = re_STREET_NUMBER.sub("", s_address)
        if b_debug: print("remove_number:", s_address)
    
    s_address = re_ADDR_EXTRA_SPACES.sub("", s_address).strip()
    
    if b_lowercase:
        s_address = s_address.lower()
    elif b_titlecase:
        s_address = s_address.title()
    
    return s_address


cpdef standardize_city_names(str s_city, bint b_lowercase=False, bint b_titlecase=False, bint b_debug=False):
    ''' try to standardize abbreviations and punctuations for city words '''
    cdef list a_city_abbreviations
    cdef str s_city_abbr, s_replacement
    
    if not isinstance(s_city, str) or not s_city: return None
    s_city = re_ADDR_EXTRA_PUNCT.sub(" ", s_city)
    
    a_city_abbreviations = list(re_CITY_WORDS_ABBR.finditer(s_city))
    if a_city_abbreviations:
        a_city_abbreviations.sort(key=sort_regex_end)
        
        re_match = a_city_abbreviations[-1]
        s_city_abbr = re_match.group()
        s_replacement = e_CITY_WORDS_TO_ABBREVIATE[s_city_abbr.lower()]
        s_city = "".join([s_city[:re_match.start()], s_replacement, s_city[re_match.end():]])
        if b_debug: print("abbreviate_city:", s_city)
    
    s_city = re_ADDR_EXTRA_SPACES.sub("", s_city).strip()
    if b_lowercase:
        s_city = s_city.lower()
    elif b_titlecase:
        s_city = s_city.title()
    
    return s_city

cpdef standardize_states(s_state, bint b_lowercase=False, bint b_uppercase=False):
    ''' try to standardize states to their abbreviations '''
    if b_lowercase:
        s_state = e_STATE_ABBREVIATIONS.get(s_state.lower(), s_state).lower()
    elif b_uppercase:
        s_state = e_STATE_ABBREVIATIONS.get(s_state.lower(), s_state).upper()
    else:
        s_state = e_STATE_ABBREVIATIONS.get(s_state.lower(), s_state)
    return s_state


cpdef standardize_zip_codes(v_zip, bint b_return_extension=False):
    ''' simplify zip code down to just 5 digits '''
    cdef str s_zip, s_ext
    
    if pd.isnull(v_zip): return None
    if isinstance(v_zip, (float, int)) and v_zip:
        s_zip = str(int(v_zip))
    elif not isinstance(v_zip, str) or not v_zip:
        return None
    else:
        s_zip = v_zip
    
    re_zip = re_ZIP_CODE.search(s_zip)
    if not re_zip: return None
    s_zip = re_zip.group("zip")
    if len(s_zip) <= 3:
        return None
    elif len(s_zip) == 4:
        s_zip = "0".format(s_zip)
    
    s_ext = re_zip.group("ext")
    if b_return_extension and s_ext:
        return s_zip, s_ext
    else:
        return s_zip

cpdef standardize_countries(s_country, bint b_lowercase=False, bint b_uppercase=False):
    ''' try to standardize countries to their 2-letter abbreviations '''
    if b_lowercase:
        s_country = e_COUNTRY_ABBREVIATIONS.get(s_country.lower(), s_country).lower()
    elif b_uppercase:
        s_country = e_COUNTRY_ABBREVIATIONS.get(s_country.lower(), s_country).upper()
    else:
        s_country = e_COUNTRY_ABBREVIATIONS.get(s_country.lower(), s_country)
    return s_country


cpdef standardize_phone(v_phone, str s_country=""):
    ''' format us phone numbers '''
    cdef str s_phone
    if isinstance(v_phone, (float, int)):
        s_phone = str(int(v_phone))
    elif isinstance(v_phone, str):
        s_phone = re_PHONE_LABEL.sub("", v_phone.strip())
    else:
        return None
    if s_phone in {"", "0", "nan"}: return None
    
    if not s_country: s_country = "US"
    try:
        o_phone = phonenumbers.parse(s_phone, region=s_country)
        if o_phone.country_code == 1:
            return phonenumbers.format_number(o_phone, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
        else:
            return phonenumbers.format_number(o_phone, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
    except:
        print(f"Invalid phone number: '{s_phone}'")
        #raise Exception(f"Invalid phone number: '{s_phone}'")
        return None

cpdef standardize_company(str s_company, bint b_last_only=False, bint b_lowercase=False, bint b_titlecase=False, bint b_debug=False):
    ''' try to standardize abbreviations and punctuations for company words '''
    cdef list a_company_abbreviations
    cdef str s_co_abbr, s_replacement
    
    if not isinstance(s_company, str) or not s_company: return None
    #s_company = re_ADDR_EXTRA_PUNCT.sub(" ", s_company)
    
    a_company_abbreviations = list(re_COMPANY_ABBR.finditer(s_company))
    if a_company_abbreviations:
        a_company_abbreviations.sort(key=sort_regex_end)
        
        if b_last_only:
            re_match = a_company_abbreviations[-1]
            s_co_abbr = re_match.group()
            s_replacement = e_COMPANY_ABBREVIATIONS[s_co_abbr.lower()]
            s_company = "".join([s_company[:re_match.start()], s_replacement, s_company[re_match.end():]])
            if b_debug: print("abbreviate_company:", s_company)
        else:
            for re_match in reversed(a_company_abbreviations):
                s_co_abbr = re_match.group()
                s_replacement = e_COMPANY_ABBREVIATIONS[s_co_abbr.lower()]
                s_company = "".join([s_company[:re_match.start()], s_replacement, s_company[re_match.end():]])
        if b_debug: print("abbreviate_company:", s_company)
        
    s_company = re_ADDR_EXTRA_SPACES.sub("", s_company).strip()
    if b_lowercase:
        s_company = s_company.lower()
    elif b_titlecase:
        s_company = s_company.title()
    
    return s_company



cpdef dict to_owner_contact(dict e_contact):
    ''' Take in a contact dictionary that might have differently named keys and standarize it
        to the owner field names. Where there are multiple input fields for only one standard field,
        it will select the first option according to the field name order in e_FIELDS. If there are
        multiple standard fields (like owner_email, owner_email_2, owner_email_3), it will fill them
        up in that same order.
        Arguments:
            e_contacts: {dict} with {str} keys
        Returns:
            {dict} containing keys like "owner_first_name", "owner_last_name", "owner_email", "owner_address"...
    '''
    cdef dict e_owner = {}
    
    cdef str k, x
    cdef y
    cdef list a_hubspot_vids, a_emails, a_phones, a_faxes, a_first_names, a_last_names, a_names, a_companies, a_statuses
    cdef list a_units, a_addresses, a_cities, a_states, a_zips, a_countries, a_websites, a_other_addresses, a_other_names
    cdef set c_null = {"", "NaN", "nan", "None", "none", "null", "Null", "NaT", "nat"}
    
    e_contact = {k.lower(): v for k, v in e_contact.items()}
    a_hubspot_vids = [y for y in [e_contact.get(k) for k in e_FIELDS["hubspot_vid"]] if y]
    if a_hubspot_vids: e_owner["hubspot_vid"] = a_hubspot_vids[0]
    
    a_emails = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_email"]] if x not in c_null]
    a_emails.extend([x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_email_2"]] if x not in c_null])
    a_emails.extend([x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_email_3"]] if x not in c_null])
    if a_emails:
        e_owner["owner_email"] = a_emails[0]
        if len(a_emails) > 1: e_owner["owner_email_2"] = a_emails[1]
        if len(a_emails) > 2: e_owner["owner_email_3"] = a_emails[2]
    
    
    a_phones = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_phone"]] if x not in c_null]
    a_phones.extend([x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_phone_2"]] if x not in c_null])
    a_phones.extend([x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_phone_3"]] if x not in c_null])
    if a_phones:
        e_owner["owner_phone"] = a_phones[0]
        if len(a_phones) > 1: e_owner["owner_phone_2"] = a_phones[1]
        if len(a_phones) > 2: e_owner["owner_phone_3"] = a_phones[2]
    
    a_faxes = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_fax"]] if x not in c_null]
    if a_faxes: e_owner["owner_fax"] = a_faxes[0]
    
    a_first_names = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_first_name"]] if x not in c_null]
    if a_first_names: e_owner["owner_first_name"] = a_first_names[0]
    
    a_last_names = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_last_name"]] if x not in c_null]
    if a_last_names: e_owner["owner_last_name"] = a_last_names[0]
    
    a_names = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_name"]] if x not in c_null]
    if a_names:
        e_owner["owner_name"] = a_names[0]
    elif e_owner.get("owner_last_name"):
        e_owner["owner_name"] = " ".join([e_owner.get("owner_first_name", ""), e_owner["owner_last_name"]]).strip()
    
    a_companies = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_company"]] if x not in c_null]
    if a_companies: e_owner["owner_company"] = a_companies[0]
    
    a_statuses = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["ownership_status"]] if x not in c_null]
    if a_statuses: e_owner["ownership_status"] = a_statuses[0]
    
    a_units = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_unit"]] if x not in c_null]
    if a_units: e_owner["owner_unit"] = a_units[0]
        
    a_addresses = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_address"]] if x not in c_null]
    if a_addresses: e_owner["owner_address"] = a_addresses[0]
    a_addresses = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_address_2"]] if x not in c_null]
    if a_addresses: e_owner["owner_address_2"] = a_addresses[0]
    
    a_cities = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_city"]] if x not in c_null]
    if a_cities: e_owner["owner_city"] = a_cities[0]
    a_states = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_state"]] if x not in c_null]
    if a_states: e_owner["owner_state"] = a_states[0]
    a_zips = [y for y in [str(e_contact.get(k)) for k in e_FIELDS["owner_zip"]] if y]
    if a_zips: e_owner["owner_zip"] = a_zips[0]
    a_countries = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_country"]] if x not in c_null]
    if a_countries: e_owner["owner_country"] = a_countries[0]
    
    a_websites = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["owner_website"]] if x not in c_null]
    if a_websites: e_owner["owner_website"] = a_websites[0]
    
    a_other_addresses = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["other_address"]] if x not in c_null]
    if a_other_addresses: e_owner["other_address"] = a_other_addresses[0]
    
    a_other_names = [x for x in [str(e_contact.get(k)) for k in e_FIELDS["contact_name"]] if x not in c_null]
    if a_other_names: e_owner["contact_name"] = a_other_names[0]
    
    return e_owner


def to_hubspot_contact(e_contact):
    ''' Take in a contact dictionary that might have differently named keys and standarize it
        to the hubspot field names. Where there are multiple input fields for only one standard field,
        it will select the first option according to the field name order in e_HUBSPOT_FIELDS. If there are
        multiple standard fields (like address, street_address_2), it will fill them
        up in that same order.
        Arguments:
            e_contacts: {dict} with {str} keys
        Returns:
            {dict} containing keys like "firstname", "lastname", "email", "phone"...
    '''
    
    e_hubspot_contact = {}
    
    e_contact = {k.lower(): v for k, v in e_contact.items()}
    
    a_distinct_fields = [
        "vid", "firstname", "lastname", "salutation", "fax", "city", "state", "zip", "country", "twitterhandle",
        "company", "website", "numemployees", "annualrevenue", "total_revenue", "industry", "owner_type", "company_size",
        "jobtitle", "job_function", "seniority", "start_date",
        "gender", "date_of_birth", "marital_status", "relationship_status", "military_status",
        "degree", "field_of_study", "graduation_date", "school",]
    # handle distinct fields that allow for one value to go to one field
    for s_field in a_distinct_fields:
        a_vals = [x for x in [e_contact.get(k) for k in e_HUBSPOT_FIELDS[s_field]] if x]
        if a_vals: e_hubspot_contact[s_field] = a_vals[0]
    
    a_multiple_fields = [
        ("email", "work_email"),
        ("phone", "mobilephone"),
        ("address", "street_address_2"),
    ]
    for t_fields in a_multiple_fields:
        a_vals = []
        for s_field in t_fields:
            a_vals.extend([x for x in [e_contact.get(k) for k in e_HUBSPOT_FIELDS[s_field]] if x])
        for v_val, s_field in zip(a_vals, t_fields):
            e_hubspot_contact[s_field] = v_val
    
    if not e_hubspot_contact.get("firstname") and not e_hubspot_contact.get("lastname"):
        a_names = [x for x in [e_contact.get(k) for k in ["owner_name", "name", "full_name", "full-name", "full_Name"]] if x]
        if a_names:
            o_name = HumanName(a_names[0])
            e_hubspot_contact["firstname"] = o_name.first
            if o_name.nickname: e_hubspot_contact["firstname"] += f" ({o_name.nickname})"
            if o_name.suffix: e_hubspot_contact["firstname"] += f", {o_name.suffix}"
            e_hubspot_contact["lastname"] = o_name.last
            if o_name.title and not e_hubspot_contact.get("salutation"): e_hubspot_contact["salutation"] = o_name.title
    
    return e_hubspot_contact


def standardize_owner(e_owner, **kwargs):
    ''' Alter the e_owner dict to standardize values like names, addresses, phones, etc '''
    if "owner_name" in e_owner:
        s_name = e_owner["owner_name"].title()
        s_name = re.sub(",(?!= )", ", ", s_name)
        s_name = re.sub(" *(&Amp;|&) *", " and ", s_name, flags=re.I)
        e_owner["owner_name"] = s_name.strip()
        
    if "owner_first_name" in e_owner:
        s_name = e_owner["owner_first_name"].title()
        s_name = re.sub(",(?!= )", ", ", s_name)
        s_name = re.sub(" *(&Amp;|&) *", " and ", s_name, flags=re.I)
        
        e_owner["owner_first_name"] = s_name.strip()
    
    if "owner_last_name" in e_owner:
        s_name = e_owner["owner_last_name"].title()
        s_name = re.sub(",(?!= )", ", ", s_name)
        s_name = re.sub(" *(&Amp;|&) *", " and ", s_name, flags=re.I)
        
        e_owner["owner_last_name"] = s_name.strip()
    
    if "owner_address" in e_owner:
        e_owner["owner_address"] = standardize_street_addresses(e_owner["owner_address"].title())
    if "owner_address_2" in e_owner:
        e_owner["owner_address_2"] = standardize_street_addresses(e_owner["owner_address_2"].title())
    if "owner_city" in e_owner:
        e_owner["owner_city"] = standardize_city_names(e_owner["owner_city"], b_titlecase=True)
    if "owner_state" in e_owner:
        e_owner["owner_state"] = standardize_states(e_owner["owner_state"])
    if "owner_zip" in e_owner:
        e_owner["owner_zip"] = standardize_zip_codes(e_owner["owner_zip"])
    if "owner_country" in e_owner:
        e_owner["owner_country"] = standardize_countries(e_owner["owner_country"])
    if "owner_county" in e_owner:
        e_owner["owner_county"] = e_owner["owner_county"].title()
    
    if "owner_phone" in e_owner:
        #print("owner_phone", e_owner["owner_phone"])
        e_owner["owner_phone"] = standardize_phone(e_owner["owner_phone"])
    if "owner_phone_2" in e_owner:
        #print("owner_phone_2", e_owner["owner_phone_2"])
        e_owner["owner_phone_2"] = standardize_phone(e_owner["owner_phone_2"])
    if "owner_fax" in e_owner:
        #print("owner_fax", e_owner["owner_fax"])
        e_owner["owner_fax"] = standardize_phone(e_owner["owner_fax"])
    
    if "owner_company" in e_owner:
        e_owner["owner_company"] = standardize_company(e_owner["owner_company"].title())
    return e_owner
