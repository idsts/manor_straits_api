cd /home/tim
git config --global credential.helper 'cache --timeout 86400'

git clone https://bitbucket.org/idsts/manor_straits_api

cd manor_straits_api
cd manor_straits_api
python -m venv env
source env/bin/activate

pip install --upgrade pip
pip install wheel
pip install gunicorn
pip install git+https://bitbucket.org/idsts/manor_straits_api
deactivate

# with environment variables
sudo cp flask_idsts.service /etc/systemd/system/flask_idsts.service

sudo systemctl start flask_idsts
sudo systemctl enable flask_idsts


sudo apt-get install build-essential libssl-dev libffi-dev python-dev
sudo apt-get install libapache2-mod-proxy-html libxml2-dev -y

add environment variables to /etc/environment


sudo apt update
sudo apt upgrade

sudo apt install apache2
sudo systemctl start apache2
sudo systemctl enable apache2

sudo rm /etc/apache2/sites-enabled/000-default.conf
sudo nano /etc/apache2/sites-available/flask_idsts.conf
sudo ln /etc/apache2/sites-available/flask_idsts.conf /etc/apache2/sites-enabled/flask_idsts.conf

apt install python3-certbot-apache
certbot -d manorstraits.timothygilbert.com
systemctl reload apache2

sudo a2enmod proxy
sudo a2enmod proxy_http
sudo a2enmod proxy_balancer
sudo a2enmod lbmethod_byrequests
sudo a2enmod rewrite

manorstraits.the-algorithmist.com