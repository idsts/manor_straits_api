﻿# Tables for python scripts and master records
s_DB = "SandboxKE"

s_PROCESS_TABLE = "python_scripts"
s_FIELD_MAPS_TABLE = "field_maps"

s_TOUCH_TABLE = "hubspot_touches"
s_TOUCH_SUMMARY_TABLE =  "hubspot_touch_summary"

s_OWNERS_TABLE = "owners"
s_OWNER_MAPS_TABLE =  "owner_maps"

s_PROPERTIES_TABLE = "properties"

s_BLOCKED_TABLE = "blocked_emails"

# Tables of related data
s_DB_SOURCES = "turnover"
e_SOURCE_TABLES = {
    "ProspectNow": "ProspectNow",
    "Infutor": "Infutor",
    "RealtyMole": "RealtyMole",
}

e_SOURCE_STANDARDIZATION = {
    "hubspot": "Hubspot", "hubspot_vid": "Hubspot", "hubspot_id": "Hubspot", "hubspotvid": "Hubspot",
    "prospectnow": "ProspectNow", "prospect_now": "ProspectNow", "prospect-now": "ProspectNow", "prospect now": "ProspectNow",
    "realtymole": "RealtyMole", "realty_mole": "RealtyMole", "realty mole": "RealtyMole", "realty-mole": "RealtyMole",
}