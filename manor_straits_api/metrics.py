﻿import os, sys, re, json
from datetime import datetime, timedelta
import pandas as pd

try:
    from . import utils, db_tables, mysql_db
except:
    import utils, db_tables, mysql_db

try:
    from . import hubspot_api
except:
    import hubspot_api


e_STAGES = {
    "investor": ["marketing qualify", "prospect qualify", "deal qualify",],
    "owner": ["qarketing qualify", "prospect qualify", "deal qualify",],
}
re_STAGE_NUMBER = re.compile(r"\d+")

a_RATIO_PAIRS = [
    ("marketing_email_percent_opened", "count_of_marketing_email_delivered", "count_of_marketing_email_open"),
    ("marketing_email_percent_clicked", "days_since_last_marketing_email_open", "count_of_marketing_email_click"),
]


def get_stage(s_email_name, **kwargs):
    s_email_name_lower = s_email_name.lower()
    for s_type, a_stages in e_STAGES.items():
        if s_type not in s_email_name_lower: continue
        for s_stage in a_stages:
            if s_stage not in s_email_name_lower: continue
            
            s_number = ""
            a_numbers = sorted(re_STAGE_NUMBER.findall(s_email_name_lower), key=lambda x: int(x))
            #print("a_numbers", a_numbers)
            if a_numbers: s_number = a_numbers[0]
            return (s_type, s_stage, s_number)


def filter_duplicate_touches(df_touch_details, **kwargs):
    df_unique_direct = df_touch_details[~df_touch_details["event_type"].str.startswith("MARKETING")].copy()
    df_unique_direct['created_rounded'] = df_unique_direct['created'].apply(lambda dt: utils.roundTime(dt, timedelta(minutes=15)))
    df_unique_direct.sort_values(["created_rounded", "hubspot_vid", "event_type", "metadata"])
    df_unique_direct = df_unique_direct.drop_duplicates(subset=["hubspot_vid", "event_type", "created_rounded"]).drop(columns=["created_rounded"])

    df_unique_marketing = df_touch_details[df_touch_details["event_type"].str.startswith("MARKETING")].copy()
    df_unique_marketing.drop_duplicates(subset=["hubspot_vid", "event_type", "email_campaign", "email_campaign_group"], inplace=True)
    
    df_unique = pd.concat([df_unique_direct, df_unique_marketing], axis=0)
    return df_unique


def split_call_dispositions(df_touches, **kwargs):
    e_CALL_DISPOSITIONS = kwargs.get("call_dispositions", {})
    if not e_CALL_DISPOSITIONS and kwargs.get("hubspot"):
        o_hubspot = kwargs["hubspot"]
        e_CALL_DISPOSITIONS = o_hubspot.get_call_dispositions(**kwargs)
    
    a_split_types = []
    for s_type, metadata in zip(df_touches["event_type"], df_touches["metadata"]):
        try:
            if isinstance(metadata, str): metadata = json.loads(metadata)
            if s_type not in {"CALL"} or not metadata or not isinstance(metadata, dict):
                a_split_types.append(s_type)
                continue
            
            if e_CALL_DISPOSITIONS:
                s_disposition = e_CALL_DISPOSITIONS.get(metadata.get("disposition"), "")
            else:
                s_disposition = metadata.get("disposition", "")
            
            #print(s_type, s_disposition)
            if s_disposition:
                s_type = f"{s_type} {s_disposition.upper()}"
        except:
            #print("Couldn't json.loads() metadata value", metadata)
            pass
        a_split_types.append(s_type)
    df_touches["event_type"] = a_split_types
    return df_touches


def dedupe_sequential(a_order, **kwargs):
    if not a_order: return []
    a_deduped = a_order[:1]
    for x in a_order[2:]:
        if x != a_deduped[-1]: a_deduped.append(x)
    return a_deduped


def add_ratios(df_model_inputs, **kwargs):
    for s_ratio_col, s_col1, s_col2 in a_RATIO_PAIRS:
        if s_col1 in df_model_inputs.columns and s_col2 in df_model_inputs.columns:
            df_model_inputs[s_ratio_col] = df_model_inputs[s_col1] / df_model_inputs[s_col2]