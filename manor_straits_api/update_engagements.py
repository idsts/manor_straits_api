﻿#!/opt/anaconda3/bin/python
import os, sys, re, json
from collections import Counter
from datetime import datetime
import dateutil.parser
import pandas as pd

try:
    from . import utils, db_tables, mysql_db
except:
    import utils, db_tables, mysql_db

try:
    from . import hubspot_api
except:
    import hubspot_api


s_DB = db_tables.s_DB # "SandboxKE"
s_TOUCH_TABLE = db_tables.s_TOUCH_TABLE  # "hubspot_touches"
s_PROCESS_TABLE = db_tables.s_PROCESS_TABLE  # "python_scripts"

HUBSPOT_API_KEY = os.environ.get("IDSTS_HUBSPOT_API_KEY", "")
AWS_DB_USER = os.environ.get("IDSTS_AWS_DB_USER", "")
AWS_DB_PASSWORD = os.environ.get("IDSTS_AWS_DB_PASS", "")

o_DB = None
o_HUBSPOT = None

a_KEY_COLS = [
    "hubspot_vid",
    "email",
]
a_VALUE_COLS = [
    'event_type',
    'created',
    'hubspot_owner',
    'item_id',
    'workflow_id',
    'email_campaign',
    'email_campaign_group',
    'metadata',
 ]


@utils.timer
def update_touches_db(**kwargs):
    load_db()
    load_api()
    
    print("Getting last script run times")
    df_python_scripts = o_DB.get_table_as_df(s_PROCESS_TABLE)
    e_scripts = df_python_scripts.set_index("process").to_dict(orient="index")
    
    last_run = e_scripts.get("update_touches", {}).get("last_run", None)
    if last_run: print("Last update was run on:", last_run.isoformat())
    since = last_run
    
    if kwargs.get("since"):
        if not isinstance(kwargs["since"], datetime):
            kwargs["since"] = dateutil.parser.parse(str(kwargs["since"]))
        since = kwargs["since"]
        if last_run: print(f"Using specified 'since': {since} rather than last update run {last_run}")
    
    e_tables = dict(o_DB.list_tables())
    if s_TOUCH_TABLE not in e_tables:
        create_table()
        since = None
    elif kwargs.get("overwrite") == True:
        print(f"Overwriting already copied entries in table: {s_TOUCH_TABLE}")
        since = None
        o_DB.delete_all_rows(s_TOUCH_TABLE)
    
    e_vid_email, e_email_vid = o_HUBSPOT.get_contact_emails(kwargs.get("a_all_contacts"))
    
    a_touches = o_HUBSPOT.get_touches(e_vid_email=e_vid_email, e_email_vid=e_email_vid, since=since)
    if not a_touches: return None
    
    print(f"Found {len(a_touches)} touch objects")
    
    # upload new or changed rows
    a_update_rows = []
    e_dup_counter = Counter()
    for i_row, e_row in enumerate(a_touches):
        if not e_row or not e_row.get("hubspot_vid"):
            print(f"Skipping {i_row} because it has no hubspot_vid")
            continue
        
        s_metadata = ""
        if e_row.get("metadata"):
            if isinstance(e_row["metadata"], str):
                s_metadata = e_row["metadata"]
            elif isinstance(e_row["metadata"], (float, int)):
                s_metadata = str(e_row["metadata"])
            else:
                s_metadata = json.dumps(utils.json_dict_prep(e_row["metadata"]))
        
        if e_row.get("created") and isinstance(e_row["created"], datetime):
            e_row["created"] = e_row["created"].isoformat()
        
        s_event_type = e_row.get("event_type")
        if s_event_type == "NOTE":
            re_start = None
            if isinstance(e_row["metadata"], dict):
                re_start = re.search(r"^[-_ A-Za-z0-9]{,50}(?=:)", e_row["metadata"].get("body", ""))
            elif isinstance(e_row["metadata"], str):
                re_start = re.search(r"^[-_ A-Za-z0-9]{,50}(?=:)", e_row["metadata"])
            if re_start:
                s_event_type = re_start.group()
        
        t_row = (
            s_event_type,
            e_row.get("created"),
            e_row.get("hubspot_owner"),
            e_row.get("item_id"),
            e_row.get("workflow_id"),
            e_row.get("email_campaign"),
            e_row.get("email_campaign_group"),
            s_metadata,
            e_row["hubspot_vid"],
            e_row.get("email"),
        )
        e_dup_counter[(e_row["hubspot_vid"],  e_row.get("created"), s_event_type,)] +=1
            
        a_update_rows.append(t_row)
    
    e_dup_counter = {k: i for k, i in e_dup_counter.items() if i > 1}
    if kwargs.get("b_debug"): print("Duplicate touches", e_dup_counter)
    
    print(f"Trying to add {len(a_update_rows)} contact touches to the '{s_TOUCH_TABLE}' table.")
    
    o_DB.insert_batch(s_TOUCH_TABLE, a_VALUE_COLS + a_KEY_COLS, a_update_rows)
    #o_DB.update("python_scripts", {"last_run": datetime.now().isoformat()}, {"process": "update_touches"})
    o_DB.insert(s_PROCESS_TABLE, {"process": "update_touches", "script": "update_engagements.py", "last_run": datetime.now().isoformat()})

def load_db():
    global o_DB
    #print("AWS_DB", AWS_DB_USER, AWS_DB_PASSWORD)
    o_DB = mysql_db.mySqlDbConn(
        s_host = mysql_db._host,
        i_port = mysql_db._port,
        s_user = AWS_DB_USER,
        s_pass = AWS_DB_PASSWORD,
        s_database = s_DB,
        chunk_size = 50000,
    )
    return o_DB


def load_api():
    global o_HUBSPOT
    o_HUBSPOT = hubspot_api.HubspotAPI(api_key=HUBSPOT_API_KEY)


def create_table():
    o_DB.load()
    o_DB.create_table(
        s_TOUCH_TABLE,
        [
            #"id int AUTO_INCREMENT PRIMARY KEY",
            "hubspot_vid int",
            "email varchar(255)",
            
            "event_type varchar(63)",
            "created datetime",
            "hubspot_owner int",
            "item_id varchar(63)",
            
            "workflow_id int",
            "email_campaign int",
            "email_campaign_group int",
            
            "metadata text",
        ])
    o_DB.cursor.execute(f"CREATE INDEX hubspot_vid_index ON {s_TOUCH_TABLE}(hubspot_vid)")
    o_DB.cursor.execute(f"CREATE INDEX hubspot_owner_index ON {s_TOUCH_TABLE}(hubspot_owner)")
    o_DB.cursor.execute(f"CREATE INDEX created_index ON {s_TOUCH_TABLE}(created)")
    o_DB.cursor.execute(f"CREATE INDEX email_index ON {s_TOUCH_TABLE}(email(255))")
    o_DB.cursor.execute(f"CREATE INDEX event_type_index ON {s_TOUCH_TABLE}(event_type(63))")
    o_DB.cursor.execute(f"ALTER TABLE {s_TOUCH_TABLE} ADD CONSTRAINT unique_items UNIQUE (hubspot_vid, created, event_type, item_id)")
    o_DB.close()


if __name__ == "__main__":  
    ''' This gets run when update_engagements.py is run from the command prompt
        Args:
            args: {list} of the positional command line options.
                [0] 
            kwargs:
                --overwrite: {bool}. Do complete update of all engagements/activity instead of only most recent.
                --since: {str} of date. Override last run date in db to specify datetime to begin analysis.
    '''
    #print('sys.argv', sys.argv, '\n')
    args, kwargs = utils.parse_command_line_args(sys.argv)
    
    if "since" in kwargs:
        #print(kwargs["since"])
        kwargs["since"] = dt_since = dateutil.parser.parse(str(kwargs["since"]))
        print("Update touches since:", kwargs["since"])
    
    if not HUBSPOT_API_KEY:
        raise Exception("""
            The environment variable for the hubspot API key hasn't been set up.
            Remember to run 'IDSTS_HUBSPOT_API_KEY=your_hubspot_api_key' from shell or add it to your /etc/environment file.""")
    if not AWS_DB_USER:
        raise Exception("""
            The environment variable for the AWS MYSQL Db Username hasn't been set up.
            Remember to run 'AWS_DB_USER=your_aws_db_username' from shell or add it to your /etc/environment file.""")
    if not AWS_DB_PASSWORD:
        raise Exception("""
            The environment variable for the AWS MYSQL Db Password hasn't been set up.
            Remember to run 'AWS_DB_PASSWORD=your_aws_db_password' from shell or add it to your /etc/environment file.""")

    #print('args', args, '\n')
    #print('kwargs', kwargs, '\n')
    update_touches_db(**kwargs)
    