import os
import sys
import re
import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'manor_straits_api/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = re.findall(r"\d+\.\d+\.\d+", version_line)[0]
            
class custom_install(install):
    def run(self):
        print("This is a custom installation")
        self.install_private_dependencies()
        install.run(self)
    
    def install_private_dependencies(self):
        print("About to install any private repositories dependencies...")
        #os.system("pip install git+https://bitbucket.org/idsts/_other_private_repository_")

setuptools.setup(
    name="manor_straits_api",
    version=__version__,
    url="https://bitbucket.org/idsts/manor_straits_api",

    author="IDSTS",

    description="Real Estate Marketing APIs for Manor Straits",
    long_description=open('README.md').read(),

    packages = setuptools.find_packages(),
    package_data={'': ["*.pyx","*.txt", "*.json"]},
    py_modules=['manor_straits_api'],
    zip_safe=False,
    platforms='any',
    install_requires=[
        "cython",
        "numpy",
        "pandas",
        "requests",
        "urllib3",
        "pymysql",
        "flask",
        "flask-restful",
        "flask-cors",
        "postal_address",
        "phonenumbers",
        "nameparser",
        "boto3",
    ],

    cmdclass={'install': custom_install}
)