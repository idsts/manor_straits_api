# manor_straits_apis

This is a REST API server (on flask) to host endpoints that are meant to either write to Manor Straits AWS DB or access its API accounts


# Installation

```
pip install git+https://bitbucket.org/idsts/manor_straits_api
```

# Tests

Set up for testing, by installing the dev and dist dependencies:
```
pip install -r requirements.dev.txt
pip install -r requirements.dist.txt
```

Run the unittests with:
```
pytest tests.py
```

## REST API Host

This contains a REST server API, as well as classes for connecting to other DBs and APIs.


Start main REST API:
```
python flask_idsts.py
```

Optional keyword arguments for starting the server are:

*  --debug:False
*  --host:aws.manorstraits.com
*  --port:5000


## Hubspot API Usage:
```
#!python

import manor_straits_api.hubspot_api

# load Hubspot
o_HUBSPOT = HubspotAPI(
    api_key="hubspot_account_api_key",
    owner_id="id_number_to_use_for_records_created_through_api")

# find contact ids for given properties like email or phone number
a_hub_ids = o_HUBSPOT.find_contacts(["tim@timothygilbert.com", "650-814-0020"])

# get contact lists
e_contact_lists = self.get_contact_lists(by_type=True)

# get all contacts (may not contain all contact properties
a_all_contacts = self.get_contacts()

# to get full contact properties of single contact by id
e_contact = o_HUBSPOT.get_contact(101)

# update properties of a contact
e_contact_updates = {
  "properties": [
    {"property": "number_of_emails", "value": 0,},
    {"property": "idsts_score", "value": 0.5,},]
}
o_HUBSPOT.update_contact(101, e_contact_updates)

# create new kind of contact prpperty
o_HUBSPOT.create_contact_property("idsts_score", "number", "number", group_name="IDSTS")

# get activity records of interactions with marketing emails
e_all_activity = o_HUBSPOT.get_event_activity()

# get existing marketing emails
a_emails = o_HUBSPOT.get_marketing_emails(with_details=True)

# create new marketing email
s_email_name = "test email from api 004"
s_email_subject = "Test email created from api with body content changes"
e_email_props = {
    "module_15911926642472": "<h2>Hi {{ contact.firstname }}, this an invitation from your friends at Manor Straits</h2>",
    "builtin_module_2_0_0": "<h1>Is your property an untapped gold mine?</h1>\n<p>You might be sitting on valuable real estate that isn't earning you the money it should. Based on an analysis of the social media trends in your neighborhood, this is near peak market for property values.</p>",
}
s_body = ' '.join([e_email_props["module_15911926642472"], e_email_props["module_15911926642472"]])
o_HUBSPOT.create_marketing_email(
    name=s_email_name,
    subject=s_email_subject,
    body=s_body,
    email_properties=e_email_props)

# get existing workflows
e_workflows = o_HUBSPOT.get_workflows(by_type=True, with_details=True)

# create a new workflow
o_created_workflow = o_HUBSPOT.create_workflow(
    name="Test workflow created from API",
    workflow_type="DRIP_DELAY",
    workflow_properties={"enabled": False},
    actions=[
        {
            'type': 'BRANCH',
            'filtersListId': 7,
            'actionId': 7,
            'info': [],
            'errors': [],
            'warnings': [],
            'filters': [[{'filterFamily': 'Subscriptions',
              'withinTimeMode': 'PAST',
              'subscriptionsFilter': {'subscriptionIds': [9662393],
               'optStatuses': ['OPT_IN']}}]],

            'rejectActions': [],
            'acceptActions': [
                {
                    'type': 'DELAY',
                    'stepListCount': 0,
                    'delayMillis': 0,
                    'actionId': 4,
                    'stepId': 1},
                {
                    'type': 'EMAIL',
                    'emailContentId': 30028434909,
                    'emailCampaignId': 89781207,
                    'emailCampaignGroupId': 89780788,
                    'actionId': 1,
                    'stepId': 1,
                    'goalListCount': 0
                },
                {
                    'type': 'ADD_SUBTRACT_PROPERTY',
                    'propertyName': 'number_of_emails',
                    'valueToModifyPropertyBy': 1,
                    'actionId': 2,
                    'stepId': 1
                },
                {
                    'type': 'DELAY',
                    'stepListCount': 0,
                    'delayMillis': 172800000,
                    'actionId': 3,
                    'stepId': 2
                },
                {
                    'type': 'SET_CONTACT_PROPERTY',
                    'propertyName': 'hs_lead_status',
                    'newValue': 'IN_PROGRESS',
                    'actionId': 5,
                    'stepId': 2
                },
            ],
        },
    ],
)

# get enagements with direct messaging/calls/meetings/texts
a_engagements = o_HUBSPOT.get_engagements()

# create an enagement:
o_note_confirm = o_HUBSPOT.create_engagement(
    e_engagement = e_engagement,
    e_association = e_association,
    e_metadata=e_metadata,)
```

## Sakari API Usage:
```
#!python

import manor_straits_api.sakari_api

# load Sakari
o_SAKARI = SakariAPI()

# send a text message to two people
o_confirmation = o_SAKARI.send_message(
    [
        {"mobile": "800-555,5555", "firstName": "Sam", "lastName": "Pull"},
        {"mobile": "650-814-0020", "firstName": "Tim", "lastName": "Gilbert"},
    ],
    "message": "Hello {{ firstName }}, this is a test via the Sakari API"
)
```

## Handwrite API Usage:
```
#!python

import manor_straits_api.handwrite_api

# load Handwrite
o_HANDWRITE = HandwriteAPI(api_key="live_product_api_key")

# send a post card message to one person
o_confirmation = o_HANDWRITE.send_message(
    [
        {
                    "firstName": "The",
                    "lastName": "Dude",
                    "company": "Unemployed",
                    "street1": "25 Main Street",
                    "city": "Los Angeles",
                    "state": "CA",
                    "zip": "90210"
        },
    ],
    e_from={
                    "firstName": "Timothy",
                    "lastName": "Gilbert",
                    "company": "IDSTS",
                    "street1": "398 E Eaglewood Ln",
                    "city": "Mt. Jackson",
                    "state": "VA",
                    "zip": "22842",
                    "phone": "650-814-0020",
                }
    s_message="Hello {firstName}, this is a test postcard via the Handwrite API",
    s_card_id="5dc304cfbc08d20016f1ec2f",
    s_handwriting_id="5dc30652bc08d20016f1ec33",
)
```